﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim BarraTitulo As System.Windows.Forms.Panel
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormPrincipal))
        Me.btnMinimizar = New System.Windows.Forms.Button()
        Me.btnMaximizar = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.panelMenu = New System.Windows.Forms.Panel()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.btnReportes = New System.Windows.Forms.Button()
        Me.pictureBox8 = New System.Windows.Forms.PictureBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.btnContabilidad = New System.Windows.Forms.Button()
        Me.btnTratamientos = New System.Windows.Forms.Button()
        Me.btnFuncionarios = New System.Windows.Forms.Button()
        Me.btnCitas = New System.Windows.Forms.Button()
        Me.btnConsultas = New System.Windows.Forms.Button()
        Me.btnPacientes = New System.Windows.Forms.Button()
        Me.btnInicio = New System.Windows.Forms.Button()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.pictureBox5 = New System.Windows.Forms.PictureBox()
        Me.lbFecha = New System.Windows.Forms.Label()
        Me.pictureBox6 = New System.Windows.Forms.PictureBox()
        Me.pictureBox3 = New System.Windows.Forms.PictureBox()
        Me.pictureBox4 = New System.Windows.Forms.PictureBox()
        Me.lblHora = New System.Windows.Forms.Label()
        Me.pictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pictureBox1 = New System.Windows.Forms.PictureBox()
        Me.tmFechaHora = New System.Windows.Forms.Timer(Me.components)
        Me.panelContenedor = New System.Windows.Forms.Panel()
        Me.cmsCitas = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnMSCOdontologia = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnMSCMedicinaGeneral = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsConsultas = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnCMSConsultasOdontologia = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnCMSConsultasMedicina = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsContabilidad = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnFacturasConsulta = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsReportes = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnCMSOdontoReportes = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        BarraTitulo = New System.Windows.Forms.Panel()
        BarraTitulo.SuspendLayout()
        Me.panelMenu.SuspendLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsCitas.SuspendLayout()
        Me.cmsConsultas.SuspendLayout()
        Me.cmsContabilidad.SuspendLayout()
        Me.cmsReportes.SuspendLayout()
        Me.SuspendLayout()
        '
        'BarraTitulo
        '
        BarraTitulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        BarraTitulo.Controls.Add(Me.btnMinimizar)
        BarraTitulo.Controls.Add(Me.btnMaximizar)
        BarraTitulo.Controls.Add(Me.btnCerrar)
        resources.ApplyResources(BarraTitulo, "BarraTitulo")
        BarraTitulo.Name = "BarraTitulo"
        AddHandler BarraTitulo.MouseDown, AddressOf Me.BarraTitulo_MouseDown
        AddHandler BarraTitulo.MouseMove, AddressOf Me.BarraTitulo_MouseMove
        '
        'btnMinimizar
        '
        resources.ApplyResources(Me.btnMinimizar, "btnMinimizar")
        Me.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMinimizar.FlatAppearance.BorderSize = 0
        Me.btnMinimizar.Name = "btnMinimizar"
        Me.btnMinimizar.UseVisualStyleBackColor = True
        '
        'btnMaximizar
        '
        resources.ApplyResources(Me.btnMaximizar, "btnMaximizar")
        Me.btnMaximizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMaximizar.FlatAppearance.BorderSize = 0
        Me.btnMaximizar.Name = "btnMaximizar"
        Me.btnMaximizar.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        resources.ApplyResources(Me.btnCerrar, "btnCerrar")
        Me.btnCerrar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCerrar.FlatAppearance.BorderSize = 0
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'panelMenu
        '
        Me.panelMenu.AllowDrop = True
        Me.panelMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.panelMenu.Controls.Add(Me.PictureBox10)
        Me.panelMenu.Controls.Add(Me.btnSalir)
        Me.panelMenu.Controls.Add(Me.Label2)
        Me.panelMenu.Controls.Add(Me.label1)
        Me.panelMenu.Controls.Add(Me.btnReportes)
        Me.panelMenu.Controls.Add(Me.pictureBox8)
        Me.panelMenu.Controls.Add(Me.PictureBox9)
        Me.panelMenu.Controls.Add(Me.btnContabilidad)
        Me.panelMenu.Controls.Add(Me.btnTratamientos)
        Me.panelMenu.Controls.Add(Me.btnFuncionarios)
        Me.panelMenu.Controls.Add(Me.btnCitas)
        Me.panelMenu.Controls.Add(Me.btnConsultas)
        Me.panelMenu.Controls.Add(Me.btnPacientes)
        Me.panelMenu.Controls.Add(Me.btnInicio)
        Me.panelMenu.Controls.Add(Me.PictureBox7)
        Me.panelMenu.Controls.Add(Me.pictureBox5)
        Me.panelMenu.Controls.Add(Me.lbFecha)
        Me.panelMenu.Controls.Add(Me.pictureBox6)
        Me.panelMenu.Controls.Add(Me.pictureBox3)
        Me.panelMenu.Controls.Add(Me.pictureBox4)
        Me.panelMenu.Controls.Add(Me.lblHora)
        Me.panelMenu.Controls.Add(Me.pictureBox2)
        Me.panelMenu.Controls.Add(Me.pictureBox1)
        resources.ApplyResources(Me.panelMenu, "panelMenu")
        Me.panelMenu.Name = "panelMenu"
        '
        'PictureBox10
        '
        Me.PictureBox10.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.PictureBox10, "PictureBox10")
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.TabStop = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSalir.FlatAppearance.BorderSize = 0
        Me.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        resources.ApplyResources(Me.btnSalir, "btnSalir")
        Me.btnSalir.ForeColor = System.Drawing.Color.White
        Me.btnSalir.Image = Global.ProyectoFinalIngenieria.My.Resources.Resources.logout
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Name = "Label2"
        '
        'label1
        '
        resources.ApplyResources(Me.label1, "label1")
        Me.label1.ForeColor = System.Drawing.Color.White
        Me.label1.Name = "label1"
        '
        'btnReportes
        '
        Me.btnReportes.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnReportes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnReportes.FlatAppearance.BorderSize = 0
        Me.btnReportes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnReportes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        resources.ApplyResources(Me.btnReportes, "btnReportes")
        Me.btnReportes.ForeColor = System.Drawing.Color.White
        Me.btnReportes.Name = "btnReportes"
        Me.btnReportes.UseVisualStyleBackColor = False
        '
        'pictureBox8
        '
        resources.ApplyResources(Me.pictureBox8, "pictureBox8")
        Me.pictureBox8.Name = "pictureBox8"
        Me.pictureBox8.TabStop = False
        '
        'PictureBox9
        '
        Me.PictureBox9.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.PictureBox9, "PictureBox9")
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.TabStop = False
        '
        'btnContabilidad
        '
        Me.btnContabilidad.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnContabilidad.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnContabilidad.FlatAppearance.BorderSize = 0
        Me.btnContabilidad.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnContabilidad.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        resources.ApplyResources(Me.btnContabilidad, "btnContabilidad")
        Me.btnContabilidad.ForeColor = System.Drawing.Color.White
        Me.btnContabilidad.Name = "btnContabilidad"
        Me.btnContabilidad.UseVisualStyleBackColor = False
        '
        'btnTratamientos
        '
        Me.btnTratamientos.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnTratamientos.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnTratamientos.FlatAppearance.BorderSize = 0
        Me.btnTratamientos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnTratamientos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        resources.ApplyResources(Me.btnTratamientos, "btnTratamientos")
        Me.btnTratamientos.ForeColor = System.Drawing.Color.White
        Me.btnTratamientos.Name = "btnTratamientos"
        Me.btnTratamientos.UseVisualStyleBackColor = False
        '
        'btnFuncionarios
        '
        Me.btnFuncionarios.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnFuncionarios.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnFuncionarios.FlatAppearance.BorderSize = 0
        Me.btnFuncionarios.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnFuncionarios.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        resources.ApplyResources(Me.btnFuncionarios, "btnFuncionarios")
        Me.btnFuncionarios.ForeColor = System.Drawing.Color.White
        Me.btnFuncionarios.Name = "btnFuncionarios"
        Me.btnFuncionarios.UseVisualStyleBackColor = False
        '
        'btnCitas
        '
        Me.btnCitas.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnCitas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCitas.FlatAppearance.BorderSize = 0
        Me.btnCitas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnCitas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        resources.ApplyResources(Me.btnCitas, "btnCitas")
        Me.btnCitas.ForeColor = System.Drawing.Color.White
        Me.btnCitas.Name = "btnCitas"
        Me.btnCitas.UseVisualStyleBackColor = False
        '
        'btnConsultas
        '
        Me.btnConsultas.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnConsultas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnConsultas.FlatAppearance.BorderSize = 0
        Me.btnConsultas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnConsultas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        resources.ApplyResources(Me.btnConsultas, "btnConsultas")
        Me.btnConsultas.ForeColor = System.Drawing.Color.White
        Me.btnConsultas.Name = "btnConsultas"
        Me.btnConsultas.UseVisualStyleBackColor = False
        '
        'btnPacientes
        '
        Me.btnPacientes.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnPacientes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPacientes.FlatAppearance.BorderSize = 0
        Me.btnPacientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnPacientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        resources.ApplyResources(Me.btnPacientes, "btnPacientes")
        Me.btnPacientes.ForeColor = System.Drawing.Color.White
        Me.btnPacientes.Name = "btnPacientes"
        Me.btnPacientes.UseVisualStyleBackColor = False
        '
        'btnInicio
        '
        Me.btnInicio.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnInicio.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnInicio.FlatAppearance.BorderSize = 0
        Me.btnInicio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnInicio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        resources.ApplyResources(Me.btnInicio, "btnInicio")
        Me.btnInicio.ForeColor = System.Drawing.Color.White
        Me.btnInicio.Name = "btnInicio"
        Me.btnInicio.UseVisualStyleBackColor = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.PictureBox7, "PictureBox7")
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.TabStop = False
        '
        'pictureBox5
        '
        Me.pictureBox5.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.pictureBox5, "pictureBox5")
        Me.pictureBox5.Name = "pictureBox5"
        Me.pictureBox5.TabStop = False
        '
        'lbFecha
        '
        Me.lbFecha.AllowDrop = True
        resources.ApplyResources(Me.lbFecha, "lbFecha")
        Me.lbFecha.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lbFecha.Name = "lbFecha"
        '
        'pictureBox6
        '
        Me.pictureBox6.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.pictureBox6, "pictureBox6")
        Me.pictureBox6.Name = "pictureBox6"
        Me.pictureBox6.TabStop = False
        '
        'pictureBox3
        '
        Me.pictureBox3.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.pictureBox3, "pictureBox3")
        Me.pictureBox3.Name = "pictureBox3"
        Me.pictureBox3.TabStop = False
        '
        'pictureBox4
        '
        Me.pictureBox4.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.pictureBox4, "pictureBox4")
        Me.pictureBox4.Name = "pictureBox4"
        Me.pictureBox4.TabStop = False
        '
        'lblHora
        '
        resources.ApplyResources(Me.lblHora, "lblHora")
        Me.lblHora.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblHora.Name = "lblHora"
        '
        'pictureBox2
        '
        Me.pictureBox2.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.pictureBox2, "pictureBox2")
        Me.pictureBox2.Name = "pictureBox2"
        Me.pictureBox2.TabStop = False
        '
        'pictureBox1
        '
        Me.pictureBox1.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.pictureBox1, "pictureBox1")
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.TabStop = False
        '
        'tmFechaHora
        '
        Me.tmFechaHora.Enabled = True
        '
        'panelContenedor
        '
        resources.ApplyResources(Me.panelContenedor, "panelContenedor")
        Me.panelContenedor.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.panelContenedor.Name = "panelContenedor"
        '
        'cmsCitas
        '
        Me.cmsCitas.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.cmsCitas, "cmsCitas")
        Me.cmsCitas.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMSCOdontologia, Me.btnMSCMedicinaGeneral})
        Me.cmsCitas.Name = "cmsCitas"
        '
        'btnMSCOdontologia
        '
        Me.btnMSCOdontologia.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnMSCOdontologia.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.btnMSCOdontologia, "btnMSCOdontologia")
        Me.btnMSCOdontologia.Name = "btnMSCOdontologia"
        '
        'btnMSCMedicinaGeneral
        '
        Me.btnMSCMedicinaGeneral.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnMSCMedicinaGeneral.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.btnMSCMedicinaGeneral, "btnMSCMedicinaGeneral")
        Me.btnMSCMedicinaGeneral.Name = "btnMSCMedicinaGeneral"
        '
        'cmsConsultas
        '
        Me.cmsConsultas.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.cmsConsultas, "cmsConsultas")
        Me.cmsConsultas.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnCMSConsultasOdontologia, Me.btnCMSConsultasMedicina})
        Me.cmsConsultas.Name = "cmsCitas"
        '
        'btnCMSConsultasOdontologia
        '
        Me.btnCMSConsultasOdontologia.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnCMSConsultasOdontologia.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.btnCMSConsultasOdontologia, "btnCMSConsultasOdontologia")
        Me.btnCMSConsultasOdontologia.Name = "btnCMSConsultasOdontologia"
        '
        'btnCMSConsultasMedicina
        '
        Me.btnCMSConsultasMedicina.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnCMSConsultasMedicina.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.btnCMSConsultasMedicina, "btnCMSConsultasMedicina")
        Me.btnCMSConsultasMedicina.Name = "btnCMSConsultasMedicina"
        '
        'cmsContabilidad
        '
        Me.cmsContabilidad.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.cmsContabilidad, "cmsContabilidad")
        Me.cmsContabilidad.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnFacturasConsulta, Me.ToolStripMenuItem2})
        Me.cmsContabilidad.Name = "cmsCitas"
        '
        'btnFacturasConsulta
        '
        Me.btnFacturasConsulta.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnFacturasConsulta.ForeColor = System.Drawing.Color.White
        Me.btnFacturasConsulta.Image = Global.ProyectoFinalIngenieria.My.Resources.Resources.colones_factura
        Me.btnFacturasConsulta.Name = "btnFacturasConsulta"
        resources.ApplyResources(Me.btnFacturasConsulta, "btnFacturasConsulta")
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.ToolStripMenuItem2.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.ToolStripMenuItem2, "ToolStripMenuItem2")
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        '
        'cmsReportes
        '
        Me.cmsReportes.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.cmsReportes, "cmsReportes")
        Me.cmsReportes.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnCMSOdontoReportes, Me.ToolStripMenuItem3, Me.GeneralToolStripMenuItem})
        Me.cmsReportes.Name = "cmsCitas"
        '
        'btnCMSOdontoReportes
        '
        Me.btnCMSOdontoReportes.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnCMSOdontoReportes.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.btnCMSOdontoReportes, "btnCMSOdontoReportes")
        Me.btnCMSOdontoReportes.Name = "btnCMSOdontoReportes"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.ToolStripMenuItem3.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.ToolStripMenuItem3, "ToolStripMenuItem3")
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        '
        'GeneralToolStripMenuItem
        '
        Me.GeneralToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.GeneralToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.GeneralToolStripMenuItem.Image = Global.ProyectoFinalIngenieria.My.Resources.Resources.icono_general
        Me.GeneralToolStripMenuItem.Name = "GeneralToolStripMenuItem"
        resources.ApplyResources(Me.GeneralToolStripMenuItem, "GeneralToolStripMenuItem")
        '
        'FormPrincipal
        '
        resources.ApplyResources(Me, "$this")
        Me.Controls.Add(Me.panelContenedor)
        Me.Controls.Add(Me.panelMenu)
        Me.Controls.Add(BarraTitulo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FormPrincipal"
        BarraTitulo.ResumeLayout(False)
        Me.panelMenu.ResumeLayout(False)
        Me.panelMenu.PerformLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsCitas.ResumeLayout(False)
        Me.cmsConsultas.ResumeLayout(False)
        Me.cmsContabilidad.ResumeLayout(False)
        Me.cmsReportes.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents panelMenu As Panel
    Private WithEvents pictureBox5 As PictureBox
    Private WithEvents pictureBox6 As PictureBox
    Private WithEvents pictureBox3 As PictureBox
    Private WithEvents pictureBox4 As PictureBox
    Private WithEvents pictureBox2 As PictureBox
    Private WithEvents pictureBox1 As PictureBox
    Friend WithEvents tmFechaHora As Timer
    Private WithEvents lbFecha As Label
    Private WithEvents lblHora As Label
    Private WithEvents PictureBox7 As PictureBox
    Private WithEvents btnPacientes As Button
    Private WithEvents btnInicio As Button
    Private WithEvents btnCitas As Button
    Private WithEvents btnConsultas As Button
    Private WithEvents btnTratamientos As Button
    Private WithEvents btnFuncionarios As Button
    Private WithEvents label1 As Label
    Private WithEvents pictureBox8 As PictureBox
    Private WithEvents PictureBox9 As PictureBox
    Friend WithEvents Label2 As Label
    Private WithEvents btnMaximizar As Button
    Private WithEvents btnMinimizar As Button
    Private WithEvents btnCerrar As Button
    Private WithEvents btnReportes As Button
    Private WithEvents btnContabilidad As Button
    Public WithEvents panelContenedor As Panel
    Friend WithEvents cmsCitas As ContextMenuStrip
    Friend WithEvents btnMSCOdontologia As ToolStripMenuItem
    Friend WithEvents btnMSCMedicinaGeneral As ToolStripMenuItem
    Friend WithEvents cmsConsultas As ContextMenuStrip
    Friend WithEvents btnCMSConsultasOdontologia As ToolStripMenuItem
    Friend WithEvents btnCMSConsultasMedicina As ToolStripMenuItem
    Friend WithEvents cmsContabilidad As ContextMenuStrip
    Friend WithEvents btnFacturasConsulta As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents cmsReportes As ContextMenuStrip
    Friend WithEvents btnCMSOdontoReportes As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents GeneralToolStripMenuItem As ToolStripMenuItem
    Private WithEvents PictureBox10 As PictureBox
    Private WithEvents btnSalir As Button
End Class

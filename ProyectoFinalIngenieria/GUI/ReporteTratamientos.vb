﻿Public Class ReporteTratamientos

    Dim Especialidad As String

    Public Sub New(especialidadU As String)
        InitializeComponent()
        Especialidad = especialidadU
    End Sub

    Private Sub ReporteTratamientos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DataReportes.ConsultarTratamientosReporte' Puede moverla o quitarla según sea necesario.
        Me.ConsultarTratamientosReporteTableAdapter.Fill(Me.DataReportes.ConsultarTratamientosReporte, Especialidad)

        Me.ReportViewer1.RefreshReport()
    End Sub
End Class
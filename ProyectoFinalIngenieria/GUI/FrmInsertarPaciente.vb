﻿Imports ProyectoFinalIngenieria.Ln_Responsable
Imports ProyectoFinalIngenieria.Ln_Paciente

Public Class FrmInsertarPaciente

    Dim responsable As New Ln_Responsable
    Dim paciente As New Ln_Paciente
    Dim historial As New LN_HistorialFamiliar_Alergico
    Dim resp As Responsables
    Dim resp_remplazo As Responsables
    Dim paci As Pacientes
    Dim hist As HistorialFamiliar_Alergico

    Private Sub FrmInsertarPaciente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DB_A494E9_PFIngenieriaDataSet1.pacientesActivosTodos' Puede moverla o quitarla según sea necesario.
        Me.PacientesActivosTodosTableAdapter.Fill(Me.DB_A494E9_PFIngenieriaDataSet1.pacientesActivosTodos)
        'TODO: esta línea de código carga datos en la tabla 'DB_A494E9_PFIngenieriaDataSet.Responsables' Puede moverla o quitarla según sea necesario.
        Me.ResponsablesTableAdapter.Fill(Me.DB_A494E9_PFIngenieriaDataSet.Responsables)
        dgvResponsables.Columns(0).Visible = False
        tbEdadResponsable.Enabled = False
        txtEdadPaciente.Enabled = False

    End Sub

    'Inserta en las tres tablas PACIENTES / RESPONSABLES / HISTORIAL FAMILIAR ALERGICO
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        'VALIDA SI EL PACIENTE ES MAYOR O MENOR DE EDAD PARA LLAMAR A LOS METODOS DE INSERTAR CORRESPONDIENTES.
        If (txtCedulaPaciente.Text <> "") And (txtNombrePaciente.Text <> "") And (txtTelefono1Paciente.Text <> "") And (cbGeneroPaciente.SelectedItem <> "") Then

            'PACIENTE MAYOR DE EDAD
            If CInt(txtEdadPaciente.Text) >= 18 Then
                paciente.insertarPacienteMayor(New Pacientes(txtNombrePaciente.Text, CInt(txtCedulaPaciente.Text),
                                              tbxDireccionPaciente.Text, txtCorreoPaciente.Text, txtOcupacionPaciente.Text, getGenero(cbGeneroPaciente.SelectedItem),
                                              dtpFechaNacimientoPaciente.Value.Date, txtTelefono1Paciente.Text, txtTelefono2Paciente.Text, 0))

                historial.insertarHistorialP(New HistorialFamiliar_Alergico(historial.ExtraerIDPaciente(CInt(txtCedulaPaciente.Text)), tbxQuejaPrincipal.Text, tbxAntecedentesFamiliares.Text,
                                                   tbxAntecedentesPersonales.Text, tbxAntecedentesNoPersonales.Text, tbxAntecedentesQuirurgicos.Text,
                                                   tbxAntecedentesAlergicos.Text, dtpFechaHistorial.Value.Date, 0))

                dgvPacientes.DataSource = paciente.cargartablaActivos()
                limpiarCamposTexto()

                'PACIENTE MENOR DE EDAD
            Else
                If tbCedulaResponsable.Text <> "" And tbNombreResponsable.Text <> "" And tbTelefono1Responsable.Text <> "" And cbGeneroResponsable.SelectedItem <> "" Then

                    responsable.insertarResponsable(New Responsables(tbNombreResponsable.Text, CInt(tbCedulaResponsable.Text),
                                                    tbxDireccionResponsable.Text, tbCorreoResponsable.Text, tbTelefono1Responsable.Text,
                                                    tbTelefono2Responsable.Text, tbParentescoResponsable.Text, getGenero(cbGeneroResponsable.SelectedItem),
                                                    dtpFechaNaciResponsable.Value.Date, 0))

                    paciente.insertarPaciente(New Pacientes(txtNombrePaciente.Text, CInt(txtCedulaPaciente.Text),
                                                   tbxDireccionPaciente.Text, txtCorreoPaciente.Text, txtOcupacionPaciente.Text, getGenero(cbGeneroPaciente.SelectedItem),
                                                   dtpFechaNacimientoPaciente.Value.Date, txtTelefono1Paciente.Text, txtTelefono2Paciente.Text,
                                                   responsable.ExtraerIDResponsable(CInt(tbCedulaResponsable.Text)), 0))

                    historial.insertarHistorialP(New HistorialFamiliar_Alergico(historial.ExtraerIDPaciente(CInt(txtCedulaPaciente.Text)), tbxQuejaPrincipal.Text, tbxAntecedentesFamiliares.Text,
                                                   tbxAntecedentesPersonales.Text, tbxAntecedentesNoPersonales.Text, tbxAntecedentesQuirurgicos.Text,
                                                   tbxAntecedentesAlergicos.Text, dtpFechaHistorial.Value.Date, 0))

                    dgvPacientes.DataSource = paciente.cargartablaActivos()
                    dgvResponsables.DataSource = responsable.ConsultarResponsables()
                    limpiarCamposTexto()
                Else
                    MsgBox("Debe registrar un encargado para el paciente, porque es menor de edad!!!", MsgBoxStyle.Critical, "Clinica Medica")

                End If

            End If
        Else
            MsgBox("Debe ingresar al menos los datos obligatorios (*)!!!", MsgBoxStyle.Critical, "Clinica Medica")

        End If
    End Sub

    'Detecta los cambios en el date time de paciente
    Private Sub dtpFechaNacimientoPaciente_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaNacimientoPaciente.ValueChanged
        Dim vAño As Integer = dtpFechaNacimientoPaciente.Value.Year
        txtEdadPaciente.Text = "" & getEdad(vAño)
        'VALIDA SI ES MAYOR O MENOR DE EDAD PARA DESABILITAR EL TABRESPONSABLES
        If CInt(txtEdadPaciente.Text) >= 18 Then
            Panel4.Visible = False
            lcondicion.Visible = True

        Else
            Panel4.Visible = True
            lcondicion.Visible = False
        End If
    End Sub

    'Detecta los cambios en el date time de responsable
    Private Sub dtpFechaNaciResponsable_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaNaciResponsable.ValueChanged
        Dim vAño As Integer = dtpFechaNaciResponsable.Value.Year
        tbEdadResponsable.Text = "" & getEdad(vAño)
    End Sub

    'OBTIENE EL GENERO DEL COMBO BOX
    Function getGenero(txt As String) As Boolean
        If txt = "Femenino" Then
            getGenero = 0
        Else
            getGenero = 1
        End If
        Return getGenero
    End Function

    'OBTIENE LA EDAD DEL PACIENTE/RESPONSABLE
    Function getEdad(año As Integer) As Integer
        Dim añoactual As Integer = Year(Now)
        getEdad = añoactual - año
    End Function

    'Detecta si se escribe en el textbox busqueda paciente
    Private Sub txtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        dgvPacientes.DataSource = paciente.cargarTabla1paciente(txtBusqueda.Text)
    End Sub

    'Detecta si se escribe en el textbox busqueda responsable
    Private Sub tbBusquedaResponsable_TextChanged(sender As Object, e As EventArgs) Handles tbBusquedaResponsable.TextChanged
        If tbBusquedaResponsable.Text <> "" Then
            dgvResponsables.DataSource = responsable.Consultar1ResponsableActivo(tbBusquedaResponsable.Text)
        Else
            dgvResponsables.DataSource = responsable.ConsultarResponsables()
        End If

    End Sub

    'Detectar cambios en la seleccion del checkbox activos
    Private Sub cbxActivos_CheckedChanged(sender As Object, e As EventArgs) Handles cbxActivos.CheckedChanged
        cbxActivos.Visible = False
        cbxPapelera.Visible = True
        dgvPacientes.DataSource = paciente.cargartablaActivos()
        btnRestablecer.Visible = False
        btnEliminar.Visible = True
        btnActualizar.Visible = True
        btnNUevoPaciente.Visible = True
        btnEliminar.Enabled = False
        btnActualizar.Enabled = False
    End Sub

    'Detectar cambios en la seleccion del checkbox papelera
    Private Sub cbxPapelera_CheckedChanged(sender As Object, e As EventArgs) Handles cbxPapelera.CheckedChanged
        cbxActivos.Visible = True
        cbxPapelera.Visible = False
        dgvPacientes.DataSource = paciente.cargartablaInactivos()
        btnRestablecer.Visible = True
        btnEliminar.Visible = False
        btnNUevoPaciente.Visible = False
        btnActualizar.Visible = False
        btnRestablecer.Enabled = False
    End Sub

    'Seleccion fila de tabla pacientes
    Private Sub dgvPacientes_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles dgvPacientes.MouseDoubleClick
        btnEliminar.Enabled = True
        btnActualizar.Enabled = True
        btnRestablecer.Enabled = True
        btnNUevoPaciente.Enabled = True
        If dgvPacientes.SelectedRows.Count > 0 Then
            Dim genero, estado As Boolean
            If dgvPacientes.CurrentRow.Cells(6).Value = 0 Then
                genero = 0
                cbGeneroPaciente.Text = "Femenino"
            Else
                genero = 1
                cbGeneroPaciente.Text = "Masculino"
            End If

            If dgvPacientes.CurrentRow.Cells(11).Value = 0 Then
                estado = 0
            Else
                estado = 1
            End If

            paci = New Pacientes(CInt(dgvPacientes.CurrentRow.Cells(0).Value.ToString), dgvPacientes.CurrentRow.Cells(2).Value.ToString,
                                 CInt(dgvPacientes.CurrentRow.Cells(1).Value.ToString), dgvPacientes.CurrentRow.Cells(3).Value.ToString,
                                 dgvPacientes.CurrentRow.Cells(4).Value.ToString, dgvPacientes.CurrentRow.Cells(5).Value.ToString,
                                 genero, Convert.ToDateTime(dgvPacientes.CurrentRow.Cells(7).Value.ToString), dgvPacientes.CurrentRow.Cells(8).Value.ToString,
                                 dgvPacientes.CurrentRow.Cells(9).Value.ToString, estado)

            txtNombrePaciente.Text = paci.NombreCompleto_
            txtCedulaPaciente.Text = paci.Cedula_
            txtCorreoPaciente.Text = paci.Correo_
            txtOcupacionPaciente.Text = paci.Ocupacion_
            txtTelefono1Paciente.Text = paci.Telefono1_
            txtTelefono2Paciente.Text = paci.Telefono2_
            tbxDireccionPaciente.Text = paci.Direccion_
            dtpFechaNacimientoPaciente.Value = paci.FechaNacimiento_

            hist = Nothing
            hist = historial.Consultar1Historial(CInt(dgvPacientes.CurrentRow.Cells(0).Value.ToString))
            If Not (hist Is Nothing) Then

                dtpFechaHistorial.Value = hist.Fecha_
                tbxAntecedentesAlergicos.Text = hist.Antecedentes_alergicos_
                tbxAntecedentesFamiliares.Text = hist.Antecedentes_patologicos_familiares_
                tbxAntecedentesNoPersonales.Text = hist.Antecedentes_patologicos_no_personales_
                tbxAntecedentesPersonales.Text = hist.Antecedentes_patologicos_personales_
                tbxAntecedentesQuirurgicos.Text = hist.Antecedentes_quirurgicos_
                tbxQuejaPrincipal.Text = hist.Queja_principal_
            Else

                dtpFechaHistorial.ResetText()
                tbxAntecedentesAlergicos.Text = ""
                tbxAntecedentesFamiliares.Text = ""
                tbxAntecedentesNoPersonales.Text = ""
                tbxAntecedentesPersonales.Text = ""
                tbxAntecedentesQuirurgicos.Text = ""
                tbxQuejaPrincipal.Text = ""
            End If


            If dgvPacientes.CurrentRow.Cells(10).Value.ToString <> Nothing Then
                    If CInt(txtEdadPaciente.Text) < 18 Then

                        resp = responsable.Consultar1Responsable(CInt(dgvPacientes.CurrentRow.Cells(10).Value.ToString))
                        tbNombreResponsable.Text = resp.NombreCompleto_
                        tbCedulaResponsable.Text = resp.Cedula_
                        tbCorreoResponsable.Text = resp.Correo_
                        tbParentescoResponsable.Text = resp.Parentesco_
                        tbTelefono1Responsable.Text = resp.Telefono1_
                        tbTelefono2Responsable.Text = resp.Telefono2_
                        tbxDireccionResponsable.Text = resp.Direccion_
                        dtpFechaNaciResponsable.Value = resp.FechaNacimiento_

                        If resp.Genero_ = 0 Then
                            cbGeneroResponsable.Text = "Femenino"
                        Else
                            cbGeneroResponsable.Text = "Masculino"
                        End If
                    End If
                End If
            End If
    End Sub

    'Seleccion fila de tabla responsables
    Private Sub dgvResponsables_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles dgvResponsables.MouseDoubleClick
        btnRemplazar.Enabled = True
        btnOlvidar.Enabled = True
        btnActualizarResponsable.Enabled = False
        If dgvResponsables.SelectedRows.Count > 0 Then
            Dim genero, estado As Boolean
            If dgvResponsables.CurrentRow.Cells(8).Value = 0 Then
                genero = 0
                cbGeneroResponsable.Text = "Femenino"
            Else
                genero = 1
                cbGeneroResponsable.Text = "Masculino"
            End If

            If dgvResponsables.CurrentRow.Cells(10).Value = 0 Then
                estado = 0
            Else
                estado = 1
            End If
            resp_remplazo = New Responsables(CInt(dgvResponsables.CurrentRow.Cells(0).Value.ToString), dgvResponsables.CurrentRow.Cells(2).Value.ToString,
                                            CInt(dgvResponsables.CurrentRow.Cells(1).Value.ToString), dgvResponsables.CurrentRow.Cells(3).Value.ToString,
                                            dgvResponsables.CurrentRow.Cells(4).Value.ToString, dgvResponsables.CurrentRow.Cells(5).Value.ToString,
                                            dgvResponsables.CurrentRow.Cells(6).Value.ToString, dgvResponsables.CurrentRow.Cells(7).Value.ToString, genero,
                                            Convert.ToDateTime(dgvResponsables.CurrentRow.Cells(9).Value.ToString), estado)

            tbNombreResponsable.Text = resp_remplazo.NombreCompleto_
            tbCedulaResponsable.Text = resp_remplazo.Cedula_
            tbCorreoResponsable.Text = resp_remplazo.Correo_
            tbTelefono1Responsable.Text = resp_remplazo.Telefono1_
            tbTelefono2Responsable.Text = resp_remplazo.Telefono2_
            tbParentescoResponsable.Text = resp_remplazo.Parentesco_
            tbxDireccionResponsable.Text = resp_remplazo.Direccion_
            dtpFechaNaciResponsable.Value = resp_remplazo.FechaNacimiento_
        End If
    End Sub

    'Eliminar un paciente
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        paciente.bloquearPaciente(CInt(dgvPacientes.CurrentRow.Cells(0).Value.ToString))
        dgvPacientes.DataSource = paciente.cargartablaActivos()
        btnEliminar.Enabled = False
        btnActualizar.Enabled = False
        limpiarCamposTexto()
    End Sub

    'Restablecer un paciente
    Private Sub btnRestablecer_Click(sender As Object, e As EventArgs) Handles btnRestablecer.Click
        paciente.RestablecerPaciente(CInt(dgvPacientes.CurrentRow.Cells(0).Value.ToString))
        cbxActivos.Visible = False
        cbxPapelera.Visible = True
        btnRestablecer.Visible = False
        btnEliminar.Visible = True
        btnActualizar.Visible = True
        btnEliminar.Enabled = False
        btnActualizar.Enabled = False
        dgvPacientes.DataSource = paciente.cargartablaActivos()
        limpiarCamposTexto()
    End Sub

    'Actualizar un paciente
    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        If CInt(txtEdadPaciente.Text) >= 18 Then
            paciente.actualizarPacienteMayor(New Pacientes(CInt(dgvPacientes.CurrentRow.Cells(0).Value.ToString), txtNombrePaciente.Text,
                                                  CInt(txtCedulaPaciente.Text), tbxDireccionPaciente.Text, txtCorreoPaciente.Text,
                                                  txtOcupacionPaciente.Text, getGenero(cbGeneroPaciente.SelectedValue),
                                           dtpFechaNacimientoPaciente.Value.Date, txtTelefono1Paciente.Text, txtTelefono2Paciente.Text, 0))

            dgvPacientes.DataSource = paciente.cargartablaActivos()
        Else
            paciente.actualizarPaciente(New Pacientes(CInt(dgvPacientes.CurrentRow.Cells(0).Value.ToString), txtNombrePaciente.Text,
                                                  CInt(txtCedulaPaciente.Text), tbxDireccionPaciente.Text, txtCorreoPaciente.Text,
                                                  txtOcupacionPaciente.Text, getGenero(cbGeneroPaciente.SelectedValue),
                                           dtpFechaNacimientoPaciente.Value.Date, txtTelefono1Paciente.Text, txtTelefono2Paciente.Text,
                                           CInt(dgvPacientes.CurrentRow.Cells(10).Value.ToString), 0))

            dgvPacientes.DataSource = paciente.cargartablaActivos()
        End If
    End Sub

    'Actualizar responsable
    Private Sub btnActualizarResponsable_Click(sender As Object, e As EventArgs) Handles btnActualizarResponsable.Click
        resp.NombreCompleto_ = tbNombreResponsable.Text
        resp.Cedula_ = tbCedulaResponsable.Text
        resp.Correo_ = tbCorreoResponsable.Text
        resp.Telefono1_ = tbTelefono1Responsable.Text
        resp.Telefono2_ = tbTelefono2Responsable.Text
        resp.Parentesco_ = tbParentescoResponsable.Text
        resp.Direccion_ = tbxDireccionResponsable.Text
        resp.FechaNacimiento_ = dtpFechaNaciResponsable.Value
        resp.Genero_ = getGenero(cbGeneroResponsable.SelectedItem)

        responsable.actualizarResponsable(resp)
        dgvResponsables.DataSource = responsable.ConsultarResponsables()
    End Sub

    'Actualizar historial
    Private Sub btnActualizarHistorial_Click(sender As Object, e As EventArgs) Handles btnActualizarHistorial.Click
        hist.Fecha_ = dtpFechaHistorial.Value
        hist.Antecedentes_alergicos_ = tbxAntecedentesAlergicos.Text
        hist.Antecedentes_patologicos_familiares_ = tbxAntecedentesFamiliares.Text
        hist.Antecedentes_patologicos_no_personales_ = tbxAntecedentesNoPersonales.Text
        hist.Antecedentes_patologicos_personales_ = tbxAntecedentesPersonales.Text
        hist.Antecedentes_quirurgicos_ = tbxAntecedentesQuirurgicos.Text
        hist.Queja_principal_ = tbxQuejaPrincipal.Text
        hist.ID_Paciente_ = paci.ID_Paciente_

        historial.actualizarHistorialP(hist)
    End Sub

    'Remplazar un responsable a paciente determinado
    Private Sub btnRemplazar_Click(sender As Object, e As EventArgs) Handles btnRemplazar.Click
        btnRemplazar.Enabled = False
        btnOlvidar.Enabled = False
        btnActualizarResponsable.Enabled = True
        If Not (resp_remplazo Is Nothing) Then
            responsable.reemplazarResponsable(paci.ID_Paciente_, resp_remplazo.ID_Responsable_)
            dgvResponsables.DataSource = responsable.ConsultarResponsables()
            dgvPacientes.DataSource = paciente.cargartablaActivos()

            resp = responsable.Consultar1Responsable(resp_remplazo.ID_Responsable_)
            tbNombreResponsable.Text = resp.NombreCompleto_
            tbCedulaResponsable.Text = resp.Cedula_
            tbCorreoResponsable.Text = resp.Correo_
            tbParentescoResponsable.Text = resp.Parentesco_
            tbTelefono1Responsable.Text = resp.Telefono1_
            tbTelefono2Responsable.Text = resp.Telefono2_
            tbxDireccionResponsable.Text = resp.Direccion_
            dtpFechaNaciResponsable.Value = resp.FechaNacimiento_

            If resp.Genero_ = 0 Then
                cbGeneroResponsable.Text = "Femenino"
            Else
                cbGeneroResponsable.Text = "Masculino"
            End If
        End If
    End Sub

    'Carga de nuevo el responsable del paciente determinado
    Private Sub btnOlvidar_Click(sender As Object, e As EventArgs) Handles btnOlvidar.Click
        btnRemplazar.Enabled = False
        btnOlvidar.Enabled = False
        btnActualizarResponsable.Enabled = True

        If Not IsNothing(resp) Then
            tbNombreResponsable.Text = resp.NombreCompleto_
            tbCedulaResponsable.Text = resp.Cedula_
            tbCorreoResponsable.Text = resp.Correo_
            tbParentescoResponsable.Text = resp.Parentesco_
            tbTelefono1Responsable.Text = resp.Telefono1_
            tbTelefono2Responsable.Text = resp.Telefono2_
            tbxDireccionResponsable.Text = resp.Direccion_
            dtpFechaNaciResponsable.Value = resp.FechaNacimiento_

            If resp.Genero_ = 0 Then
                cbGeneroResponsable.Text = "Femenino"
            Else
                cbGeneroResponsable.Text = "Masculino"
            End If
        Else
            MsgBox("Debe estableser un Responsable al paciente.", MsgBoxStyle.Information, "Clinica Médica")
        End If
    End Sub

    'Vacia todo para ingresar un nuevo responsable
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        'Responsables
        tbNombreResponsable.Text = ""
        tbCedulaResponsable.Text = ""
        tbCorreoResponsable.Text = ""
        tbParentescoResponsable.Text = ""
        tbTelefono1Responsable.Text = ""
        tbTelefono2Responsable.Text = ""
        tbxDireccionResponsable.Text = ""
        dtpFechaNaciResponsable.ResetText()
        cbGeneroResponsable.Text = ""

        btnNuevo.Visible = False
        btnGuardarNuevoR.Visible = True
        btnOlvidar.Enabled = True
    End Sub

    'Guardar un nuevo responsable a un paciente determinado
    Private Sub btnGuardarNuevoR_Click(sender As Object, e As EventArgs) Handles btnGuardarNuevoR.Click

        responsable.insertarResponsable(New Responsables(tbNombreResponsable.Text, CInt(tbCedulaResponsable.Text),
                                                tbxDireccionResponsable.Text, tbCorreoResponsable.Text, tbTelefono1Responsable.Text,
                                                tbTelefono2Responsable.Text, tbParentescoResponsable.Text, getGenero(cbGeneroResponsable.SelectedItem),
                                                dtpFechaNaciResponsable.Value.Date, 0))
        Dim id_r As Integer = responsable.ExtraerIDResponsable(CInt(tbCedulaResponsable.Text))

        responsable.reemplazarResponsable(paci.ID_Paciente_, id_r)
        dgvResponsables.DataSource = responsable.ConsultarResponsables()
        dgvPacientes.DataSource = paciente.cargartablaActivos()

        btnGuardarNuevoR.Visible = False
        btnNuevo.Visible = True
    End Sub

    'Vaciar para ingresar nuevo paciente
    Private Sub btnNUevoPaciente_Click(sender As Object, e As EventArgs) Handles btnNUevoPaciente.Click
        ''Pacientes
        If MsgBox("¿Está seguro que desea ingresar un nuevo paciente, se eliminará la busqueda realizada?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            txtNombrePaciente.Text = ""
            txtCedulaPaciente.Text = ""
            txtCorreoPaciente.Text = ""
            txtOcupacionPaciente.Text = ""
            txtTelefono1Paciente.Text = ""
            txtTelefono2Paciente.Text = ""
            tbxDireccionPaciente.Text = ""
            dtpFechaNacimientoPaciente.ResetText()
            cbGeneroPaciente.Text = ""

            btnActualizar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    'Limpiar campos
    Sub limpiarCamposTexto()
        'Responsables
        tbNombreResponsable.Text = ""
        tbCedulaResponsable.Text = ""
        tbCorreoResponsable.Text = ""
        tbParentescoResponsable.Text = ""
        tbTelefono1Responsable.Text = ""
        tbTelefono2Responsable.Text = ""
        tbxDireccionResponsable.Text = ""
        dtpFechaNaciResponsable.ResetText()
        cbGeneroResponsable.Text = ""

        ''Pacientes
        txtNombrePaciente.Text = ""
        txtCedulaPaciente.Text = ""
        txtCorreoPaciente.Text = ""
        txtOcupacionPaciente.Text = ""
        txtTelefono1Paciente.Text = ""
        txtTelefono2Paciente.Text = ""
        tbxDireccionPaciente.Text = ""
        dtpFechaNacimientoPaciente.ResetText()
        cbGeneroPaciente.Text = ""

        'Historial
        dtpFechaHistorial.ResetText()
        tbxAntecedentesAlergicos.Text = ""
        tbxAntecedentesFamiliares.Text = ""
        tbxAntecedentesNoPersonales.Text = ""
        tbxAntecedentesPersonales.Text = ""
        tbxAntecedentesQuirurgicos.Text = ""
        tbxQuejaPrincipal.Text = ""
    End Sub

End Class
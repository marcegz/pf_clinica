﻿Public Class FrmFacturas

    Dim idCitaFactura As Integer
    Dim lncita As New Ln_Citas
    Dim lnPaciente As New Ln_Paciente
    Dim lnFuncionario As New Ln_Funcionario
    Dim lnTratamiento As New Ln_Tratamientos
    Dim lnHistorialMedicina As New Ln_Historial_Tratamientos_Consultas
    Dim lnFacturas As New Ln_Facturas
    Dim lnHora As New Ln_Horas
    Dim cita As New Citas()
    Dim paciente As New Pacientes()
    Dim especialidad As String

    Dim lx, ly As Integer
    Dim sw, sh As Integer
    Dim x, y As Integer
    Dim newpoint As Point

    Public Sub New(idCita As Integer, especialidadU As String)
        InitializeComponent()
        idCitaFactura = idCita
        especialidad = especialidadU
    End Sub

    Private Sub FrmFacturas_Load(sender As Object, e As EventArgs) Handles Me.Load
        cita = lncita.ExtraerCita(idCitaFactura)
        paciente = lnPaciente.ConsultarPacienteCOMPLETO(cita.ID_Paciente_)
        Dim funcionario() = lnFuncionario.ConsultarFuncionarioFactura(cita.ID_Funcionario_).Split("-")
        txtIDFuncionario.Text = funcionario(0)
        txtFuncionario.Text = funcionario(1)
        txtPaciente.Text = paciente.NombreCompleto_
        txtIDPaciente.Text = paciente.Cedula_
        dtpFecha.Value = cita.Fecha_
        txtHora.Text = Convert.ToString(lnHora.Consultar1Hora(cita.ID_Hora_).Hora_)
        cbDescuento.SelectedIndex = 0
        llenarTablaDetalleTratamientos()
        lMontoSubTotal.Text = getSubTotal()

        If lnFacturas.verificarFacturaExiste(cita.ID_Cita_) = "" Then
            txtIDFactura.Text = lnFacturas.getIdFactura()
        Else
            txtIDFactura.Text = lnFacturas.verificarFacturaExiste(cita.ID_Cita_)
        End If

        If lnFacturas.verificarFacturaExiste(cita.ID_Cita_) <> "" Then
            lMontoDescuento.Text = lnFacturas.getDescuentoFactura(cita.ID_Cita_)
            cbDescuento.SelectedItem = getPorcentaje()
            cbDescuento.Enabled = False
        End If

    End Sub

    Sub llenarTablaDetalleTratamientos()

        If especialidad = "Odontología" Then
            dgvDetalle.DataSource = lnTratamiento.ConsultarTratamientosFactura(idCitaFactura)
        Else
            dgvDetalle.DataSource = lnHistorialMedicina.ConsultarHistorialMedicinaGeneral(idCitaFactura)
        End If


    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        If MsgBox("¿Está seguro que desea cerrar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then
            Me.Close()
        End If
    End Sub

    Private Sub btnMinimizar_Click(sender As Object, e As EventArgs) Handles btnMinimizar.Click
        WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btnMedicina_Click(sender As Object, e As EventArgs) Handles btnMedicina.Click

        Dim f As New Facturas(idCitaFactura, cita.Fecha_, lnHora.Consultar1Hora(cita.ID_Hora_).Hora_, montoDescontado())

        If lnFacturas.verificarFacturaExiste(cita.ID_Cita_) = "" Then
            lnFacturas.insertarFactura(f)
            cbDescuento.Enabled = False
        End If


        If especialidad = "Odontología" Then
            Dim reporte As New ReporteFactura(idCitaFactura, lnFacturas.getIdFacturaReporte(idCitaFactura))
            reporte.Show()
        Else
            Dim reporte As New Form1(idCitaFactura, lnFacturas.getIdFacturaReporte(idCitaFactura))
            reporte.Show()
        End If


    End Sub

    Function montoDescontado() As Decimal

        Dim descuento As Decimal

        descuento = getSubTotal() * (CInt(cbDescuento.SelectedItem.ToString) / 100)

        Return descuento

    End Function

    Function getSubTotal() As Decimal

        Dim total As Decimal

        If especialidad = "Odontología" Then
            For Each tratamiento As DataRow In lnTratamiento.ConsultarTratamientosFactura(idCitaFactura).Rows
                total += Convert.ToDecimal(tratamiento("Precio"))
            Next
        Else
            For Each tratamiento As DataRow In lnHistorialMedicina.ConsultarHistorialMedicinaGeneral(idCitaFactura).Rows
                total += Convert.ToDecimal(tratamiento("Precio"))
            Next
        End If

        For Each tratamiento As DataRow In lnTratamiento.ConsultarTratamientosFactura(idCitaFactura).Rows
            total += Convert.ToDecimal(tratamiento("Precio"))
        Next

        Return total

    End Function

    Function getPorcentaje() As String

        Dim descuento As String = ""

        If lnFacturas.getDescuentoFactura(idCitaFactura) = Convert.ToDecimal((getSubTotal() * (0 / 100))) Then
            descuento = "0"
        Else
            If lnFacturas.getDescuentoFactura(idCitaFactura) = Convert.ToDecimal((getSubTotal() * (1 / 100))) Then
                descuento = "1"
            Else
                If lnFacturas.getDescuentoFactura(idCitaFactura) = Convert.ToDecimal((getSubTotal() * (2 / 100))) Then
                    descuento = "2"
                Else
                    If lnFacturas.getDescuentoFactura(idCitaFactura) = Convert.ToDecimal((getSubTotal() * (3 / 100))) Then
                        descuento = "3"
                    Else
                        If lnFacturas.getDescuentoFactura(idCitaFactura) = Convert.ToDecimal((getSubTotal() * (4 / 100))) Then
                            descuento = "4"
                        Else
                            If lnFacturas.getDescuentoFactura(idCitaFactura) = Convert.ToDecimal((getSubTotal() * (5 / 100))) Then
                                descuento = "5"
                            Else
                                If lnFacturas.getDescuentoFactura(idCitaFactura) = Convert.ToDecimal((getSubTotal() * (6 / 100))) Then
                                    descuento = "6"
                                Else
                                    If lnFacturas.getDescuentoFactura(idCitaFactura) = Convert.ToDecimal((getSubTotal() * (7 / 100))) Then
                                        descuento = "7"
                                    Else
                                        If lnFacturas.getDescuentoFactura(idCitaFactura) = Convert.ToDecimal((getSubTotal() * (8 / 100))) Then
                                            descuento = "8"
                                        Else
                                            If lnFacturas.getDescuentoFactura(idCitaFactura) = Convert.ToDecimal((getSubTotal() * (9 / 100))) Then
                                                descuento = "9"
                                            Else
                                                If lnFacturas.getDescuentoFactura(idCitaFactura) = Convert.ToDecimal((getSubTotal() * (10 / 100))) Then
                                                    descuento = "10"
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If

        Return descuento

    End Function

    Private Sub cbDescuento_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbDescuento.SelectedValueChanged
        lMontoDescuento.Text = montoDescontado()
        lMontoTotal.Text = getSubTotal() - montoDescontado()
    End Sub

    Private Sub BarraTitulo_MouseDown(sender As Object, e As MouseEventArgs)
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub BarraTitulo_MouseMove(sender As Object, e As MouseEventArgs)
        If e.Button = MouseButtons.Left Then
            newpoint = Control.MousePosition
            newpoint.X -= x
            newpoint.Y -= y
            Me.Location = newpoint
            Application.DoEvents()
        End If
    End Sub

End Class
﻿Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Mail

Public Class FrmRecuperarContraseña

    Dim lx, ly As Integer
    Dim sw, sh As Integer

    Dim x, y As Integer
    Dim newpoint As Point

    Private Sub FrmRecuperarContraseña_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Abrir()
    End Sub

    '------------------------------

    Dim randomCode As String
    Dim para As String

    Sub recuperarContraseña()

        Dim de As String
        Dim clave As String
        Dim cuerpoMensaje As String

        Dim message As New MailMessage()
        Dim smtpClient As New SmtpClient

        Dim rand As Random
        rand = New Random

        If txtCedula.Text = "CÉDULA" Or txtCedula.Text = "" Then 'puedo poner =Nothing
            msgError("Debe llenar ambos campos")
            txtCedula.Text = ""
        Else

            Dim rs As New SqlCommand("select * from Funcionarios where Cedula = '" & txtCedula.Text & "'", conn)

            Dim dr As SqlDataReader
            ' dr.Close()
            dr = rs.ExecuteReader
            If dr.Read Then
                Ced_Funcionario = dr.Item("Cedula")
                Nom_Funcionario = dr.Item("NombreCompleto")
                Correo_Funcionario = dr.Item("Correo")
                Contraseña_Funcionario = dr.Item("Contraseña")

                randomCode = (rand.Next(999999).ToString)
                para = Correo_Funcionario
                de = "stephannie.villalobos@gmail.com"
                clave = "16031997"
                cuerpoMensaje = "Hola " + Nom_Funcionario + "\n" +
                                "Tu código es " + randomCode

                message.To.Add(para)
                message.From = New MailAddress(de)
                message.Body = cuerpoMensaje
                message.Subject = "Codigo temporal de Restauración de Contraseña"
                message.Priority = MailPriority.Normal


                smtpClient.Host = "smtp.gmail.com"
                smtpClient.Port = 587 '465 '25 '587
                smtpClient.EnableSsl = True

                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network
                smtpClient.Credentials = New Net.NetworkCredential(de, clave)

                Dim cmdBloquearUser As New SqlCommand("update Funcionarios set Contraseña = '" & randomCode & "' where Cedula = '" & Ced_Funcionario & "'", conn)
                cmdBloquearUser.ExecuteNonQuery() 'EndExecuteNonQuery significa que NO es una consulta


                Try

                    smtpClient.Send(message)
                    msgError("Código enviado correctamente, Por favor verifique su correo")



                    lRecuperarContraseña.Visible = False
                    txtCedula.Visible = False
                    btnEnviarCodigo.Visible = False
                    lUsuario.Visible = False

                    lVerificarCódigo.Visible = True
                    txtCódigoVerificación.Visible = True
                    btnVerificarCódigo.Visible = True


                    'Me.Hide()
                    'FrmLogin.Show()

                Catch ex As Exception
                    MessageBox.Show(ex.Message.ToString)
                    msgError(ex.Message.ToString + "")
                End Try

            Else
                msgError("Usted no tiene una cuenta")

            End If

        End If


    End Sub

    Private Sub btnEnviarCodigo_Click(sender As Object, e As EventArgs) Handles btnEnviarCodigo.Click
        recuperarContraseña()
    End Sub

    Public Sub msgError(msg As String)
        txtCedula.Text = ""
        lMensajeError.Text = "     " + msg
        lMensajeError.Visible = True

    End Sub

    Private Sub btnVerificarCódigo_Click(sender As Object, e As EventArgs) Handles btnVerificarCódigo.Click
        If randomCode = txtCódigoVerificación.Text Then
            Contraseña_Funcionario = randomCode
            Me.Hide()
            FrmCambiarContraseña.Show()
        End If
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        'Application.Exit()
        If MsgBox("¿Está seguro de cerrar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then
            Me.Hide()
            FrmLogin.Show()
        End If
    End Sub


    '------------- ENTRAR Y SALIR DEL TXTCEDULA

    Private Sub txtCedula_Enter(sender As Object, e As EventArgs) Handles txtCedula.Enter
        If txtCedula.Text = "CÉDULA" Then
            txtCedula.Text = ""
            txtCedula.ForeColor = Color.LightGray
        End If
    End Sub

    Private Sub txtCedula_Leave(sender As Object, e As EventArgs) Handles txtCedula.Leave
        If txtCedula.Text = "" Then
            txtCedula.Text = "CÉDULA"
            txtCedula.ForeColor = Color.DarkGray
        End If
    End Sub

    '------------- ENTRAR Y SALIR DEL txtCódigoVerificación

    Private Sub txtCódigoVerificación_Enter(sender As Object, e As EventArgs) Handles txtCódigoVerificación.Enter
        If txtCódigoVerificación.Text = "CÓDIGO VERIFICACIÓN" Then
            txtCódigoVerificación.Text = ""
            txtCódigoVerificación.ForeColor = Color.LightGray

        End If
    End Sub

    Private Sub btnMinimizar_Click(sender As Object, e As EventArgs) Handles btnMinimizar.Click
        WindowState = FormWindowState.Minimized
    End Sub

    Private Sub txtCódigoVerificación_Leave(sender As Object, e As EventArgs) Handles txtCódigoVerificación.Leave
        If txtCódigoVerificación.Text = "" Then
            txtCódigoVerificación.Text = "CÓDIGO VERIFICACIÓN"
            txtCódigoVerificación.ForeColor = Color.DarkGray

        End If
    End Sub


    'Mover Formulario-------------------------------------------------------------------------------------

    Private Sub FrmRecuperarContraseña_MouseDown(sender As Object, e As MouseEventArgs) Handles MyBase.MouseDown
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub FrmRecuperarContraseña_MouseMove(sender As Object, e As MouseEventArgs) Handles MyBase.MouseMove
        If e.Button = MouseButtons.Left Then
            newpoint = Control.MousePosition
            newpoint.X -= x
            newpoint.Y -= y
            Me.Location = newpoint
            Application.DoEvents()
        End If
    End Sub

    '-------------------------------------------------------------------------------------

End Class
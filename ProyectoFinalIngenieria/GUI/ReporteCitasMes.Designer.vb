﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ReporteCitasMes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ConsultarCitasMesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataReportes = New ProyectoFinalIngenieria.DataReportes()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.ConsultarCitasMesTableAdapter = New ProyectoFinalIngenieria.DataReportesTableAdapters.ConsultarCitasMesTableAdapter()
        CType(Me.ConsultarCitasMesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ConsultarCitasMesBindingSource
        '
        Me.ConsultarCitasMesBindingSource.DataMember = "ConsultarCitasMes"
        Me.ConsultarCitasMesBindingSource.DataSource = Me.DataReportes
        '
        'DataReportes
        '
        Me.DataReportes.DataSetName = "DataReportes"
        Me.DataReportes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "DataReporte"
        ReportDataSource1.Value = Me.ConsultarCitasMesBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ProyectoFinalIngenieria.ReporteCitasMes.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(856, 450)
        Me.ReportViewer1.TabIndex = 0
        '
        'ConsultarCitasMesTableAdapter
        '
        Me.ConsultarCitasMesTableAdapter.ClearBeforeFill = True
        '
        'ReporteCitasMes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(856, 450)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "ReporteCitasMes"
        Me.Text = "Reporte Citas Mes"
        CType(Me.ConsultarCitasMesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents ConsultarCitasMesBindingSource As BindingSource
    Friend WithEvents DataReportes As DataReportes
    Friend WithEvents ConsultarCitasMesTableAdapter As DataReportesTableAdapters.ConsultarCitasMesTableAdapter
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAtencionOdontologica
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAtencionOdontologica))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lMontoTotal = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lTotal = New System.Windows.Forms.Label()
        Me.cbPacientesDelDia = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn38 = New System.Windows.Forms.Button()
        Me.btn37 = New System.Windows.Forms.Button()
        Me.btn36 = New System.Windows.Forms.Button()
        Me.btn35 = New System.Windows.Forms.Button()
        Me.btn34 = New System.Windows.Forms.Button()
        Me.btn33 = New System.Windows.Forms.Button()
        Me.btn32 = New System.Windows.Forms.Button()
        Me.btn31 = New System.Windows.Forms.Button()
        Me.btn41 = New System.Windows.Forms.Button()
        Me.btn42 = New System.Windows.Forms.Button()
        Me.btn43 = New System.Windows.Forms.Button()
        Me.btn44 = New System.Windows.Forms.Button()
        Me.btn45 = New System.Windows.Forms.Button()
        Me.btn46 = New System.Windows.Forms.Button()
        Me.btn47 = New System.Windows.Forms.Button()
        Me.btn48 = New System.Windows.Forms.Button()
        Me.btn75 = New System.Windows.Forms.Button()
        Me.btn74 = New System.Windows.Forms.Button()
        Me.btn73 = New System.Windows.Forms.Button()
        Me.btn72 = New System.Windows.Forms.Button()
        Me.btn71 = New System.Windows.Forms.Button()
        Me.btn81 = New System.Windows.Forms.Button()
        Me.btn82 = New System.Windows.Forms.Button()
        Me.btn83 = New System.Windows.Forms.Button()
        Me.btn84 = New System.Windows.Forms.Button()
        Me.btn85 = New System.Windows.Forms.Button()
        Me.btn65 = New System.Windows.Forms.Button()
        Me.btn64 = New System.Windows.Forms.Button()
        Me.btn63 = New System.Windows.Forms.Button()
        Me.btn62 = New System.Windows.Forms.Button()
        Me.btn61 = New System.Windows.Forms.Button()
        Me.btn51 = New System.Windows.Forms.Button()
        Me.btn52 = New System.Windows.Forms.Button()
        Me.btn53 = New System.Windows.Forms.Button()
        Me.btn54 = New System.Windows.Forms.Button()
        Me.btn55 = New System.Windows.Forms.Button()
        Me.btn28 = New System.Windows.Forms.Button()
        Me.btn27 = New System.Windows.Forms.Button()
        Me.btn26 = New System.Windows.Forms.Button()
        Me.btn25 = New System.Windows.Forms.Button()
        Me.btn24 = New System.Windows.Forms.Button()
        Me.btn23 = New System.Windows.Forms.Button()
        Me.btn22 = New System.Windows.Forms.Button()
        Me.btn21 = New System.Windows.Forms.Button()
        Me.btn11 = New System.Windows.Forms.Button()
        Me.btn12 = New System.Windows.Forms.Button()
        Me.btn13 = New System.Windows.Forms.Button()
        Me.btn14 = New System.Windows.Forms.Button()
        Me.btn15 = New System.Windows.Forms.Button()
        Me.btn16 = New System.Windows.Forms.Button()
        Me.btn17 = New System.Windows.Forms.Button()
        Me.l38 = New System.Windows.Forms.Label()
        Me.l37 = New System.Windows.Forms.Label()
        Me.l36 = New System.Windows.Forms.Label()
        Me.l35 = New System.Windows.Forms.Label()
        Me.l34 = New System.Windows.Forms.Label()
        Me.l33 = New System.Windows.Forms.Label()
        Me.l32 = New System.Windows.Forms.Label()
        Me.l31 = New System.Windows.Forms.Label()
        Me.l41 = New System.Windows.Forms.Label()
        Me.l42 = New System.Windows.Forms.Label()
        Me.l43 = New System.Windows.Forms.Label()
        Me.l44 = New System.Windows.Forms.Label()
        Me.l45 = New System.Windows.Forms.Label()
        Me.l46 = New System.Windows.Forms.Label()
        Me.l47 = New System.Windows.Forms.Label()
        Me.l48 = New System.Windows.Forms.Label()
        Me.l75 = New System.Windows.Forms.Label()
        Me.l74 = New System.Windows.Forms.Label()
        Me.l73 = New System.Windows.Forms.Label()
        Me.l72 = New System.Windows.Forms.Label()
        Me.l71 = New System.Windows.Forms.Label()
        Me.l81 = New System.Windows.Forms.Label()
        Me.l82 = New System.Windows.Forms.Label()
        Me.l83 = New System.Windows.Forms.Label()
        Me.l84 = New System.Windows.Forms.Label()
        Me.l85 = New System.Windows.Forms.Label()
        Me.l65 = New System.Windows.Forms.Label()
        Me.l64 = New System.Windows.Forms.Label()
        Me.l63 = New System.Windows.Forms.Label()
        Me.l62 = New System.Windows.Forms.Label()
        Me.l61 = New System.Windows.Forms.Label()
        Me.l51 = New System.Windows.Forms.Label()
        Me.l52 = New System.Windows.Forms.Label()
        Me.l53 = New System.Windows.Forms.Label()
        Me.l54 = New System.Windows.Forms.Label()
        Me.l55 = New System.Windows.Forms.Label()
        Me.l28 = New System.Windows.Forms.Label()
        Me.l27 = New System.Windows.Forms.Label()
        Me.l26 = New System.Windows.Forms.Label()
        Me.l25 = New System.Windows.Forms.Label()
        Me.l24 = New System.Windows.Forms.Label()
        Me.l23 = New System.Windows.Forms.Label()
        Me.l22 = New System.Windows.Forms.Label()
        Me.l21 = New System.Windows.Forms.Label()
        Me.l11 = New System.Windows.Forms.Label()
        Me.l12 = New System.Windows.Forms.Label()
        Me.l13 = New System.Windows.Forms.Label()
        Me.l14 = New System.Windows.Forms.Label()
        Me.l15 = New System.Windows.Forms.Label()
        Me.l16 = New System.Windows.Forms.Label()
        Me.l17 = New System.Windows.Forms.Label()
        Me.l18 = New System.Windows.Forms.Label()
        Me.btn18 = New System.Windows.Forms.Button()
        Me.dgvTratamientosConsultaOdontologia = New System.Windows.Forms.DataGridView()
        Me.txtFuncionario = New System.Windows.Forms.TextBox()
        Me.btnListo = New System.Windows.Forms.Button()
        Me.txtHora = New System.Windows.Forms.TextBox()
        Me.txtEdad = New System.Windows.Forms.TextBox()
        Me.txtCedula = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.rtbDescripcion = New System.Windows.Forms.RichTextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.BarraTitulo = New System.Windows.Forms.Panel()
        Me.btnMinimizar = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvTratamientosConsultaOdontologia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BarraTitulo.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lMontoTotal)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.lTotal)
        Me.Panel1.Controls.Add(Me.cbPacientesDelDia)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.btn38)
        Me.Panel1.Controls.Add(Me.btn37)
        Me.Panel1.Controls.Add(Me.btn36)
        Me.Panel1.Controls.Add(Me.btn35)
        Me.Panel1.Controls.Add(Me.btn34)
        Me.Panel1.Controls.Add(Me.btn33)
        Me.Panel1.Controls.Add(Me.btn32)
        Me.Panel1.Controls.Add(Me.btn31)
        Me.Panel1.Controls.Add(Me.btn41)
        Me.Panel1.Controls.Add(Me.btn42)
        Me.Panel1.Controls.Add(Me.btn43)
        Me.Panel1.Controls.Add(Me.btn44)
        Me.Panel1.Controls.Add(Me.btn45)
        Me.Panel1.Controls.Add(Me.btn46)
        Me.Panel1.Controls.Add(Me.btn47)
        Me.Panel1.Controls.Add(Me.btn48)
        Me.Panel1.Controls.Add(Me.btn75)
        Me.Panel1.Controls.Add(Me.btn74)
        Me.Panel1.Controls.Add(Me.btn73)
        Me.Panel1.Controls.Add(Me.btn72)
        Me.Panel1.Controls.Add(Me.btn71)
        Me.Panel1.Controls.Add(Me.btn81)
        Me.Panel1.Controls.Add(Me.btn82)
        Me.Panel1.Controls.Add(Me.btn83)
        Me.Panel1.Controls.Add(Me.btn84)
        Me.Panel1.Controls.Add(Me.btn85)
        Me.Panel1.Controls.Add(Me.btn65)
        Me.Panel1.Controls.Add(Me.btn64)
        Me.Panel1.Controls.Add(Me.btn63)
        Me.Panel1.Controls.Add(Me.btn62)
        Me.Panel1.Controls.Add(Me.btn61)
        Me.Panel1.Controls.Add(Me.btn51)
        Me.Panel1.Controls.Add(Me.btn52)
        Me.Panel1.Controls.Add(Me.btn53)
        Me.Panel1.Controls.Add(Me.btn54)
        Me.Panel1.Controls.Add(Me.btn55)
        Me.Panel1.Controls.Add(Me.btn28)
        Me.Panel1.Controls.Add(Me.btn27)
        Me.Panel1.Controls.Add(Me.btn26)
        Me.Panel1.Controls.Add(Me.btn25)
        Me.Panel1.Controls.Add(Me.btn24)
        Me.Panel1.Controls.Add(Me.btn23)
        Me.Panel1.Controls.Add(Me.btn22)
        Me.Panel1.Controls.Add(Me.btn21)
        Me.Panel1.Controls.Add(Me.btn11)
        Me.Panel1.Controls.Add(Me.btn12)
        Me.Panel1.Controls.Add(Me.btn13)
        Me.Panel1.Controls.Add(Me.btn14)
        Me.Panel1.Controls.Add(Me.btn15)
        Me.Panel1.Controls.Add(Me.btn16)
        Me.Panel1.Controls.Add(Me.btn17)
        Me.Panel1.Controls.Add(Me.l38)
        Me.Panel1.Controls.Add(Me.l37)
        Me.Panel1.Controls.Add(Me.l36)
        Me.Panel1.Controls.Add(Me.l35)
        Me.Panel1.Controls.Add(Me.l34)
        Me.Panel1.Controls.Add(Me.l33)
        Me.Panel1.Controls.Add(Me.l32)
        Me.Panel1.Controls.Add(Me.l31)
        Me.Panel1.Controls.Add(Me.l41)
        Me.Panel1.Controls.Add(Me.l42)
        Me.Panel1.Controls.Add(Me.l43)
        Me.Panel1.Controls.Add(Me.l44)
        Me.Panel1.Controls.Add(Me.l45)
        Me.Panel1.Controls.Add(Me.l46)
        Me.Panel1.Controls.Add(Me.l47)
        Me.Panel1.Controls.Add(Me.l48)
        Me.Panel1.Controls.Add(Me.l75)
        Me.Panel1.Controls.Add(Me.l74)
        Me.Panel1.Controls.Add(Me.l73)
        Me.Panel1.Controls.Add(Me.l72)
        Me.Panel1.Controls.Add(Me.l71)
        Me.Panel1.Controls.Add(Me.l81)
        Me.Panel1.Controls.Add(Me.l82)
        Me.Panel1.Controls.Add(Me.l83)
        Me.Panel1.Controls.Add(Me.l84)
        Me.Panel1.Controls.Add(Me.l85)
        Me.Panel1.Controls.Add(Me.l65)
        Me.Panel1.Controls.Add(Me.l64)
        Me.Panel1.Controls.Add(Me.l63)
        Me.Panel1.Controls.Add(Me.l62)
        Me.Panel1.Controls.Add(Me.l61)
        Me.Panel1.Controls.Add(Me.l51)
        Me.Panel1.Controls.Add(Me.l52)
        Me.Panel1.Controls.Add(Me.l53)
        Me.Panel1.Controls.Add(Me.l54)
        Me.Panel1.Controls.Add(Me.l55)
        Me.Panel1.Controls.Add(Me.l28)
        Me.Panel1.Controls.Add(Me.l27)
        Me.Panel1.Controls.Add(Me.l26)
        Me.Panel1.Controls.Add(Me.l25)
        Me.Panel1.Controls.Add(Me.l24)
        Me.Panel1.Controls.Add(Me.l23)
        Me.Panel1.Controls.Add(Me.l22)
        Me.Panel1.Controls.Add(Me.l21)
        Me.Panel1.Controls.Add(Me.l11)
        Me.Panel1.Controls.Add(Me.l12)
        Me.Panel1.Controls.Add(Me.l13)
        Me.Panel1.Controls.Add(Me.l14)
        Me.Panel1.Controls.Add(Me.l15)
        Me.Panel1.Controls.Add(Me.l16)
        Me.Panel1.Controls.Add(Me.l17)
        Me.Panel1.Controls.Add(Me.l18)
        Me.Panel1.Controls.Add(Me.btn18)
        Me.Panel1.Controls.Add(Me.dgvTratamientosConsultaOdontologia)
        Me.Panel1.Controls.Add(Me.txtFuncionario)
        Me.Panel1.Controls.Add(Me.btnListo)
        Me.Panel1.Controls.Add(Me.txtHora)
        Me.Panel1.Controls.Add(Me.txtEdad)
        Me.Panel1.Controls.Add(Me.txtCedula)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.rtbDescripcion)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txtNombre)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.ShapeContainer1)
        Me.Panel1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(0, 109)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1183, 592)
        Me.Panel1.TabIndex = 12
        '
        'lMontoTotal
        '
        Me.lMontoTotal.AutoSize = True
        Me.lMontoTotal.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMontoTotal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lMontoTotal.Location = New System.Drawing.Point(782, 482)
        Me.lMontoTotal.Margin = New System.Windows.Forms.Padding(0)
        Me.lMontoTotal.Name = "lMontoTotal"
        Me.lMontoTotal.Size = New System.Drawing.Size(93, 44)
        Me.lMontoTotal.TabIndex = 250
        Me.lMontoTotal.Text = "0.00"
        Me.lMontoTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.Location = New System.Drawing.Point(744, 482)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 44)
        Me.Label3.TabIndex = 249
        Me.Label3.Text = "₡"
        '
        'lTotal
        '
        Me.lTotal.AutoSize = True
        Me.lTotal.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lTotal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lTotal.Location = New System.Drawing.Point(682, 418)
        Me.lTotal.Name = "lTotal"
        Me.lTotal.Size = New System.Drawing.Size(114, 44)
        Me.lTotal.TabIndex = 248
        Me.lTotal.Text = "Total:"
        '
        'cbPacientesDelDia
        '
        Me.cbPacientesDelDia.FormattingEnabled = True
        Me.cbPacientesDelDia.Location = New System.Drawing.Point(23, 34)
        Me.cbPacientesDelDia.Name = "cbPacientesDelDia"
        Me.cbPacientesDelDia.Size = New System.Drawing.Size(369, 29)
        Me.cbPacientesDelDia.TabIndex = 247
        Me.cbPacientesDelDia.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label2.Location = New System.Drawing.Point(921, 305)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 19)
        Me.Label2.TabIndex = 246
        Me.Label2.Text = "IZQUIERDA"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label1.Location = New System.Drawing.Point(560, 305)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 19)
        Me.Label1.TabIndex = 245
        Me.Label1.Text = "DERECHA"
        '
        'btn38
        '
        Me.btn38.BackColor = System.Drawing.Color.Transparent
        Me.btn38.FlatAppearance.BorderSize = 0
        Me.btn38.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn38.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn38.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn38.Location = New System.Drawing.Point(1085, 246)
        Me.btn38.Name = "btn38"
        Me.btn38.Size = New System.Drawing.Size(30, 30)
        Me.btn38.TabIndex = 243
        Me.btn38.UseVisualStyleBackColor = False
        '
        'btn37
        '
        Me.btn37.BackColor = System.Drawing.Color.Transparent
        Me.btn37.FlatAppearance.BorderSize = 0
        Me.btn37.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn37.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn37.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn37.Location = New System.Drawing.Point(1045, 246)
        Me.btn37.Name = "btn37"
        Me.btn37.Size = New System.Drawing.Size(30, 30)
        Me.btn37.TabIndex = 242
        Me.btn37.UseVisualStyleBackColor = False
        '
        'btn36
        '
        Me.btn36.BackColor = System.Drawing.Color.Transparent
        Me.btn36.FlatAppearance.BorderSize = 0
        Me.btn36.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn36.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn36.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn36.Location = New System.Drawing.Point(1005, 246)
        Me.btn36.Name = "btn36"
        Me.btn36.Size = New System.Drawing.Size(30, 30)
        Me.btn36.TabIndex = 241
        Me.btn36.UseVisualStyleBackColor = False
        '
        'btn35
        '
        Me.btn35.BackColor = System.Drawing.Color.Transparent
        Me.btn35.FlatAppearance.BorderSize = 0
        Me.btn35.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn35.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn35.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn35.Location = New System.Drawing.Point(965, 246)
        Me.btn35.Name = "btn35"
        Me.btn35.Size = New System.Drawing.Size(30, 30)
        Me.btn35.TabIndex = 240
        Me.btn35.UseVisualStyleBackColor = False
        '
        'btn34
        '
        Me.btn34.BackColor = System.Drawing.Color.Transparent
        Me.btn34.FlatAppearance.BorderSize = 0
        Me.btn34.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn34.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn34.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn34.Location = New System.Drawing.Point(925, 246)
        Me.btn34.Name = "btn34"
        Me.btn34.Size = New System.Drawing.Size(30, 30)
        Me.btn34.TabIndex = 239
        Me.btn34.UseVisualStyleBackColor = False
        '
        'btn33
        '
        Me.btn33.BackColor = System.Drawing.Color.Transparent
        Me.btn33.FlatAppearance.BorderSize = 0
        Me.btn33.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn33.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn33.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn33.Location = New System.Drawing.Point(885, 246)
        Me.btn33.Name = "btn33"
        Me.btn33.Size = New System.Drawing.Size(30, 30)
        Me.btn33.TabIndex = 238
        Me.btn33.UseVisualStyleBackColor = False
        '
        'btn32
        '
        Me.btn32.BackColor = System.Drawing.Color.Transparent
        Me.btn32.FlatAppearance.BorderSize = 0
        Me.btn32.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn32.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn32.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn32.Location = New System.Drawing.Point(845, 246)
        Me.btn32.Name = "btn32"
        Me.btn32.Size = New System.Drawing.Size(30, 30)
        Me.btn32.TabIndex = 237
        Me.btn32.UseVisualStyleBackColor = False
        '
        'btn31
        '
        Me.btn31.BackColor = System.Drawing.Color.Transparent
        Me.btn31.FlatAppearance.BorderSize = 0
        Me.btn31.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn31.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn31.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn31.Location = New System.Drawing.Point(805, 246)
        Me.btn31.Name = "btn31"
        Me.btn31.Size = New System.Drawing.Size(30, 30)
        Me.btn31.TabIndex = 236
        Me.btn31.UseVisualStyleBackColor = False
        '
        'btn41
        '
        Me.btn41.BackColor = System.Drawing.Color.Transparent
        Me.btn41.FlatAppearance.BorderSize = 0
        Me.btn41.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn41.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn41.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn41.Location = New System.Drawing.Point(730, 246)
        Me.btn41.Name = "btn41"
        Me.btn41.Size = New System.Drawing.Size(30, 30)
        Me.btn41.TabIndex = 235
        Me.btn41.UseVisualStyleBackColor = False
        '
        'btn42
        '
        Me.btn42.BackColor = System.Drawing.Color.Transparent
        Me.btn42.FlatAppearance.BorderSize = 0
        Me.btn42.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn42.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn42.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn42.Location = New System.Drawing.Point(690, 246)
        Me.btn42.Name = "btn42"
        Me.btn42.Size = New System.Drawing.Size(30, 30)
        Me.btn42.TabIndex = 234
        Me.btn42.UseVisualStyleBackColor = False
        '
        'btn43
        '
        Me.btn43.BackColor = System.Drawing.Color.Transparent
        Me.btn43.FlatAppearance.BorderSize = 0
        Me.btn43.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn43.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn43.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn43.Location = New System.Drawing.Point(650, 246)
        Me.btn43.Name = "btn43"
        Me.btn43.Size = New System.Drawing.Size(30, 30)
        Me.btn43.TabIndex = 233
        Me.btn43.UseVisualStyleBackColor = False
        '
        'btn44
        '
        Me.btn44.BackColor = System.Drawing.Color.Transparent
        Me.btn44.FlatAppearance.BorderSize = 0
        Me.btn44.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn44.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn44.Location = New System.Drawing.Point(610, 246)
        Me.btn44.Name = "btn44"
        Me.btn44.Size = New System.Drawing.Size(30, 30)
        Me.btn44.TabIndex = 232
        Me.btn44.UseVisualStyleBackColor = False
        '
        'btn45
        '
        Me.btn45.BackColor = System.Drawing.Color.Transparent
        Me.btn45.FlatAppearance.BorderSize = 0
        Me.btn45.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn45.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn45.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn45.Location = New System.Drawing.Point(570, 246)
        Me.btn45.Name = "btn45"
        Me.btn45.Size = New System.Drawing.Size(30, 30)
        Me.btn45.TabIndex = 231
        Me.btn45.UseVisualStyleBackColor = False
        '
        'btn46
        '
        Me.btn46.BackColor = System.Drawing.Color.Transparent
        Me.btn46.FlatAppearance.BorderSize = 0
        Me.btn46.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn46.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn46.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn46.Location = New System.Drawing.Point(530, 246)
        Me.btn46.Name = "btn46"
        Me.btn46.Size = New System.Drawing.Size(30, 30)
        Me.btn46.TabIndex = 230
        Me.btn46.UseVisualStyleBackColor = False
        '
        'btn47
        '
        Me.btn47.BackColor = System.Drawing.Color.Transparent
        Me.btn47.FlatAppearance.BorderSize = 0
        Me.btn47.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn47.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn47.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn47.Location = New System.Drawing.Point(490, 246)
        Me.btn47.Name = "btn47"
        Me.btn47.Size = New System.Drawing.Size(30, 30)
        Me.btn47.TabIndex = 229
        Me.btn47.UseVisualStyleBackColor = False
        '
        'btn48
        '
        Me.btn48.BackColor = System.Drawing.Color.Transparent
        Me.btn48.FlatAppearance.BorderSize = 0
        Me.btn48.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn48.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn48.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn48.Location = New System.Drawing.Point(450, 246)
        Me.btn48.Name = "btn48"
        Me.btn48.Size = New System.Drawing.Size(30, 30)
        Me.btn48.TabIndex = 228
        Me.btn48.UseVisualStyleBackColor = False
        '
        'btn75
        '
        Me.btn75.BackColor = System.Drawing.Color.Transparent
        Me.btn75.FlatAppearance.BorderSize = 0
        Me.btn75.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn75.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn75.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn75.Location = New System.Drawing.Point(965, 176)
        Me.btn75.Name = "btn75"
        Me.btn75.Size = New System.Drawing.Size(30, 30)
        Me.btn75.TabIndex = 227
        Me.btn75.UseVisualStyleBackColor = False
        '
        'btn74
        '
        Me.btn74.BackColor = System.Drawing.Color.Transparent
        Me.btn74.FlatAppearance.BorderSize = 0
        Me.btn74.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn74.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn74.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn74.Location = New System.Drawing.Point(925, 176)
        Me.btn74.Name = "btn74"
        Me.btn74.Size = New System.Drawing.Size(30, 30)
        Me.btn74.TabIndex = 226
        Me.btn74.UseVisualStyleBackColor = False
        '
        'btn73
        '
        Me.btn73.BackColor = System.Drawing.Color.Transparent
        Me.btn73.FlatAppearance.BorderSize = 0
        Me.btn73.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn73.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn73.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn73.Location = New System.Drawing.Point(885, 176)
        Me.btn73.Name = "btn73"
        Me.btn73.Size = New System.Drawing.Size(30, 30)
        Me.btn73.TabIndex = 225
        Me.btn73.UseVisualStyleBackColor = False
        '
        'btn72
        '
        Me.btn72.BackColor = System.Drawing.Color.Transparent
        Me.btn72.FlatAppearance.BorderSize = 0
        Me.btn72.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn72.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn72.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn72.Location = New System.Drawing.Point(845, 176)
        Me.btn72.Name = "btn72"
        Me.btn72.Size = New System.Drawing.Size(30, 30)
        Me.btn72.TabIndex = 224
        Me.btn72.UseVisualStyleBackColor = False
        '
        'btn71
        '
        Me.btn71.BackColor = System.Drawing.Color.Transparent
        Me.btn71.FlatAppearance.BorderSize = 0
        Me.btn71.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn71.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn71.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn71.Location = New System.Drawing.Point(805, 176)
        Me.btn71.Name = "btn71"
        Me.btn71.Size = New System.Drawing.Size(30, 30)
        Me.btn71.TabIndex = 223
        Me.btn71.UseVisualStyleBackColor = False
        '
        'btn81
        '
        Me.btn81.BackColor = System.Drawing.Color.Transparent
        Me.btn81.FlatAppearance.BorderSize = 0
        Me.btn81.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn81.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn81.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn81.Location = New System.Drawing.Point(730, 176)
        Me.btn81.Name = "btn81"
        Me.btn81.Size = New System.Drawing.Size(30, 30)
        Me.btn81.TabIndex = 222
        Me.btn81.UseVisualStyleBackColor = False
        '
        'btn82
        '
        Me.btn82.BackColor = System.Drawing.Color.Transparent
        Me.btn82.FlatAppearance.BorderSize = 0
        Me.btn82.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn82.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn82.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn82.Location = New System.Drawing.Point(690, 176)
        Me.btn82.Name = "btn82"
        Me.btn82.Size = New System.Drawing.Size(30, 30)
        Me.btn82.TabIndex = 221
        Me.btn82.UseVisualStyleBackColor = False
        '
        'btn83
        '
        Me.btn83.BackColor = System.Drawing.Color.Transparent
        Me.btn83.FlatAppearance.BorderSize = 0
        Me.btn83.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn83.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn83.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn83.Location = New System.Drawing.Point(650, 176)
        Me.btn83.Name = "btn83"
        Me.btn83.Size = New System.Drawing.Size(30, 30)
        Me.btn83.TabIndex = 220
        Me.btn83.UseVisualStyleBackColor = False
        '
        'btn84
        '
        Me.btn84.BackColor = System.Drawing.Color.Transparent
        Me.btn84.FlatAppearance.BorderSize = 0
        Me.btn84.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn84.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn84.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn84.Location = New System.Drawing.Point(610, 176)
        Me.btn84.Name = "btn84"
        Me.btn84.Size = New System.Drawing.Size(30, 30)
        Me.btn84.TabIndex = 219
        Me.btn84.UseVisualStyleBackColor = False
        '
        'btn85
        '
        Me.btn85.BackColor = System.Drawing.Color.Transparent
        Me.btn85.FlatAppearance.BorderSize = 0
        Me.btn85.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn85.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn85.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn85.Location = New System.Drawing.Point(570, 176)
        Me.btn85.Name = "btn85"
        Me.btn85.Size = New System.Drawing.Size(30, 30)
        Me.btn85.TabIndex = 218
        Me.btn85.UseVisualStyleBackColor = False
        '
        'btn65
        '
        Me.btn65.BackColor = System.Drawing.Color.Transparent
        Me.btn65.FlatAppearance.BorderSize = 0
        Me.btn65.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn65.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn65.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn65.Location = New System.Drawing.Point(965, 106)
        Me.btn65.Name = "btn65"
        Me.btn65.Size = New System.Drawing.Size(30, 30)
        Me.btn65.TabIndex = 217
        Me.btn65.UseVisualStyleBackColor = False
        '
        'btn64
        '
        Me.btn64.BackColor = System.Drawing.Color.Transparent
        Me.btn64.FlatAppearance.BorderSize = 0
        Me.btn64.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn64.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn64.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn64.Location = New System.Drawing.Point(925, 106)
        Me.btn64.Name = "btn64"
        Me.btn64.Size = New System.Drawing.Size(30, 30)
        Me.btn64.TabIndex = 216
        Me.btn64.UseVisualStyleBackColor = False
        '
        'btn63
        '
        Me.btn63.BackColor = System.Drawing.Color.Transparent
        Me.btn63.FlatAppearance.BorderSize = 0
        Me.btn63.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn63.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn63.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn63.Location = New System.Drawing.Point(885, 106)
        Me.btn63.Name = "btn63"
        Me.btn63.Size = New System.Drawing.Size(30, 30)
        Me.btn63.TabIndex = 215
        Me.btn63.UseVisualStyleBackColor = False
        '
        'btn62
        '
        Me.btn62.BackColor = System.Drawing.Color.Transparent
        Me.btn62.FlatAppearance.BorderSize = 0
        Me.btn62.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn62.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn62.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn62.Location = New System.Drawing.Point(845, 106)
        Me.btn62.Name = "btn62"
        Me.btn62.Size = New System.Drawing.Size(30, 30)
        Me.btn62.TabIndex = 214
        Me.btn62.UseVisualStyleBackColor = False
        '
        'btn61
        '
        Me.btn61.BackColor = System.Drawing.Color.Transparent
        Me.btn61.FlatAppearance.BorderSize = 0
        Me.btn61.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn61.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn61.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn61.Location = New System.Drawing.Point(805, 106)
        Me.btn61.Name = "btn61"
        Me.btn61.Size = New System.Drawing.Size(30, 30)
        Me.btn61.TabIndex = 213
        Me.btn61.UseVisualStyleBackColor = False
        '
        'btn51
        '
        Me.btn51.BackColor = System.Drawing.Color.Transparent
        Me.btn51.FlatAppearance.BorderSize = 0
        Me.btn51.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn51.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn51.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn51.Location = New System.Drawing.Point(730, 106)
        Me.btn51.Name = "btn51"
        Me.btn51.Size = New System.Drawing.Size(30, 30)
        Me.btn51.TabIndex = 212
        Me.btn51.UseVisualStyleBackColor = False
        '
        'btn52
        '
        Me.btn52.BackColor = System.Drawing.Color.Transparent
        Me.btn52.FlatAppearance.BorderSize = 0
        Me.btn52.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn52.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn52.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn52.Location = New System.Drawing.Point(690, 106)
        Me.btn52.Name = "btn52"
        Me.btn52.Size = New System.Drawing.Size(30, 30)
        Me.btn52.TabIndex = 211
        Me.btn52.UseVisualStyleBackColor = False
        '
        'btn53
        '
        Me.btn53.BackColor = System.Drawing.Color.Transparent
        Me.btn53.FlatAppearance.BorderSize = 0
        Me.btn53.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn53.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn53.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn53.Location = New System.Drawing.Point(650, 106)
        Me.btn53.Name = "btn53"
        Me.btn53.Size = New System.Drawing.Size(30, 30)
        Me.btn53.TabIndex = 210
        Me.btn53.UseVisualStyleBackColor = False
        '
        'btn54
        '
        Me.btn54.BackColor = System.Drawing.Color.Transparent
        Me.btn54.FlatAppearance.BorderSize = 0
        Me.btn54.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn54.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn54.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn54.Location = New System.Drawing.Point(610, 106)
        Me.btn54.Name = "btn54"
        Me.btn54.Size = New System.Drawing.Size(30, 30)
        Me.btn54.TabIndex = 209
        Me.btn54.UseVisualStyleBackColor = False
        '
        'btn55
        '
        Me.btn55.BackColor = System.Drawing.Color.Transparent
        Me.btn55.FlatAppearance.BorderSize = 0
        Me.btn55.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn55.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn55.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn55.Location = New System.Drawing.Point(570, 106)
        Me.btn55.Name = "btn55"
        Me.btn55.Size = New System.Drawing.Size(30, 30)
        Me.btn55.TabIndex = 208
        Me.btn55.UseVisualStyleBackColor = False
        '
        'btn28
        '
        Me.btn28.BackColor = System.Drawing.Color.Transparent
        Me.btn28.FlatAppearance.BorderSize = 0
        Me.btn28.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn28.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn28.Location = New System.Drawing.Point(1085, 43)
        Me.btn28.Name = "btn28"
        Me.btn28.Size = New System.Drawing.Size(30, 30)
        Me.btn28.TabIndex = 207
        Me.btn28.UseVisualStyleBackColor = False
        '
        'btn27
        '
        Me.btn27.BackColor = System.Drawing.Color.Transparent
        Me.btn27.FlatAppearance.BorderSize = 0
        Me.btn27.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn27.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn27.Location = New System.Drawing.Point(1045, 43)
        Me.btn27.Name = "btn27"
        Me.btn27.Size = New System.Drawing.Size(30, 30)
        Me.btn27.TabIndex = 206
        Me.btn27.UseVisualStyleBackColor = False
        '
        'btn26
        '
        Me.btn26.BackColor = System.Drawing.Color.Transparent
        Me.btn26.FlatAppearance.BorderSize = 0
        Me.btn26.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn26.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn26.Location = New System.Drawing.Point(1005, 43)
        Me.btn26.Name = "btn26"
        Me.btn26.Size = New System.Drawing.Size(30, 30)
        Me.btn26.TabIndex = 205
        Me.btn26.UseVisualStyleBackColor = False
        '
        'btn25
        '
        Me.btn25.BackColor = System.Drawing.Color.Transparent
        Me.btn25.FlatAppearance.BorderSize = 0
        Me.btn25.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn25.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn25.Location = New System.Drawing.Point(965, 43)
        Me.btn25.Name = "btn25"
        Me.btn25.Size = New System.Drawing.Size(30, 30)
        Me.btn25.TabIndex = 204
        Me.btn25.UseVisualStyleBackColor = False
        '
        'btn24
        '
        Me.btn24.BackColor = System.Drawing.Color.Transparent
        Me.btn24.FlatAppearance.BorderSize = 0
        Me.btn24.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn24.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn24.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn24.Location = New System.Drawing.Point(925, 43)
        Me.btn24.Name = "btn24"
        Me.btn24.Size = New System.Drawing.Size(30, 30)
        Me.btn24.TabIndex = 203
        Me.btn24.UseVisualStyleBackColor = False
        '
        'btn23
        '
        Me.btn23.BackColor = System.Drawing.Color.Transparent
        Me.btn23.FlatAppearance.BorderSize = 0
        Me.btn23.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn23.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn23.Location = New System.Drawing.Point(885, 43)
        Me.btn23.Name = "btn23"
        Me.btn23.Size = New System.Drawing.Size(30, 30)
        Me.btn23.TabIndex = 202
        Me.btn23.UseVisualStyleBackColor = False
        '
        'btn22
        '
        Me.btn22.BackColor = System.Drawing.Color.Transparent
        Me.btn22.FlatAppearance.BorderSize = 0
        Me.btn22.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn22.Location = New System.Drawing.Point(845, 43)
        Me.btn22.Name = "btn22"
        Me.btn22.Size = New System.Drawing.Size(30, 30)
        Me.btn22.TabIndex = 201
        Me.btn22.UseVisualStyleBackColor = False
        '
        'btn21
        '
        Me.btn21.BackColor = System.Drawing.Color.Transparent
        Me.btn21.FlatAppearance.BorderSize = 0
        Me.btn21.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn21.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn21.Location = New System.Drawing.Point(805, 43)
        Me.btn21.Name = "btn21"
        Me.btn21.Size = New System.Drawing.Size(30, 30)
        Me.btn21.TabIndex = 200
        Me.btn21.UseVisualStyleBackColor = False
        '
        'btn11
        '
        Me.btn11.BackColor = System.Drawing.Color.Transparent
        Me.btn11.FlatAppearance.BorderSize = 0
        Me.btn11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn11.Location = New System.Drawing.Point(730, 43)
        Me.btn11.Name = "btn11"
        Me.btn11.Size = New System.Drawing.Size(30, 30)
        Me.btn11.TabIndex = 199
        Me.btn11.UseVisualStyleBackColor = False
        '
        'btn12
        '
        Me.btn12.BackColor = System.Drawing.Color.Transparent
        Me.btn12.FlatAppearance.BorderSize = 0
        Me.btn12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn12.Location = New System.Drawing.Point(690, 43)
        Me.btn12.Name = "btn12"
        Me.btn12.Size = New System.Drawing.Size(30, 30)
        Me.btn12.TabIndex = 198
        Me.btn12.UseVisualStyleBackColor = False
        '
        'btn13
        '
        Me.btn13.BackColor = System.Drawing.Color.Transparent
        Me.btn13.FlatAppearance.BorderSize = 0
        Me.btn13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn13.Location = New System.Drawing.Point(650, 43)
        Me.btn13.Name = "btn13"
        Me.btn13.Size = New System.Drawing.Size(30, 30)
        Me.btn13.TabIndex = 197
        Me.btn13.UseVisualStyleBackColor = False
        '
        'btn14
        '
        Me.btn14.BackColor = System.Drawing.Color.Transparent
        Me.btn14.FlatAppearance.BorderSize = 0
        Me.btn14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn14.Location = New System.Drawing.Point(610, 43)
        Me.btn14.Name = "btn14"
        Me.btn14.Size = New System.Drawing.Size(30, 30)
        Me.btn14.TabIndex = 196
        Me.btn14.UseVisualStyleBackColor = False
        '
        'btn15
        '
        Me.btn15.BackColor = System.Drawing.Color.Transparent
        Me.btn15.FlatAppearance.BorderSize = 0
        Me.btn15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn15.Location = New System.Drawing.Point(570, 43)
        Me.btn15.Name = "btn15"
        Me.btn15.Size = New System.Drawing.Size(30, 30)
        Me.btn15.TabIndex = 195
        Me.btn15.UseVisualStyleBackColor = False
        '
        'btn16
        '
        Me.btn16.BackColor = System.Drawing.Color.Transparent
        Me.btn16.FlatAppearance.BorderSize = 0
        Me.btn16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn16.Location = New System.Drawing.Point(530, 43)
        Me.btn16.Name = "btn16"
        Me.btn16.Size = New System.Drawing.Size(30, 30)
        Me.btn16.TabIndex = 194
        Me.btn16.UseVisualStyleBackColor = False
        '
        'btn17
        '
        Me.btn17.BackColor = System.Drawing.Color.Transparent
        Me.btn17.FlatAppearance.BorderSize = 0
        Me.btn17.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn17.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn17.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn17.Location = New System.Drawing.Point(490, 43)
        Me.btn17.Name = "btn17"
        Me.btn17.Size = New System.Drawing.Size(30, 30)
        Me.btn17.TabIndex = 193
        Me.btn17.UseVisualStyleBackColor = False
        '
        'l38
        '
        Me.l38.AutoSize = True
        Me.l38.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l38.ForeColor = System.Drawing.Color.White
        Me.l38.Location = New System.Drawing.Point(1088, 224)
        Me.l38.Name = "l38"
        Me.l38.Size = New System.Drawing.Size(24, 18)
        Me.l38.TabIndex = 192
        Me.l38.Text = "38"
        '
        'l37
        '
        Me.l37.AutoSize = True
        Me.l37.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l37.ForeColor = System.Drawing.Color.White
        Me.l37.Location = New System.Drawing.Point(1048, 224)
        Me.l37.Name = "l37"
        Me.l37.Size = New System.Drawing.Size(24, 18)
        Me.l37.TabIndex = 191
        Me.l37.Text = "37"
        '
        'l36
        '
        Me.l36.AutoSize = True
        Me.l36.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l36.ForeColor = System.Drawing.Color.White
        Me.l36.Location = New System.Drawing.Point(1008, 224)
        Me.l36.Name = "l36"
        Me.l36.Size = New System.Drawing.Size(24, 18)
        Me.l36.TabIndex = 190
        Me.l36.Text = "36"
        '
        'l35
        '
        Me.l35.AutoSize = True
        Me.l35.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l35.ForeColor = System.Drawing.Color.White
        Me.l35.Location = New System.Drawing.Point(968, 224)
        Me.l35.Name = "l35"
        Me.l35.Size = New System.Drawing.Size(24, 18)
        Me.l35.TabIndex = 189
        Me.l35.Text = "35"
        '
        'l34
        '
        Me.l34.AutoSize = True
        Me.l34.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l34.ForeColor = System.Drawing.Color.White
        Me.l34.Location = New System.Drawing.Point(928, 224)
        Me.l34.Name = "l34"
        Me.l34.Size = New System.Drawing.Size(24, 18)
        Me.l34.TabIndex = 188
        Me.l34.Text = "34"
        '
        'l33
        '
        Me.l33.AutoSize = True
        Me.l33.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l33.ForeColor = System.Drawing.Color.White
        Me.l33.Location = New System.Drawing.Point(888, 224)
        Me.l33.Name = "l33"
        Me.l33.Size = New System.Drawing.Size(24, 18)
        Me.l33.TabIndex = 187
        Me.l33.Text = "33"
        '
        'l32
        '
        Me.l32.AutoSize = True
        Me.l32.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l32.ForeColor = System.Drawing.Color.White
        Me.l32.Location = New System.Drawing.Point(848, 224)
        Me.l32.Name = "l32"
        Me.l32.Size = New System.Drawing.Size(24, 18)
        Me.l32.TabIndex = 186
        Me.l32.Text = "32"
        '
        'l31
        '
        Me.l31.AutoSize = True
        Me.l31.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l31.ForeColor = System.Drawing.Color.White
        Me.l31.Location = New System.Drawing.Point(808, 224)
        Me.l31.Name = "l31"
        Me.l31.Size = New System.Drawing.Size(24, 18)
        Me.l31.TabIndex = 185
        Me.l31.Text = "31"
        '
        'l41
        '
        Me.l41.AutoSize = True
        Me.l41.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l41.ForeColor = System.Drawing.Color.White
        Me.l41.Location = New System.Drawing.Point(733, 224)
        Me.l41.Name = "l41"
        Me.l41.Size = New System.Drawing.Size(24, 18)
        Me.l41.TabIndex = 184
        Me.l41.Text = "41"
        '
        'l42
        '
        Me.l42.AutoSize = True
        Me.l42.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l42.ForeColor = System.Drawing.Color.White
        Me.l42.Location = New System.Drawing.Point(693, 224)
        Me.l42.Name = "l42"
        Me.l42.Size = New System.Drawing.Size(24, 18)
        Me.l42.TabIndex = 183
        Me.l42.Text = "42"
        '
        'l43
        '
        Me.l43.AutoSize = True
        Me.l43.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l43.ForeColor = System.Drawing.Color.White
        Me.l43.Location = New System.Drawing.Point(653, 224)
        Me.l43.Name = "l43"
        Me.l43.Size = New System.Drawing.Size(24, 18)
        Me.l43.TabIndex = 182
        Me.l43.Text = "43"
        '
        'l44
        '
        Me.l44.AutoSize = True
        Me.l44.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l44.ForeColor = System.Drawing.Color.White
        Me.l44.Location = New System.Drawing.Point(613, 224)
        Me.l44.Name = "l44"
        Me.l44.Size = New System.Drawing.Size(24, 18)
        Me.l44.TabIndex = 181
        Me.l44.Text = "44"
        '
        'l45
        '
        Me.l45.AutoSize = True
        Me.l45.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l45.ForeColor = System.Drawing.Color.White
        Me.l45.Location = New System.Drawing.Point(573, 224)
        Me.l45.Name = "l45"
        Me.l45.Size = New System.Drawing.Size(24, 18)
        Me.l45.TabIndex = 180
        Me.l45.Text = "45"
        '
        'l46
        '
        Me.l46.AutoSize = True
        Me.l46.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l46.ForeColor = System.Drawing.Color.White
        Me.l46.Location = New System.Drawing.Point(533, 224)
        Me.l46.Name = "l46"
        Me.l46.Size = New System.Drawing.Size(24, 18)
        Me.l46.TabIndex = 179
        Me.l46.Text = "46"
        '
        'l47
        '
        Me.l47.AutoSize = True
        Me.l47.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l47.ForeColor = System.Drawing.Color.White
        Me.l47.Location = New System.Drawing.Point(493, 224)
        Me.l47.Name = "l47"
        Me.l47.Size = New System.Drawing.Size(24, 18)
        Me.l47.TabIndex = 178
        Me.l47.Text = "47"
        '
        'l48
        '
        Me.l48.AutoSize = True
        Me.l48.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l48.ForeColor = System.Drawing.Color.White
        Me.l48.Location = New System.Drawing.Point(453, 224)
        Me.l48.Name = "l48"
        Me.l48.Size = New System.Drawing.Size(24, 18)
        Me.l48.TabIndex = 177
        Me.l48.Text = "48"
        '
        'l75
        '
        Me.l75.AutoSize = True
        Me.l75.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l75.ForeColor = System.Drawing.Color.White
        Me.l75.Location = New System.Drawing.Point(968, 152)
        Me.l75.Name = "l75"
        Me.l75.Size = New System.Drawing.Size(24, 18)
        Me.l75.TabIndex = 176
        Me.l75.Text = "75"
        '
        'l74
        '
        Me.l74.AutoSize = True
        Me.l74.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l74.ForeColor = System.Drawing.Color.White
        Me.l74.Location = New System.Drawing.Point(928, 152)
        Me.l74.Name = "l74"
        Me.l74.Size = New System.Drawing.Size(24, 18)
        Me.l74.TabIndex = 175
        Me.l74.Text = "74"
        '
        'l73
        '
        Me.l73.AutoSize = True
        Me.l73.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l73.ForeColor = System.Drawing.Color.White
        Me.l73.Location = New System.Drawing.Point(888, 152)
        Me.l73.Name = "l73"
        Me.l73.Size = New System.Drawing.Size(24, 18)
        Me.l73.TabIndex = 174
        Me.l73.Text = "73"
        '
        'l72
        '
        Me.l72.AutoSize = True
        Me.l72.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l72.ForeColor = System.Drawing.Color.White
        Me.l72.Location = New System.Drawing.Point(848, 152)
        Me.l72.Name = "l72"
        Me.l72.Size = New System.Drawing.Size(24, 18)
        Me.l72.TabIndex = 173
        Me.l72.Text = "72"
        '
        'l71
        '
        Me.l71.AutoSize = True
        Me.l71.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l71.ForeColor = System.Drawing.Color.White
        Me.l71.Location = New System.Drawing.Point(808, 152)
        Me.l71.Name = "l71"
        Me.l71.Size = New System.Drawing.Size(24, 18)
        Me.l71.TabIndex = 172
        Me.l71.Text = "71"
        '
        'l81
        '
        Me.l81.AutoSize = True
        Me.l81.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l81.ForeColor = System.Drawing.Color.White
        Me.l81.Location = New System.Drawing.Point(733, 152)
        Me.l81.Name = "l81"
        Me.l81.Size = New System.Drawing.Size(24, 18)
        Me.l81.TabIndex = 171
        Me.l81.Text = "81"
        '
        'l82
        '
        Me.l82.AutoSize = True
        Me.l82.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l82.ForeColor = System.Drawing.Color.White
        Me.l82.Location = New System.Drawing.Point(693, 152)
        Me.l82.Name = "l82"
        Me.l82.Size = New System.Drawing.Size(24, 18)
        Me.l82.TabIndex = 170
        Me.l82.Text = "82"
        '
        'l83
        '
        Me.l83.AutoSize = True
        Me.l83.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l83.ForeColor = System.Drawing.Color.White
        Me.l83.Location = New System.Drawing.Point(653, 152)
        Me.l83.Name = "l83"
        Me.l83.Size = New System.Drawing.Size(24, 18)
        Me.l83.TabIndex = 169
        Me.l83.Text = "83"
        '
        'l84
        '
        Me.l84.AutoSize = True
        Me.l84.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l84.ForeColor = System.Drawing.Color.White
        Me.l84.Location = New System.Drawing.Point(613, 152)
        Me.l84.Name = "l84"
        Me.l84.Size = New System.Drawing.Size(24, 18)
        Me.l84.TabIndex = 168
        Me.l84.Text = "84"
        '
        'l85
        '
        Me.l85.AutoSize = True
        Me.l85.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l85.ForeColor = System.Drawing.Color.White
        Me.l85.Location = New System.Drawing.Point(573, 152)
        Me.l85.Name = "l85"
        Me.l85.Size = New System.Drawing.Size(24, 18)
        Me.l85.TabIndex = 167
        Me.l85.Text = "85"
        '
        'l65
        '
        Me.l65.AutoSize = True
        Me.l65.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l65.ForeColor = System.Drawing.Color.White
        Me.l65.Location = New System.Drawing.Point(968, 85)
        Me.l65.Name = "l65"
        Me.l65.Size = New System.Drawing.Size(24, 18)
        Me.l65.TabIndex = 166
        Me.l65.Text = "65"
        '
        'l64
        '
        Me.l64.AutoSize = True
        Me.l64.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l64.ForeColor = System.Drawing.Color.White
        Me.l64.Location = New System.Drawing.Point(928, 85)
        Me.l64.Name = "l64"
        Me.l64.Size = New System.Drawing.Size(24, 18)
        Me.l64.TabIndex = 165
        Me.l64.Text = "64"
        '
        'l63
        '
        Me.l63.AutoSize = True
        Me.l63.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l63.ForeColor = System.Drawing.Color.White
        Me.l63.Location = New System.Drawing.Point(888, 85)
        Me.l63.Name = "l63"
        Me.l63.Size = New System.Drawing.Size(24, 18)
        Me.l63.TabIndex = 164
        Me.l63.Text = "63"
        '
        'l62
        '
        Me.l62.AutoSize = True
        Me.l62.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l62.ForeColor = System.Drawing.Color.White
        Me.l62.Location = New System.Drawing.Point(848, 85)
        Me.l62.Name = "l62"
        Me.l62.Size = New System.Drawing.Size(24, 18)
        Me.l62.TabIndex = 163
        Me.l62.Text = "62"
        '
        'l61
        '
        Me.l61.AutoSize = True
        Me.l61.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l61.ForeColor = System.Drawing.Color.White
        Me.l61.Location = New System.Drawing.Point(808, 85)
        Me.l61.Name = "l61"
        Me.l61.Size = New System.Drawing.Size(24, 18)
        Me.l61.TabIndex = 162
        Me.l61.Text = "61"
        '
        'l51
        '
        Me.l51.AutoSize = True
        Me.l51.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l51.ForeColor = System.Drawing.Color.White
        Me.l51.Location = New System.Drawing.Point(733, 85)
        Me.l51.Name = "l51"
        Me.l51.Size = New System.Drawing.Size(24, 18)
        Me.l51.TabIndex = 161
        Me.l51.Text = "51"
        '
        'l52
        '
        Me.l52.AutoSize = True
        Me.l52.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l52.ForeColor = System.Drawing.Color.White
        Me.l52.Location = New System.Drawing.Point(693, 85)
        Me.l52.Name = "l52"
        Me.l52.Size = New System.Drawing.Size(24, 18)
        Me.l52.TabIndex = 160
        Me.l52.Text = "52"
        '
        'l53
        '
        Me.l53.AutoSize = True
        Me.l53.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l53.ForeColor = System.Drawing.Color.White
        Me.l53.Location = New System.Drawing.Point(653, 85)
        Me.l53.Name = "l53"
        Me.l53.Size = New System.Drawing.Size(24, 18)
        Me.l53.TabIndex = 159
        Me.l53.Text = "53"
        '
        'l54
        '
        Me.l54.AutoSize = True
        Me.l54.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l54.ForeColor = System.Drawing.Color.White
        Me.l54.Location = New System.Drawing.Point(613, 85)
        Me.l54.Name = "l54"
        Me.l54.Size = New System.Drawing.Size(24, 18)
        Me.l54.TabIndex = 158
        Me.l54.Text = "54"
        '
        'l55
        '
        Me.l55.AutoSize = True
        Me.l55.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l55.ForeColor = System.Drawing.Color.White
        Me.l55.Location = New System.Drawing.Point(573, 85)
        Me.l55.Name = "l55"
        Me.l55.Size = New System.Drawing.Size(24, 18)
        Me.l55.TabIndex = 157
        Me.l55.Text = "55"
        '
        'l28
        '
        Me.l28.AutoSize = True
        Me.l28.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l28.ForeColor = System.Drawing.Color.White
        Me.l28.Location = New System.Drawing.Point(1088, 22)
        Me.l28.Name = "l28"
        Me.l28.Size = New System.Drawing.Size(24, 18)
        Me.l28.TabIndex = 156
        Me.l28.Text = "28"
        '
        'l27
        '
        Me.l27.AutoSize = True
        Me.l27.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l27.ForeColor = System.Drawing.Color.White
        Me.l27.Location = New System.Drawing.Point(1048, 22)
        Me.l27.Name = "l27"
        Me.l27.Size = New System.Drawing.Size(24, 18)
        Me.l27.TabIndex = 155
        Me.l27.Text = "27"
        '
        'l26
        '
        Me.l26.AutoSize = True
        Me.l26.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l26.ForeColor = System.Drawing.Color.White
        Me.l26.Location = New System.Drawing.Point(1008, 22)
        Me.l26.Name = "l26"
        Me.l26.Size = New System.Drawing.Size(24, 18)
        Me.l26.TabIndex = 154
        Me.l26.Text = "26"
        '
        'l25
        '
        Me.l25.AutoSize = True
        Me.l25.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l25.ForeColor = System.Drawing.Color.White
        Me.l25.Location = New System.Drawing.Point(968, 22)
        Me.l25.Name = "l25"
        Me.l25.Size = New System.Drawing.Size(24, 18)
        Me.l25.TabIndex = 153
        Me.l25.Text = "25"
        '
        'l24
        '
        Me.l24.AutoSize = True
        Me.l24.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l24.ForeColor = System.Drawing.Color.White
        Me.l24.Location = New System.Drawing.Point(928, 22)
        Me.l24.Name = "l24"
        Me.l24.Size = New System.Drawing.Size(24, 18)
        Me.l24.TabIndex = 152
        Me.l24.Text = "24"
        '
        'l23
        '
        Me.l23.AutoSize = True
        Me.l23.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l23.ForeColor = System.Drawing.Color.White
        Me.l23.Location = New System.Drawing.Point(888, 22)
        Me.l23.Name = "l23"
        Me.l23.Size = New System.Drawing.Size(24, 18)
        Me.l23.TabIndex = 151
        Me.l23.Text = "23"
        '
        'l22
        '
        Me.l22.AutoSize = True
        Me.l22.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l22.ForeColor = System.Drawing.Color.White
        Me.l22.Location = New System.Drawing.Point(848, 22)
        Me.l22.Name = "l22"
        Me.l22.Size = New System.Drawing.Size(24, 18)
        Me.l22.TabIndex = 150
        Me.l22.Text = "22"
        '
        'l21
        '
        Me.l21.AutoSize = True
        Me.l21.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l21.ForeColor = System.Drawing.Color.White
        Me.l21.Location = New System.Drawing.Point(808, 22)
        Me.l21.Name = "l21"
        Me.l21.Size = New System.Drawing.Size(24, 18)
        Me.l21.TabIndex = 149
        Me.l21.Text = "21"
        '
        'l11
        '
        Me.l11.AutoSize = True
        Me.l11.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l11.ForeColor = System.Drawing.Color.White
        Me.l11.Location = New System.Drawing.Point(733, 22)
        Me.l11.Name = "l11"
        Me.l11.Size = New System.Drawing.Size(24, 18)
        Me.l11.TabIndex = 148
        Me.l11.Text = "11"
        '
        'l12
        '
        Me.l12.AutoSize = True
        Me.l12.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l12.ForeColor = System.Drawing.Color.White
        Me.l12.Location = New System.Drawing.Point(693, 22)
        Me.l12.Name = "l12"
        Me.l12.Size = New System.Drawing.Size(24, 18)
        Me.l12.TabIndex = 146
        Me.l12.Text = "12"
        '
        'l13
        '
        Me.l13.AutoSize = True
        Me.l13.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l13.ForeColor = System.Drawing.Color.White
        Me.l13.Location = New System.Drawing.Point(653, 22)
        Me.l13.Name = "l13"
        Me.l13.Size = New System.Drawing.Size(24, 18)
        Me.l13.TabIndex = 145
        Me.l13.Text = "13"
        '
        'l14
        '
        Me.l14.AutoSize = True
        Me.l14.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l14.ForeColor = System.Drawing.Color.White
        Me.l14.Location = New System.Drawing.Point(613, 22)
        Me.l14.Name = "l14"
        Me.l14.Size = New System.Drawing.Size(24, 18)
        Me.l14.TabIndex = 144
        Me.l14.Text = "14"
        '
        'l15
        '
        Me.l15.AutoSize = True
        Me.l15.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l15.ForeColor = System.Drawing.Color.White
        Me.l15.Location = New System.Drawing.Point(573, 22)
        Me.l15.Name = "l15"
        Me.l15.Size = New System.Drawing.Size(24, 18)
        Me.l15.TabIndex = 143
        Me.l15.Text = "15"
        '
        'l16
        '
        Me.l16.AutoSize = True
        Me.l16.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l16.ForeColor = System.Drawing.Color.White
        Me.l16.Location = New System.Drawing.Point(533, 22)
        Me.l16.Name = "l16"
        Me.l16.Size = New System.Drawing.Size(24, 18)
        Me.l16.TabIndex = 142
        Me.l16.Text = "16"
        '
        'l17
        '
        Me.l17.AutoSize = True
        Me.l17.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l17.ForeColor = System.Drawing.Color.White
        Me.l17.Location = New System.Drawing.Point(493, 22)
        Me.l17.Name = "l17"
        Me.l17.Size = New System.Drawing.Size(24, 18)
        Me.l17.TabIndex = 141
        Me.l17.Text = "17"
        '
        'l18
        '
        Me.l18.AutoSize = True
        Me.l18.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l18.ForeColor = System.Drawing.Color.White
        Me.l18.Location = New System.Drawing.Point(453, 22)
        Me.l18.Name = "l18"
        Me.l18.Size = New System.Drawing.Size(24, 18)
        Me.l18.TabIndex = 140
        Me.l18.Text = "18"
        '
        'btn18
        '
        Me.btn18.BackColor = System.Drawing.Color.Transparent
        Me.btn18.FlatAppearance.BorderSize = 0
        Me.btn18.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn18.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn18.Location = New System.Drawing.Point(450, 43)
        Me.btn18.Name = "btn18"
        Me.btn18.Size = New System.Drawing.Size(30, 30)
        Me.btn18.TabIndex = 0
        Me.btn18.UseVisualStyleBackColor = False
        '
        'dgvTratamientosConsultaOdontologia
        '
        Me.dgvTratamientosConsultaOdontologia.AllowUserToAddRows = False
        Me.dgvTratamientosConsultaOdontologia.AllowUserToDeleteRows = False
        Me.dgvTratamientosConsultaOdontologia.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTratamientosConsultaOdontologia.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.dgvTratamientosConsultaOdontologia.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTratamientosConsultaOdontologia.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTratamientosConsultaOdontologia.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvTratamientosConsultaOdontologia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTratamientosConsultaOdontologia.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvTratamientosConsultaOdontologia.GridColor = System.Drawing.SystemColors.Control
        Me.dgvTratamientosConsultaOdontologia.Location = New System.Drawing.Point(25, 381)
        Me.dgvTratamientosConsultaOdontologia.Name = "dgvTratamientosConsultaOdontologia"
        Me.dgvTratamientosConsultaOdontologia.ReadOnly = True
        Me.dgvTratamientosConsultaOdontologia.RowHeadersVisible = False
        Me.dgvTratamientosConsultaOdontologia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTratamientosConsultaOdontologia.Size = New System.Drawing.Size(612, 180)
        Me.dgvTratamientosConsultaOdontologia.TabIndex = 128
        '
        'txtFuncionario
        '
        Me.txtFuncionario.Enabled = False
        Me.txtFuncionario.Location = New System.Drawing.Point(25, 164)
        Me.txtFuncionario.Name = "txtFuncionario"
        Me.txtFuncionario.Size = New System.Drawing.Size(174, 27)
        Me.txtFuncionario.TabIndex = 123
        '
        'btnListo
        '
        Me.btnListo.BackColor = System.Drawing.Color.Transparent
        Me.btnListo.BackgroundImage = Global.ProyectoFinalIngenieria.My.Resources.Resources.Atendido
        Me.btnListo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnListo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnListo.FlatAppearance.BorderSize = 0
        Me.btnListo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnListo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnListo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnListo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.btnListo.ForeColor = System.Drawing.Color.White
        Me.btnListo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnListo.Location = New System.Drawing.Point(1011, 434)
        Me.btnListo.Name = "btnListo"
        Me.btnListo.Size = New System.Drawing.Size(133, 127)
        Me.btnListo.TabIndex = 122
        Me.btnListo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnListo.UseVisualStyleBackColor = False
        '
        'txtHora
        '
        Me.txtHora.Enabled = False
        Me.txtHora.Location = New System.Drawing.Point(220, 164)
        Me.txtHora.Name = "txtHora"
        Me.txtHora.Size = New System.Drawing.Size(172, 27)
        Me.txtHora.TabIndex = 120
        '
        'txtEdad
        '
        Me.txtEdad.Enabled = False
        Me.txtEdad.Location = New System.Drawing.Point(220, 96)
        Me.txtEdad.Name = "txtEdad"
        Me.txtEdad.Size = New System.Drawing.Size(174, 27)
        Me.txtEdad.TabIndex = 118
        '
        'txtCedula
        '
        Me.txtCedula.Enabled = False
        Me.txtCedula.Location = New System.Drawing.Point(25, 96)
        Me.txtCedula.Name = "txtCedula"
        Me.txtCedula.Size = New System.Drawing.Size(174, 27)
        Me.txtCedula.TabIndex = 117
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label9.Location = New System.Drawing.Point(26, 211)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(104, 21)
        Me.Label9.TabIndex = 115
        Me.Label9.Text = "Descripción:"
        '
        'rtbDescripcion
        '
        Me.rtbDescripcion.Enabled = False
        Me.rtbDescripcion.Location = New System.Drawing.Point(25, 235)
        Me.rtbDescripcion.Name = "rtbDescripcion"
        Me.rtbDescripcion.Size = New System.Drawing.Size(373, 80)
        Me.rtbDescripcion.TabIndex = 114
        Me.rtbDescripcion.Text = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label7.Location = New System.Drawing.Point(225, 72)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 21)
        Me.Label7.TabIndex = 113
        Me.Label7.Text = "Edad:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label8.Location = New System.Drawing.Point(26, 140)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(104, 21)
        Me.Label8.TabIndex = 112
        Me.Label8.Text = "Funcionario:"
        '
        'txtNombre
        '
        Me.txtNombre.Enabled = False
        Me.txtNombre.Location = New System.Drawing.Point(25, 36)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(367, 27)
        Me.txtNombre.TabIndex = 111
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label4.Location = New System.Drawing.Point(225, 140)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 21)
        Me.Label4.TabIndex = 110
        Me.Label4.Text = "Hora:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label5.Location = New System.Drawing.Point(21, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 21)
        Me.Label5.TabIndex = 109
        Me.Label5.Text = "Cédula:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label6.Location = New System.Drawing.Point(26, 12)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 21)
        Me.Label6.TabIndex = 108
        Me.Label6.Text = "Nombre:"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape2, Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(1183, 592)
        Me.ShapeContainer1.TabIndex = 244
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape2
        '
        Me.LineShape2.BorderColor = System.Drawing.Color.White
        Me.LineShape2.BorderWidth = 3
        Me.LineShape2.Name = "LineShape2"
        Me.LineShape2.SelectionColor = System.Drawing.Color.White
        Me.LineShape2.X1 = 1129
        Me.LineShape2.X2 = 432
        Me.LineShape2.Y1 = 296
        Me.LineShape2.Y2 = 296
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.Color.White
        Me.LineShape1.BorderWidth = 3
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.SelectionColor = System.Drawing.Color.White
        Me.LineShape1.X1 = 781
        Me.LineShape1.X2 = 782
        Me.LineShape1.Y1 = 16
        Me.LineShape1.Y2 = 333
        '
        'BarraTitulo
        '
        Me.BarraTitulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.BarraTitulo.Controls.Add(Me.btnMinimizar)
        Me.BarraTitulo.Controls.Add(Me.btnCerrar)
        Me.BarraTitulo.Location = New System.Drawing.Point(0, 0)
        Me.BarraTitulo.Name = "BarraTitulo"
        Me.BarraTitulo.Size = New System.Drawing.Size(1183, 43)
        Me.BarraTitulo.TabIndex = 11
        '
        'btnMinimizar
        '
        Me.btnMinimizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMinimizar.FlatAppearance.BorderSize = 0
        Me.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMinimizar.Image = CType(resources.GetObject("btnMinimizar.Image"), System.Drawing.Image)
        Me.btnMinimizar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnMinimizar.Location = New System.Drawing.Point(1083, 0)
        Me.btnMinimizar.Name = "btnMinimizar"
        Me.btnMinimizar.Size = New System.Drawing.Size(43, 43)
        Me.btnMinimizar.TabIndex = 4
        Me.btnMinimizar.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCerrar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCerrar.FlatAppearance.BorderSize = 0
        Me.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCerrar.Image = CType(resources.GetObject("btnCerrar.Image"), System.Drawing.Image)
        Me.btnCerrar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnCerrar.Location = New System.Drawing.Point(1132, 1)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(39, 39)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel7.Controls.Add(Me.Label21)
        Me.Panel7.Controls.Add(Me.Label22)
        Me.Panel7.Location = New System.Drawing.Point(0, 43)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(1183, 66)
        Me.Panel7.TabIndex = 10
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(536, 15)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(185, 33)
        Me.Label21.TabIndex = 15
        Me.Label21.Text = "Odontología"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.Label22.Location = New System.Drawing.Point(391, 15)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(154, 33)
        Me.Label22.TabIndex = 14
        Me.Label22.Text = "Atención /"
        '
        'FrmAtencionOdontologica
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1183, 700)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.BarraTitulo)
        Me.Controls.Add(Me.Panel7)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmAtencionOdontologica"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmAtencionOdontologica"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvTratamientosConsultaOdontologia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BarraTitulo.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents txtFuncionario As TextBox
    Private WithEvents btnListo As Button
    Friend WithEvents txtHora As TextBox
    Friend WithEvents txtEdad As TextBox
    Friend WithEvents txtCedula As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents rtbDescripcion As RichTextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtNombre As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents BarraTitulo As Panel
    Private WithEvents btnMinimizar As Button
    Private WithEvents btnCerrar As Button
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents dgvTratamientosConsultaOdontologia As DataGridView
    Friend WithEvents btn18 As Button
    Friend WithEvents l18 As Label
    Friend WithEvents l17 As Label
    Friend WithEvents l12 As Label
    Friend WithEvents l13 As Label
    Friend WithEvents l14 As Label
    Friend WithEvents l15 As Label
    Friend WithEvents l16 As Label
    Friend WithEvents l11 As Label
    Friend WithEvents l21 As Label
    Friend WithEvents l28 As Label
    Friend WithEvents l27 As Label
    Friend WithEvents l26 As Label
    Friend WithEvents l25 As Label
    Friend WithEvents l24 As Label
    Friend WithEvents l23 As Label
    Friend WithEvents l22 As Label
    Friend WithEvents l65 As Label
    Friend WithEvents l64 As Label
    Friend WithEvents l63 As Label
    Friend WithEvents l62 As Label
    Friend WithEvents l61 As Label
    Friend WithEvents l51 As Label
    Friend WithEvents l52 As Label
    Friend WithEvents l53 As Label
    Friend WithEvents l54 As Label
    Friend WithEvents l55 As Label
    Friend WithEvents l75 As Label
    Friend WithEvents l74 As Label
    Friend WithEvents l73 As Label
    Friend WithEvents l72 As Label
    Friend WithEvents l71 As Label
    Friend WithEvents l81 As Label
    Friend WithEvents l82 As Label
    Friend WithEvents l83 As Label
    Friend WithEvents l84 As Label
    Friend WithEvents l85 As Label
    Friend WithEvents l38 As Label
    Friend WithEvents l37 As Label
    Friend WithEvents l36 As Label
    Friend WithEvents l35 As Label
    Friend WithEvents l34 As Label
    Friend WithEvents l33 As Label
    Friend WithEvents l32 As Label
    Friend WithEvents l31 As Label
    Friend WithEvents l41 As Label
    Friend WithEvents l42 As Label
    Friend WithEvents l43 As Label
    Friend WithEvents l44 As Label
    Friend WithEvents l45 As Label
    Friend WithEvents l46 As Label
    Friend WithEvents l47 As Label
    Friend WithEvents l48 As Label
    Friend WithEvents btn28 As Button
    Friend WithEvents btn27 As Button
    Friend WithEvents btn26 As Button
    Friend WithEvents btn25 As Button
    Friend WithEvents btn24 As Button
    Friend WithEvents btn23 As Button
    Friend WithEvents btn22 As Button
    Friend WithEvents btn21 As Button
    Friend WithEvents btn11 As Button
    Friend WithEvents btn12 As Button
    Friend WithEvents btn13 As Button
    Friend WithEvents btn14 As Button
    Friend WithEvents btn15 As Button
    Friend WithEvents btn16 As Button
    Friend WithEvents btn17 As Button
    Friend WithEvents btn65 As Button
    Friend WithEvents btn64 As Button
    Friend WithEvents btn63 As Button
    Friend WithEvents btn62 As Button
    Friend WithEvents btn61 As Button
    Friend WithEvents btn51 As Button
    Friend WithEvents btn52 As Button
    Friend WithEvents btn53 As Button
    Friend WithEvents btn54 As Button
    Friend WithEvents btn55 As Button
    Friend WithEvents btn38 As Button
    Friend WithEvents btn37 As Button
    Friend WithEvents btn36 As Button
    Friend WithEvents btn35 As Button
    Friend WithEvents btn34 As Button
    Friend WithEvents btn33 As Button
    Friend WithEvents btn32 As Button
    Friend WithEvents btn31 As Button
    Friend WithEvents btn41 As Button
    Friend WithEvents btn42 As Button
    Friend WithEvents btn43 As Button
    Friend WithEvents btn44 As Button
    Friend WithEvents btn45 As Button
    Friend WithEvents btn46 As Button
    Friend WithEvents btn47 As Button
    Friend WithEvents btn48 As Button
    Friend WithEvents btn75 As Button
    Friend WithEvents btn74 As Button
    Friend WithEvents btn73 As Button
    Friend WithEvents btn72 As Button
    Friend WithEvents btn71 As Button
    Friend WithEvents btn81 As Button
    Friend WithEvents btn82 As Button
    Friend WithEvents btn83 As Button
    Friend WithEvents btn84 As Button
    Friend WithEvents btn85 As Button
    Friend WithEvents ShapeContainer1 As PowerPacks.ShapeContainer
    Friend WithEvents LineShape2 As PowerPacks.LineShape
    Friend WithEvents LineShape1 As PowerPacks.LineShape
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cbPacientesDelDia As ComboBox
    Friend WithEvents lMontoTotal As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lTotal As Label
End Class

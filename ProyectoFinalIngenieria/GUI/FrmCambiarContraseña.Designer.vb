﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCambiarContraseña
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCambiarContraseña))
        Me.lMensajeError = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtContraseñaActual = New System.Windows.Forms.TextBox()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape3 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.btnMinimizar = New System.Windows.Forms.PictureBox()
        Me.btnCerrar = New System.Windows.Forms.PictureBox()
        Me.txtNuevaContraseña = New System.Windows.Forms.TextBox()
        Me.txtConfirmarContraseña = New System.Windows.Forms.TextBox()
        Me.btnVisualizarOcultarNuevaContraseña = New System.Windows.Forms.PictureBox()
        Me.btnVisualizarOcultarConfirmarContraseña = New System.Windows.Forms.PictureBox()
        CType(Me.btnMinimizar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCerrar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnVisualizarOcultarNuevaContraseña, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnVisualizarOcultarConfirmarContraseña, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lMensajeError
        '
        Me.lMensajeError.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.5!)
        Me.lMensajeError.ForeColor = System.Drawing.Color.DarkGray
        Me.lMensajeError.Image = CType(resources.GetObject("lMensajeError.Image"), System.Drawing.Image)
        Me.lMensajeError.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.lMensajeError.Location = New System.Drawing.Point(65, 262)
        Me.lMensajeError.Name = "lMensajeError"
        Me.lMensajeError.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lMensajeError.Size = New System.Drawing.Size(407, 39)
        Me.lMensajeError.TabIndex = 14
        Me.lMensajeError.Text = "Error"
        Me.lMensajeError.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lMensajeError.Visible = False
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnAceptar.FlatAppearance.BorderSize = 0
        Me.btnAceptar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(28, Byte), Integer), CType(CType(28, Byte), Integer))
        Me.btnAceptar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.ForeColor = System.Drawing.Color.LightGray
        Me.btnAceptar.Location = New System.Drawing.Point(65, 305)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(408, 40)
        Me.btnAceptar.TabIndex = 12
        Me.btnAceptar.Text = "ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 20.0!)
        Me.Label1.ForeColor = System.Drawing.Color.DarkGray
        Me.Label1.Location = New System.Drawing.Point(107, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(322, 33)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "CAMBIAR CONTRASEÑA"
        '
        'txtContraseñaActual
        '
        Me.txtContraseñaActual.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer))
        Me.txtContraseñaActual.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtContraseñaActual.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.txtContraseñaActual.ForeColor = System.Drawing.Color.DarkGray
        Me.txtContraseñaActual.Location = New System.Drawing.Point(65, 94)
        Me.txtContraseñaActual.Name = "txtContraseñaActual"
        Me.txtContraseñaActual.Size = New System.Drawing.Size(408, 20)
        Me.txtContraseñaActual.TabIndex = 11
        Me.txtContraseñaActual.Text = "Código Verificación"
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.Color.DarkGray
        Me.LineShape1.Enabled = False
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 65
        Me.LineShape1.X2 = 472
        Me.LineShape1.Y1 = 117
        Me.LineShape1.Y2 = 117
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape3, Me.LineShape2, Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(530, 364)
        Me.ShapeContainer1.TabIndex = 15
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape3
        '
        Me.LineShape3.BorderColor = System.Drawing.Color.DarkGray
        Me.LineShape3.Enabled = False
        Me.LineShape3.Name = "LineShape3"
        Me.LineShape3.X1 = 62
        Me.LineShape3.X2 = 469
        Me.LineShape3.Y1 = 239
        Me.LineShape3.Y2 = 239
        '
        'LineShape2
        '
        Me.LineShape2.BorderColor = System.Drawing.Color.DarkGray
        Me.LineShape2.Enabled = False
        Me.LineShape2.Name = "LineShape2"
        Me.LineShape2.X1 = 62
        Me.LineShape2.X2 = 469
        Me.LineShape2.Y1 = 180
        Me.LineShape2.Y2 = 180
        '
        'btnMinimizar
        '
        Me.btnMinimizar.BackgroundImage = Global.ProyectoFinalIngenieria.My.Resources.Resources.Minimize
        Me.btnMinimizar.Image = Global.ProyectoFinalIngenieria.My.Resources.Resources.Minimize1
        Me.btnMinimizar.Location = New System.Drawing.Point(491, 3)
        Me.btnMinimizar.Name = "btnMinimizar"
        Me.btnMinimizar.Size = New System.Drawing.Size(15, 15)
        Me.btnMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnMinimizar.TabIndex = 17
        Me.btnMinimizar.TabStop = False
        '
        'btnCerrar
        '
        Me.btnCerrar.Image = Global.ProyectoFinalIngenieria.My.Resources.Resources.Close
        Me.btnCerrar.Location = New System.Drawing.Point(512, 3)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(15, 15)
        Me.btnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCerrar.TabIndex = 16
        Me.btnCerrar.TabStop = False
        '
        'txtNuevaContraseña
        '
        Me.txtNuevaContraseña.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer))
        Me.txtNuevaContraseña.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNuevaContraseña.Enabled = False
        Me.txtNuevaContraseña.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.txtNuevaContraseña.ForeColor = System.Drawing.Color.DarkGray
        Me.txtNuevaContraseña.Location = New System.Drawing.Point(64, 156)
        Me.txtNuevaContraseña.Name = "txtNuevaContraseña"
        Me.txtNuevaContraseña.Size = New System.Drawing.Size(408, 20)
        Me.txtNuevaContraseña.TabIndex = 18
        Me.txtNuevaContraseña.Text = "Nueva Contraseña"
        '
        'txtConfirmarContraseña
        '
        Me.txtConfirmarContraseña.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer))
        Me.txtConfirmarContraseña.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtConfirmarContraseña.Enabled = False
        Me.txtConfirmarContraseña.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.txtConfirmarContraseña.ForeColor = System.Drawing.Color.DarkGray
        Me.txtConfirmarContraseña.Location = New System.Drawing.Point(64, 216)
        Me.txtConfirmarContraseña.Name = "txtConfirmarContraseña"
        Me.txtConfirmarContraseña.Size = New System.Drawing.Size(408, 20)
        Me.txtConfirmarContraseña.TabIndex = 19
        Me.txtConfirmarContraseña.Text = "Confirmar Contraseña"
        '
        'btnVisualizarOcultarNuevaContraseña
        '
        Me.btnVisualizarOcultarNuevaContraseña.Image = CType(resources.GetObject("btnVisualizarOcultarNuevaContraseña.Image"), System.Drawing.Image)
        Me.btnVisualizarOcultarNuevaContraseña.Location = New System.Drawing.Point(446, 155)
        Me.btnVisualizarOcultarNuevaContraseña.Name = "btnVisualizarOcultarNuevaContraseña"
        Me.btnVisualizarOcultarNuevaContraseña.Size = New System.Drawing.Size(24, 24)
        Me.btnVisualizarOcultarNuevaContraseña.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnVisualizarOcultarNuevaContraseña.TabIndex = 20
        Me.btnVisualizarOcultarNuevaContraseña.TabStop = False
        '
        'btnVisualizarOcultarConfirmarContraseña
        '
        Me.btnVisualizarOcultarConfirmarContraseña.Image = CType(resources.GetObject("btnVisualizarOcultarConfirmarContraseña.Image"), System.Drawing.Image)
        Me.btnVisualizarOcultarConfirmarContraseña.Location = New System.Drawing.Point(448, 214)
        Me.btnVisualizarOcultarConfirmarContraseña.Name = "btnVisualizarOcultarConfirmarContraseña"
        Me.btnVisualizarOcultarConfirmarContraseña.Size = New System.Drawing.Size(24, 24)
        Me.btnVisualizarOcultarConfirmarContraseña.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnVisualizarOcultarConfirmarContraseña.TabIndex = 21
        Me.btnVisualizarOcultarConfirmarContraseña.TabStop = False
        '
        'FrmCambiarContraseña
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(530, 364)
        Me.Controls.Add(Me.btnVisualizarOcultarConfirmarContraseña)
        Me.Controls.Add(Me.btnVisualizarOcultarNuevaContraseña)
        Me.Controls.Add(Me.txtConfirmarContraseña)
        Me.Controls.Add(Me.txtNuevaContraseña)
        Me.Controls.Add(Me.btnMinimizar)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.lMensajeError)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtContraseñaActual)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmCambiarContraseña"
        Me.Opacity = 0.9R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmCambiarContraseña"
        CType(Me.btnMinimizar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCerrar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnVisualizarOcultarNuevaContraseña, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnVisualizarOcultarConfirmarContraseña, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lMensajeError As Label
    Friend WithEvents btnAceptar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtContraseñaActual As TextBox
    Friend WithEvents LineShape1 As PowerPacks.LineShape
    Friend WithEvents ShapeContainer1 As PowerPacks.ShapeContainer
    Friend WithEvents btnMinimizar As PictureBox
    Friend WithEvents btnCerrar As PictureBox
    Friend WithEvents LineShape3 As PowerPacks.LineShape
    Friend WithEvents LineShape2 As PowerPacks.LineShape
    Friend WithEvents txtNuevaContraseña As TextBox
    Friend WithEvents txtConfirmarContraseña As TextBox
    Friend WithEvents btnVisualizarOcultarNuevaContraseña As PictureBox
    Friend WithEvents btnVisualizarOcultarConfirmarContraseña As PictureBox
End Class

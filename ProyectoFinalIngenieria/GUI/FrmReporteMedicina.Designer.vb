﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmReporteMedicina
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmReporteMedicina))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lMensaje = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.cbMes = New System.Windows.Forms.ComboBox()
        Me.cbAño = New System.Windows.Forms.ComboBox()
        Me.lPaciente = New System.Windows.Forms.Label()
        Me.cbFuncionarios = New System.Windows.Forms.ComboBox()
        Me.cbPacientes = New System.Windows.Forms.ComboBox()
        Me.lFuncionario = New System.Windows.Forms.Label()
        Me.lMes = New System.Windows.Forms.Label()
        Me.dtpFechaInicio = New System.Windows.Forms.DateTimePicker()
        Me.lAño = New System.Windows.Forms.Label()
        Me.lFechaFinal = New System.Windows.Forms.Label()
        Me.dtpFechaFinal = New System.Windows.Forms.DateTimePicker()
        Me.lFechaInicio = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxBusqueda = New System.Windows.Forms.ComboBox()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.lFecha = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbConsulta = New System.Windows.Forms.ComboBox()
        Me.btnReporte = New System.Windows.Forms.Button()
        Me.dgvCitasMedicinaGeneral = New System.Windows.Forms.DataGridView()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvCitasMedicinaGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel2.Controls.Add(Me.PictureBox3)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(1, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(914, 71)
        Me.Panel2.TabIndex = 28
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(901, 3)
        Me.PictureBox3.TabIndex = 37
        Me.PictureBox3.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(387, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(328, 33)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Citas Medicina General"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(246, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(150, 33)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Reportes /"
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel1.Controls.Add(Me.lMensaje)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.btnReporte)
        Me.Panel1.Controls.Add(Me.dgvCitasMedicinaGeneral)
        Me.Panel1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(1, 53)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(914, 576)
        Me.Panel1.TabIndex = 29
        '
        'lMensaje
        '
        Me.lMensaje.AutoSize = True
        Me.lMensaje.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMensaje.ForeColor = System.Drawing.Color.White
        Me.lMensaje.Location = New System.Drawing.Point(469, 249)
        Me.lMensaje.Name = "lMensaje"
        Me.lMensaje.Size = New System.Drawing.Size(267, 25)
        Me.lMensaje.TabIndex = 149
        Me.lMensaje.Text = "No existen coincidencias"
        Me.lMensaje.Visible = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.cbMes)
        Me.Panel3.Controls.Add(Me.cbAño)
        Me.Panel3.Controls.Add(Me.lPaciente)
        Me.Panel3.Controls.Add(Me.cbFuncionarios)
        Me.Panel3.Controls.Add(Me.cbPacientes)
        Me.Panel3.Controls.Add(Me.lFuncionario)
        Me.Panel3.Controls.Add(Me.lMes)
        Me.Panel3.Controls.Add(Me.dtpFechaInicio)
        Me.Panel3.Controls.Add(Me.lAño)
        Me.Panel3.Controls.Add(Me.lFechaFinal)
        Me.Panel3.Controls.Add(Me.dtpFechaFinal)
        Me.Panel3.Controls.Add(Me.lFechaInicio)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.cbxBusqueda)
        Me.Panel3.Controls.Add(Me.dtpFecha)
        Me.Panel3.Controls.Add(Me.lFecha)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.cbConsulta)
        Me.Panel3.Location = New System.Drawing.Point(3, 77)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(256, 480)
        Me.Panel3.TabIndex = 124
        '
        'cbMes
        '
        Me.cbMes.FormattingEnabled = True
        Me.cbMes.Items.AddRange(New Object() {"Seleccione el mes", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"})
        Me.cbMes.Location = New System.Drawing.Point(8, 415)
        Me.cbMes.Name = "cbMes"
        Me.cbMes.Size = New System.Drawing.Size(243, 29)
        Me.cbMes.TabIndex = 149
        '
        'cbAño
        '
        Me.cbAño.FormattingEnabled = True
        Me.cbAño.Items.AddRange(New Object() {"Seleccione el año", "2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010"})
        Me.cbAño.Location = New System.Drawing.Point(8, 329)
        Me.cbAño.Name = "cbAño"
        Me.cbAño.Size = New System.Drawing.Size(243, 29)
        Me.cbAño.TabIndex = 148
        '
        'lPaciente
        '
        Me.lPaciente.AutoSize = True
        Me.lPaciente.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lPaciente.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lPaciente.Location = New System.Drawing.Point(11, 112)
        Me.lPaciente.Name = "lPaciente"
        Me.lPaciente.Size = New System.Drawing.Size(84, 21)
        Me.lPaciente.TabIndex = 147
        Me.lPaciente.Text = "Paciente:"
        '
        'cbFuncionarios
        '
        Me.cbFuncionarios.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbFuncionarios.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.cbFuncionarios.FormattingEnabled = True
        Me.cbFuncionarios.Items.AddRange(New Object() {"Por Año.", "Por Dia.", "Por Mes.", "Por Rango de fechas."})
        Me.cbFuncionarios.Location = New System.Drawing.Point(8, 154)
        Me.cbFuncionarios.Name = "cbFuncionarios"
        Me.cbFuncionarios.Size = New System.Drawing.Size(243, 29)
        Me.cbFuncionarios.TabIndex = 146
        '
        'cbPacientes
        '
        Me.cbPacientes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbPacientes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.cbPacientes.FormattingEnabled = True
        Me.cbPacientes.Items.AddRange(New Object() {"Por Año.", "Por Dia.", "Por Mes.", "Por Rango de fechas."})
        Me.cbPacientes.Location = New System.Drawing.Point(8, 154)
        Me.cbPacientes.Name = "cbPacientes"
        Me.cbPacientes.Size = New System.Drawing.Size(243, 29)
        Me.cbPacientes.TabIndex = 145
        '
        'lFuncionario
        '
        Me.lFuncionario.AutoSize = True
        Me.lFuncionario.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lFuncionario.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lFuncionario.Location = New System.Drawing.Point(11, 112)
        Me.lFuncionario.Name = "lFuncionario"
        Me.lFuncionario.Size = New System.Drawing.Size(104, 21)
        Me.lFuncionario.TabIndex = 140
        Me.lFuncionario.Text = "Funcionario:"
        '
        'lMes
        '
        Me.lMes.AutoSize = True
        Me.lMes.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMes.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lMes.Location = New System.Drawing.Point(11, 379)
        Me.lMes.Name = "lMes"
        Me.lMes.Size = New System.Drawing.Size(45, 21)
        Me.lMes.TabIndex = 136
        Me.lMes.Text = "Mes:"
        Me.lMes.Visible = False
        '
        'dtpFechaInicio
        '
        Me.dtpFechaInicio.CustomFormat = "mm/dd/aa"
        Me.dtpFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInicio.Location = New System.Drawing.Point(8, 331)
        Me.dtpFechaInicio.Name = "dtpFechaInicio"
        Me.dtpFechaInicio.Size = New System.Drawing.Size(243, 27)
        Me.dtpFechaInicio.TabIndex = 131
        Me.dtpFechaInicio.Visible = False
        '
        'lAño
        '
        Me.lAño.AutoSize = True
        Me.lAño.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lAño.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lAño.Location = New System.Drawing.Point(10, 291)
        Me.lAño.Name = "lAño"
        Me.lAño.Size = New System.Drawing.Size(47, 21)
        Me.lAño.TabIndex = 134
        Me.lAño.Text = "Año:"
        Me.lAño.Visible = False
        '
        'lFechaFinal
        '
        Me.lFechaFinal.AutoSize = True
        Me.lFechaFinal.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lFechaFinal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lFechaFinal.Location = New System.Drawing.Point(11, 379)
        Me.lFechaFinal.Name = "lFechaFinal"
        Me.lFechaFinal.Size = New System.Drawing.Size(99, 21)
        Me.lFechaFinal.TabIndex = 130
        Me.lFechaFinal.Text = "Fecha final:"
        Me.lFechaFinal.Visible = False
        '
        'dtpFechaFinal
        '
        Me.dtpFechaFinal.CustomFormat = "mm/dd/aa"
        Me.dtpFechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFinal.Location = New System.Drawing.Point(8, 415)
        Me.dtpFechaFinal.Name = "dtpFechaFinal"
        Me.dtpFechaFinal.Size = New System.Drawing.Size(243, 27)
        Me.dtpFechaFinal.TabIndex = 129
        Me.dtpFechaFinal.Visible = False
        '
        'lFechaInicio
        '
        Me.lFechaInicio.AutoSize = True
        Me.lFechaInicio.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lFechaInicio.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lFechaInicio.Location = New System.Drawing.Point(11, 291)
        Me.lFechaInicio.Name = "lFechaInicio"
        Me.lFechaInicio.Size = New System.Drawing.Size(106, 21)
        Me.lFechaInicio.TabIndex = 128
        Me.lFechaInicio.Text = "Fecha inicio:"
        Me.lFechaInicio.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label4.Location = New System.Drawing.Point(11, 200)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(95, 21)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'cbxBusqueda
        '
        Me.cbxBusqueda.FormattingEnabled = True
        Me.cbxBusqueda.Items.AddRange(New Object() {"Día", "Mes y año", "Año", "Rango de fechas"})
        Me.cbxBusqueda.Location = New System.Drawing.Point(8, 236)
        Me.cbxBusqueda.Name = "cbxBusqueda"
        Me.cbxBusqueda.Size = New System.Drawing.Size(243, 29)
        Me.cbxBusqueda.TabIndex = 126
        '
        'dtpFecha
        '
        Me.dtpFecha.Checked = False
        Me.dtpFecha.CustomFormat = "mm/dd/aa"
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(8, 331)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(243, 27)
        Me.dtpFecha.TabIndex = 125
        Me.dtpFecha.Visible = False
        '
        'lFecha
        '
        Me.lFecha.AutoSize = True
        Me.lFecha.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lFecha.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lFecha.Location = New System.Drawing.Point(11, 291)
        Me.lFecha.Name = "lFecha"
        Me.lFecha.Size = New System.Drawing.Size(63, 21)
        Me.lFecha.TabIndex = 124
        Me.lFecha.Text = "Fecha:"
        Me.lFecha.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.Location = New System.Drawing.Point(11, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 21)
        Me.Label3.TabIndex = 123
        Me.Label3.Text = "Usuario:"
        '
        'cbConsulta
        '
        Me.cbConsulta.FormattingEnabled = True
        Me.cbConsulta.Items.AddRange(New Object() {"Paciente", "Funcionario"})
        Me.cbConsulta.Location = New System.Drawing.Point(8, 64)
        Me.cbConsulta.Name = "cbConsulta"
        Me.cbConsulta.Size = New System.Drawing.Size(243, 29)
        Me.cbConsulta.TabIndex = 122
        '
        'btnReporte
        '
        Me.btnReporte.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnReporte.FlatAppearance.BorderSize = 0
        Me.btnReporte.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReporte.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReporte.ForeColor = System.Drawing.Color.White
        Me.btnReporte.Location = New System.Drawing.Point(538, 502)
        Me.btnReporte.Name = "btnReporte"
        Me.btnReporte.Size = New System.Drawing.Size(120, 35)
        Me.btnReporte.TabIndex = 123
        Me.btnReporte.Text = "Reporte"
        Me.btnReporte.UseVisualStyleBackColor = False
        '
        'dgvCitasMedicinaGeneral
        '
        Me.dgvCitasMedicinaGeneral.AllowUserToAddRows = False
        Me.dgvCitasMedicinaGeneral.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dgvCitasMedicinaGeneral.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCitasMedicinaGeneral.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.dgvCitasMedicinaGeneral.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCitasMedicinaGeneral.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvCitasMedicinaGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCitasMedicinaGeneral.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvCitasMedicinaGeneral.Location = New System.Drawing.Point(273, 46)
        Me.dgvCitasMedicinaGeneral.Name = "dgvCitasMedicinaGeneral"
        Me.dgvCitasMedicinaGeneral.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCitasMedicinaGeneral.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvCitasMedicinaGeneral.RowHeadersVisible = False
        Me.dgvCitasMedicinaGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCitasMedicinaGeneral.Size = New System.Drawing.Size(641, 431)
        Me.dgvCitasMedicinaGeneral.TabIndex = 120
        '
        'FrmReporteMedicina
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(915, 622)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmReporteMedicina"
        Me.Text = "FrmReporteOdontologia"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgvCitasMedicinaGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel2 As Panel
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents dgvCitasMedicinaGeneral As DataGridView
    Private WithEvents btnReporte As Button
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents cbConsulta As ComboBox
    Friend WithEvents lMes As Label
    Friend WithEvents lAño As Label
    Friend WithEvents dtpFechaInicio As DateTimePicker
    Friend WithEvents lFechaFinal As Label
    Friend WithEvents dtpFechaFinal As DateTimePicker
    Friend WithEvents lFechaInicio As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cbxBusqueda As ComboBox
    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents lFecha As Label
    Friend WithEvents lFuncionario As Label
    Friend WithEvents cbFuncionarios As ComboBox
    Friend WithEvents cbPacientes As ComboBox
    Friend WithEvents lPaciente As Label
    Friend WithEvents cbAño As ComboBox
    Friend WithEvents lMensaje As Label
    Friend WithEvents cbMes As ComboBox
End Class

﻿Imports System.Runtime.InteropServices
Imports System.Data.SqlClient


Public Class FrmLogin

    Dim lx, ly As Integer
    Dim sw, sh As Integer

    Dim x, y As Integer
    Dim newpoint As Point


    Dim intentos As Integer

    Private Sub FrmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Abrir()
    End Sub

    'Mover Formulario-------------------------------------------------------------------------------------

    Private Sub FrmLogin_MouseDown(sender As Object, e As MouseEventArgs) Handles MyBase.MouseDown
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub FrmLogin_MouseMove(sender As Object, e As MouseEventArgs) Handles MyBase.MouseMove
        If e.Button = MouseButtons.Left Then
            newpoint = Control.MousePosition
            newpoint.X -= x
            newpoint.Y -= y
            Me.Location = newpoint
            Application.DoEvents()
        End If
    End Sub

    Private Sub pLogo_MouseDown(sender As Object, e As MouseEventArgs) Handles pLogo.MouseDown
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub pLogo_MouseMove(sender As Object, e As MouseEventArgs) Handles pLogo.MouseMove
        If e.Button = MouseButtons.Left Then
            newpoint = Control.MousePosition
            newpoint.X -= x
            newpoint.Y -= y
            Me.Location = newpoint
            Application.DoEvents()
        End If
    End Sub

    '-------------------------------------------------------------------------------------------------------

    Private Sub txtUsuario_Enter(sender As Object, e As EventArgs) Handles txtCedula.Enter
        If txtCedula.Text = "CÉDULA" Then
            txtCedula.Text = ""
            txtCedula.ForeColor = Color.LightGray

        End If
    End Sub

    Private Sub txtUsuario_Leave(sender As Object, e As EventArgs) Handles txtCedula.Leave
        If txtCedula.Text = "" Then
            txtCedula.Text = "CÉDULA"
            txtCedula.ForeColor = Color.DarkGray

        End If
    End Sub

    Private Sub btnVisualizarOcultar_Click(sender As Object, e As EventArgs) Handles btnVisualizarOcultar.Click
        If txtClave.UseSystemPasswordChar = True Then
            txtClave.UseSystemPasswordChar = False
            btnVisualizarOcultar.Image = Image.FromFile("C:\Users\Usuario\Dropbox\PROYECTO FINAL INGENIERIA\VB\ProyectoFinalIngenieria\ProyectoFinalIngenieria\Resources\ocultarContraseña.png")

        ElseIf txtClave.UseSystemPasswordChar = False Then
            txtClave.UseSystemPasswordChar = True
            btnVisualizarOcultar.Image = Image.FromFile("C:\Users\Usuario\Dropbox\PROYECTO FINAL INGENIERIA\VB\ProyectoFinalIngenieria\ProyectoFinalIngenieria\Resources\verContraseña.png")
        End If
    End Sub

    Private Sub txtClave_Enter(sender As Object, e As EventArgs) Handles txtClave.Enter
        If txtClave.Text = "CONTRASEÑA" Then
            txtClave.Text = ""
            txtClave.ForeColor = Color.LightGray
            txtClave.UseSystemPasswordChar = True

        End If
    End Sub

    Private Sub txtClave_Leave(sender As Object, e As EventArgs) Handles txtClave.Leave
        If txtClave.Text = "" Then
            txtClave.Text = "CONTRASEÑA"
            txtClave.ForeColor = Color.DarkGray
            txtClave.UseSystemPasswordChar = False

        End If
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        'Application.Exit()
        If MsgBox("¿Está seguro de cerrar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then
            End
        End If
    End Sub

    Private Sub btnAcceder_Click(sender As Object, e As EventArgs) Handles btnAcceder.Click
        botonAceptar()
    End Sub

    '----------------- MENSAJE DE ERROR

    Public Sub msgError(msg As String)
        lMensajeError.Text = "     " + msg
        lMensajeError.Visible = True

    End Sub

    Private Sub btnMinimizar_Click(sender As Object, e As EventArgs) Handles btnMinimizar.Click
        WindowState = FormWindowState.Minimized
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Me.Hide()
        FrmRecuperarContraseña.Show()
    End Sub

    Sub botonAceptar()
        Try
            If txtCedula.Text = "CÉDULA" Or txtClave.Text = "CONTRASEÑA" Then 'puedo poner =Nothing
                msgError("Debe llenar ambos campos")
                limpiar()

            Else
                'Debemos declarar una variable que representará la tabla de la base de datos
                'Viene siendo como un recordset
                Dim rs As New SqlCommand("select * from Funcionarios where Cedula = '" & txtCedula.Text & "'", conn)

                Dim dr As SqlDataReader
                dr = rs.ExecuteReader
                If dr.Read Then
                    ID_Funcionario = dr.Item("ID_Funcionario")

                    If Ced_Funcionario <> dr.Item("Cedula") Then
                        intentos = 0
                    End If

                    ID_Funcionario = dr.Item("ID_Funcionario")
                    Ced_Funcionario = dr.Item("Cedula")
                    Nom_Funcionario = dr.Item("NombreCompleto").ToString
                    Correo_Funcionario = dr.Item("Correo").ToString
                    Direccion_Funcionario = dr.Item("Direccion").ToString
                    Profesion_Funcionario = dr.Item("Profesion").ToString

                    Contraseña_Funcionario = dr.Item("Contraseña").ToString
                    Genero_Funcionario = dr.Item("Genero").ToString
                    FecNacimiento_Funcionario = dr.Item("FechaNacimiento").ToString
                    Tel1_Funcionario = dr.Item("Telefono1").ToString
                    Tel2_Funcionario = dr.Item("Telefono2").ToString
                    Estado = dr.Item("Estado").ToString


                    dr.Close()

                    'MsgBox(Profesion_Funcionario, MsgBoxStyle.Critical, "Información")

                    If Estado = True Then
                        msgError("El usuario se encuentra temporalmente BLOQUEADO, consulte con el Administrador del sistema")
                        '''' End 'Cierra la aplicación
                        txtCedula.Text = ""
                        txtClave.Text = ""
                    Else
                        If txtClave.Text = Contraseña_Funcionario Then
                            Me.Hide()
                            FormPrincipal.Show()

                        Else

                            msgError("Usuario y/o contraseña son invalidos")
                            limpiar()
                            intentos += 1

                        End If
                    End If
                Else
                    msgError("Usuario y/o contraseña son invalidos")
                    limpiar()
                    intentos += 1

                End If
                dr.Close()
            End If


            If intentos = 3 Then
                msgError("Usted ha excedido el máximo de intentos, su cuenta será BLOQUEADA, consulte con el Administrador del Sistema")

                Dim cmdBloquearUser As New SqlCommand("update Funcionarios set Estado = '" & True & "' where Cedula = '" & Ced_Funcionario & "'", conn)
                cmdBloquearUser.ExecuteNonQuery() 'EndExecuteNonQuery significa que NO es una consulta
                '''' conn.Close()
                txtCedula.Text = ""
                txtClave.Text = ""
                End

            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub limpiar() ' procedimiento o metodo void / function devuelve algo
        txtCedula.Text = ""
        txtClave.Text = ""
        txtCedula.Focus() ' campo usuario aparezca en rojo
    End Sub



End Class
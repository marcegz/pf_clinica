﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFacturas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmFacturas))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnMinimizar = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lMontoSubTotal = New System.Windows.Forms.Label()
        Me.lMontoDescuento = New System.Windows.Forms.Label()
        Me.lMontoTotal = New System.Windows.Forms.Label()
        Me.lDescuento = New System.Windows.Forms.Label()
        Me.lSubtotal = New System.Windows.Forms.Label()
        Me.txtIDFuncionario = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lTotal = New System.Windows.Forms.Label()
        Me.cbDescuento = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnMedicina = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.txtFuncionario = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtIDPaciente = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtPaciente = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtHora = New System.Windows.Forms.TextBox()
        Me.txtIDFactura = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.btnMinimizar)
        Me.Panel1.Controls.Add(Me.btnCerrar)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(774, 43)
        Me.Panel1.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label4.Location = New System.Drawing.Point(17, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(130, 21)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "FACTURACIÓN"
        '
        'btnMinimizar
        '
        Me.btnMinimizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMinimizar.FlatAppearance.BorderSize = 0
        Me.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMinimizar.Image = CType(resources.GetObject("btnMinimizar.Image"), System.Drawing.Image)
        Me.btnMinimizar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnMinimizar.Location = New System.Drawing.Point(694, -1)
        Me.btnMinimizar.Name = "btnMinimizar"
        Me.btnMinimizar.Size = New System.Drawing.Size(43, 43)
        Me.btnMinimizar.TabIndex = 6
        Me.btnMinimizar.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCerrar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCerrar.FlatAppearance.BorderSize = 0
        Me.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCerrar.Image = CType(resources.GetObject("btnCerrar.Image"), System.Drawing.Image)
        Me.btnCerrar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnCerrar.Location = New System.Drawing.Point(732, 1)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(39, 39)
        Me.btnCerrar.TabIndex = 5
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Controls.Add(Me.Label15)
        Me.Panel2.Controls.Add(Me.lMontoSubTotal)
        Me.Panel2.Controls.Add(Me.lMontoDescuento)
        Me.Panel2.Controls.Add(Me.lMontoTotal)
        Me.Panel2.Controls.Add(Me.lDescuento)
        Me.Panel2.Controls.Add(Me.lSubtotal)
        Me.Panel2.Controls.Add(Me.txtIDFuncionario)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.lTotal)
        Me.Panel2.Controls.Add(Me.cbDescuento)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.btnMedicina)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.dgvDetalle)
        Me.Panel2.Controls.Add(Me.txtFuncionario)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.txtIDPaciente)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.txtPaciente)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Me.dtpFecha)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.txtHora)
        Me.Panel2.Controls.Add(Me.txtIDFactura)
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.ForeColor = System.Drawing.Color.White
        Me.Panel2.Location = New System.Drawing.Point(0, 40)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(774, 523)
        Me.Panel2.TabIndex = 1
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label16.Location = New System.Drawing.Point(139, 416)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(22, 21)
        Me.Label16.TabIndex = 151
        Me.Label16.Text = "₡"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label15.Location = New System.Drawing.Point(139, 375)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(22, 21)
        Me.Label15.TabIndex = 150
        Me.Label15.Text = "₡"
        '
        'lMontoSubTotal
        '
        Me.lMontoSubTotal.AutoSize = True
        Me.lMontoSubTotal.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMontoSubTotal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lMontoSubTotal.Location = New System.Drawing.Point(158, 375)
        Me.lMontoSubTotal.Name = "lMontoSubTotal"
        Me.lMontoSubTotal.Size = New System.Drawing.Size(41, 21)
        Me.lMontoSubTotal.TabIndex = 149
        Me.lMontoSubTotal.Text = "0.00"
        '
        'lMontoDescuento
        '
        Me.lMontoDescuento.AutoSize = True
        Me.lMontoDescuento.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMontoDescuento.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lMontoDescuento.Location = New System.Drawing.Point(158, 416)
        Me.lMontoDescuento.Name = "lMontoDescuento"
        Me.lMontoDescuento.Size = New System.Drawing.Size(41, 21)
        Me.lMontoDescuento.TabIndex = 148
        Me.lMontoDescuento.Text = "0.00"
        '
        'lMontoTotal
        '
        Me.lMontoTotal.AutoSize = True
        Me.lMontoTotal.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMontoTotal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lMontoTotal.Location = New System.Drawing.Point(173, 456)
        Me.lMontoTotal.Margin = New System.Windows.Forms.Padding(0)
        Me.lMontoTotal.Name = "lMontoTotal"
        Me.lMontoTotal.Size = New System.Drawing.Size(93, 44)
        Me.lMontoTotal.TabIndex = 147
        Me.lMontoTotal.Text = "0.00"
        Me.lMontoTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lDescuento
        '
        Me.lDescuento.AutoSize = True
        Me.lDescuento.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lDescuento.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lDescuento.Location = New System.Drawing.Point(37, 416)
        Me.lDescuento.Name = "lDescuento"
        Me.lDescuento.Size = New System.Drawing.Size(99, 21)
        Me.lDescuento.TabIndex = 146
        Me.lDescuento.Text = "Descuento:"
        '
        'lSubtotal
        '
        Me.lSubtotal.AutoSize = True
        Me.lSubtotal.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lSubtotal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lSubtotal.Location = New System.Drawing.Point(37, 375)
        Me.lSubtotal.Name = "lSubtotal"
        Me.lSubtotal.Size = New System.Drawing.Size(86, 21)
        Me.lSubtotal.TabIndex = 145
        Me.lSubtotal.Text = "Sub Total:"
        '
        'txtIDFuncionario
        '
        Me.txtIDFuncionario.Enabled = False
        Me.txtIDFuncionario.Location = New System.Drawing.Point(149, 102)
        Me.txtIDFuncionario.Name = "txtIDFuncionario"
        Me.txtIDFuncionario.Size = New System.Drawing.Size(173, 27)
        Me.txtIDFuncionario.TabIndex = 143
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(18, 105)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(125, 21)
        Me.Label5.TabIndex = 142
        Me.Label5.Text = "ID Funcionario:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.Location = New System.Drawing.Point(135, 456)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 44)
        Me.Label3.TabIndex = 141
        Me.Label3.Text = "₡"
        '
        'lTotal
        '
        Me.lTotal.AutoSize = True
        Me.lTotal.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lTotal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lTotal.Location = New System.Drawing.Point(28, 456)
        Me.lTotal.Name = "lTotal"
        Me.lTotal.Size = New System.Drawing.Size(114, 44)
        Me.lTotal.TabIndex = 140
        Me.lTotal.Text = "Total:"
        '
        'cbDescuento
        '
        Me.cbDescuento.FormattingEnabled = True
        Me.cbDescuento.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"})
        Me.cbDescuento.Location = New System.Drawing.Point(533, 372)
        Me.cbDescuento.Name = "cbDescuento"
        Me.cbDescuento.Size = New System.Drawing.Size(49, 29)
        Me.cbDescuento.TabIndex = 139
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label10.Location = New System.Drawing.Point(412, 375)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(115, 21)
        Me.Label10.TabIndex = 138
        Me.Label10.Text = "Descuento %:"
        '
        'btnMedicina
        '
        Me.btnMedicina.BackColor = System.Drawing.Color.Transparent
        Me.btnMedicina.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMedicina.FlatAppearance.BorderSize = 0
        Me.btnMedicina.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnMedicina.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.btnMedicina.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMedicina.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.btnMedicina.ForeColor = System.Drawing.Color.White
        Me.btnMedicina.Image = CType(resources.GetObject("btnMedicina.Image"), System.Drawing.Image)
        Me.btnMedicina.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnMedicina.Location = New System.Drawing.Point(588, 359)
        Me.btnMedicina.Name = "btnMedicina"
        Me.btnMedicina.Size = New System.Drawing.Size(152, 141)
        Me.btnMedicina.TabIndex = 123
        Me.btnMedicina.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMedicina.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(71, 164)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 22)
        Me.Label1.TabIndex = 55
        Me.Label1.Text = "Detalle:"
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.dgvDetalle.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalle.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalle.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDetalle.Location = New System.Drawing.Point(75, 198)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.ReadOnly = True
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(622, 148)
        Me.dgvDetalle.TabIndex = 52
        '
        'txtFuncionario
        '
        Me.txtFuncionario.Enabled = False
        Me.txtFuncionario.Location = New System.Drawing.Point(461, 99)
        Me.txtFuncionario.Name = "txtFuncionario"
        Me.txtFuncionario.Size = New System.Drawing.Size(293, 27)
        Me.txtFuncionario.TabIndex = 51
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(340, 105)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(104, 21)
        Me.Label9.TabIndex = 50
        Me.Label9.Text = "Funcionario:"
        '
        'txtIDPaciente
        '
        Me.txtIDPaciente.Enabled = False
        Me.txtIDPaciente.Location = New System.Drawing.Point(149, 57)
        Me.txtIDPaciente.Name = "txtIDPaciente"
        Me.txtIDPaciente.Size = New System.Drawing.Size(173, 27)
        Me.txtIDPaciente.TabIndex = 49
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(18, 60)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(105, 21)
        Me.Label20.TabIndex = 48
        Me.Label20.Text = "ID Paciente:"
        '
        'txtPaciente
        '
        Me.txtPaciente.Enabled = False
        Me.txtPaciente.Location = New System.Drawing.Point(461, 57)
        Me.txtPaciente.Name = "txtPaciente"
        Me.txtPaciente.Size = New System.Drawing.Size(293, 27)
        Me.txtPaciente.TabIndex = 47
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(340, 60)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(84, 21)
        Me.Label18.TabIndex = 46
        Me.Label18.Text = "Paciente:"
        '
        'dtpFecha
        '
        Me.dtpFecha.CustomFormat = "mm/dd/aa"
        Me.dtpFecha.Enabled = False
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(461, 13)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(146, 27)
        Me.dtpFecha.TabIndex = 45
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(340, 18)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(115, 21)
        Me.Label12.TabIndex = 44
        Me.Label12.Text = "Fecha / Hora:"
        '
        'txtHora
        '
        Me.txtHora.Enabled = False
        Me.txtHora.Location = New System.Drawing.Point(613, 13)
        Me.txtHora.Name = "txtHora"
        Me.txtHora.Size = New System.Drawing.Size(141, 27)
        Me.txtHora.TabIndex = 41
        '
        'txtIDFactura
        '
        Me.txtIDFactura.Enabled = False
        Me.txtIDFactura.Location = New System.Drawing.Point(149, 15)
        Me.txtIDFactura.Name = "txtIDFactura"
        Me.txtIDFactura.Size = New System.Drawing.Size(173, 27)
        Me.txtIDFactura.TabIndex = 39
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(18, 18)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(97, 21)
        Me.Label19.TabIndex = 38
        Me.Label19.Text = "ID Factura:"
        '
        'FrmFacturas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(775, 558)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmFacturas"
        Me.Text = "FrmFacturas"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Private WithEvents btnMinimizar As Button
    Private WithEvents btnCerrar As Button
    Friend WithEvents txtFuncionario As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtIDPaciente As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents txtPaciente As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents Label12 As Label
    Friend WithEvents txtHora As TextBox
    Friend WithEvents txtIDFactura As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents dgvDetalle As DataGridView
    Private WithEvents btnMedicina As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents lTotal As Label
    Friend WithEvents cbDescuento As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtIDFuncionario As TextBox
    Friend WithEvents lMontoTotal As Label
    Friend WithEvents lDescuento As Label
    Friend WithEvents lSubtotal As Label
    Friend WithEvents lMontoDescuento As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents lMontoSubTotal As Label
    Friend WithEvents Label16 As Label
End Class

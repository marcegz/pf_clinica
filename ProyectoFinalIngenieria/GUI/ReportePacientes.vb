﻿Public Class ReportePacientes

    Dim Especialidad As String

    Public Sub New(especialidadU As String)
        InitializeComponent()
        Especialidad = especialidadU
    End Sub

    Private Sub ReportePacientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DataReportes.ConsultarPacientesReporte' Puede moverla o quitarla según sea necesario.
        Me.ConsultarPacientesReporteTableAdapter.Fill(Me.DataReportes.ConsultarPacientesReporte, Especialidad)

        Me.ReportViewer1.RefreshReport()
    End Sub
End Class
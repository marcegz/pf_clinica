﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCitas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCitas))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.dtpFechaCita = New System.Windows.Forms.DateTimePicker()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.btnLimpiar = New System.Windows.Forms.Button()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtEspecialidad = New System.Windows.Forms.TextBox()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnActualizar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbFuncionario = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtEdad = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cbHora = New System.Windows.Forms.ComboBox()
        Me.txtCedula = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvCitas = New System.Windows.Forms.DataGridView()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.txtBusqueda = New System.Windows.Forms.TextBox()
        Me.tbxDescripcion = New System.Windows.Forms.RichTextBox()
        Me.cbPaciente = New System.Windows.Forms.ComboBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtFuncionarioPacienteDia = New System.Windows.Forms.TextBox()
        Me.txtEspecialidadPacienteDia = New System.Windows.Forms.TextBox()
        Me.txtHoraPacienteDia = New System.Windows.Forms.TextBox()
        Me.txtNombrePacienteDia = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtEdadPacienteDia = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtCedulaPacienteDia = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.dgvCitasDia = New System.Windows.Forms.DataGridView()
        Me.rtbDescripcionPacienteDia = New System.Windows.Forms.RichTextBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.PacientesActivosTodosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DB_A494E9_PFIngenieriaDataSet3 = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSet3()
        Me.FuncionariosActivosTodosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DB_A494E9_PFIngenieriaDataSet4 = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSet4()
        Me.FuncionariosActivosporEspecialidadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FuncionariosXEspecialidadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PacientesActivosTodosTableAdapter = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSet3TableAdapters.pacientesActivosTodosTableAdapter()
        Me.ExtraerHorasDisponiblesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ExtraerHorasDisponiblesTableAdapter = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSet4TableAdapters.extraerHorasDisponiblesTableAdapter()
        Me.FuncionariosActivosTodosTableAdapter = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSet4TableAdapters.funcionariosActivosTodosTableAdapter()
        Me.FuncionariosInactivosTodosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FuncionariosInactivosTodosTableAdapter = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSet4TableAdapters.funcionariosInactivosTodosTableAdapter()
        Me.FuncionariosXEspecialidadTableAdapter = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSet4TableAdapters.funcionariosXEspecialidadTableAdapter()
        Me.FuncionariosActivosporEspecialidadTableAdapter = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSet4TableAdapters.funcionariosActivosporEspecialidadTableAdapter()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCitas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvCitasDia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PacientesActivosTodosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DB_A494E9_PFIngenieriaDataSet3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuncionariosActivosTodosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DB_A494E9_PFIngenieriaDataSet4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuncionariosActivosporEspecialidadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuncionariosXEspecialidadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExtraerHorasDisponiblesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuncionariosInactivosTodosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 58)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(882, 552)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 30)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(874, 518)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Citas"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Label24)
        Me.Panel1.Controls.Add(Me.dtpFechaCita)
        Me.Panel1.Controls.Add(Me.Label21)
        Me.Panel1.Controls.Add(Me.btnLimpiar)
        Me.Panel1.Controls.Add(Me.Label22)
        Me.Panel1.Controls.Add(Me.Label23)
        Me.Panel1.Controls.Add(Me.Label20)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.txtEspecialidad)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.btnActualizar)
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.cbFuncionario)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txtEdad)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.cbHora)
        Me.Panel1.Controls.Add(Me.txtCedula)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.dgvCitas)
        Me.Panel1.Controls.Add(Me.dtpFecha)
        Me.Panel1.Controls.Add(Me.txtBusqueda)
        Me.Panel1.Controls.Add(Me.tbxDescripcion)
        Me.Panel1.Controls.Add(Me.cbPaciente)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(881, 518)
        Me.Panel1.TabIndex = 0
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Red
        Me.Label24.Location = New System.Drawing.Point(272, 150)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(19, 22)
        Me.Label24.TabIndex = 97
        Me.Label24.Text = "*"
        '
        'dtpFechaCita
        '
        Me.dtpFechaCita.CustomFormat = "mm/dd/aa"
        Me.dtpFechaCita.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaCita.Location = New System.Drawing.Point(216, 182)
        Me.dtpFechaCita.Name = "dtpFechaCita"
        Me.dtpFechaCita.Size = New System.Drawing.Size(177, 27)
        Me.dtpFechaCita.TabIndex = 96
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label21.Location = New System.Drawing.Point(216, 152)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(63, 21)
        Me.Label21.TabIndex = 94
        Me.Label21.Text = "Fecha:"
        '
        'btnLimpiar
        '
        Me.btnLimpiar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnLimpiar.FlatAppearance.BorderSize = 0
        Me.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLimpiar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLimpiar.ForeColor = System.Drawing.Color.White
        Me.btnLimpiar.Location = New System.Drawing.Point(20, 474)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(99, 35)
        Me.btnLimpiar.TabIndex = 93
        Me.btnLimpiar.Text = "Nuevo"
        Me.btnLimpiar.UseVisualStyleBackColor = False
        Me.btnLimpiar.Visible = False
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.Red
        Me.Label22.Location = New System.Drawing.Point(849, 25)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(19, 22)
        Me.Label22.TabIndex = 92
        Me.Label22.Text = "*"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Red
        Me.Label23.Location = New System.Drawing.Point(68, 151)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(19, 22)
        Me.Label23.TabIndex = 91
        Me.Label23.Text = "*"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Red
        Me.Label20.Location = New System.Drawing.Point(126, 353)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(19, 22)
        Me.Label20.TabIndex = 88
        Me.Label20.Text = "*"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Red
        Me.Label19.Location = New System.Drawing.Point(119, 223)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(19, 22)
        Me.Label19.TabIndex = 87
        Me.Label19.Text = "*"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Red
        Me.Label18.Location = New System.Drawing.Point(101, 10)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(19, 22)
        Me.Label18.TabIndex = 86
        Me.Label18.Text = "*"
        '
        'txtEspecialidad
        '
        Me.txtEspecialidad.Enabled = False
        Me.txtEspecialidad.Location = New System.Drawing.Point(144, 310)
        Me.txtEspecialidad.Name = "txtEspecialidad"
        Me.txtEspecialidad.Size = New System.Drawing.Size(246, 27)
        Me.txtEspecialidad.TabIndex = 85
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnGuardar.FlatAppearance.BorderSize = 0
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.White
        Me.btnGuardar.Location = New System.Drawing.Point(20, 474)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(99, 35)
        Me.btnGuardar.TabIndex = 82
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'btnActualizar
        '
        Me.btnActualizar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnActualizar.Enabled = False
        Me.btnActualizar.FlatAppearance.BorderSize = 0
        Me.btnActualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActualizar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActualizar.ForeColor = System.Drawing.Color.White
        Me.btnActualizar.Location = New System.Drawing.Point(160, 474)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(99, 35)
        Me.btnActualizar.TabIndex = 84
        Me.btnActualizar.Text = "Actualizar"
        Me.btnActualizar.UseVisualStyleBackColor = False
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnEliminar.Enabled = False
        Me.btnEliminar.FlatAppearance.BorderSize = 0
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.ForeColor = System.Drawing.Color.White
        Me.btnEliminar.Location = New System.Drawing.Point(294, 474)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(99, 35)
        Me.btnEliminar.TabIndex = 83
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(826, 24)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox1.TabIndex = 81
        Me.PictureBox1.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label9.Location = New System.Drawing.Point(23, 353)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(104, 21)
        Me.Label9.TabIndex = 80
        Me.Label9.Text = "Descripción:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label8.Location = New System.Drawing.Point(23, 224)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(104, 21)
        Me.Label8.TabIndex = 79
        Me.Label8.Text = "Funcionario:"
        '
        'cbFuncionario
        '
        Me.cbFuncionario.FormattingEnabled = True
        Me.cbFuncionario.Location = New System.Drawing.Point(20, 257)
        Me.cbFuncionario.Name = "cbFuncionario"
        Me.cbFuncionario.Size = New System.Drawing.Size(372, 29)
        Me.cbFuncionario.TabIndex = 78
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label7.Location = New System.Drawing.Point(25, 313)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(113, 21)
        Me.Label7.TabIndex = 77
        Me.Label7.Text = "Especialidad:"
        '
        'txtEdad
        '
        Me.txtEdad.Enabled = False
        Me.txtEdad.Location = New System.Drawing.Point(216, 115)
        Me.txtEdad.Name = "txtEdad"
        Me.txtEdad.Size = New System.Drawing.Size(174, 27)
        Me.txtEdad.TabIndex = 75
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label6.Location = New System.Drawing.Point(216, 82)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 21)
        Me.Label6.TabIndex = 74
        Me.Label6.Text = "Edad:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label5.Location = New System.Drawing.Point(23, 151)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 21)
        Me.Label5.TabIndex = 72
        Me.Label5.Text = "Hora:"
        '
        'cbHora
        '
        Me.cbHora.FormattingEnabled = True
        Me.cbHora.Location = New System.Drawing.Point(20, 184)
        Me.cbHora.Name = "cbHora"
        Me.cbHora.Size = New System.Drawing.Size(174, 29)
        Me.cbHora.TabIndex = 71
        '
        'txtCedula
        '
        Me.txtCedula.Enabled = False
        Me.txtCedula.Location = New System.Drawing.Point(21, 115)
        Me.txtCedula.Name = "txtCedula"
        Me.txtCedula.Size = New System.Drawing.Size(174, 27)
        Me.txtCedula.TabIndex = 70
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label2.Location = New System.Drawing.Point(21, 82)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 21)
        Me.Label2.TabIndex = 69
        Me.Label2.Text = "Cédula:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label1.Location = New System.Drawing.Point(23, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 21)
        Me.Label1.TabIndex = 68
        Me.Label1.Text = "Paciente:"
        '
        'dgvCitas
        '
        Me.dgvCitas.AllowUserToAddRows = False
        Me.dgvCitas.AllowUserToDeleteRows = False
        Me.dgvCitas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCitas.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.dgvCitas.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvCitas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCitas.Location = New System.Drawing.Point(427, 66)
        Me.dgvCitas.MultiSelect = False
        Me.dgvCitas.Name = "dgvCitas"
        Me.dgvCitas.ReadOnly = True
        Me.dgvCitas.RowHeadersVisible = False
        Me.dgvCitas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCitas.Size = New System.Drawing.Size(419, 432)
        Me.dgvCitas.TabIndex = 66
        '
        'dtpFecha
        '
        Me.dtpFecha.CustomFormat = "mm/dd/aa"
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(711, 23)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(109, 27)
        Me.dtpFecha.TabIndex = 65
        '
        'txtBusqueda
        '
        Me.txtBusqueda.Location = New System.Drawing.Point(427, 23)
        Me.txtBusqueda.Name = "txtBusqueda"
        Me.txtBusqueda.Size = New System.Drawing.Size(269, 27)
        Me.txtBusqueda.TabIndex = 64
        '
        'tbxDescripcion
        '
        Me.tbxDescripcion.Location = New System.Drawing.Point(20, 386)
        Me.tbxDescripcion.Name = "tbxDescripcion"
        Me.tbxDescripcion.Size = New System.Drawing.Size(373, 72)
        Me.tbxDescripcion.TabIndex = 59
        Me.tbxDescripcion.Text = ""
        '
        'cbPaciente
        '
        Me.cbPaciente.FormattingEnabled = True
        Me.cbPaciente.Location = New System.Drawing.Point(20, 43)
        Me.cbPaciente.Name = "cbPaciente"
        Me.cbPaciente.Size = New System.Drawing.Size(370, 29)
        Me.cbPaciente.TabIndex = 49
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(809, 512)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(86, 31)
        Me.Button3.TabIndex = 19
        Me.Button3.Text = "Exportar PDF"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 30)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(874, 518)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Citas del Día"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel3.Controls.Add(Me.Label26)
        Me.Panel3.Controls.Add(Me.txtFuncionarioPacienteDia)
        Me.Panel3.Controls.Add(Me.txtEspecialidadPacienteDia)
        Me.Panel3.Controls.Add(Me.txtHoraPacienteDia)
        Me.Panel3.Controls.Add(Me.txtNombrePacienteDia)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.Label12)
        Me.Panel3.Controls.Add(Me.txtEdadPacienteDia)
        Me.Panel3.Controls.Add(Me.Label13)
        Me.Panel3.Controls.Add(Me.Label14)
        Me.Panel3.Controls.Add(Me.txtCedulaPacienteDia)
        Me.Panel3.Controls.Add(Me.Label15)
        Me.Panel3.Controls.Add(Me.Label16)
        Me.Panel3.Controls.Add(Me.dgvCitasDia)
        Me.Panel3.Controls.Add(Me.rtbDescripcionPacienteDia)
        Me.Panel3.Controls.Add(Me.Button9)
        Me.Panel3.Controls.Add(Me.Button10)
        Me.Panel3.Controls.Add(Me.Button11)
        Me.Panel3.Location = New System.Drawing.Point(-4, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(881, 514)
        Me.Panel3.TabIndex = 1
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.Label26.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label26.Location = New System.Drawing.Point(463, 13)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(358, 19)
        Me.Label26.TabIndex = 93
        Me.Label26.Text = "** Datos Cita: doble clic en fila de la cita deseada"
        '
        'txtFuncionarioPacienteDia
        '
        Me.txtFuncionarioPacienteDia.Location = New System.Drawing.Point(23, 268)
        Me.txtFuncionarioPacienteDia.Name = "txtFuncionarioPacienteDia"
        Me.txtFuncionarioPacienteDia.Size = New System.Drawing.Size(370, 27)
        Me.txtFuncionarioPacienteDia.TabIndex = 91
        '
        'txtEspecialidadPacienteDia
        '
        Me.txtEspecialidadPacienteDia.Location = New System.Drawing.Point(218, 194)
        Me.txtEspecialidadPacienteDia.Name = "txtEspecialidadPacienteDia"
        Me.txtEspecialidadPacienteDia.Size = New System.Drawing.Size(174, 27)
        Me.txtEspecialidadPacienteDia.TabIndex = 90
        '
        'txtHoraPacienteDia
        '
        Me.txtHoraPacienteDia.Location = New System.Drawing.Point(23, 194)
        Me.txtHoraPacienteDia.Name = "txtHoraPacienteDia"
        Me.txtHoraPacienteDia.Size = New System.Drawing.Size(174, 27)
        Me.txtHoraPacienteDia.TabIndex = 89
        '
        'txtNombrePacienteDia
        '
        Me.txtNombrePacienteDia.Location = New System.Drawing.Point(20, 48)
        Me.txtNombrePacienteDia.Name = "txtNombrePacienteDia"
        Me.txtNombrePacienteDia.Size = New System.Drawing.Size(370, 27)
        Me.txtNombrePacienteDia.TabIndex = 85
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label10.Location = New System.Drawing.Point(23, 305)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(104, 21)
        Me.Label10.TabIndex = 80
        Me.Label10.Text = "Descripción:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label11.Location = New System.Drawing.Point(23, 232)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(104, 21)
        Me.Label11.TabIndex = 79
        Me.Label11.Text = "Funcionario:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label12.Location = New System.Drawing.Point(223, 159)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(113, 21)
        Me.Label12.TabIndex = 77
        Me.Label12.Text = "Especialidad:"
        '
        'txtEdadPacienteDia
        '
        Me.txtEdadPacienteDia.Location = New System.Drawing.Point(218, 122)
        Me.txtEdadPacienteDia.Name = "txtEdadPacienteDia"
        Me.txtEdadPacienteDia.Size = New System.Drawing.Size(174, 27)
        Me.txtEdadPacienteDia.TabIndex = 75
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label13.Location = New System.Drawing.Point(218, 89)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(56, 21)
        Me.Label13.TabIndex = 74
        Me.Label13.Text = "Edad:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label14.Location = New System.Drawing.Point(23, 159)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(51, 21)
        Me.Label14.TabIndex = 72
        Me.Label14.Text = "Hora:"
        '
        'txtCedulaPacienteDia
        '
        Me.txtCedulaPacienteDia.Location = New System.Drawing.Point(23, 122)
        Me.txtCedulaPacienteDia.Name = "txtCedulaPacienteDia"
        Me.txtCedulaPacienteDia.Size = New System.Drawing.Size(174, 27)
        Me.txtCedulaPacienteDia.TabIndex = 70
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label15.Location = New System.Drawing.Point(23, 89)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(72, 21)
        Me.Label15.TabIndex = 69
        Me.Label15.Text = "Cédula:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label16.Location = New System.Drawing.Point(23, 24)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(77, 21)
        Me.Label16.TabIndex = 68
        Me.Label16.Text = "Nombre:"
        '
        'dgvCitasDia
        '
        Me.dgvCitasDia.AllowUserToAddRows = False
        Me.dgvCitasDia.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCitasDia.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.dgvCitasDia.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvCitasDia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCitasDia.Location = New System.Drawing.Point(427, 48)
        Me.dgvCitasDia.Name = "dgvCitasDia"
        Me.dgvCitasDia.ReadOnly = True
        Me.dgvCitasDia.RowHeadersVisible = False
        Me.dgvCitasDia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCitasDia.Size = New System.Drawing.Size(428, 448)
        Me.dgvCitasDia.TabIndex = 66
        '
        'rtbDescripcionPacienteDia
        '
        Me.rtbDescripcionPacienteDia.Location = New System.Drawing.Point(20, 338)
        Me.rtbDescripcionPacienteDia.Name = "rtbDescripcionPacienteDia"
        Me.rtbDescripcionPacienteDia.Size = New System.Drawing.Size(373, 137)
        Me.rtbDescripcionPacienteDia.TabIndex = 59
        Me.rtbDescripcionPacienteDia.Text = ""
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button9.ForeColor = System.Drawing.Color.White
        Me.Button9.Location = New System.Drawing.Point(204, 512)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(86, 31)
        Me.Button9.TabIndex = 21
        Me.Button9.Text = "Eliminar"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button10.ForeColor = System.Drawing.Color.White
        Me.Button10.Location = New System.Drawing.Point(310, 512)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(86, 31)
        Me.Button10.TabIndex = 20
        Me.Button10.Text = "Agregar"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button11.ForeColor = System.Drawing.Color.White
        Me.Button11.Location = New System.Drawing.Point(809, 512)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(86, 31)
        Me.Button11.TabIndex = 19
        Me.Button11.Text = "Exportar PDF"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'PacientesActivosTodosBindingSource
        '
        Me.PacientesActivosTodosBindingSource.DataMember = "pacientesActivosTodos"
        Me.PacientesActivosTodosBindingSource.DataSource = Me.DB_A494E9_PFIngenieriaDataSet3
        '
        'DB_A494E9_PFIngenieriaDataSet3
        '
        Me.DB_A494E9_PFIngenieriaDataSet3.DataSetName = "DB_A494E9_PFIngenieriaDataSet3"
        Me.DB_A494E9_PFIngenieriaDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FuncionariosActivosTodosBindingSource
        '
        Me.FuncionariosActivosTodosBindingSource.DataMember = "funcionariosActivosTodos"
        Me.FuncionariosActivosTodosBindingSource.DataSource = Me.DB_A494E9_PFIngenieriaDataSet4
        '
        'DB_A494E9_PFIngenieriaDataSet4
        '
        Me.DB_A494E9_PFIngenieriaDataSet4.DataSetName = "DB_A494E9_PFIngenieriaDataSet4"
        Me.DB_A494E9_PFIngenieriaDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FuncionariosActivosporEspecialidadBindingSource
        '
        Me.FuncionariosActivosporEspecialidadBindingSource.DataMember = "funcionariosActivosporEspecialidad"
        Me.FuncionariosActivosporEspecialidadBindingSource.DataSource = Me.DB_A494E9_PFIngenieriaDataSet4
        '
        'FuncionariosXEspecialidadBindingSource
        '
        Me.FuncionariosXEspecialidadBindingSource.DataMember = "funcionariosXEspecialidad"
        Me.FuncionariosXEspecialidadBindingSource.DataSource = Me.DB_A494E9_PFIngenieriaDataSet4
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel2.Controls.Add(Me.PictureBox2)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(901, 56)
        Me.Panel2.TabIndex = 3
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(0, 3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(901, 3)
        Me.PictureBox2.TabIndex = 36
        Me.PictureBox2.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(453, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 33)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Citas"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(286, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(171, 33)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Formulario /"
        '
        'PacientesActivosTodosTableAdapter
        '
        Me.PacientesActivosTodosTableAdapter.ClearBeforeFill = True
        '
        'ExtraerHorasDisponiblesBindingSource
        '
        Me.ExtraerHorasDisponiblesBindingSource.DataMember = "extraerHorasDisponibles"
        Me.ExtraerHorasDisponiblesBindingSource.DataSource = Me.DB_A494E9_PFIngenieriaDataSet4
        '
        'ExtraerHorasDisponiblesTableAdapter
        '
        Me.ExtraerHorasDisponiblesTableAdapter.ClearBeforeFill = True
        '
        'FuncionariosActivosTodosTableAdapter
        '
        Me.FuncionariosActivosTodosTableAdapter.ClearBeforeFill = True
        '
        'FuncionariosInactivosTodosBindingSource
        '
        Me.FuncionariosInactivosTodosBindingSource.DataMember = "funcionariosInactivosTodos"
        Me.FuncionariosInactivosTodosBindingSource.DataSource = Me.DB_A494E9_PFIngenieriaDataSet4
        '
        'FuncionariosInactivosTodosTableAdapter
        '
        Me.FuncionariosInactivosTodosTableAdapter.ClearBeforeFill = True
        '
        'FuncionariosXEspecialidadTableAdapter
        '
        Me.FuncionariosXEspecialidadTableAdapter.ClearBeforeFill = True
        '
        'FuncionariosActivosporEspecialidadTableAdapter
        '
        Me.FuncionariosActivosporEspecialidadTableAdapter.ClearBeforeFill = True
        '
        'FrmCitas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(903, 622)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Panel2)
        Me.Name = "FrmCitas"
        Me.Text = "FrmCitas"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCitas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgvCitasDia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PacientesActivosTodosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DB_A494E9_PFIngenieriaDataSet3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuncionariosActivosTodosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DB_A494E9_PFIngenieriaDataSet4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuncionariosActivosporEspecialidadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuncionariosXEspecialidadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExtraerHorasDisponiblesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuncionariosInactivosTodosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Button3 As Button
    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents tbxDescripcion As RichTextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cbPaciente As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents cbHora As ComboBox
    Friend WithEvents txtCedula As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents cbFuncionario As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtEdad As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Private WithEvents btnGuardar As Button
    Private WithEvents btnActualizar As Button
    Private WithEvents btnEliminar As Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtEdadPacienteDia As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents txtCedulaPacienteDia As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents dgvCitasDia As DataGridView
    Friend WithEvents rtbDescripcionPacienteDia As RichTextBox
    Friend WithEvents Button9 As Button
    Friend WithEvents Button10 As Button
    Friend WithEvents Button11 As Button
    Friend WithEvents txtFuncionarioPacienteDia As TextBox
    Friend WithEvents txtEspecialidadPacienteDia As TextBox
    Friend WithEvents txtHoraPacienteDia As TextBox
    Friend WithEvents txtNombrePacienteDia As TextBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents txtEspecialidad As TextBox
    Friend WithEvents dgvCitas As DataGridView
    Friend WithEvents DB_A494E9_PFIngenieriaDataSet3 As DB_A494E9_PFIngenieriaDataSet3
    Friend WithEvents PacientesActivosTodosBindingSource As BindingSource
    Friend WithEvents PacientesActivosTodosTableAdapter As DB_A494E9_PFIngenieriaDataSet3TableAdapters.pacientesActivosTodosTableAdapter
    Friend WithEvents ExtraerHorasDisponiblesBindingSource As BindingSource
    Friend WithEvents DB_A494E9_PFIngenieriaDataSet4 As DB_A494E9_PFIngenieriaDataSet4
    Friend WithEvents ExtraerHorasDisponiblesTableAdapter As DB_A494E9_PFIngenieriaDataSet4TableAdapters.extraerHorasDisponiblesTableAdapter
    Friend WithEvents FuncionariosActivosTodosBindingSource As BindingSource
    Friend WithEvents FuncionariosActivosTodosTableAdapter As DB_A494E9_PFIngenieriaDataSet4TableAdapters.funcionariosActivosTodosTableAdapter
    Friend WithEvents FuncionariosInactivosTodosBindingSource As BindingSource
    Friend WithEvents FuncionariosInactivosTodosTableAdapter As DB_A494E9_PFIngenieriaDataSet4TableAdapters.funcionariosInactivosTodosTableAdapter
    Friend WithEvents FuncionariosXEspecialidadBindingSource As BindingSource
    Friend WithEvents FuncionariosXEspecialidadTableAdapter As DB_A494E9_PFIngenieriaDataSet4TableAdapters.funcionariosXEspecialidadTableAdapter
    Friend WithEvents FuncionariosActivosporEspecialidadBindingSource As BindingSource
    Friend WithEvents FuncionariosActivosporEspecialidadTableAdapter As DB_A494E9_PFIngenieriaDataSet4TableAdapters.funcionariosActivosporEspecialidadTableAdapter
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents txtBusqueda As TextBox
    Private WithEvents btnLimpiar As Button
    Friend WithEvents Label21 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents dtpFechaCita As DateTimePicker
    Friend WithEvents Label26 As Label
End Class

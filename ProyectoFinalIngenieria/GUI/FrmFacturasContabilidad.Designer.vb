﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFacturasContabilidad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmFacturasContabilidad))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.btnBuscar2 = New System.Windows.Forms.Button()
        Me.btnBuscar1 = New System.Windows.Forms.Button()
        Me.lMonto = New System.Windows.Forms.Label()
        Me.lCalculoTotal = New System.Windows.Forms.Label()
        Me.txtMes = New System.Windows.Forms.TextBox()
        Me.lMes = New System.Windows.Forms.Label()
        Me.txtAño = New System.Windows.Forms.TextBox()
        Me.lAño = New System.Windows.Forms.Label()
        Me.lPaciente = New System.Windows.Forms.Label()
        Me.txtPaciente = New System.Windows.Forms.TextBox()
        Me.dtpFechaInicio = New System.Windows.Forms.DateTimePicker()
        Me.lFechaFinal = New System.Windows.Forms.Label()
        Me.dtpFechaFinal = New System.Windows.Forms.DateTimePicker()
        Me.lFechaInicio = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbConsulta = New System.Windows.Forms.ComboBox()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.lFecha = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel2.Controls.Add(Me.PictureBox3)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(1, 1)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(901, 71)
        Me.Panel2.TabIndex = 26
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(901, 3)
        Me.PictureBox3.TabIndex = 37
        Me.PictureBox3.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(458, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(125, 33)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Facturas"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(307, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(150, 33)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Consulta /"
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.DataGridView1)
        Me.Panel1.Controls.Add(Me.btnBuscar2)
        Me.Panel1.Controls.Add(Me.btnBuscar1)
        Me.Panel1.Controls.Add(Me.lMonto)
        Me.Panel1.Controls.Add(Me.lCalculoTotal)
        Me.Panel1.Controls.Add(Me.txtMes)
        Me.Panel1.Controls.Add(Me.lMes)
        Me.Panel1.Controls.Add(Me.txtAño)
        Me.Panel1.Controls.Add(Me.lAño)
        Me.Panel1.Controls.Add(Me.lPaciente)
        Me.Panel1.Controls.Add(Me.txtPaciente)
        Me.Panel1.Controls.Add(Me.dtpFechaInicio)
        Me.Panel1.Controls.Add(Me.lFechaFinal)
        Me.Panel1.Controls.Add(Me.dtpFechaFinal)
        Me.Panel1.Controls.Add(Me.lFechaInicio)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.cbConsulta)
        Me.Panel1.Controls.Add(Me.dtpFecha)
        Me.Panel1.Controls.Add(Me.lFecha)
        Me.Panel1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(1, 54)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(901, 567)
        Me.Panel1.TabIndex = 27
        '
        'DataGridView1
        '
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(323, 42)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(548, 432)
        Me.DataGridView1.TabIndex = 101
        '
        'btnBuscar2
        '
        Me.btnBuscar2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnBuscar2.FlatAppearance.BorderSize = 0
        Me.btnBuscar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscar2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscar2.ForeColor = System.Drawing.Color.White
        Me.btnBuscar2.Location = New System.Drawing.Point(89, 225)
        Me.btnBuscar2.Name = "btnBuscar2"
        Me.btnBuscar2.Size = New System.Drawing.Size(120, 35)
        Me.btnBuscar2.TabIndex = 100
        Me.btnBuscar2.Text = "Buscar"
        Me.btnBuscar2.UseVisualStyleBackColor = False
        Me.btnBuscar2.Visible = False
        '
        'btnBuscar1
        '
        Me.btnBuscar1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnBuscar1.FlatAppearance.BorderSize = 0
        Me.btnBuscar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscar1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscar1.ForeColor = System.Drawing.Color.White
        Me.btnBuscar1.Location = New System.Drawing.Point(89, 321)
        Me.btnBuscar1.Name = "btnBuscar1"
        Me.btnBuscar1.Size = New System.Drawing.Size(120, 35)
        Me.btnBuscar1.TabIndex = 99
        Me.btnBuscar1.Text = "Buscar"
        Me.btnBuscar1.UseVisualStyleBackColor = False
        Me.btnBuscar1.Visible = False
        '
        'lMonto
        '
        Me.lMonto.AutoSize = True
        Me.lMonto.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMonto.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lMonto.Location = New System.Drawing.Point(134, 468)
        Me.lMonto.Name = "lMonto"
        Me.lMonto.Size = New System.Drawing.Size(41, 44)
        Me.lMonto.TabIndex = 98
        Me.lMonto.Text = "0"
        Me.lMonto.Visible = False
        '
        'lCalculoTotal
        '
        Me.lCalculoTotal.AutoSize = True
        Me.lCalculoTotal.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lCalculoTotal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lCalculoTotal.Location = New System.Drawing.Point(85, 421)
        Me.lCalculoTotal.Name = "lCalculoTotal"
        Me.lCalculoTotal.Size = New System.Drawing.Size(135, 23)
        Me.lCalculoTotal.TabIndex = 97
        Me.lCalculoTotal.Text = "Cálculo total:"
        Me.lCalculoTotal.Visible = False
        '
        'txtMes
        '
        Me.txtMes.Location = New System.Drawing.Point(39, 256)
        Me.txtMes.Name = "txtMes"
        Me.txtMes.Size = New System.Drawing.Size(248, 27)
        Me.txtMes.TabIndex = 96
        Me.txtMes.Visible = False
        '
        'lMes
        '
        Me.lMes.AutoSize = True
        Me.lMes.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMes.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lMes.Location = New System.Drawing.Point(38, 225)
        Me.lMes.Name = "lMes"
        Me.lMes.Size = New System.Drawing.Size(192, 21)
        Me.lMes.TabIndex = 95
        Me.lMes.Text = "Inserte el mes deseado."
        Me.lMes.Visible = False
        '
        'txtAño
        '
        Me.txtAño.Location = New System.Drawing.Point(39, 169)
        Me.txtAño.Name = "txtAño"
        Me.txtAño.Size = New System.Drawing.Size(248, 27)
        Me.txtAño.TabIndex = 94
        Me.txtAño.Visible = False
        '
        'lAño
        '
        Me.lAño.AutoSize = True
        Me.lAño.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lAño.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lAño.Location = New System.Drawing.Point(38, 136)
        Me.lAño.Name = "lAño"
        Me.lAño.Size = New System.Drawing.Size(192, 21)
        Me.lAño.TabIndex = 93
        Me.lAño.Text = "Inserte el año deseado."
        Me.lAño.Visible = False
        '
        'lPaciente
        '
        Me.lPaciente.AutoSize = True
        Me.lPaciente.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lPaciente.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lPaciente.Location = New System.Drawing.Point(38, 136)
        Me.lPaciente.Name = "lPaciente"
        Me.lPaciente.Size = New System.Drawing.Size(235, 21)
        Me.lPaciente.TabIndex = 92
        Me.lPaciente.Text = "Paciente (Cedula o Nombre)"
        Me.lPaciente.Visible = False
        '
        'txtPaciente
        '
        Me.txtPaciente.Location = New System.Drawing.Point(38, 169)
        Me.txtPaciente.Name = "txtPaciente"
        Me.txtPaciente.Size = New System.Drawing.Size(248, 27)
        Me.txtPaciente.TabIndex = 91
        Me.txtPaciente.Visible = False
        '
        'dtpFechaInicio
        '
        Me.dtpFechaInicio.CustomFormat = "mm/dd/aa"
        Me.dtpFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInicio.Location = New System.Drawing.Point(38, 171)
        Me.dtpFechaInicio.Name = "dtpFechaInicio"
        Me.dtpFechaInicio.Size = New System.Drawing.Size(247, 27)
        Me.dtpFechaInicio.TabIndex = 90
        Me.dtpFechaInicio.Visible = False
        '
        'lFechaFinal
        '
        Me.lFechaFinal.AutoSize = True
        Me.lFechaFinal.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lFechaFinal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lFechaFinal.Location = New System.Drawing.Point(38, 225)
        Me.lFechaFinal.Name = "lFechaFinal"
        Me.lFechaFinal.Size = New System.Drawing.Size(99, 21)
        Me.lFechaFinal.TabIndex = 89
        Me.lFechaFinal.Text = "Fecha final:"
        Me.lFechaFinal.Visible = False
        '
        'dtpFechaFinal
        '
        Me.dtpFechaFinal.CustomFormat = "mm/dd/aa"
        Me.dtpFechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFinal.Location = New System.Drawing.Point(38, 256)
        Me.dtpFechaFinal.Name = "dtpFechaFinal"
        Me.dtpFechaFinal.Size = New System.Drawing.Size(249, 27)
        Me.dtpFechaFinal.TabIndex = 88
        Me.dtpFechaFinal.Visible = False
        '
        'lFechaInicio
        '
        Me.lFechaInicio.AutoSize = True
        Me.lFechaInicio.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lFechaInicio.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lFechaInicio.Location = New System.Drawing.Point(38, 136)
        Me.lFechaInicio.Name = "lFechaInicio"
        Me.lFechaInicio.Size = New System.Drawing.Size(106, 21)
        Me.lFechaInicio.TabIndex = 87
        Me.lFechaInicio.Text = "Fecha inicio:"
        Me.lFechaInicio.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.Location = New System.Drawing.Point(38, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 21)
        Me.Label3.TabIndex = 86
        Me.Label3.Text = "Buscar por:"
        '
        'cbConsulta
        '
        Me.cbConsulta.FormattingEnabled = True
        Me.cbConsulta.Items.AddRange(New Object() {"Por Año.", "Por Dia.", "Por Mes.", "Por Rango de fechas.", "Por Paciente.", ""})
        Me.cbConsulta.Location = New System.Drawing.Point(38, 76)
        Me.cbConsulta.Name = "cbConsulta"
        Me.cbConsulta.Size = New System.Drawing.Size(247, 29)
        Me.cbConsulta.TabIndex = 58
        '
        'dtpFecha
        '
        Me.dtpFecha.CustomFormat = "mm/dd/aa"
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(38, 169)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(245, 27)
        Me.dtpFecha.TabIndex = 37
        Me.dtpFecha.Visible = False
        '
        'lFecha
        '
        Me.lFecha.AutoSize = True
        Me.lFecha.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lFecha.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lFecha.Location = New System.Drawing.Point(38, 136)
        Me.lFecha.Name = "lFecha"
        Me.lFecha.Size = New System.Drawing.Size(63, 21)
        Me.lFecha.TabIndex = 36
        Me.lFecha.Text = "Fecha:"
        Me.lFecha.Visible = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnGuardar.FlatAppearance.BorderSize = 0
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.White
        Me.btnGuardar.Location = New System.Drawing.Point(751, 497)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(120, 35)
        Me.btnGuardar.TabIndex = 124
        Me.btnGuardar.Text = "Exportar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'FrmFacturasContabilidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(903, 622)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmFacturasContabilidad"
        Me.Text = "FrmFacturasContabilidad"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel2 As Panel
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents lPaciente As Label
    Friend WithEvents txtPaciente As TextBox
    Friend WithEvents dtpFechaInicio As DateTimePicker
    Friend WithEvents lFechaFinal As Label
    Friend WithEvents dtpFechaFinal As DateTimePicker
    Friend WithEvents lFechaInicio As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents cbConsulta As ComboBox
    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents lFecha As Label
    Friend WithEvents txtMes As TextBox
    Friend WithEvents lMes As Label
    Friend WithEvents txtAño As TextBox
    Friend WithEvents lAño As Label
    Friend WithEvents lMonto As Label
    Friend WithEvents lCalculoTotal As Label
    Private WithEvents btnBuscar1 As Button
    Private WithEvents btnBuscar2 As Button
    Friend WithEvents DataGridView1 As DataGridView
    Private WithEvents btnGuardar As Button
End Class

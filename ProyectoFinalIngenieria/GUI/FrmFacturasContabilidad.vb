﻿Public Class FrmFacturasContabilidad
    Private Sub cbConsulta_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbConsulta.SelectedValueChanged

        Dim v As Integer = cbConsulta.SelectedIndex

        Select Case v
            Case 0
                apagarTodos()
                lAño.Visible = True
                txtAño.Visible = True
                encenderMontos()
                btnBuscar2.Visible = True
            Case 1
                apagarTodos()
                lFecha.Visible = True
                dtpFecha.Visible = True
                encenderMontos()
            Case 2
                apagarTodos()
                lAño.Visible = True
                txtAño.Visible = True
                lMes.Visible = True
                txtMes.Visible = True
                encenderMontos()
                btnBuscar1.Visible = True
            Case 3
                apagarTodos()
                lFechaInicio.Visible = True
                dtpFechaInicio.Visible = True
                lFechaFinal.Visible = True
                dtpFechaFinal.Visible = True
                encenderMontos()
                btnBuscar1.Visible = True
            Case 4
                apagarTodos()
                lPaciente.Visible = True
                txtPaciente.Visible = True
                encenderMontos()
        End Select

    End Sub

    Sub apagarTodos()
        lAño.Visible = False
        txtAño.Visible = False
        lMes.Visible = False
        txtMes.Visible = False
        lFecha.Visible = False
        dtpFecha.Visible = False
        lFechaInicio.Visible = False
        dtpFechaInicio.Visible = False
        lFechaFinal.Visible = False
        dtpFechaFinal.Visible = False
        lPaciente.Visible = False
        txtPaciente.Visible = False
        lCalculoTotal.Visible = False
        lMonto.Visible = False
        btnBuscar1.Visible = False
        btnBuscar2.Visible = False
    End Sub

    Sub encenderMontos()
        lCalculoTotal.Visible = True
        lMonto.Visible = True
    End Sub
End Class
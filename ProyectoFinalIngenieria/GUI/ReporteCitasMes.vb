﻿Public Class ReporteCitasMes

    Dim Usuario As String
    Dim ID As Integer
    Dim Fecha As DateTime
    Dim Especialidad As String

    Public Sub New(UsuarioU As String, IDU As Integer, FechaU As DateTime, EspecialidadU As String)
        InitializeComponent()
        Usuario = UsuarioU
        ID = IDU
        Fecha = FechaU
        Especialidad = EspecialidadU
    End Sub


    Private Sub ReporteCitasMes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DataReportes.ConsultarCitasMes' Puede moverla o quitarla según sea necesario.
        Me.ConsultarCitasMesTableAdapter.Fill(Me.DataReportes.ConsultarCitasMes, Usuario, ID, Fecha, Especialidad)

    End Sub
End Class
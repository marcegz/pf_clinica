﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ReporteFacturaMedicina
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.DataReportes = New ProyectoFinalIngenieria.DataReportes()
        Me.getFacturaMedicinaReporteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.getFacturaMedicinaReporteTableAdapter = New ProyectoFinalIngenieria.DataReportesTableAdapters.getFacturaMedicinaReporteTableAdapter()
        Me.ConsultarHistorialMedicinaGeneralBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultarHistorialMedicinaGeneralTableAdapter = New ProyectoFinalIngenieria.DataReportesTableAdapters.ConsultarHistorialMedicinaGeneralTableAdapter()
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.getFacturaMedicinaReporteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultarHistorialMedicinaGeneralBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        ReportDataSource1.Name = "DataSet1"
        ReportDataSource1.Value = Me.getFacturaMedicinaReporteBindingSource
        ReportDataSource2.Name = "DataFactura"
        ReportDataSource2.Value = Me.ConsultarHistorialMedicinaGeneralBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ProyectoFinalIngenieria.FacturaMedicina.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(765, 581)
        Me.ReportViewer1.TabIndex = 0
        '
        'DataReportes
        '
        Me.DataReportes.DataSetName = "DataReportes"
        Me.DataReportes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'getFacturaMedicinaReporteBindingSource
        '
        Me.getFacturaMedicinaReporteBindingSource.DataMember = "getFacturaMedicinaReporte"
        Me.getFacturaMedicinaReporteBindingSource.DataSource = Me.DataReportes
        '
        'getFacturaMedicinaReporteTableAdapter
        '
        Me.getFacturaMedicinaReporteTableAdapter.ClearBeforeFill = True
        '
        'ConsultarHistorialMedicinaGeneralBindingSource
        '
        Me.ConsultarHistorialMedicinaGeneralBindingSource.DataMember = "ConsultarHistorialMedicinaGeneral"
        Me.ConsultarHistorialMedicinaGeneralBindingSource.DataSource = Me.DataReportes
        '
        'ConsultarHistorialMedicinaGeneralTableAdapter
        '
        Me.ConsultarHistorialMedicinaGeneralTableAdapter.ClearBeforeFill = True
        '
        'ReporteFacturaMedicina
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(765, 581)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "ReporteFacturaMedicina"
        Me.Text = "Reporte Factura"
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.getFacturaMedicinaReporteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultarHistorialMedicinaGeneralBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents getFacturaMedicinaReporteBindingSource As BindingSource
    Friend WithEvents DataReportes As DataReportes
    Friend WithEvents ConsultarHistorialMedicinaGeneralBindingSource As BindingSource
    Friend WithEvents getFacturaMedicinaReporteTableAdapter As DataReportesTableAdapters.getFacturaMedicinaReporteTableAdapter
    Friend WithEvents ConsultarHistorialMedicinaGeneralTableAdapter As DataReportesTableAdapters.ConsultarHistorialMedicinaGeneralTableAdapter
End Class

﻿Public Class FrmAtencionMedicinaGeneral
    Dim ID_Cit As Integer ' RECIBO X CONSTRUCTOR
    Dim ID_Dient As Integer ' RECIBO X CONSTRUCTOR
    Dim ID_Paci As Integer ' RECIBO X CONSTRUCTOR

    Dim fr_p As New FormPrincipal ' DEVOLVERME AL PRINCIPAL

    Dim x, y As Integer
    Dim newpoint As Point

    Dim ln_Paciente As New Ln_Paciente
    Dim ln_citas As New Ln_Citas
    Dim ln_funcionario As New Ln_Funcionario
    Dim ln_Horas As New Ln_Horas
    Dim ln_tratamiento As New Ln_Tratamientos
    Dim ln_consultas As New Ln_Consultas
    Dim ln_analisis As New Ln_Analisis
    Dim ln_histo As New Ln_Historial_Tratamientos_Consultas

    Dim lista_tratamientos As New List(Of Tratamientos)

    Dim paci As Pacientes
    Dim cita As Citas
    Dim hora As Horas

    Public Sub New(id_Cita As Integer, id_Paciente As Integer, fr_pr As FormPrincipal)
        InitializeComponent()
        ID_Cit = id_Cita
        ID_Paci = id_Paciente
        Me.fr_p = fr_pr

        Dim analisis As Analisis = ln_histo.sacarAnalisisConsulta(ID_Cit)
        If Not IsNothing(analisis) Then
            txtPeso.Text = analisis.Peso_
            txtPeso.Enabled = False
            txtTamaño.Text = analisis.Tamaño_
            txtTamaño.Enabled = False
            txtPresionArterial.Text = analisis.Presion_Arterial_
            txtPresionArterial.Enabled = False
        End If

        Dim identificacion_consulta As Integer = ln_histo.extraerIdConsulta(ID_Cit)

        Dim historialTratamientos As Historial_Tratamientos_Consultas = ln_histo.consultarHistorialTratamientosConsultas(identificacion_consulta)
        If Not IsNothing(historialTratamientos) Then
            tbxSintomas.Text = historialTratamientos.Sintomas1
            tbxSintomas.Enabled = False
        End If

        dgvTratamientos.Enabled = False
        Dim data As DataTable = ln_tratamiento.consultarTratamientosConsultas(identificacion_consulta)

        'dgvTratamientos.Rows.Add(data.Columns("ID_Tratamiento")., data.Columns("Nombre"),
        '                     data.Columns("Precio"))
        Dim int As Decimal
        For Each DR As DataRow In data.Rows
            'Dim tratam As New Tratamientos()

            dgvTratamientos.Rows.Add(DR.Item("ID_Tratamiento").ToString, DR.Item("Nombre").ToString,
                                     DR.Item("Descripcion").ToString, DR.Item("Precio").ToString)

            Int += Convert.ToDecimal(DR.Item("Precio").ToString)

        Next
        lMontoTotal.Text = int
    End Sub

    Public Sub New(fr_pr As FormPrincipal)
        InitializeComponent()
        Me.fr_p = fr_p
        ID_Cit = 0
    End Sub

    Private Sub FrmAtencionMedicinaGeneral_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If ID_Cit = 0 Then
            txtNombre.Visible = False
            cbPacientesDelDia.Visible = True
            llenarComboPacientesDia()
            llenarComboTratamientos()
            cbPacientesDelDia.SelectedIndex = -1
            limpiar()
        Else
            completarInfoPaciente(ID_Cit)
        End If
    End Sub

    Sub limpiar()
        txtCedula.Text = ""
        txtEdad.Text = ""
        txtFuncionario.Text = ""
        txtHora.Text = ""
        tbxSintomas.Text = ""
        rtbDescripcion.Text = ""
        txtPeso.Text = ""
        txtPresionArterial.Text = ""
        txtTamaño.Text = ""
        dgvTratamientos.Rows.Clear()
    End Sub


    Sub llenarComboPacientesDia()
        cbPacientesDelDia.DisplayMember = "NombreCompleto"
        cbPacientesDelDia.ValueMember = "Cedula"
        cbPacientesDelDia.DataSource = ln_citas.extraerPacientesDia(Date.Now, "Medicina General")
    End Sub

    Sub llenarComboTratamientos()
        cbxTratamientos.DisplayMember = "Nombre"
        cbxTratamientos.ValueMember = "ID_Tratamiento"
        cbxTratamientos.DataSource = ln_consultas.TratamientosActivosporEspecialidad("Medicina General")
    End Sub

    ' DETECTA EL CAMBIO DEL COMBO PACIENTES Y AGREGA LOS DATOS NECESARIOS
    Private Sub cbPacientesDelDia_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbPacientesDelDia.SelectedValueChanged

        Try

            paci = ln_Paciente.Consultar1Paciente(cbPacientesDelDia.SelectedValue)
            cbPacientesDelDia.Text = paci.NombreCompleto_
            txtCedula.Text = paci.Cedula_
            txtEdad.Text = getEdad(paci.FechaNacimiento_.Year())

            cita = ln_citas.ExtraerCita(getCita(cbPacientesDelDia.SelectedValue))
            hora = ln_Horas.Consultar1Hora(cita.ID_Hora_)

            txtFuncionario.Text = ln_funcionario.ConsultarFuncionario(cita.ID_Funcionario_)
            txtHora.Text = hora.Hora_.ToString
            rtbDescripcion.Text = cita.Descripcion_

            ID_Paci = paci.ID_Paciente_
            ID_Cit = cita.ID_Cita_

            'dgvTratamientos.DataSource = ln_tratamiento.tratamientosConsultaOdontologia(ID_Cit)
            'dgvTratamientos.Columns("ID_Historial").Visible = False

            lMontoTotal.Text = getSubTotal()

        Catch

        End Try
    End Sub

    Function getCita(cedula As String) As Integer

        Dim ID_Cita As Integer

        For Each p As DataRow In ln_citas.extraerPacientesDia(Date.Now, "Medicina General").Rows

            If Convert.ToString(p("Cedula")) = cedula Then
                ID_Cita = Convert.ToString(p("ID_Cita"))
            End If

        Next

        Return ID_Cita

    End Function


    Function getSubTotal() As Decimal

        Dim total As Decimal

        For cont As Integer = 0 To lista_tratamientos.Count - 1
            total += lista_tratamientos(cont).Precio_
        Next

        Return total

    End Function

    Function getEdad(año As Integer) As Integer
        Dim añoactual As Integer = Year(Now)
        getEdad = añoactual - año
    End Function

    Sub completarInfoPaciente(id As Integer)
        Dim paci As Pacientes
        Dim cita As Citas
        Dim hora As Horas

        cita = ln_citas.ExtraerCita(id)
        paci = ln_Paciente.ConsultarPacienteCOMPLETO(cita.ID_Paciente_)

        hora = ln_Horas.Consultar1Hora(cita.ID_Hora_)

        txtNombre.Visible = True
        cbPacientesDelDia.Visible = False
        txtNombre.Text = paci.NombreCompleto_
        txtCedula.Text = paci.Cedula_
        txtEdad.Text = getEdad(paci.FechaNacimiento_.Year())
        txtFuncionario.Text = ln_funcionario.ConsultarFuncionario(cita.ID_Funcionario_)
        txtHora.Text = hora.Hora_.ToString
        rtbDescripcion.Text = cita.Descripcion_

    End Sub

    'botones tabla tratamientos
    Private Sub btnAgregarTratamiento_Click(sender As Object, e As EventArgs) Handles btnAgregarTratamiento.Click

        Dim tratamiento As Tratamientos = ln_tratamiento.consultarTratamiento(cbxTratamientos.SelectedValue)

        If lista_tratamientos.Count = 0 Then
            lista_tratamientos.Add(tratamiento)
            dgvTratamientos.Rows.Add(lista_tratamientos(0).ID_Tratamiento_, lista_tratamientos(0).Nombre_,
                             lista_tratamientos(0).Descripcion_, lista_tratamientos(0).Precio_)
            lMontoTotal.Text = getSubTotal().ToString
        Else
            Dim contador As Integer
            For contador = 0 To lista_tratamientos.Count
                If lista_tratamientos(contador).ID_Tratamiento_ <> tratamiento.ID_Tratamiento_ Then
                    lista_tratamientos.Add(tratamiento)
                    dgvTratamientos.Rows.Add(tratamiento.ID_Tratamiento_, tratamiento.Nombre_,
                                     tratamiento.Descripcion_, tratamiento.Precio_)
                    lMontoTotal.Text = "" + getSubTotal().ToString
                Else
                    MsgBox("El tratamiento ya se encuentra registrado en la lista", MsgBoxStyle.Information, "Clinica Medica")
                End If
            Next contador

        End If

    End Sub

    Private Sub btnEliminarTratamiento_Click(sender As Object, e As EventArgs) Handles btnEliminarTratamiento.Click
        If MsgBox("¿Está seguro de eliminar el registro del tratammiento?" + dgvTratamientos.CurrentRow.Cells(0).Value.ToString, MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then
            For cont As Integer = 0 To lista_tratamientos.Count - 1
                If lista_tratamientos(cont).ID_Tratamiento_ = dgvTratamientos.CurrentRow.Cells(0).Value.ToString Then
                    lista_tratamientos.RemoveAt(cont)
                End If
            Next cont

            For contador As Integer = 0 To lista_tratamientos.Count - 1
                dgvTratamientos.Rows.Clear()
                dgvTratamientos.Rows.Add(lista_tratamientos(contador).ID_Tratamiento_, lista_tratamientos(contador).Nombre_,
                                     lista_tratamientos(contador).Descripcion_, lista_tratamientos(contador).Precio_)

            Next contador
            lMontoTotal.Text = "" + getSubTotal().ToString
        End If
    End Sub


    'Mover Formulario-----------------------------------------------------------------------------------

    Private Sub BarraTitulo_MouseDown(sender As Object, e As MouseEventArgs) Handles BarraTitulo.MouseDown
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub BarraTitulo_MouseMove(sender As Object, e As MouseEventArgs) Handles BarraTitulo.MouseMove
        If e.Button = MouseButtons.Left Then
            newpoint = Control.MousePosition
            newpoint.X -= x
            newpoint.Y -= y
            Me.Location = newpoint
            Application.DoEvents()
        End If
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        If MsgBox("¿Está seguro de cerrar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then

            Me.fr_p.Visible = True
            Me.Close()

        End If

    End Sub

    Private Sub btnListo_Click_1(sender As Object, e As EventArgs) Handles btnListo.Click

        If MsgBox("¿Finalizar Atención?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then
            ln_citas.actualizarEstadoCita(ID_Cit, "Atendido")

            If cbPacientesDelDia.Visible = False Then
                fr_p.Visible = True
                Me.Close()
            Else
                If (txtTamaño.Text <> "") And (txtPeso.Text <> "") And (txtPresionArterial.Text <> "") Then

                    If Not IsNothing(paci) Then
                        ln_analisis.insertarAnalisis(New Analisis(CInt(txtTamaño.Text), CInt(txtPeso.Text), CInt(txtPresionArterial.Text)))

                        Dim id_2 As Integer = ln_histo.extraerIdAnalisis()
                        ln_consultas.insertarConsulta(New Consultas(id_2, cita.ID_Cita_))


                        Dim id As Integer = ln_histo.extraerIdConsulta(ID_Cit)
                        For contador As Integer = 0 To lista_tratamientos.Count - 1
                            ln_histo.insertarHistorialTratamientosConsultas(New Historial_Tratamientos_Consultas(lista_tratamientos(contador).ID_Tratamiento_,
                                                                                                                 id, tbxSintomas.Text, Nothing))
                        Next

                        txtNombre.Visible = False
                        cbPacientesDelDia.Visible = True
                        llenarComboPacientesDia()
                        llenarComboTratamientos()
                        cbPacientesDelDia.SelectedIndex = -1
                        limpiar()

                    End If


                End If

                lMontoTotal.Text = getSubTotal()
                btnListo.Enabled = False

            End If
        End If
    End Sub

    Private Sub btnMinimizar_Click(sender As Object, e As EventArgs) Handles btnMinimizar.Click
        WindowState = FormWindowState.Minimized
    End Sub

End Class
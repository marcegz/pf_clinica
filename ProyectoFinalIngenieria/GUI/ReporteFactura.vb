﻿Public Class ReporteFactura

    Dim ID_Cita As Integer
    Dim ID_Factura As Integer

    Public Sub New(ID_CitaA As Integer, ID_FacturaU As Integer)
        InitializeComponent()
        ID_Cita = ID_CitaA
        ID_Factura = ID_FacturaU
    End Sub

    Private Sub Reporte_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.getFacturaReporteTableAdapter.Fill(Me.DataReportes.getFacturaReporte, ID_Factura)
        Me.ConsultarHistorialDientesTableAdapter.Fill(Me.DataReportes.ConsultarHistorialDientes, ID_Cita)
        Me.ReportViewer1.RefreshReport()
    End Sub

End Class
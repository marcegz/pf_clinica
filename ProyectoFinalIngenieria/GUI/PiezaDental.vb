﻿Imports System.Drawing.Drawing2D

Public Class PiezaDental
    Private partes(4) As GraphicsPath
    Private colores(4) As Brush
    Enum PartesPiezaDental
        Superior
        Inferior
        Derecha
        Izquierda
        Centro
        Desconocida
    End Enum
    Public Sub New(x As Integer, y As Integer, cx As Integer, cy As Integer)

        Dim pt1 As Point = New Point(x, y)
        Dim pt2 As Point = New Point(x + cx, y)
        Dim pt3 As Point = New Point(x, y + cy)
        Dim pt4 As Point = New Point(x + cx, y + cy)
        Dim pt5 As Point = New Point(x + cx \ 3, y + cy \ 3)
        Dim pt6 As Point = New Point(x + 2 * cx \ 3, y + cy \ 3)
        Dim pt7 As Point = New Point(x + cx \ 3, y + 2 * cy \ 3)
        Dim pt8 As Point = New Point(x + 2 * cx \ 3, y + 2 * cy \ 3)

        partes(PartesPiezaDental.Superior) = New GraphicsPath()
        partes(PartesPiezaDental.Superior).AddPolygon(New Point() {pt1, pt2, pt6, pt5})
        colores(PartesPiezaDental.Superior) = Brushes.White

        partes(PartesPiezaDental.Izquierda) = New GraphicsPath()
        partes(PartesPiezaDental.Izquierda).AddPolygon(New Point() {pt1, pt3, pt7, pt5})
        colores(PartesPiezaDental.Izquierda) = Brushes.White

        partes(PartesPiezaDental.Inferior) = New GraphicsPath()
        partes(PartesPiezaDental.Inferior).AddPolygon(New Point() {pt3, pt4, pt8, pt7})
        colores(PartesPiezaDental.Inferior) = Brushes.White

        partes(PartesPiezaDental.Derecha) = New GraphicsPath()
        partes(PartesPiezaDental.Derecha).AddPolygon(New Point() {pt4, pt2, pt6, pt8})
        colores(PartesPiezaDental.Derecha) = Brushes.White

        partes(PartesPiezaDental.Centro) = New GraphicsPath()
        partes(PartesPiezaDental.Centro).AddPolygon(New Point() {pt5, pt6, pt8, pt7})
        colores(PartesPiezaDental.Centro) = Brushes.White

    End Sub
    Public Sub Dibuja(g As Graphics)
        For Each p As GraphicsPath In partes
            g.FillPath(colores(Array.IndexOf(partes, p)), p)
            g.DrawPath(Pens.Black, p)
        Next
    End Sub

    Public Function DetectaPartePiezaDental(pt As Point) As PartesPiezaDental
        For Each p As GraphicsPath In partes
            If p.IsVisible(pt) Then
                Return CType(Array.IndexOf(partes, p), PartesPiezaDental)
            End If
        Next

        Return PartesPiezaDental.Desconocida
    End Function

    Public Sub ColoreaParte(b As Brush, p As PartesPiezaDental)
        colores(p) = b
    End Sub

    Public Function RectPartePiezaDental(p As PartesPiezaDental) As Rectangle
        Return Rectangle.Truncate(partes(p).GetBounds())
    End Function
End Class

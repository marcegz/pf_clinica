﻿Public Class ReporteCitasRangos

    Dim Usuario As String
    Dim ID As Integer
    Dim Fecha1 As DateTime
    Dim Fecha2 As DateTime
    Dim Especialidad As String

    Public Sub New(UsuarioU As String, IDU As Integer, FechaUno As DateTime, FechaDos As DateTime, EspecialidadU As String)
        InitializeComponent()
        Usuario = UsuarioU
        ID = IDU
        Fecha1 = FechaUno
        Fecha2 = FechaUno
        Especialidad = EspecialidadU
    End Sub


    Private Sub ReporteCitasRangos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DataReportes.ConsultarCitasRangoFechas' Puede moverla o quitarla según sea necesario.
        Me.ConsultarCitasRangoFechasTableAdapter.Fill(Me.DataReportes.ConsultarCitasRangoFechas, Usuario, ID, Fecha1, Fecha2, Especialidad)

    End Sub
End Class
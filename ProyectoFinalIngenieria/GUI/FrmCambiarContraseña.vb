﻿Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Mail

Public Class FrmCambiarContraseña

    Dim lx, ly As Integer
    Dim sw, sh As Integer

    Dim x, y As Integer
    Dim newpoint As Point

    Private Sub FrmCambiarContraseña_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Abrir()
    End Sub

    '------------------------------

    Dim randomCode As String
    Dim para As String



    Private Sub txtContraseñaActual_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtContraseñaActual.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            If txtContraseñaActual.Text = "" Then
                msgError("Debe ingresar la clave")
                txtContraseñaActual.Text = ""
                txtContraseñaActual.Focus()
            Else
                If txtContraseñaActual.Text = Contraseña_Funcionario Then
                    txtNuevaContraseña.Enabled = True
                    txtConfirmarContraseña.Enabled = True
                    txtContraseñaActual.Enabled = False
                    txtContraseñaActual.Focus()
                Else
                    msgError("Contraseña incorrecta, Por Favor verifique")
                    txtNuevaContraseña.Enabled = False
                    txtConfirmarContraseña.Enabled = False
                    txtContraseñaActual.Text = ""
                    txtContraseñaActual.Focus()
                End If
            End If
        End If
    End Sub




    Private Sub btnEnviarCodigo_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        If txtNuevaContraseña.Text = "" Or txtNuevaContraseña.Text = "Nueva Contraseña" Or txtConfirmarContraseña.Text = "" Or txtConfirmarContraseña.Text = "Confirmar Contraseña" Then
            msgError("Los campos no pueden estar vacíos")
            txtNuevaContraseña.Focus()
        Else
            If txtNuevaContraseña.Text = txtConfirmarContraseña.Text Then
                Dim myCommand As New SqlCommand("update Funcionarios set Contraseña = '" & txtNuevaContraseña.Text & "' where Cedula = '" & Ced_Funcionario & "'", conn)
                myCommand.ExecuteNonQuery()
                msgError("La contraseña se cambio exitosamente")
                Contraseña_Funcionario = txtNuevaContraseña.Text
                ' Me.Close()
                Me.Hide()
                FrmLogin.Show()
            Else
                msgError("La nueva clave y la confirmación NO COINCIDEN")
                txtNuevaContraseña.Text = ""
                txtConfirmarContraseña.Text = ""
                txtNuevaContraseña.Focus()
            End If
        End If
    End Sub

    Public Sub msgError(msg As String)
        txtContraseñaActual.Text = ""
        lMensajeError.Text = "     " + msg
        lMensajeError.Visible = True

    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        'Application.Exit()
        If MsgBox("¿Está seguro de cerrar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then
            End
        End If
    End Sub









    '------------- ENTRAR Y SALIR DEL txtContraseñaActual

    Private Sub txtContraseñaActual_Enter(sender As Object, e As EventArgs) Handles txtContraseñaActual.Enter
        If txtContraseñaActual.Text = "Código Verificación" Then
            txtContraseñaActual.Text = ""
            txtContraseñaActual.ForeColor = Color.LightGray
        End If
    End Sub

    Private Sub txtContraseñaActual_Leave(sender As Object, e As EventArgs) Handles txtContraseñaActual.Leave
        If txtContraseñaActual.Text = "" Then
            txtContraseñaActual.Text = "Código Verificación"
            txtContraseñaActual.ForeColor = Color.DarkGray
        End If
    End Sub

    '------------- ENTRAR Y SALIR DEL txtNuevaContraseña

    Private Sub txtNuevaContraseña_Enter(sender As Object, e As EventArgs) Handles txtNuevaContraseña.Enter
        If txtNuevaContraseña.Text = "Nueva Contraseña" Then
            txtNuevaContraseña.Text = ""
            txtNuevaContraseña.ForeColor = Color.LightGray
            txtNuevaContraseña.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub txtNuevaContraseña_Leave(sender As Object, e As EventArgs) Handles txtNuevaContraseña.Leave
        If txtNuevaContraseña.Text = "" Then
            txtNuevaContraseña.Text = "Nueva Contraseña"
            txtNuevaContraseña.ForeColor = Color.DarkGray
            txtNuevaContraseña.UseSystemPasswordChar = False
        End If
    End Sub

    '------------- ENTRAR Y SALIR DEL txtConfirmarContraseña

    Private Sub txtConfirmarContraseña_Enter(sender As Object, e As EventArgs) Handles txtConfirmarContraseña.Enter
        If txtConfirmarContraseña.Text = "Confirmar Contraseña" Then
            txtConfirmarContraseña.Text = ""
            txtConfirmarContraseña.ForeColor = Color.LightGray
            txtConfirmarContraseña.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub btnVisualizarOcultarNuevaContraseña_Click(sender As Object, e As EventArgs) Handles btnVisualizarOcultarNuevaContraseña.Click

        If txtNuevaContraseña.UseSystemPasswordChar = True Then
            txtNuevaContraseña.UseSystemPasswordChar = False
            btnVisualizarOcultarNuevaContraseña.Image = Image.FromFile("C:\Users\Usuario\Dropbox\PROYECTO FINAL INGENIERIA\VB\ProyectoFinalIngenieria\ProyectoFinalIngenieria\Resources\ocultarContraseña.png")

        ElseIf txtNuevaContraseña.UseSystemPasswordChar = False Then
            txtNuevaContraseña.UseSystemPasswordChar = True
            btnVisualizarOcultarNuevaContraseña.Image = Image.FromFile("C:\Users\Usuario\Dropbox\PROYECTO FINAL INGENIERIA\VB\ProyectoFinalIngenieria\ProyectoFinalIngenieria\Resources\verContraseña.png")
        End If

    End Sub

    Private Sub btnVisualizarOcultarConfirmarContraseña_Click(sender As Object, e As EventArgs) Handles btnVisualizarOcultarConfirmarContraseña.Click

        If txtConfirmarContraseña.UseSystemPasswordChar = True Then
            txtConfirmarContraseña.UseSystemPasswordChar = False
            btnVisualizarOcultarNuevaContraseña.Image = Image.FromFile("C:\Users\Usuario\Dropbox\PROYECTO FINAL INGENIERIA\VB\ProyectoFinalIngenieria\ProyectoFinalIngenieria\Resources\ocultarContraseña.png")

        ElseIf txtConfirmarContraseña.UseSystemPasswordChar = False Then
            txtConfirmarContraseña.UseSystemPasswordChar = True
            btnVisualizarOcultarNuevaContraseña.Image = Image.FromFile("C:\Users\Usuario\Dropbox\PROYECTO FINAL INGENIERIA\VB\ProyectoFinalIngenieria\ProyectoFinalIngenieria\Resources\verContraseña.png")
        End If

    End Sub

    Private Sub btnMinimizar_Click(sender As Object, e As EventArgs) Handles btnMinimizar.Click
        WindowState = FormWindowState.Minimized
    End Sub

    Private Sub txtConfirmarContraseña_Leave(sender As Object, e As EventArgs) Handles txtConfirmarContraseña.Leave
        If txtConfirmarContraseña.Text = "" Then
            txtConfirmarContraseña.Text = "Confirmar Contraseña"
            txtConfirmarContraseña.ForeColor = Color.DarkGray
            txtConfirmarContraseña.UseSystemPasswordChar = False
        End If
    End Sub




    'Mover Formulario-------------------------------------------------------------------------------------

    Private Sub FrmRecuperarContraseña_MouseDown(sender As Object, e As MouseEventArgs) Handles MyBase.MouseDown
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub FrmRecuperarContraseña_MouseMove(sender As Object, e As MouseEventArgs) Handles MyBase.MouseMove
        If e.Button = MouseButtons.Left Then
            newpoint = Control.MousePosition
            newpoint.X -= x
            newpoint.Y -= y
            Me.Location = newpoint
            Application.DoEvents()
        End If
    End Sub


    '-------------------------------------------------------------------------------------


End Class
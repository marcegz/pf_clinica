﻿Public Class FrmReportesGeneral

    Dim ln_paciente As New Ln_Paciente
    Dim ln_tratamiento As New Ln_Tratamientos

    Private Sub FrmReportesGeneral_Load(sender As Object, e As EventArgs) Handles Me.Load

        lCedula.Visible = False
        lAsterisco.Visible = False
        txtConsultaPersona.Visible = False
        cbxBusqueda.SelectedIndex = 0

    End Sub

    Private Sub cbxBusqueda_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbxBusqueda.SelectedValueChanged

        Select Case cbxBusqueda.SelectedIndex

            Case 0
                dgvReportesGenerales.DataSource = ln_paciente.ConsultarPacientesReporte("Odontología")
                dgvReportesGenerales.Columns(4).Visible = False

            Case 1
                dgvReportesGenerales.DataSource = ln_paciente.ConsultarPacientesReporte("Medicina General")
                dgvReportesGenerales.Columns(4).Visible = False

            Case 2
                dgvReportesGenerales.DataSource = ln_tratamiento.ConsultarTratamientosReporte("Odontología")
                dgvReportesGenerales.Columns(3).Visible = False

            Case 3
                dgvReportesGenerales.DataSource = ln_tratamiento.ConsultarTratamientosReporte("Medicina General")
                dgvReportesGenerales.Columns(3).Visible = False

            Case 4
                lCedula.Visible = True
                lAsterisco.Visible = True
                txtConsultaPersona.Visible = True

        End Select

    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Dim especialidad = ""

        If cbxBusqueda.SelectedIndex = 0 Or cbxBusqueda.SelectedIndex = 2 Then
            especialidad = "Odontología"
        End If

        If cbxBusqueda.SelectedIndex = 1 Or cbxBusqueda.SelectedIndex = 3 Then
            especialidad = "Medicina General"
        End If


        If cbxBusqueda.SelectedIndex = 0 Or cbxBusqueda.SelectedIndex = 1 Then
            Dim reporte As New ReportePacientes(especialidad)
            reporte.Show()
        End If

        If cbxBusqueda.SelectedIndex = 2 Or cbxBusqueda.SelectedIndex = 3 Then
            Dim reporte As New ReporteTratamientos(especialidad)
            reporte.Show()
        End If

    End Sub


    Private Sub dgvReportesGenerales_DataSourceChanged(sender As Object, e As EventArgs) Handles dgvReportesGenerales.DataSourceChanged
        If dgvReportesGenerales.Rows.Count = 0 Then
            dgvReportesGenerales.DataSource = Nothing
            lMensaje.Visible = True
        Else
            lMensaje.Visible = False
        End If
    End Sub

End Class
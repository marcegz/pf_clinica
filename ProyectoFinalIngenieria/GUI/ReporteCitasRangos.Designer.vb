﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ReporteCitasRangos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.DataReportes = New ProyectoFinalIngenieria.DataReportes()
        Me.ConsultarCitasRangoFechasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultarCitasRangoFechasTableAdapter = New ProyectoFinalIngenieria.DataReportesTableAdapters.ConsultarCitasRangoFechasTableAdapter()
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultarCitasRangoFechasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        ReportDataSource1.Name = "DataReporte"
        ReportDataSource1.Value = Me.ConsultarCitasRangoFechasBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ProyectoFinalIngenieria.ReporteCitasRangos.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(855, 449)
        Me.ReportViewer1.TabIndex = 0
        '
        'DataReportes
        '
        Me.DataReportes.DataSetName = "DataReportes"
        Me.DataReportes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConsultarCitasRangoFechasBindingSource
        '
        Me.ConsultarCitasRangoFechasBindingSource.DataMember = "ConsultarCitasRangoFechas"
        Me.ConsultarCitasRangoFechasBindingSource.DataSource = Me.DataReportes
        '
        'ConsultarCitasRangoFechasTableAdapter
        '
        Me.ConsultarCitasRangoFechasTableAdapter.ClearBeforeFill = True
        '
        'ReporteCitasRangos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(856, 450)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "ReporteCitasRangos"
        Me.Text = "Reporte Citas Rangos"
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultarCitasRangoFechasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents ConsultarCitasRangoFechasBindingSource As BindingSource
    Friend WithEvents DataReportes As DataReportes
    Friend WithEvents ConsultarCitasRangoFechasTableAdapter As DataReportesTableAdapters.ConsultarCitasRangoFechasTableAdapter
End Class

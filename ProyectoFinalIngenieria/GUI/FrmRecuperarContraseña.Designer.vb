﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRecuperarContraseña
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRecuperarContraseña))
        Me.btnEnviarCodigo = New System.Windows.Forms.Button()
        Me.lRecuperarContraseña = New System.Windows.Forms.Label()
        Me.txtCedula = New System.Windows.Forms.TextBox()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.btnMinimizar = New System.Windows.Forms.PictureBox()
        Me.btnCerrar = New System.Windows.Forms.PictureBox()
        Me.lMensajeError = New System.Windows.Forms.Label()
        Me.btnVerificarCódigo = New System.Windows.Forms.Button()
        Me.txtCódigoVerificación = New System.Windows.Forms.TextBox()
        Me.lVerificarCódigo = New System.Windows.Forms.Label()
        Me.lUsuario = New System.Windows.Forms.Label()
        CType(Me.btnMinimizar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCerrar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnEnviarCodigo
        '
        Me.btnEnviarCodigo.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnEnviarCodigo.FlatAppearance.BorderSize = 0
        Me.btnEnviarCodigo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(28, Byte), Integer), CType(CType(28, Byte), Integer))
        Me.btnEnviarCodigo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnEnviarCodigo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEnviarCodigo.ForeColor = System.Drawing.Color.LightGray
        Me.btnEnviarCodigo.Location = New System.Drawing.Point(63, 243)
        Me.btnEnviarCodigo.Name = "btnEnviarCodigo"
        Me.btnEnviarCodigo.Size = New System.Drawing.Size(408, 40)
        Me.btnEnviarCodigo.TabIndex = 12
        Me.btnEnviarCodigo.Text = "ENVIAR CÓDIGO"
        Me.btnEnviarCodigo.UseVisualStyleBackColor = False
        '
        'lRecuperarContraseña
        '
        Me.lRecuperarContraseña.AutoSize = True
        Me.lRecuperarContraseña.Font = New System.Drawing.Font("Century Gothic", 20.0!)
        Me.lRecuperarContraseña.ForeColor = System.Drawing.Color.DarkGray
        Me.lRecuperarContraseña.Location = New System.Drawing.Point(93, 9)
        Me.lRecuperarContraseña.Name = "lRecuperarContraseña"
        Me.lRecuperarContraseña.Size = New System.Drawing.Size(349, 33)
        Me.lRecuperarContraseña.TabIndex = 13
        Me.lRecuperarContraseña.Text = "RECUPERAR CONTRASEÑA"
        '
        'txtCedula
        '
        Me.txtCedula.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer))
        Me.txtCedula.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCedula.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.txtCedula.ForeColor = System.Drawing.Color.DarkGray
        Me.txtCedula.Location = New System.Drawing.Point(65, 131)
        Me.txtCedula.Name = "txtCedula"
        Me.txtCedula.Size = New System.Drawing.Size(408, 20)
        Me.txtCedula.TabIndex = 11
        Me.txtCedula.Text = "CÉDULA"
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.Color.DarkGray
        Me.LineShape1.Enabled = False
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 65
        Me.LineShape1.X2 = 472
        Me.LineShape1.Y1 = 131
        Me.LineShape1.Y2 = 131
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(530, 330)
        Me.ShapeContainer1.TabIndex = 15
        Me.ShapeContainer1.TabStop = False
        '
        'btnMinimizar
        '
        Me.btnMinimizar.BackgroundImage = Global.ProyectoFinalIngenieria.My.Resources.Resources.Minimize
        Me.btnMinimizar.Image = Global.ProyectoFinalIngenieria.My.Resources.Resources.Minimize1
        Me.btnMinimizar.Location = New System.Drawing.Point(491, 3)
        Me.btnMinimizar.Name = "btnMinimizar"
        Me.btnMinimizar.Size = New System.Drawing.Size(15, 15)
        Me.btnMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnMinimizar.TabIndex = 17
        Me.btnMinimizar.TabStop = False
        '
        'btnCerrar
        '
        Me.btnCerrar.Image = Global.ProyectoFinalIngenieria.My.Resources.Resources.Close
        Me.btnCerrar.Location = New System.Drawing.Point(512, 3)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(15, 15)
        Me.btnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCerrar.TabIndex = 16
        Me.btnCerrar.TabStop = False
        '
        'lMensajeError
        '
        Me.lMensajeError.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.5!)
        Me.lMensajeError.ForeColor = System.Drawing.Color.DarkGray
        Me.lMensajeError.Image = CType(resources.GetObject("lMensajeError.Image"), System.Drawing.Image)
        Me.lMensajeError.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.lMensajeError.Location = New System.Drawing.Point(60, 188)
        Me.lMensajeError.Name = "lMensajeError"
        Me.lMensajeError.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lMensajeError.Size = New System.Drawing.Size(410, 39)
        Me.lMensajeError.TabIndex = 14
        Me.lMensajeError.Text = "Error"
        Me.lMensajeError.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lMensajeError.Visible = False
        '
        'btnVerificarCódigo
        '
        Me.btnVerificarCódigo.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnVerificarCódigo.FlatAppearance.BorderSize = 0
        Me.btnVerificarCódigo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(28, Byte), Integer), CType(CType(28, Byte), Integer))
        Me.btnVerificarCódigo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnVerificarCódigo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVerificarCódigo.ForeColor = System.Drawing.Color.LightGray
        Me.btnVerificarCódigo.Location = New System.Drawing.Point(64, 243)
        Me.btnVerificarCódigo.Name = "btnVerificarCódigo"
        Me.btnVerificarCódigo.Size = New System.Drawing.Size(408, 40)
        Me.btnVerificarCódigo.TabIndex = 18
        Me.btnVerificarCódigo.Text = "VERIFICAR CÓDIGO"
        Me.btnVerificarCódigo.UseVisualStyleBackColor = False
        Me.btnVerificarCódigo.Visible = False
        '
        'txtCódigoVerificación
        '
        Me.txtCódigoVerificación.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer))
        Me.txtCódigoVerificación.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCódigoVerificación.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.txtCódigoVerificación.ForeColor = System.Drawing.Color.DarkGray
        Me.txtCódigoVerificación.Location = New System.Drawing.Point(64, 131)
        Me.txtCódigoVerificación.Name = "txtCódigoVerificación"
        Me.txtCódigoVerificación.Size = New System.Drawing.Size(408, 20)
        Me.txtCódigoVerificación.TabIndex = 19
        Me.txtCódigoVerificación.Text = "CÓDIGO VERIFICACIÓN"
        Me.txtCódigoVerificación.Visible = False
        '
        'lVerificarCódigo
        '
        Me.lVerificarCódigo.AutoSize = True
        Me.lVerificarCódigo.Font = New System.Drawing.Font("Century Gothic", 20.0!)
        Me.lVerificarCódigo.ForeColor = System.Drawing.Color.DarkGray
        Me.lVerificarCódigo.Location = New System.Drawing.Point(131, 9)
        Me.lVerificarCódigo.Name = "lVerificarCódigo"
        Me.lVerificarCódigo.Size = New System.Drawing.Size(271, 33)
        Me.lVerificarCódigo.TabIndex = 20
        Me.lVerificarCódigo.Text = "VERIFICAR CÓDIGO"
        Me.lVerificarCódigo.Visible = False
        '
        'lUsuario
        '
        Me.lUsuario.AutoSize = True
        Me.lUsuario.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lUsuario.ForeColor = System.Drawing.Color.DarkGray
        Me.lUsuario.Location = New System.Drawing.Point(60, 87)
        Me.lUsuario.Name = "lUsuario"
        Me.lUsuario.Size = New System.Drawing.Size(86, 21)
        Me.lUsuario.TabIndex = 21
        Me.lUsuario.Text = "USUARIO:"
        '
        'FrmRecuperarContraseña
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(530, 330)
        Me.Controls.Add(Me.lUsuario)
        Me.Controls.Add(Me.lVerificarCódigo)
        Me.Controls.Add(Me.txtCódigoVerificación)
        Me.Controls.Add(Me.btnVerificarCódigo)
        Me.Controls.Add(Me.btnMinimizar)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.lMensajeError)
        Me.Controls.Add(Me.btnEnviarCodigo)
        Me.Controls.Add(Me.lRecuperarContraseña)
        Me.Controls.Add(Me.txtCedula)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmRecuperarContraseña"
        Me.Opacity = 0.9R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmRecuperarContraseña"
        CType(Me.btnMinimizar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCerrar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lMensajeError As Label
    Friend WithEvents btnEnviarCodigo As Button
    Friend WithEvents lRecuperarContraseña As Label
    Friend WithEvents txtCedula As TextBox
    Friend WithEvents LineShape1 As PowerPacks.LineShape
    Friend WithEvents ShapeContainer1 As PowerPacks.ShapeContainer
    Friend WithEvents btnMinimizar As PictureBox
    Friend WithEvents btnCerrar As PictureBox
    Friend WithEvents btnVerificarCódigo As Button
    Friend WithEvents txtCódigoVerificación As TextBox
    Friend WithEvents lVerificarCódigo As Label
    Friend WithEvents lUsuario As Label
End Class

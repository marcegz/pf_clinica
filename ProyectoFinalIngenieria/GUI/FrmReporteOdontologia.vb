﻿Public Class FrmReporteOdontologia

    Dim ln_paci As New Ln_Paciente
    Dim ln_funcionario As New Ln_Funcionario
    Dim ln_citas As New Ln_Citas
    Dim n = 0

    Private Sub FrmReporteOdontologia_Load(sender As Object, e As EventArgs) Handles Me.Load
        llenarComboFuncionarios()
        llenarComboPacientes()
        cbConsulta.SelectedIndex = 0
        cbFuncionarios.Visible = False
        lFuncionario.Visible = False
        cbxBusqueda.SelectedIndex = 0
    End Sub

    Sub apagarTodos()
        lAño.Visible = False
        cbAño.Visible = False
        lMes.Visible = False
        cbMes.Visible = False
        lFecha.Visible = False
        dtpFecha.Visible = False
        lFechaInicio.Visible = False
        dtpFechaInicio.Visible = False
        lFechaFinal.Visible = False
        dtpFechaFinal.Visible = False
    End Sub

    Private Sub cbxBusqueda_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbxBusqueda.SelectedValueChanged

        dgvCitasOdontologia.DataSource = Nothing

        Select Case cbxBusqueda.SelectedIndex

            Case 0
                apagarTodos()
                lFecha.Visible = True
                dtpFecha.Visible = True
                dia()

            Case 1
                apagarTodos()
                lAño.Visible = True
                cbAño.Visible = True
                lMes.Visible = True
                cbMes.Visible = True
                cbMes.SelectedIndex = 0
                cbAño.SelectedIndex = 0

            Case 2
                apagarTodos()
                lAño.Visible = True
                cbAño.Visible = True
                cbAño.SelectedIndex = 1
                año()

            Case 3
                apagarTodos()
                lFechaInicio.Visible = True
                dtpFechaInicio.Visible = True
                lFechaFinal.Visible = True
                dtpFechaFinal.Visible = True
                dtpFechaInicio.Value = Date.Now
                dtpFechaFinal.Value = Date.Now
                rangoFechas()

        End Select
    End Sub

    Sub llenarComboFuncionarios()
        cbFuncionarios.DisplayMember = "NombreCompleto"
        cbFuncionarios.ValueMember = "ID_Funcionario"
        cbFuncionarios.DataSource = ln_funcionario.FuncionariosReporte("Odontología")
        cbFuncionarios.AutoCompleteCustomSource = getStringCollectionCombo(ln_funcionario.FuncionariosReporte("Odontología"))

    End Sub

    Sub llenarComboPacientes()
        cbPacientes.DisplayMember = "NombreCompleto"
        cbPacientes.ValueMember = "ID_Paciente"
        cbPacientes.DataSource = ln_paci.PacientesReporte()
        cbPacientes.AutoCompleteCustomSource = getStringCollectionCombo(ln_paci.PacientesReporte())
    End Sub

    Function getStringCollectionCombo(tabla As DataTable) As AutoCompleteStringCollection

        Dim collecion As New AutoCompleteStringCollection

        For Each row As DataRow In tabla.Rows

            collecion.Add(row("NombreCompleto"))

        Next

        Return collecion

    End Function

    Private Sub cbConsulta_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbConsulta.SelectedValueChanged

        If cbConsulta.SelectedItem.ToString = "Paciente" Then
            cbPacientes.Visible = True
            lPaciente.Visible = True
            cbFuncionarios.Visible = False
            lFuncionario.Visible = False
        Else
            cbPacientes.Visible = False
            lPaciente.Visible = False
            cbFuncionarios.Visible = True
            lFuncionario.Visible = True
        End If

        ejecutar()

    End Sub

    Sub ocultarColumnas()
        dgvCitasOdontologia.Columns(0).Visible = False
        dgvCitasOdontologia.Columns(1).Visible = False
        dgvCitasOdontologia.Columns(3).Visible = False
        dgvCitasOdontologia.Columns(5).Visible = False
        dgvCitasOdontologia.Columns(10).Visible = False
        dgvCitasOdontologia.Columns(6).HeaderText = "ID Paciente"
        dgvCitasOdontologia.Columns(8).HeaderText = "ID Funcionario"
    End Sub

    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged
        dia()
    End Sub

    Private Sub cbAño_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbAño.SelectedValueChanged
        If cbxBusqueda.SelectedIndex = 1 Then
            If cbMes.SelectedIndex = 0 Or cbAño.SelectedIndex = 0 Then
            Else
                mes()
            End If

        Else
            If cbxBusqueda.SelectedIndex = 2 Then
                año()
            End If
        End If
    End Sub

    Private Sub cbFuncionarios_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbFuncionarios.SelectedValueChanged
        ejecutar()
    End Sub

    Private Sub cbPacientes_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbPacientes.SelectedValueChanged
        ejecutar()
    End Sub

    Sub dia()
        If cbConsulta.SelectedItem.ToString = "Paciente" Then
            dgvCitasOdontologia.DataSource = ln_citas.ConsultaCitasDia(cbConsulta.SelectedItem.ToString, cbPacientes.SelectedValue, dtpFecha.Value, "Odontología")
        Else
            dgvCitasOdontologia.DataSource = ln_citas.ConsultaCitasDia(cbConsulta.SelectedItem.ToString, cbFuncionarios.SelectedValue, dtpFecha.Value, "Odontología")
        End If
    End Sub

    Sub mes()
        If cbConsulta.SelectedItem.ToString = "Paciente" Then
            dgvCitasOdontologia.DataSource = ln_citas.ConsultaCitasMes(cbConsulta.SelectedItem.ToString, cbPacientes.SelectedValue, Convert.ToDateTime("01-" & getMes(cbMes.SelectedItem.ToString) & "-" & cbAño.SelectedItem.ToString), "Odontología")
        Else
            dgvCitasOdontologia.DataSource = ln_citas.ConsultaCitasMes(cbConsulta.SelectedItem.ToString, cbFuncionarios.SelectedValue, Convert.ToDateTime("01-" & getMes(cbMes.SelectedItem.ToString) & "-" & cbAño.SelectedItem.ToString), "Odontología")
        End If
    End Sub

    Sub año()
        If cbConsulta.SelectedItem.ToString = "Paciente" Then
            dgvCitasOdontologia.DataSource = ln_citas.ConsultaCitasAño(cbConsulta.SelectedItem.ToString, cbPacientes.SelectedValue, Convert.ToDateTime("01-01-" & cbAño.SelectedItem.ToString), "Odontología")
        Else
            dgvCitasOdontologia.DataSource = ln_citas.ConsultaCitasAño(cbConsulta.SelectedItem.ToString, cbFuncionarios.SelectedValue, Convert.ToDateTime("01-01-" & cbAño.SelectedItem.ToString), "Odontología")
        End If
    End Sub

    Sub rangoFechas()

        If dtpFechaInicio.Value.Date <= dtpFechaFinal.Value.Date Then
            If cbConsulta.SelectedItem.ToString = "Paciente" Then
                dgvCitasOdontologia.DataSource = ln_citas.ConsultaCitasRangoFechas(cbConsulta.SelectedItem.ToString, cbPacientes.SelectedValue, dtpFechaInicio.Value.Date, dtpFechaFinal.Value.Date, "Odontología")
            Else
                dgvCitasOdontologia.DataSource = ln_citas.ConsultaCitasRangoFechas(cbConsulta.SelectedItem.ToString, cbFuncionarios.SelectedValue, dtpFechaInicio.Value.Date, dtpFechaFinal.Value.Date, "Odontología")
            End If
        Else
            MsgBox("Estimado usuario, la fecha inicial no puede ser mayor a la final.", MsgBoxStyle.Critical, "Clínica Médica")
        End If


    End Sub

    Private Sub dgvConsultaOdontologia_DataSourceChanged(sender As Object, e As EventArgs) Handles dgvCitasOdontologia.DataSourceChanged
        If dgvCitasOdontologia.Rows.Count = 0 Then
            dgvCitasOdontologia.DataSource = Nothing
            lMensaje.Visible = True

        Else
            lMensaje.Visible = False
            ocultarColumnas()
        End If
    End Sub

    Function getMes(mes As String)

        Dim nMes As Integer

        Select Case mes

            Case "Enero"
                nMes = 1

            Case "Febrero"
                nMes = 2

            Case "Marzo"
                nMes = 3

            Case "Abril"
                nMes = 4

            Case "Mayo"
                nMes = 5

            Case "Junio"
                nMes = 6

            Case "Julio"
                nMes = 7

            Case "Agosto"
                nMes = 8

            Case "Setiembre"
                nMes = 9

            Case "Octubre"
                nMes = 10

            Case "Noviembre"
                nMes = 11

            Case "Diciembre"
                nMes = 12

        End Select

        Return nMes

    End Function

    Sub ejecutar()
        If cbxBusqueda.SelectedIndex = 0 Then
            dia()
        Else
            If cbxBusqueda.SelectedIndex = 1 Then
                mes()
            Else
                If cbxBusqueda.SelectedIndex = 2 Then
                    año()
                Else
                    If cbxBusqueda.SelectedIndex = 3 Then
                        rangoFechas()
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub dtpFechaInicio_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaInicio.ValueChanged
        rangoFechas()
    End Sub

    Private Sub dtpFechaFinal_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaFinal.ValueChanged
        rangoFechas()
    End Sub

    Private Sub cbMes_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbMes.SelectedValueChanged
        If cbMes.SelectedIndex = 0 Or cbAño.SelectedIndex = 0 Then
        Else
            mes()
        End If
    End Sub

    Private Sub btnReporte_Click(sender As Object, e As EventArgs) Handles btnReporte.Click

        If cbxBusqueda.SelectedIndex = 0 Then

            If cbConsulta.SelectedItem.ToString = "Paciente" Then
                Dim reporte As New ReporteCitasDia(cbConsulta.SelectedItem.ToString, cbPacientes.SelectedValue, dtpFecha.Value, "Odontología")
                reporte.Show()
            Else
                Dim reporte As New ReporteCitasDia(cbConsulta.SelectedItem.ToString, cbFuncionarios.SelectedValue, dtpFecha.Value, "Odontología")
                reporte.Show()
            End If

        End If

        If cbxBusqueda.SelectedIndex = 1 Then

            If cbConsulta.SelectedItem.ToString = "Paciente" Then
                Dim reporte As New ReporteCitasMes(cbConsulta.SelectedItem.ToString, cbPacientes.SelectedValue, Convert.ToDateTime("01-" & getMes(cbMes.SelectedItem.ToString) & "-" & cbAño.SelectedItem.ToString), "Odontología")
                reporte.Show()
            Else
                Dim reporte As New ReporteCitasMes(cbConsulta.SelectedItem.ToString, cbFuncionarios.SelectedValue, Convert.ToDateTime("01-" & getMes(cbMes.SelectedItem.ToString) & "-" & cbAño.SelectedItem.ToString), "Odontología")
                reporte.Show()
            End If

        End If


        If cbxBusqueda.SelectedIndex = 2 Then

            If cbConsulta.SelectedItem.ToString = "Paciente" Then
                Dim reporte As New ReporteCitasAño(cbConsulta.SelectedItem.ToString, cbPacientes.SelectedValue, Convert.ToDateTime("01-01-" & cbAño.SelectedItem.ToString), "Odontología")
                reporte.Show()
            Else
                Dim reporte As New ReporteCitasAño(cbConsulta.SelectedItem.ToString, cbFuncionarios.SelectedValue, Convert.ToDateTime("01-01-" & cbAño.SelectedItem.ToString), "Odontología")
                reporte.Show()
            End If

        End If

        If cbxBusqueda.SelectedIndex = 3 Then

            If cbConsulta.SelectedItem.ToString = "Paciente" Then
                Dim reporte As New ReporteCitasRangos(cbConsulta.SelectedItem.ToString, cbPacientes.SelectedValue, dtpFechaInicio.Value.Date, dtpFechaFinal.Value.Date, "Odontología")
                reporte.Show()
            Else
                Dim reporte As New ReporteCitasRangos(cbConsulta.SelectedItem.ToString, cbPacientes.SelectedValue, dtpFechaInicio.Value.Date, dtpFechaFinal.Value.Date, "Odontología")
                reporte.Show()
            End If

        End If


    End Sub


End Class
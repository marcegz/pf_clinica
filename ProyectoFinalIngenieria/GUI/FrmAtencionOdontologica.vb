﻿Imports System.Drawing.Drawing2D
Public Class FrmAtencionOdontologica
    Dim x, y As Integer
    Dim newpoint As Point

    Dim piezas As List(Of PiezaDental)
    Dim colorSeleccionado As Brush

    Dim ID_Cit As Integer ' RECIBO X CONSTRUCTOR
    Dim ID_Dient As Integer ' RECIBO X CONSTRUCTOR
    Dim ID_Paci As Integer ' RECIBO X CONSTRUCTOR

    Dim estado_cita As String
    Dim edad As Integer

    Dim fr_p As FormPrincipal ' DEVOLVERME AL PRINCIPAL

    Dim btnDiente As New Button

    Dim ln_Paciente As New Ln_Paciente
    Dim ln_citas As New Ln_Citas
    Dim ln_funcionario As New Ln_Funcionario
    Dim ln_Horas As New Ln_Horas
    Dim ln_tratamiento As New Ln_Tratamientos
    Dim ln_hdc As New Ln_Historial_Dientes_Consultas


    Enum PartesPiezaDental
        Superior
        Inferior
        Derecha
        Izquierda
        Centro
        Desconocida
    End Enum

    Public Sub New(id_Cita As Integer, id_Paciente As Integer, fr_pr As FormPrincipal)
        InitializeComponent()
        ID_Cit = id_Cita
        ID_Paci = id_Paciente
        Me.fr_p = fr_pr
    End Sub

    Public Sub New(fr_pr As FormPrincipal)
        InitializeComponent()
        Me.fr_p = fr_pr
    End Sub

    Private Sub FrmAtencionOdontologica_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If ID_Cit <> 0 Then
            completarInfoPaciente(ID_Cit)

        Else
            txtNombre.Visible = False
            cbPacientesDelDia.Visible = True
            llenarComboPacientesDia()

        End If


        If estado_cita <> "Atendido" Then

            dgvTratamientosConsultaOdontologia.DataSource = ln_tratamiento.tratamientosConsultaOdontologia(ID_Cit)
            dgvTratamientosConsultaOdontologia.Columns("ID_Historial").Visible = False

            Dim btnEliminar As DataGridViewButtonColumn = New DataGridViewButtonColumn()
            btnEliminar.Name = "Eliminar"
            btnEliminar.FlatStyle = FlatStyle.Flat
            dgvTratamientosConsultaOdontologia.Columns.Add(btnEliminar)
            dgvTratamientosConsultaOdontologia.Columns(4).HeaderText = "X"
            dgvTratamientosConsultaOdontologia.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        Else
            dgvTratamientosConsultaOdontologia.DataSource = ln_tratamiento.tratamientosConsultaOdontologia(ID_Cit)
            dgvTratamientosConsultaOdontologia.Columns("ID_Historial").Visible = False

            btnListo.Enabled = False

        End If


        lMontoTotal.Text = getSubTotal()



        If txtEdad.Text <> "18" Then

            If edad >= 18 Then
                piezas = New List(Of PiezaDental) From {
                New PiezaDental(450, 43, 30, 30), New PiezaDental(490, 43, 30, 30),
              New PiezaDental(530, 43, 30, 30), New PiezaDental(570, 43, 30, 30),
              New PiezaDental(610, 43, 30, 30), New PiezaDental(650, 43, 30, 30),
              New PiezaDental(690, 43, 30, 30), New PiezaDental(730, 43, 30, 30),
              New PiezaDental(805, 43, 30, 30), New PiezaDental(845, 43, 30, 30),
              New PiezaDental(885, 43, 30, 30), New PiezaDental(925, 43, 30, 30),
              New PiezaDental(965, 43, 30, 30), New PiezaDental(1005, 43, 30, 30),
              New PiezaDental(1045, 43, 30, 30), New PiezaDental(1085, 43, 30, 30),'siguiente fila
              New PiezaDental(450, 246, 30, 30), New PiezaDental(490, 246, 30, 30),
              New PiezaDental(530, 246, 30, 30), New PiezaDental(570, 246, 30, 30),
              New PiezaDental(610, 246, 30, 30), New PiezaDental(650, 246, 30, 30),
              New PiezaDental(690, 246, 30, 30), New PiezaDental(730, 246, 30, 30),
              New PiezaDental(805, 246, 30, 30), New PiezaDental(845, 246, 30, 30),
              New PiezaDental(885, 246, 30, 30), New PiezaDental(925, 246, 30, 30),
              New PiezaDental(965, 246, 30, 30), New PiezaDental(1005, 246, 30, 30),
              New PiezaDental(1045, 246, 30, 30), New PiezaDental(1085, 246, 30, 30)'siguiente fila
            }
                l55.Visible = False
                btn55.Visible = False
                l54.Visible = False
                btn54.Visible = False
                l53.Visible = False
                btn53.Visible = False
                l52.Visible = False
                btn52.Visible = False
                l51.Visible = False
                btn51.Visible = False

                l85.Visible = False
                btn85.Visible = False
                l84.Visible = False
                btn84.Visible = False
                l83.Visible = False
                btn83.Visible = False
                l82.Visible = False
                btn82.Visible = False
                l81.Visible = False
                btn81.Visible = False

                l61.Visible = False
                btn61.Visible = False
                l62.Visible = False
                btn62.Visible = False
                l63.Visible = False
                btn63.Visible = False
                l64.Visible = False
                btn64.Visible = False
                l65.Visible = False
                btn65.Visible = False

                l71.Visible = False
                btn71.Visible = False
                l72.Visible = False
                btn72.Visible = False
                l73.Visible = False
                btn73.Visible = False
                l74.Visible = False
                btn74.Visible = False
                l75.Visible = False
                btn75.Visible = False


            Else
                piezas = New List(Of PiezaDental) From {
              New PiezaDental(570, 106, 30, 30), New PiezaDental(610, 106, 30, 30),
              New PiezaDental(650, 106, 30, 30), New PiezaDental(690, 106, 30, 30),
              New PiezaDental(730, 106, 30, 30), New PiezaDental(805, 106, 30, 30),
              New PiezaDental(845, 106, 30, 30), New PiezaDental(885, 106, 30, 30),
              New PiezaDental(925, 106, 30, 30), New PiezaDental(965, 106, 30, 30),'siguiente fila 
              New PiezaDental(570, 176, 30, 30), New PiezaDental(610, 176, 30, 30),
              New PiezaDental(650, 176, 30, 30), New PiezaDental(690, 176, 30, 30),
              New PiezaDental(730, 176, 30, 30), New PiezaDental(805, 176, 30, 30),
              New PiezaDental(845, 176, 30, 30), New PiezaDental(885, 176, 30, 30),
              New PiezaDental(925, 176, 30, 30), New PiezaDental(965, 176, 30, 30)'siguiente fila
            }
                l18.Visible = False
                btn18.Visible = False
                l17.Visible = False
                btn17.Visible = False
                l16.Visible = False
                btn16.Visible = False
                l15.Visible = False
                btn15.Visible = False
                l14.Visible = False
                btn14.Visible = False
                l13.Visible = False
                btn13.Visible = False
                l12.Visible = False
                btn12.Visible = False
                l11.Visible = False
                btn11.Visible = False

                l21.Visible = False
                btn21.Visible = False
                l22.Visible = False
                btn22.Visible = False
                l23.Visible = False
                btn23.Visible = False
                l24.Visible = False
                btn24.Visible = False
                l25.Visible = False
                btn25.Visible = False
                l26.Visible = False
                btn26.Visible = False
                l27.Visible = False
                btn27.Visible = False
                l28.Visible = False
                btn28.Visible = False

                l48.Visible = False
                btn48.Visible = False
                l47.Visible = False
                btn47.Visible = False
                l46.Visible = False
                btn46.Visible = False
                l45.Visible = False
                btn45.Visible = False
                l44.Visible = False
                btn44.Visible = False
                l43.Visible = False
                btn43.Visible = False
                l42.Visible = False
                btn42.Visible = False
                l41.Visible = False
                btn41.Visible = False

                l31.Visible = False
                btn31.Visible = False
                l32.Visible = False
                btn32.Visible = False
                l33.Visible = False
                btn33.Visible = False
                l34.Visible = False
                btn34.Visible = False
                l35.Visible = False
                btn35.Visible = False
                l36.Visible = False
                btn36.Visible = False
                l37.Visible = False
                btn37.Visible = False
                l38.Visible = False
                btn38.Visible = False
            End If
        End If

       
        Me.DoubleBuffered = True





    End Sub

    ' ------------

    Sub llenarComboPacientesDia()
        cbPacientesDelDia.DisplayMember = "NombreCompleto"
        cbPacientesDelDia.ValueMember = "Cedula"
        cbPacientesDelDia.DataSource = ln_citas.extraerPacientesDia(Date.Now, "Odontología")
    End Sub


    ' DETECTA EL CAMBIO DEL COMBO PACIENTES Y AGREGA LOS DATOS NECESARIOS
    Private Sub cbPacientesDelDia_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbPacientesDelDia.SelectedValueChanged

        Dim paci As Pacientes
        Dim cita As Citas
        Dim hora As Horas

        Try

            paci = ln_Paciente.Consultar1Paciente(cbPacientesDelDia.SelectedValue)
            cbPacientesDelDia.Text = paci.NombreCompleto_
            txtCedula.Text = paci.Cedula_
            txtEdad.Text = getEdad(paci.FechaNacimiento_.Year())

            cita = ln_citas.ExtraerCita(getCita(cbPacientesDelDia.SelectedValue))
            hora = ln_Horas.Consultar1Hora(cita.ID_Hora_)

            txtFuncionario.Text = ln_funcionario.ConsultarFuncionario(cita.ID_Funcionario_)
            txtHora.Text = hora.Hora_.ToString
            rtbDescripcion.Text = cita.Descripcion_

            ID_Paci = paci.ID_Paciente_
            ID_Cit = cita.ID_Cita_

            dgvTratamientosConsultaOdontologia.DataSource = ln_tratamiento.tratamientosConsultaOdontologia(ID_Cit)
            dgvTratamientosConsultaOdontologia.Columns("ID_Historial").Visible = False

            lMontoTotal.Text = getSubTotal()

        Catch

        End Try
    End Sub


    Function getCita(cedula As String) As Integer

        Dim ID_Cita As Integer

        For Each p As DataRow In ln_citas.extraerPacientesDia(Date.Now, "Odontología").Rows

            If Convert.ToString(p("Cedula")) = cedula Then
                ID_Cita = Convert.ToString(p("ID_Cita"))
            End If

        Next

        Return ID_Cita

    End Function
    '------------------

    Function getSubTotal() As Decimal

        Dim total As Decimal

        For Each tratamiento As DataRow In ln_tratamiento.tratamientosConsultaOdontologia(ID_Cit).Rows
            total += Convert.ToDecimal(tratamiento("Precio"))
        Next

        Return total

    End Function



    '------------------


    'SACA LA EDAD RECIBIENDO EL AÑO
    Function getEdad(año As Integer) As Integer
        Dim añoactual As Integer = Year(Now)
        getEdad = añoactual - año
    End Function

    Sub completarInfoPaciente(id As Integer)
        Dim paci As Pacientes
        Dim cita As Citas
        Dim hora As Horas

        cita = ln_citas.ExtraerCita(id)
        paci = ln_Paciente.ConsultarPacienteCOMPLETO(cita.ID_Paciente_)

        hora = ln_Horas.Consultar1Hora(cita.ID_Hora_)

        txtNombre.Text = paci.NombreCompleto_
        txtCedula.Text = paci.Cedula_
        txtEdad.Text = getEdad(paci.FechaNacimiento_.Year())
        txtFuncionario.Text = ln_funcionario.ConsultarFuncionario(cita.ID_Funcionario_)
        txtHora.Text = hora.Hora_.ToString
        rtbDescripcion.Text = cita.Descripcion_

        estado_cita = cita.Estado_
        edad = getEdad(paci.FechaNacimiento_.Year())

    End Sub


    'Mover Formulario-----------------------------------------------------------------------------------

    Private Sub BarraTitulo_MouseDown(sender As Object, e As MouseEventArgs) Handles BarraTitulo.MouseDown
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub BarraTitulo_MouseMove(sender As Object, e As MouseEventArgs) Handles BarraTitulo.MouseMove
        If e.Button = MouseButtons.Left Then
            newpoint = Control.MousePosition
            newpoint.X -= x
            newpoint.Y -= y
            Me.Location = newpoint
            Application.DoEvents()
        End If
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        If MsgBox("¿Está seguro de cerrar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then
            Me.fr_p.Visible = True
            Me.Close()

        End If
    End Sub

    Private Sub btnMinimizar_Click(sender As Object, e As EventArgs) Handles btnMinimizar.Click
        WindowState = FormWindowState.Minimized
    End Sub

    'ODONTOGRAMA------------------------------------------------------------------------------------------

    'Public Sub New(fr_pr As FormPrincipal)

    '    ' Esta llamada es exigida por el diseñador.
    '    InitializeComponent()

    '    ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    '    fr_p = fr_pr


    'piezas = New List(Of PiezaDental) From {
    '  New PiezaDental(450, 43, 30, 30), New PiezaDental(490, 43, 30, 30),
    '  New PiezaDental(530, 43, 30, 30), New PiezaDental(570, 43, 30, 30),
    '  New PiezaDental(610, 43, 30, 30), New PiezaDental(650, 43, 30, 30),
    '  New PiezaDental(690, 43, 30, 30), New PiezaDental(730, 43, 30, 30),
    '  New PiezaDental(805, 43, 30, 30), New PiezaDental(845, 43, 30, 30),
    '  New PiezaDental(885, 43, 30, 30), New PiezaDental(925, 43, 30, 30),
    '  New PiezaDental(965, 43, 30, 30), New PiezaDental(1005, 43, 30, 30),
    '  New PiezaDental(1045, 43, 30, 30), New PiezaDental(1085, 43, 30, 30),'siguiente fila
    '  New PiezaDental(570, 106, 30, 30), New PiezaDental(610, 106, 30, 30),
    '  New PiezaDental(650, 106, 30, 30), New PiezaDental(690, 106, 30, 30),
    '  New PiezaDental(730, 106, 30, 30), New PiezaDental(805, 106, 30, 30),
    '  New PiezaDental(845, 106, 30, 30), New PiezaDental(885, 106, 30, 30),
    '  New PiezaDental(925, 106, 30, 30), New PiezaDental(965, 106, 30, 30),'siguiente fila 
    '  New PiezaDental(570, 176, 30, 30), New PiezaDental(610, 176, 30, 30),
    '  New PiezaDental(650, 176, 30, 30), New PiezaDental(690, 176, 30, 30),
    '  New PiezaDental(730, 176, 30, 30), New PiezaDental(805, 176, 30, 30),
    '  New PiezaDental(845, 176, 30, 30), New PiezaDental(885, 176, 30, 30),
    '  New PiezaDental(925, 176, 30, 30), New PiezaDental(965, 176, 30, 30),'siguiente fila
    '  New PiezaDental(450, 246, 30, 30), New PiezaDental(490, 246, 30, 30),
    '  New PiezaDental(530, 246, 30, 30), New PiezaDental(570, 246, 30, 30),
    '  New PiezaDental(610, 246, 30, 30), New PiezaDental(650, 246, 30, 30),
    '  New PiezaDental(690, 246, 30, 30), New PiezaDental(730, 246, 30, 30),
    '  New PiezaDental(805, 246, 30, 30), New PiezaDental(845, 246, 30, 30),
    '  New PiezaDental(885, 246, 30, 30), New PiezaDental(925, 246, 30, 30),
    '  New PiezaDental(965, 246, 30, 30), New PiezaDental(1005, 246, 30, 30),
    '  New PiezaDental(1045, 246, 30, 30), New PiezaDental(1085, 246, 30, 30)'siguiente fila
    '}

    ''RadioButton1.Tag = Brushes.Red
    ''RadioButton2.Tag = Brushes.Blue
    ''RadioButton3.Tag = Brushes.Green

    'Me.DoubleBuffered = True
    'End Sub



    Private Sub Panel1_MouseDown(sender As Object, e As MouseEventArgs) Handles Panel1.MouseDown
        For Each p As PiezaDental In piezas
            Dim ppd As PartesPiezaDental = p.DetectaPartePiezaDental(e.Location)

            If ppd = PartesPiezaDental.Desconocida Then Continue For

            Dim br As Brush = If(e.Button = Windows.Forms.MouseButtons.Left, colorSeleccionado, Brushes.White)
            p.ColoreaParte(br, ppd)
            Invalidate(p.RectPartePiezaDental(ppd))
            Exit For
        Next
    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint
        For Each p As PiezaDental In piezas
            p.Dibuja(e.Graphics)
        Next

        'btnDiente.SetBounds(0, 0, 30, 30)
        'btnDiente.BackColor = Color.Red

    End Sub




    '---------------------------------------- BOTONES DIENTESSSSSSSSS
    Sub cargarDetalleDienteSeleccionado(id As Integer)
        'Me.Hide()
        Dim f As New FrmDetalleDiente(ID_Cit, id, ID_Paci, Me)
        f.Show()
    End Sub

    Private Sub btn18_Click(sender As Object, e As EventArgs) Handles btn18.Click
        cargarDetalleDienteSeleccionado(18)
    End Sub

    Private Sub btn17_Click(sender As Object, e As EventArgs) Handles btn17.Click
        cargarDetalleDienteSeleccionado(17)
    End Sub

    Private Sub btn16_Click(sender As Object, e As EventArgs) Handles btn16.Click
        cargarDetalleDienteSeleccionado(16)
    End Sub

    Private Sub btn15_Click(sender As Object, e As EventArgs) Handles btn15.Click
        cargarDetalleDienteSeleccionado(15)
    End Sub

    Private Sub btn14_Click(sender As Object, e As EventArgs) Handles btn14.Click
        cargarDetalleDienteSeleccionado(14)
    End Sub

    Private Sub btn13_Click(sender As Object, e As EventArgs) Handles btn13.Click
        cargarDetalleDienteSeleccionado(13)
    End Sub

    Private Sub btn12_Click(sender As Object, e As EventArgs) Handles btn12.Click
        cargarDetalleDienteSeleccionado(12)
    End Sub

    Private Sub btn11_Click(sender As Object, e As EventArgs) Handles btn11.Click
        cargarDetalleDienteSeleccionado(11)
    End Sub

    Private Sub btn21_Click(sender As Object, e As EventArgs) Handles btn21.Click
        cargarDetalleDienteSeleccionado(21)
    End Sub

    Private Sub btn22_Click(sender As Object, e As EventArgs) Handles btn22.Click
        cargarDetalleDienteSeleccionado(22)
    End Sub

    Private Sub btn23_Click(sender As Object, e As EventArgs) Handles btn23.Click
        cargarDetalleDienteSeleccionado(23)
    End Sub

    Private Sub btn24_Click(sender As Object, e As EventArgs) Handles btn24.Click
        cargarDetalleDienteSeleccionado(24)
    End Sub

    Private Sub btn25_Click(sender As Object, e As EventArgs) Handles btn25.Click
        cargarDetalleDienteSeleccionado(25)
    End Sub

    Private Sub btn26_Click(sender As Object, e As EventArgs) Handles btn26.Click
        cargarDetalleDienteSeleccionado(26)
    End Sub

    Private Sub btn27_Click(sender As Object, e As EventArgs) Handles btn27.Click
        cargarDetalleDienteSeleccionado(27)
    End Sub

    Private Sub btn28_Click(sender As Object, e As EventArgs) Handles btn28.Click
        cargarDetalleDienteSeleccionado(28)
    End Sub

    '------------

    Private Sub btn55_Click(sender As Object, e As EventArgs) Handles btn55.Click
        cargarDetalleDienteSeleccionado(55)
    End Sub

    Private Sub btn54_Click(sender As Object, e As EventArgs) Handles btn54.Click
        cargarDetalleDienteSeleccionado(54)
    End Sub

    Private Sub btn53_Click(sender As Object, e As EventArgs) Handles btn53.Click
        cargarDetalleDienteSeleccionado(53)
    End Sub

    Private Sub btn52_Click(sender As Object, e As EventArgs) Handles btn52.Click
        cargarDetalleDienteSeleccionado(52)
    End Sub

    Private Sub btn51_Click(sender As Object, e As EventArgs) Handles btn51.Click
        cargarDetalleDienteSeleccionado(51)
    End Sub

    Private Sub btn61_Click(sender As Object, e As EventArgs) Handles btn61.Click
        cargarDetalleDienteSeleccionado(61)
    End Sub

    Private Sub btn62_Click(sender As Object, e As EventArgs) Handles btn62.Click
        cargarDetalleDienteSeleccionado(62)
    End Sub

    Private Sub btn63_Click(sender As Object, e As EventArgs) Handles btn63.Click
        cargarDetalleDienteSeleccionado(63)
    End Sub

    Private Sub btn64_Click(sender As Object, e As EventArgs) Handles btn64.Click
        cargarDetalleDienteSeleccionado(64)
    End Sub

    Private Sub btn65_Click(sender As Object, e As EventArgs) Handles btn65.Click
        cargarDetalleDienteSeleccionado(65)
    End Sub

    Private Sub btn85_Click(sender As Object, e As EventArgs) Handles btn85.Click
        cargarDetalleDienteSeleccionado(85)
    End Sub

    Private Sub btn84_Click(sender As Object, e As EventArgs) Handles btn84.Click
        cargarDetalleDienteSeleccionado(84)
    End Sub

    Private Sub btn83_Click(sender As Object, e As EventArgs) Handles btn83.Click
        cargarDetalleDienteSeleccionado(83)
    End Sub

    Private Sub btn82_Click(sender As Object, e As EventArgs) Handles btn82.Click
        cargarDetalleDienteSeleccionado(82)
    End Sub

    Private Sub btn81_Click(sender As Object, e As EventArgs) Handles btn81.Click
        cargarDetalleDienteSeleccionado(81)
    End Sub

    Private Sub btn71_Click(sender As Object, e As EventArgs) Handles btn71.Click
        cargarDetalleDienteSeleccionado(71)
    End Sub

    Private Sub btn72_Click(sender As Object, e As EventArgs) Handles btn72.Click
        cargarDetalleDienteSeleccionado(72)
    End Sub

    Private Sub btn73_Click(sender As Object, e As EventArgs) Handles btn73.Click
        cargarDetalleDienteSeleccionado(73)
    End Sub

    Private Sub btn74_Click(sender As Object, e As EventArgs) Handles btn74.Click
        cargarDetalleDienteSeleccionado(74)
    End Sub

    Private Sub btn75_Click(sender As Object, e As EventArgs) Handles btn75.Click
        cargarDetalleDienteSeleccionado(75)
    End Sub

    Private Sub btn48_Click(sender As Object, e As EventArgs) Handles btn48.Click
        cargarDetalleDienteSeleccionado(48)
    End Sub

    Private Sub btn47_Click(sender As Object, e As EventArgs) Handles btn47.Click
        cargarDetalleDienteSeleccionado(47)
    End Sub

    Private Sub btn46_Click(sender As Object, e As EventArgs) Handles btn46.Click
        cargarDetalleDienteSeleccionado(46)
    End Sub

    Private Sub btn45_Click(sender As Object, e As EventArgs) Handles btn45.Click
        cargarDetalleDienteSeleccionado(45)
    End Sub

    Private Sub btn44_Click(sender As Object, e As EventArgs) Handles btn44.Click
        cargarDetalleDienteSeleccionado(44)
    End Sub

    Private Sub btn43_Click(sender As Object, e As EventArgs) Handles btn43.Click
        cargarDetalleDienteSeleccionado(43)
    End Sub

    Private Sub btn42_Click(sender As Object, e As EventArgs) Handles btn42.Click
        cargarDetalleDienteSeleccionado(42)
    End Sub

    Private Sub btn41_Click(sender As Object, e As EventArgs) Handles btn41.Click
        cargarDetalleDienteSeleccionado(41)
    End Sub

    Private Sub btn31_Click(sender As Object, e As EventArgs) Handles btn31.Click
        cargarDetalleDienteSeleccionado(31)
    End Sub

    Private Sub btn32_Click(sender As Object, e As EventArgs) Handles btn32.Click
        cargarDetalleDienteSeleccionado(32)
    End Sub

    Private Sub btn33_Click(sender As Object, e As EventArgs) Handles btn33.Click
        cargarDetalleDienteSeleccionado(33)
    End Sub

    Private Sub btn34_Click(sender As Object, e As EventArgs) Handles btn34.Click
        cargarDetalleDienteSeleccionado(34)
    End Sub

    Private Sub btn35_Click(sender As Object, e As EventArgs) Handles btn35.Click
        cargarDetalleDienteSeleccionado(35)
    End Sub

    Private Sub btn36_Click(sender As Object, e As EventArgs) Handles btn36.Click
        cargarDetalleDienteSeleccionado(36)
    End Sub

    Private Sub btn37_Click(sender As Object, e As EventArgs) Handles btn37.Click
        cargarDetalleDienteSeleccionado(37)
    End Sub


    Private Sub btn38_Click(sender As Object, e As EventArgs) Handles btn38.Click
        cargarDetalleDienteSeleccionado(38)
    End Sub

    Private Sub btnListo_Click(sender As Object, e As EventArgs) Handles btnListo.Click
        If MsgBox("¿Finalizar Atención?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then
            ln_citas.actualizarEstadoCita(ID_Cit, "Atendido")

            If cbPacientesDelDia.Visible = False Then
                Me.Close()

            Else
                txtNombre.Visible = False
                cbPacientesDelDia.Visible = True
                llenarComboPacientesDia()

            End If

            dgvTratamientosConsultaOdontologia.DataSource = ln_tratamiento.tratamientosConsultaOdontologia(ID_Cit)
            dgvTratamientosConsultaOdontologia.Columns("ID_Historial").Visible = False

            lMontoTotal.Text = getSubTotal()
            btnListo.Enabled = False


        End If

    End Sub


    Private Sub dgvTratamientosConsultaOdontologia_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTratamientosConsultaOdontologia.CellContentClick
        If e.ColumnIndex = 0 Then

            ln_hdc.eliminarTratamientoConsultaOdontologia(CInt(dgvTratamientosConsultaOdontologia.CurrentRow.Cells(1).Value.ToString))

        End If

        dgvTratamientosConsultaOdontologia.DataSource = ln_tratamiento.tratamientosConsultaOdontologia(ID_Cit)
        dgvTratamientosConsultaOdontologia.Columns("ID_Historial").Visible = False

        lMontoTotal.Text = getSubTotal()
    End Sub



    Private Sub dgvTratamientosConsultaOdontologia_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgvTratamientosConsultaOdontologia.CellPainting
        If e.ColumnIndex >= 0 AndAlso e.RowIndex >= 0 And Me.dgvTratamientosConsultaOdontologia.Columns(e.ColumnIndex).Name = "Eliminar" Then


            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = TryCast(Me.dgvTratamientosConsultaOdontologia.Rows(e.RowIndex).Cells("Eliminar"), DataGridViewButtonCell)
            Dim icoAtomico As Icon = New Icon("D:\Dropbox\PROYECTO FINAL INGENIERIA\VB\ProyectoFinalIngenieria\ProyectoFinalIngenieria\Resources\eliminar.ico")

            e.Graphics.DrawIcon(icoAtomico, e.CellBounds.Left + 5, e.CellBounds.Top + 5)
            Me.dgvTratamientosConsultaOdontologia.Rows(e.RowIndex).Height = 40
            Me.dgvTratamientosConsultaOdontologia.Columns(e.ColumnIndex).Width = 40

            e.Handled = True

        End If

    End Sub

    '------------------------------------ FIN BOTONES DIENTESSSSSSSSSSSS


End Class
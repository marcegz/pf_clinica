﻿Imports System.Drawing.Drawing2D

Public Class FrmDetalleDiente

    Dim x, y As Integer
    Dim newpoint As Point

    Dim piezas As List(Of PiezaDental)
    Dim colorSeleccionado As Brush
    Dim Id_cita As Integer
    Dim Id_diente As Integer
    Dim Id_paciente As Integer
    Dim fr_consulta As FrmAtencionOdontologica

    Dim ln_tratamientos As New Ln_Tratamientos
    Dim ln_Diente As New Ln_Dientes
    Dim ln_HDC As New Ln_Historial_Dientes_Consultas
    Dim ln_consultas As New Ln_Consultas
    Dim ln_tratamiento As New Ln_Tratamientos
    Dim ln_cita As New Ln_Citas

    Dim d As Dientes

    Dim con As Integer
    Dim Lado_dient As String

    Enum PartesPiezaDental
        Superior
        Inferior
        Derecha
        Izquierda
        Centro
        Desconocida
    End Enum

    Public Sub New(id_citaa As Integer, id_dientee As Integer, id_pacientee As Integer, fr_con As FrmAtencionOdontologica)
        InitializeComponent()

        fr_consulta = fr_con

        Id_cita = id_citaa
        Id_diente = id_dientee
        Id_paciente = id_pacientee

    End Sub

    Sub completarInfoDiente(num_diente As Integer)

        d = ln_Diente.Consultar1Diente(num_diente)

        txtNumeroDiente.Text = num_diente
        txtNombreDiente.Text = d.Nombre_
    End Sub

    Sub llenarComboTratamientos()
        cbxNombreTratamientos.DisplayMember = "Nombre"
        cbxNombreTratamientos.ValueMember = "ID_Tratamiento"
        cbxNombreTratamientos.DataSource = ln_tratamientos.TratamientosActivosporEspecialidad("Odontología")
    End Sub

    Sub cargarTablaTratamientosDiente()
        dgvDetalleDiente.DataSource = ln_HDC.cargarTablaTratamientosDiente(d.ID_Diente_, Id_paciente)
        dgvDetalleDiente.Columns("LadoDiente_Tratamiento").Visible = False
    End Sub




    'DETECTA EL CAMBIO DEL COMBO Y AGREGA LOS DATOS NECESARIOS
    Private Sub cbxNombreTratamientos_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbxNombreTratamientos.SelectedValueChanged

        Dim tratamientoSeleccionado As Tratamientos

        'Try
        tratamientoSeleccionado = ln_tratamientos.ConsultarTratamientoCombo(cbxNombreTratamientos.SelectedValue)
        txtPrecio.Text = tratamientoSeleccionado.Precio_
        Dim col As String = tratamientoSeleccionado.Color_

        'PASA DE STRING A COLOR
        Dim pintarPanel As Color = ColorTranslator.FromHtml(col)
        'Dim brus As Brush = New SolidBrush(pintarPanel)
        Dim brus As New SolidBrush(pintarPanel)

        pColor.BackColor = pintarPanel
        RadioButton1.Tag = brus

        colorSeleccionado = brus

        '    Catch ex As Exception

        '   End Try


    End Sub

    '--------------------------------- DIENTES

    'Private Sub HerramientaSeleccionColor(sender As System.Object, e As System.EventArgs) Handles RadioButton1.CheckedChanged
    '    colorSeleccionado = CType(Controls.OfType(Of RadioButton).Where(Function(rb) rb.Checked).First.Tag, Brush)
    'End Sub

    Private Sub FrmDetalleDiente_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        piezas = New List(Of PiezaDental) From {
      New PiezaDental(60, 30, 120, 120)
    }

        Me.DoubleBuffered = True

        llenarComboTratamientos()
        completarInfoDiente(Id_diente)

        RadioButton1.Visible = False
        RadioButton1.Checked = True

        cargarTablaTratamientosDiente()



        Dim cita As Citas

        cita = ln_cita.ExtraerCita(Id_cita)

        If cita.Estado_ = "Atendido" Then
            btnAgregar.Enabled = False
        Else
            btnAgregar.Enabled = True
        End If



    End Sub



    Private Sub Panel1_MouseDown(sender As Object, e As MouseEventArgs) Handles Panel1.MouseDown

        con = 0

        For Each p As PiezaDental In piezas
            Dim ppd As PartesPiezaDental = p.DetectaPartePiezaDental(e.Location)

            If ppd = PartesPiezaDental.Desconocida Then Continue For

            Dim br As Brush = If(e.Button = Windows.Forms.MouseButtons.Left, colorSeleccionado, Brushes.White)

            ' colorSeleccionado = 

            p.ColoreaParte(br, ppd)
            Invalidate(p.RectPartePiezaDental(ppd))

            Lado_dient = ppd
            txtLadoDiente_Tratamiento.Text = ppd

            'MessageBox.Show("LADOOOOO   " & Lado_dient.ToString)

            Panel1.Refresh()
            Exit For
        Next
    End Sub


    'Private Sub Panel1_MouseDown(sender As Object, e As MouseEventArgs) Handles Panel1.MouseDown

    '    con = 0

    '    For Each p As PiezaDental In piezas
    '        Dim ppd As PartesPiezaDental = p.DetectaPartePiezaDental(e.Location)

    '        If ppd = PartesPiezaDental.Desconocida Then Continue For

    '        Dim br As Brush = If(e.Button = Windows.Forms.MouseButtons.Left, colorSeleccionado, Brushes.White)
    '        p.ColoreaParte(br, ppd)
    '        Invalidate(p.RectPartePiezaDental(ppd))

    '        Lado_dient = ppd
    '        txtLadoDiente_Tratamiento.Text = ppd

    '        'MessageBox.Show("LADOOOOO   " & Lado_dient.ToString)

    '        Panel1.Refresh()
    '        Exit For
    '    Next
    'End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint
        For Each p As PiezaDental In piezas
            p.Dibuja(e.Graphics)
        Next
    End Sub



    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        If MsgBox("¿Está seguro de cerrar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
          MsgBoxResult.Yes Then
            '   fr_consulta.Visible = True
            Me.Close()
        End If

    End Sub

    Private Sub btnMinimizar_Click(sender As Object, e As EventArgs) Handles btnMinimizar.Click
        WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click

        Dim id_consul As Integer

        If (txtNumeroDiente.Text <> "") And (txtNombreDiente.Text <> "") And (txtDescripcion.Text <> "") And
            txtPrecio.Text <> "" Then

            id_consul = ln_consultas.getIdConsulta(Id_cita)

            ln_HDC.insertarHistorialDiente(New Historial_Dientes_Consultas(id_consul, d.ID_Diente_,
                                                                           cbxNombreTratamientos.SelectedValue,
                                                                           txtDescripcion.Text, txtLadoDiente_Tratamiento.Text))

            cargarTablaTratamientosDiente()
            txtDescripcion.Text = ""
            'RadioButton1.Tag = Brushes.White


        Else
            MsgBox("Debe ingresar al menos los datos obligatorios (*)!!!", MsgBoxStyle.Critical, "Clinica Medica")

        End If

        fr_consulta.dgvTratamientosConsultaOdontologia.DataSource = ln_tratamiento.tratamientosConsultaOdontologia(Id_cita)
        fr_consulta.dgvTratamientosConsultaOdontologia.Columns("ID_Historial").Visible = False

        fr_consulta.lMontoTotal.Text = fr_consulta.getSubTotal()

    End Sub


    'Mover Formulario-----------------------------------------------------------------------------------

    Private Sub Panel2_MouseDown(sender As Object, e As MouseEventArgs) Handles Panel2.MouseDown
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub Panel2_MouseMove(sender As Object, e As MouseEventArgs) Handles Panel2.MouseMove
        If e.Button = MouseButtons.Left Then
            newpoint = Control.MousePosition
            newpoint.X -= x
            newpoint.Y -= y
            Me.Location = newpoint
            Application.DoEvents()
        End If
    End Sub



End Class

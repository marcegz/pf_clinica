﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReportePacientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ConsultarPacientesReporteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataReportes = New ProyectoFinalIngenieria.DataReportes()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.ConsultarPacientesReporteTableAdapter = New ProyectoFinalIngenieria.DataReportesTableAdapters.ConsultarPacientesReporteTableAdapter()
        CType(Me.ConsultarPacientesReporteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ConsultarPacientesReporteBindingSource
        '
        Me.ConsultarPacientesReporteBindingSource.DataMember = "ConsultarPacientesReporte"
        Me.ConsultarPacientesReporteBindingSource.DataSource = Me.DataReportes
        '
        'DataReportes
        '
        Me.DataReportes.DataSetName = "DataReportes"
        Me.DataReportes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportViewer1
        '
        ReportDataSource1.Name = "DataReporte"
        ReportDataSource1.Value = Me.ConsultarPacientesReporteBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ProyectoFinalIngenieria.ReportePacientes.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(-1, -1)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(801, 451)
        Me.ReportViewer1.TabIndex = 0
        '
        'ConsultarPacientesReporteTableAdapter
        '
        Me.ConsultarPacientesReporteTableAdapter.ClearBeforeFill = True
        '
        'ReportePacientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "ReportePacientes"
        Me.Text = "Reporte Pacientes"
        CType(Me.ConsultarPacientesReporteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents ConsultarPacientesReporteBindingSource As BindingSource
    Friend WithEvents DataReportes As DataReportes
    Friend WithEvents ConsultarPacientesReporteTableAdapter As DataReportesTableAdapters.ConsultarPacientesReporteTableAdapter
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmAtencionMedicinaGeneral
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAtencionMedicinaGeneral))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnListo = New System.Windows.Forms.Button()
        Me.btnEliminarTratamiento = New System.Windows.Forms.Button()
        Me.btnAgregarTratamiento = New System.Windows.Forms.Button()
        Me.cbxTratamientos = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.tbxSintomas = New System.Windows.Forms.RichTextBox()
        Me.txtPresionArterial = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtTamaño = New System.Windows.Forms.TextBox()
        Me.txtPeso = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtHora = New System.Windows.Forms.TextBox()
        Me.txtFuncionario = New System.Windows.Forms.TextBox()
        Me.txtEdad = New System.Windows.Forms.TextBox()
        Me.txtCedula = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.cbPacientesDelDia = New System.Windows.Forms.ComboBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.rtbDescripcion = New System.Windows.Forms.RichTextBox()
        Me.dgvTratamientos = New System.Windows.Forms.DataGridView()
        Me.ID_Tratamiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripción = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lTotal = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lMontoTotal = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.BarraTitulo = New System.Windows.Forms.Panel()
        Me.btnMinimizar = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTratamientos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.BarraTitulo.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Controls.Add(Me.PictureBox3)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(0, 43)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(903, 71)
        Me.Panel2.TabIndex = 6
        '
        'Panel1
        '
        Me.Panel1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(3, 77)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(888, 518)
        Me.Panel1.TabIndex = 7
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(901, 3)
        Me.PictureBox3.TabIndex = 37
        Me.PictureBox3.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(390, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(255, 33)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Medicina General"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(245, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(154, 33)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Atención /"
        '
        'btnListo
        '
        Me.btnListo.BackColor = System.Drawing.Color.Transparent
        Me.btnListo.BackgroundImage = Global.ProyectoFinalIngenieria.My.Resources.Resources.Atendido
        Me.btnListo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnListo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnListo.FlatAppearance.BorderSize = 0
        Me.btnListo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnListo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnListo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnListo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.btnListo.ForeColor = System.Drawing.Color.White
        Me.btnListo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnListo.Location = New System.Drawing.Point(726, 472)
        Me.btnListo.Name = "btnListo"
        Me.btnListo.Size = New System.Drawing.Size(135, 147)
        Me.btnListo.TabIndex = 123
        Me.btnListo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnListo.UseVisualStyleBackColor = False
        '
        'btnEliminarTratamiento
        '
        Me.btnEliminarTratamiento.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnEliminarTratamiento.FlatAppearance.BorderSize = 0
        Me.btnEliminarTratamiento.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminarTratamiento.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminarTratamiento.ForeColor = System.Drawing.Color.White
        Me.btnEliminarTratamiento.Location = New System.Drawing.Point(545, 429)
        Me.btnEliminarTratamiento.Name = "btnEliminarTratamiento"
        Me.btnEliminarTratamiento.Size = New System.Drawing.Size(105, 34)
        Me.btnEliminarTratamiento.TabIndex = 175
        Me.btnEliminarTratamiento.Text = "Eliminar"
        Me.btnEliminarTratamiento.UseVisualStyleBackColor = False
        '
        'btnAgregarTratamiento
        '
        Me.btnAgregarTratamiento.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnAgregarTratamiento.FlatAppearance.BorderSize = 0
        Me.btnAgregarTratamiento.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregarTratamiento.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregarTratamiento.ForeColor = System.Drawing.Color.White
        Me.btnAgregarTratamiento.Location = New System.Drawing.Point(421, 429)
        Me.btnAgregarTratamiento.Name = "btnAgregarTratamiento"
        Me.btnAgregarTratamiento.Size = New System.Drawing.Size(105, 34)
        Me.btnAgregarTratamiento.TabIndex = 173
        Me.btnAgregarTratamiento.Text = "Agregar"
        Me.btnAgregarTratamiento.UseVisualStyleBackColor = False
        '
        'cbxTratamientos
        '
        Me.cbxTratamientos.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTratamientos.FormattingEnabled = True
        Me.cbxTratamientos.Location = New System.Drawing.Point(156, 433)
        Me.cbxTratamientos.Name = "cbxTratamientos"
        Me.cbxTratamientos.Size = New System.Drawing.Size(246, 28)
        Me.cbxTratamientos.TabIndex = 172
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.Location = New System.Drawing.Point(26, 440)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(111, 21)
        Me.Label3.TabIndex = 171
        Me.Label3.Text = "Tratamiento:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label21.Location = New System.Drawing.Point(488, 252)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(80, 21)
        Me.Label21.TabIndex = 170
        Me.Label21.Text = "Síntomas"
        '
        'tbxSintomas
        '
        Me.tbxSintomas.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbxSintomas.Location = New System.Drawing.Point(487, 276)
        Me.tbxSintomas.Name = "tbxSintomas"
        Me.tbxSintomas.Size = New System.Drawing.Size(369, 128)
        Me.tbxSintomas.TabIndex = 169
        Me.tbxSintomas.Text = ""
        '
        'txtPresionArterial
        '
        Me.txtPresionArterial.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresionArterial.Location = New System.Drawing.Point(492, 219)
        Me.txtPresionArterial.Name = "txtPresionArterial"
        Me.txtPresionArterial.Size = New System.Drawing.Size(174, 26)
        Me.txtPresionArterial.TabIndex = 168
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label20.Location = New System.Drawing.Point(488, 195)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(67, 21)
        Me.Label20.TabIndex = 167
        Me.Label20.Text = "Presión:"
        '
        'txtTamaño
        '
        Me.txtTamaño.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTamaño.Location = New System.Drawing.Point(687, 159)
        Me.txtTamaño.Name = "txtTamaño"
        Me.txtTamaño.Size = New System.Drawing.Size(174, 26)
        Me.txtTamaño.TabIndex = 166
        '
        'txtPeso
        '
        Me.txtPeso.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeso.Location = New System.Drawing.Point(492, 159)
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.Size = New System.Drawing.Size(174, 26)
        Me.txtPeso.TabIndex = 165
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label18.Location = New System.Drawing.Point(683, 135)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(80, 21)
        Me.Label18.TabIndex = 164
        Me.Label18.Text = "Estatura:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label19.Location = New System.Drawing.Point(488, 135)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(49, 21)
        Me.Label19.TabIndex = 163
        Me.Label19.Text = "Peso:"
        '
        'txtHora
        '
        Me.txtHora.Enabled = False
        Me.txtHora.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHora.Location = New System.Drawing.Point(228, 276)
        Me.txtHora.Name = "txtHora"
        Me.txtHora.Size = New System.Drawing.Size(174, 26)
        Me.txtHora.TabIndex = 162
        '
        'txtFuncionario
        '
        Me.txtFuncionario.Enabled = False
        Me.txtFuncionario.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFuncionario.Location = New System.Drawing.Point(33, 276)
        Me.txtFuncionario.Name = "txtFuncionario"
        Me.txtFuncionario.Size = New System.Drawing.Size(174, 26)
        Me.txtFuncionario.TabIndex = 161
        '
        'txtEdad
        '
        Me.txtEdad.Enabled = False
        Me.txtEdad.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEdad.Location = New System.Drawing.Point(228, 219)
        Me.txtEdad.Name = "txtEdad"
        Me.txtEdad.Size = New System.Drawing.Size(174, 26)
        Me.txtEdad.TabIndex = 160
        '
        'txtCedula
        '
        Me.txtCedula.Enabled = False
        Me.txtCedula.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCedula.Location = New System.Drawing.Point(33, 219)
        Me.txtCedula.Name = "txtCedula"
        Me.txtCedula.Size = New System.Drawing.Size(174, 26)
        Me.txtCedula.TabIndex = 159
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label9.Location = New System.Drawing.Point(34, 308)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(104, 21)
        Me.Label9.TabIndex = 158
        Me.Label9.Text = "Descripción:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label7.Location = New System.Drawing.Point(233, 195)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 21)
        Me.Label7.TabIndex = 156
        Me.Label7.Text = "Edad:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label8.Location = New System.Drawing.Point(34, 252)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(104, 21)
        Me.Label8.TabIndex = 155
        Me.Label8.Text = "Funcionario:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label4.Location = New System.Drawing.Point(233, 252)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 21)
        Me.Label4.TabIndex = 153
        Me.Label4.Text = "Hora:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label5.Location = New System.Drawing.Point(29, 195)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 21)
        Me.Label5.TabIndex = 152
        Me.Label5.Text = "Cédula:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label6.Location = New System.Drawing.Point(34, 135)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 21)
        Me.Label6.TabIndex = 151
        Me.Label6.Text = "Nombre:"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(14, 419)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(874, 3)
        Me.PictureBox2.TabIndex = 150
        Me.PictureBox2.TabStop = False
        '
        'cbPacientesDelDia
        '
        Me.cbPacientesDelDia.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPacientesDelDia.FormattingEnabled = True
        Me.cbPacientesDelDia.Location = New System.Drawing.Point(30, 160)
        Me.cbPacientesDelDia.Name = "cbPacientesDelDia"
        Me.cbPacientesDelDia.Size = New System.Drawing.Size(369, 29)
        Me.cbPacientesDelDia.TabIndex = 249
        '
        'txtNombre
        '
        Me.txtNombre.Enabled = False
        Me.txtNombre.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.Location = New System.Drawing.Point(32, 159)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(367, 27)
        Me.txtNombre.TabIndex = 248
        Me.txtNombre.Visible = False
        '
        'rtbDescripcion
        '
        Me.rtbDescripcion.Enabled = False
        Me.rtbDescripcion.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtbDescripcion.Location = New System.Drawing.Point(32, 336)
        Me.rtbDescripcion.Name = "rtbDescripcion"
        Me.rtbDescripcion.Size = New System.Drawing.Size(370, 68)
        Me.rtbDescripcion.TabIndex = 252
        Me.rtbDescripcion.Text = ""
        '
        'dgvTratamientos
        '
        Me.dgvTratamientos.AllowUserToOrderColumns = True
        Me.dgvTratamientos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTratamientos.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.dgvTratamientos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTratamientos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTratamientos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID_Tratamiento, Me.Nombre, Me.Descripción, Me.Precio})
        Me.dgvTratamientos.Location = New System.Drawing.Point(0, 3)
        Me.dgvTratamientos.Name = "dgvTratamientos"
        Me.dgvTratamientos.RowHeadersVisible = False
        Me.dgvTratamientos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTratamientos.Size = New System.Drawing.Size(628, 119)
        Me.dgvTratamientos.TabIndex = 174
        '
        'ID_Tratamiento
        '
        Me.ID_Tratamiento.HeaderText = "ID_Tratamiento"
        Me.ID_Tratamiento.Name = "ID_Tratamiento"
        Me.ID_Tratamiento.Visible = False
        '
        'Nombre
        '
        Me.Nombre.HeaderText = "Tratamiento"
        Me.Nombre.Name = "Nombre"
        '
        'Descripción
        '
        Me.Descripción.HeaderText = "Descripción"
        Me.Descripción.Name = "Descripción"
        '
        'Precio
        '
        Me.Precio.HeaderText = "Precio"
        Me.Precio.Name = "Precio"
        '
        'lTotal
        '
        Me.lTotal.AutoSize = True
        Me.lTotal.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lTotal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lTotal.Location = New System.Drawing.Point(25, 612)
        Me.lTotal.Name = "lTotal"
        Me.lTotal.Size = New System.Drawing.Size(114, 44)
        Me.lTotal.TabIndex = 253
        Me.lTotal.Text = "Total:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label10.Location = New System.Drawing.Point(136, 612)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 44)
        Me.Label10.TabIndex = 254
        Me.Label10.Text = "₡"
        '
        'lMontoTotal
        '
        Me.lMontoTotal.AutoSize = True
        Me.lMontoTotal.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMontoTotal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lMontoTotal.Location = New System.Drawing.Point(173, 612)
        Me.lMontoTotal.Margin = New System.Windows.Forms.Padding(0)
        Me.lMontoTotal.Name = "lMontoTotal"
        Me.lMontoTotal.Size = New System.Drawing.Size(93, 44)
        Me.lMontoTotal.TabIndex = 255
        Me.lMontoTotal.Text = "0.00"
        Me.lMontoTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Red
        Me.Label11.Location = New System.Drawing.Point(107, 135)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(19, 22)
        Me.Label11.TabIndex = 256
        Me.Label11.Text = "*"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Red
        Me.Label12.Location = New System.Drawing.Point(536, 135)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(19, 22)
        Me.Label12.TabIndex = 257
        Me.Label12.Text = "*"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Red
        Me.Label13.Location = New System.Drawing.Point(564, 252)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(19, 22)
        Me.Label13.TabIndex = 258
        Me.Label13.Text = "*"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Red
        Me.Label14.Location = New System.Drawing.Point(549, 195)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(19, 22)
        Me.Label14.TabIndex = 259
        Me.Label14.Text = "*"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Red
        Me.Label15.Location = New System.Drawing.Point(760, 135)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(19, 22)
        Me.Label15.TabIndex = 260
        Me.Label15.Text = "*"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Red
        Me.Label16.Location = New System.Drawing.Point(131, 441)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(19, 22)
        Me.Label16.TabIndex = 261
        Me.Label16.Text = "*"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.dgvTratamientos)
        Me.Panel3.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel3.Location = New System.Drawing.Point(30, 472)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(630, 126)
        Me.Panel3.TabIndex = 262
        '
        'BarraTitulo
        '
        Me.BarraTitulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.BarraTitulo.Controls.Add(Me.btnMinimizar)
        Me.BarraTitulo.Controls.Add(Me.btnCerrar)
        Me.BarraTitulo.Location = New System.Drawing.Point(0, 0)
        Me.BarraTitulo.Name = "BarraTitulo"
        Me.BarraTitulo.Size = New System.Drawing.Size(903, 43)
        Me.BarraTitulo.TabIndex = 264
        '
        'btnMinimizar
        '
        Me.btnMinimizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMinimizar.FlatAppearance.BorderSize = 0
        Me.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMinimizar.Image = CType(resources.GetObject("btnMinimizar.Image"), System.Drawing.Image)
        Me.btnMinimizar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnMinimizar.Location = New System.Drawing.Point(812, 2)
        Me.btnMinimizar.Name = "btnMinimizar"
        Me.btnMinimizar.Size = New System.Drawing.Size(43, 43)
        Me.btnMinimizar.TabIndex = 4
        Me.btnMinimizar.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCerrar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCerrar.FlatAppearance.BorderSize = 0
        Me.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCerrar.Image = CType(resources.GetObject("btnCerrar.Image"), System.Drawing.Image)
        Me.btnCerrar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnCerrar.Location = New System.Drawing.Point(861, 3)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(39, 39)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'FrmAtencionMedicinaGeneral
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(903, 665)
        Me.Controls.Add(Me.btnListo)
        Me.Controls.Add(Me.BarraTitulo)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.lMontoTotal)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lTotal)
        Me.Controls.Add(Me.rtbDescripcion)
        Me.Controls.Add(Me.cbPacientesDelDia)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.btnEliminarTratamiento)
        Me.Controls.Add(Me.btnAgregarTratamiento)
        Me.Controls.Add(Me.cbxTratamientos)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.tbxSintomas)
        Me.Controls.Add(Me.txtPresionArterial)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.txtTamaño)
        Me.Controls.Add(Me.txtPeso)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.txtHora)
        Me.Controls.Add(Me.txtFuncionario)
        Me.Controls.Add(Me.txtEdad)
        Me.Controls.Add(Me.txtCedula)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmAtencionMedicinaGeneral"
        Me.Text = "FrmAtencionMedicinaGeneral"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTratamientos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.BarraTitulo.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel2 As Panel
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Panel1 As Panel
    Private WithEvents btnEliminarTratamiento As Button
    Private WithEvents btnAgregarTratamiento As Button
    Friend WithEvents cbxTratamientos As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents tbxSintomas As RichTextBox
    Friend WithEvents txtPresionArterial As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents txtTamaño As TextBox
    Friend WithEvents txtPeso As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents txtHora As TextBox
    Friend WithEvents txtFuncionario As TextBox
    Friend WithEvents txtEdad As TextBox
    Friend WithEvents txtCedula As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents cbPacientesDelDia As ComboBox
    Friend WithEvents txtNombre As TextBox
    Friend WithEvents rtbDescripcion As RichTextBox
    Friend WithEvents dgvTratamientos As DataGridView
    Friend WithEvents lTotal As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents lMontoTotal As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents ID_Tratamiento As DataGridViewTextBoxColumn
    Friend WithEvents Nombre As DataGridViewTextBoxColumn
    Friend WithEvents Descripción As DataGridViewTextBoxColumn
    Friend WithEvents Precio As DataGridViewTextBoxColumn
    Friend WithEvents Panel3 As Panel
    Private WithEvents btnListo As Button
    Friend WithEvents BarraTitulo As Panel
    Private WithEvents btnMinimizar As Button
    Private WithEvents btnCerrar As Button
End Class

﻿Public Class ReporteFacturaMedicina

    Dim ID_Cita As Integer
    Dim ID_Factura As Integer

    Public Sub New(ID_CitaA As Integer, ID_FacturaU As Integer)
        InitializeComponent()
        ID_Cita = ID_CitaA
        ID_Factura = ID_FacturaU
    End Sub

    Private Sub ReporteFacturaMedicina_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DataReportes.getFacturaMedicinaReporte' Puede moverla o quitarla según sea necesario.
        Me.getFacturaMedicinaReporteTableAdapter.Fill(Me.DataReportes.getFacturaMedicinaReporte, ID_Factura)
        'TODO: esta línea de código carga datos en la tabla 'DataReportes.ConsultarHistorialMedicinaGeneral' Puede moverla o quitarla según sea necesario.
        Me.ConsultarHistorialMedicinaGeneralTableAdapter.Fill(Me.DataReportes.ConsultarHistorialMedicinaGeneral, ID_Cita)

    End Sub

End Class
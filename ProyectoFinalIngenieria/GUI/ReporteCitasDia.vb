﻿Public Class ReporteCitasDia

    Dim Usuario As String
    Dim ID As Integer
    Dim Fecha As DateTime
    Dim Especialidad As String

    Public Sub New(UsuarioU As String, IDU As Integer, FechaU As DateTime, EspecialidadU As String)
        InitializeComponent()
        Usuario = UsuarioU
        ID = IDU
        Fecha = FechaU
        Especialidad = EspecialidadU
    End Sub


    Private Sub ReporteCitasDia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DataReportes.ConsultarCitasDia' Puede moverla o quitarla según sea necesario.
        Me.ConsultarCitasDiaTableAdapter.Fill(Me.DataReportes.ConsultarCitasDia, Usuario, ID, Fecha, Especialidad)
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class
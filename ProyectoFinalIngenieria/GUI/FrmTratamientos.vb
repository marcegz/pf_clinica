﻿Imports System.Drawing
Imports System.Reflection

Public Class FrmTratamientos

    Dim ln_tratamiento As New Ln_Tratamientos
    Dim tratam As Tratamientos

    Private Sub FrmTratamientos_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.BindWebColors()
        'dgvTratamientos.DataSource = ln_tratamiento.cargartablaActivos()
        cbxActivos.Checked = True
        dgvTratamientos.Columns("ID_Tratamiento").Visible = False
        dgvTratamientos.Columns("Estado").Visible = False
        dgvTratamientos.Columns("Color").Visible = False
        dgvTratamientos.Columns("Descripcion").Visible = False
    End Sub

    'CARGA LA PALETA DE COLORES WEB AL COMBO
    Private Sub BindWebColors()
        'Encuadernación combobox con colores.
        'Asignar modo de dibujo combobox
        cbColor.DrawMode = DrawMode.OwnerDrawFixed
        cbColor.ItemHeight = 20
        Dim colType As Type = GetType(System.Drawing.Color)
        For Each prop As PropertyInfo In colType.GetProperties()
            If prop.PropertyType Is GetType(System.Drawing.Color) Then
                cbColor.Items.Add(prop.Name)
            End If
        Next
    End Sub

    Private Sub cbColor_DrawItem(sender As Object, e As System.Windows.Forms.DrawItemEventArgs) Handles cbColor.DrawItem
        'Dibujando los artículos en el combobox.
        'Cada vez que se dispara cuando un elemento se dibuja en el combobox.
        'salir si no hay ningún elemento
        If e.Index = -1 Then
            Exit Sub
        End If
        'declarar un colorbrush
        Dim colBrush As Brush = New SolidBrush(Color.FromName(DirectCast(cbColor.Items(e.Index), String)))
        'Dibujando rectángulos para los valores de color.
        e.Graphics.DrawRectangle(New Pen(Brushes.Black), e.Bounds.Left + 2, e.Bounds.Top + 2, 30, e.Bounds.Height - 5)
        e.Graphics.FillRectangle(colBrush, e.Bounds.Left + 3, e.Bounds.Top + 3, 29, e.Bounds.Height - 6)
        'Dibuja el nombre del color.
        e.Graphics.DrawString(DirectCast(cbColor.Items(e.Index), String), cbColor.Font, Brushes.Black, 35, e.Bounds.Top + 2)
    End Sub

    'DETECTA EL CAMBIO EN EL COMBO COLOR Y PINTA EL PANEL PEQUEÑO
    Private Sub cbColor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbColor.SelectedIndexChanged

        'PASA DE SELECCION DEL COMBO A STRING
        Dim cmb As ComboBox = CType(sender, ComboBox)
        Dim cr As Color = Color.FromName(cmb.Items(cmb.SelectedIndex))
        Dim sColor As String = ColorTranslator.ToHtml(cr)

        'PASA DE STRING A COLOR
        Dim pintarPanel As Color = ColorTranslator.FromHtml(sColor)
        PColor.BackColor = pintarPanel
    End Sub

    'BOTON GUARDAR
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If (txtNombre.Text <> "") And (txtPrecio.Text <> "") And (tbxDescripcion.Text <> "") And (cbEspecialidad.SelectedItem <> "") Then

            If cbEspecialidad.SelectedItem = "Odontología" Then
                If (cbColor.SelectedItem <> "") Then
                    'Dim cmb As ComboBox = CType(sender, ComboBox)
                    Dim cr As Color = Color.FromName(cbColor.Items(cbColor.SelectedIndex))
                    Dim sColor As String = ColorTranslator.ToHtml(cr)
                    ln_tratamiento.insertarTratamiento(New Tratamientos(txtNombre.Text, cbEspecialidad.SelectedItem,
                                                                        tbxDescripcion.Text, sColor, Convert.ToDecimal(txtPrecio.Text), 0))
                    dgvTratamientos.DataSource = ln_tratamiento.cargartablaActivos()
                    limpiarCamposTexto()
                Else
                    MsgBox("Debe asignarle un color al tratamiento", MsgBoxStyle.Information, "Clinica Medica")
                End If

            Else

                ln_tratamiento.insertarTratamiento(New Tratamientos(txtNombre.Text, cbEspecialidad.SelectedItem,
                                                                        tbxDescripcion.Text, "", Convert.ToDecimal(txtPrecio.Text), 0))

                dgvTratamientos.DataSource = ln_tratamiento.cargartablaActivos()
                limpiarCamposTexto()
            End If

        Else
            MsgBox("Debe ingresar al menos los datos obligatorios (*)!!!", MsgBoxStyle.Critical, "Clinica Medica")

        End If
    End Sub

    Private Sub cbEspecialidad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbEspecialidad.SelectedIndexChanged
        If cbEspecialidad.SelectedItem = "Odontología" Then
            cbColor.Enabled = True
            PColor.Enabled = True
        Else
            cbColor.Enabled = False
            PColor.Enabled = False
        End If
    End Sub

    'Limpiar campos
    Sub limpiarCamposTexto()

        txtNombre.Text = ""
        txtPrecio.Text = ""
        tbxDescripcion.Text = ""
        cbColor.ResetText()
        cbEspecialidad.ResetText()

    End Sub

    'Detecta ingreso de texto en el txt de busqueda
    Private Sub txtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        If cbxActivos.Checked Then
            dgvTratamientos.DataSource = ln_tratamiento.cargarTabla1TratamientoActivo(txtBusqueda.Text)
        End If

        If cbxPapelera.Checked Then
            dgvTratamientos.DataSource = ln_tratamiento.cargarTabla1TratamientoInactivo(txtBusqueda.Text)
        End If
    End Sub

    'Detectar cambios en la seleccion del checkbox activos
    Private Sub cbxActivos_CheckedChanged(sender As Object, e As EventArgs) Handles cbxActivos.CheckedChanged
        cbxActivos.Visible = False
        cbxPapelera.Visible = True
        If cbxPapelera.Visible Then
            dgvTratamientos.DataSource = ln_tratamiento.cargartablaActivos()
        End If
        btnRestablecer.Visible = False
        btnEliminar.Visible = True
        btnActualizar.Visible = True
        btnNuevo.Visible = True
        btnEliminar.Enabled = False
        btnActualizar.Enabled = False
    End Sub

    'Detectar cambios en la seleccion del checkbox papelera
    Private Sub cbxPapelera_CheckedChanged(sender As Object, e As EventArgs) Handles cbxPapelera.CheckedChanged
        cbxActivos.Visible = True
        cbxPapelera.Visible = False
        If cbxActivos.Visible Then
            dgvTratamientos.DataSource = ln_tratamiento.cargartablaInactivos()
        End If

        btnRestablecer.Visible = True
        btnEliminar.Visible = False
        btnNuevo.Visible = False
        btnActualizar.Visible = False
        btnRestablecer.Enabled = False
    End Sub

    'Seleccion de fila de la tabla
    Private Sub dgvTratamientos_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles dgvTratamientos.MouseDoubleClick
        btnEliminar.Enabled = True
        btnActualizar.Enabled = True
        btnRestablecer.Enabled = True
        btnNuevo.Enabled = True
        If dgvTratamientos.SelectedRows.Count > 0 Then

            If dgvTratamientos.CurrentRow.Cells(6).Value = 0 Then
                Estado = 0
            Else
                Estado = 1
            End If

            tratam = New Tratamientos(CInt(dgvTratamientos.CurrentRow.Cells(0).Value.ToString),
                                      dgvTratamientos.CurrentRow.Cells(1).Value.ToString,
                                      dgvTratamientos.CurrentRow.Cells(2).Value.ToString,
                                      dgvTratamientos.CurrentRow.Cells(4).Value.ToString,
                                      dgvTratamientos.CurrentRow.Cells(5).Value.ToString,
                                      dgvTratamientos.CurrentRow.Cells(3).Value.ToString,
                                      Estado)

            txtNombre.Text = tratam.Nombre_
            txtPrecio.Text = tratam.Precio_
            tbxDescripcion.Text = tratam.Descripcion_
            cbColor.SelectedItem = tratam.Color_
            cbEspecialidad.SelectedItem = tratam.Especialidad_

        End If
    End Sub

    'Click en el boton nuevo
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        If MsgBox("¿Está seguro que desea ingresar un nuevo Tratamiento, se eliminará la busqueda realizada?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            limpiarCamposTexto()
            PColor.BackColor = Color.White
            btnActualizar.Enabled = False
            btnEliminar.Enabled = False
            btnNuevo.Enabled = False
        End If
    End Sub

    'Click en el boton Eliminar
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        ln_tratamiento.bloquearTratamiento(CInt(dgvTratamientos.CurrentRow.Cells(0).Value.ToString))
        dgvTratamientos.DataSource = ln_tratamiento.cargartablaActivos()
        btnEliminar.Enabled = False
        btnActualizar.Enabled = False
        limpiarCamposTexto()
    End Sub

    'Click en el boton Actualizar
    Private Sub btnRestablecer_Click(sender As Object, e As EventArgs) Handles btnRestablecer.Click
        ln_tratamiento.restablecerTratamiento(CInt(dgvTratamientos.CurrentRow.Cells(0).Value.ToString))
        cbxActivos.Visible = False
        cbxPapelera.Visible = True
        btnRestablecer.Visible = False
        btnEliminar.Visible = True
        btnActualizar.Visible = True
        btnNuevo.Visible = True
        btnEliminar.Enabled = False
        btnActualizar.Enabled = False
        btnNuevo.Enabled = False
        dgvTratamientos.DataSource = ln_tratamiento.cargartablaActivos()
        limpiarCamposTexto()

    End Sub

    'Click en el boton actualizar
    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        If cbEspecialidad.SelectedItem = "Odontología" Then
            Dim cr As Color = Color.FromName(cbColor.Items(cbColor.SelectedIndex))
            Dim sColor As String = ColorTranslator.ToHtml(cr)
            ln_tratamiento.actualizarTratamientos(New Tratamientos(CInt(dgvTratamientos.CurrentRow.Cells(0).Value.ToString),
                                                                txtNombre.Text, cbEspecialidad.SelectedItem,
                                                                tbxDescripcion.Text, sColor, Convert.ToDecimal(txtPrecio.Text), 0))

            dgvTratamientos.DataSource = ln_tratamiento.cargartablaActivos()
            limpiarCamposTexto()
            btnEliminar.Enabled = False
            btnActualizar.Enabled = False
            btnNuevo.Enabled = False
            PColor.BackColor = Color.White
        Else
            ln_tratamiento.actualizarTratamientos(New Tratamientos(CInt(dgvTratamientos.CurrentRow.Cells(0).Value.ToString),
                                                               txtNombre.Text, cbEspecialidad.SelectedItem,
                                                               tbxDescripcion.Text, "", Convert.ToDecimal(txtPrecio.Text), 0))

            dgvTratamientos.DataSource = ln_tratamiento.cargartablaActivos()
            limpiarCamposTexto()
            btnEliminar.Enabled = False
            btnActualizar.Enabled = False
            btnNuevo.Enabled = False
            PColor.BackColor = Color.White
        End If

    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCreditos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCreditos))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.btnGuardarACredito = New System.Windows.Forms.Button()
        Me.btnActualizar = New System.Windows.Forms.Button()
        Me.dgvCreditos = New System.Windows.Forms.DataGridView()
        Me.Paciente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tratamiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalActual = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaAbono = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaFinal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBusquedaCreditos = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtCuotaMensualCredito = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtNumMeses = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cbTratamientos = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCedulaPaciente = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtIdCredito = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TxtPaciente = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.dtmBusquedaAbono = New System.Windows.Forms.DateTimePicker()
        Me.btnGenerarAbono = New System.Windows.Forms.Button()
        Me.btnActualizarAbono = New System.Windows.Forms.Button()
        Me.dgvAbonos = New System.Windows.Forms.DataGridView()
        Me.NumeroAbonos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaldoAnterior = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaldoActual = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha_pago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBusquedaAbonos = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtSaldoActual = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtSaldoAnterior = New System.Windows.Forms.TextBox()
        Me.dtpFechaAbono = New System.Windows.Forms.DateTimePicker()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtCreditoAnono = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtCuotaMensual = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtNumeroAbono = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtIdAbono = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvCreditos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvAbonos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 66)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(879, 544)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 30)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(871, 510)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Créditos"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.DateTimePicker1)
        Me.Panel1.Controls.Add(Me.btnGuardarACredito)
        Me.Panel1.Controls.Add(Me.btnActualizar)
        Me.Panel1.Controls.Add(Me.dgvCreditos)
        Me.Panel1.Controls.Add(Me.txtBusquedaCreditos)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtTotal)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.txtCuotaMensualCredito)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.txtNumMeses)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.cbTratamientos)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txtCedulaPaciente)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.txtIdCredito)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.TxtPaciente)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(901, 514)
        Me.Panel1.TabIndex = 0
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = "mm/dd/aa"
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(729, 39)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(122, 27)
        Me.DateTimePicker1.TabIndex = 90
        '
        'btnGuardarACredito
        '
        Me.btnGuardarACredito.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnGuardarACredito.FlatAppearance.BorderSize = 0
        Me.btnGuardarACredito.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardarACredito.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardarACredito.ForeColor = System.Drawing.Color.White
        Me.btnGuardarACredito.Location = New System.Drawing.Point(54, 448)
        Me.btnGuardarACredito.Name = "btnGuardarACredito"
        Me.btnGuardarACredito.Size = New System.Drawing.Size(99, 35)
        Me.btnGuardarACredito.TabIndex = 85
        Me.btnGuardarACredito.Text = "Guardar"
        Me.btnGuardarACredito.UseVisualStyleBackColor = False
        '
        'btnActualizar
        '
        Me.btnActualizar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnActualizar.FlatAppearance.BorderSize = 0
        Me.btnActualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActualizar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActualizar.ForeColor = System.Drawing.Color.White
        Me.btnActualizar.Location = New System.Drawing.Point(205, 448)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(99, 35)
        Me.btnActualizar.TabIndex = 86
        Me.btnActualizar.Text = "Actualizar"
        Me.btnActualizar.UseVisualStyleBackColor = False
        '
        'dgvCreditos
        '
        Me.dgvCreditos.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.dgvCreditos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvCreditos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCreditos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Paciente, Me.Tratamiento, Me.TotalActual, Me.FechaAbono, Me.FechaFinal})
        Me.dgvCreditos.Location = New System.Drawing.Point(390, 81)
        Me.dgvCreditos.Name = "dgvCreditos"
        Me.dgvCreditos.Size = New System.Drawing.Size(461, 350)
        Me.dgvCreditos.TabIndex = 44
        '
        'Paciente
        '
        Me.Paciente.HeaderText = "Paciente"
        Me.Paciente.Name = "Paciente"
        '
        'Tratamiento
        '
        Me.Tratamiento.HeaderText = "Tratamiento"
        Me.Tratamiento.Name = "Tratamiento"
        Me.Tratamiento.Width = 120
        '
        'TotalActual
        '
        Me.TotalActual.HeaderText = "Total Actual"
        Me.TotalActual.Name = "TotalActual"
        Me.TotalActual.Width = 80
        '
        'FechaAbono
        '
        Me.FechaAbono.HeaderText = "Fecha Abono"
        Me.FechaAbono.Name = "FechaAbono"
        '
        'FechaFinal
        '
        Me.FechaFinal.HeaderText = "Fecha finalizacion"
        Me.FechaFinal.Name = "FechaFinal"
        Me.FechaFinal.Width = 150
        '
        'txtBusquedaCreditos
        '
        Me.txtBusquedaCreditos.Location = New System.Drawing.Point(484, 39)
        Me.txtBusquedaCreditos.Name = "txtBusquedaCreditos"
        Me.txtBusquedaCreditos.Size = New System.Drawing.Size(216, 27)
        Me.txtBusquedaCreditos.TabIndex = 41
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(386, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 21)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Busqueda:"
        '
        'txtTotal
        '
        Me.txtTotal.Location = New System.Drawing.Point(159, 90)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(189, 27)
        Me.txtTotal.TabIndex = 2
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(19, 90)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(53, 21)
        Me.Label12.TabIndex = 36
        Me.Label12.Text = "Total:"
        '
        'txtCuotaMensualCredito
        '
        Me.txtCuotaMensualCredito.Location = New System.Drawing.Point(159, 358)
        Me.txtCuotaMensualCredito.Name = "txtCuotaMensualCredito"
        Me.txtCuotaMensualCredito.Size = New System.Drawing.Size(189, 27)
        Me.txtCuotaMensualCredito.TabIndex = 35
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(19, 359)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(134, 21)
        Me.Label11.TabIndex = 34
        Me.Label11.Text = "Cuota Mensual:"
        '
        'txtNumMeses
        '
        Me.txtNumMeses.Location = New System.Drawing.Point(159, 302)
        Me.txtNumMeses.Name = "txtNumMeses"
        Me.txtNumMeses.Size = New System.Drawing.Size(189, 27)
        Me.txtNumMeses.TabIndex = 33
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(19, 303)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(106, 21)
        Me.Label10.TabIndex = 32
        Me.Label10.Text = "Num. Meses:"
        '
        'cbTratamientos
        '
        Me.cbTratamientos.FormattingEnabled = True
        Me.cbTratamientos.Location = New System.Drawing.Point(159, 244)
        Me.cbTratamientos.Name = "cbTratamientos"
        Me.cbTratamientos.Size = New System.Drawing.Size(189, 29)
        Me.cbTratamientos.TabIndex = 31
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(19, 248)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(111, 21)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Tratamiento:"
        '
        'txtCedulaPaciente
        '
        Me.txtCedulaPaciente.Location = New System.Drawing.Point(159, 190)
        Me.txtCedulaPaciente.Name = "txtCedulaPaciente"
        Me.txtCedulaPaciente.Size = New System.Drawing.Size(189, 27)
        Me.txtCedulaPaciente.TabIndex = 29
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(19, 191)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 21)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "Cédula:"
        '
        'txtIdCredito
        '
        Me.txtIdCredito.Location = New System.Drawing.Point(159, 39)
        Me.txtIdCredito.Name = "txtIdCredito"
        Me.txtIdCredito.Size = New System.Drawing.Size(189, 27)
        Me.txtIdCredito.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(19, 42)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(31, 21)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "ID:"
        '
        'TxtPaciente
        '
        Me.TxtPaciente.Location = New System.Drawing.Point(159, 136)
        Me.TxtPaciente.Name = "TxtPaciente"
        Me.TxtPaciente.Size = New System.Drawing.Size(189, 27)
        Me.TxtPaciente.TabIndex = 25
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(19, 137)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 21)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "Paciente:"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel4)
        Me.TabPage2.Location = New System.Drawing.Point(4, 30)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(871, 510)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Abonos"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel4.Controls.Add(Me.Button2)
        Me.Panel4.Controls.Add(Me.dtmBusquedaAbono)
        Me.Panel4.Controls.Add(Me.btnGenerarAbono)
        Me.Panel4.Controls.Add(Me.btnActualizarAbono)
        Me.Panel4.Controls.Add(Me.dgvAbonos)
        Me.Panel4.Controls.Add(Me.txtBusquedaAbonos)
        Me.Panel4.Controls.Add(Me.Label8)
        Me.Panel4.Controls.Add(Me.Label17)
        Me.Panel4.Controls.Add(Me.txtSaldoActual)
        Me.Panel4.Controls.Add(Me.Label14)
        Me.Panel4.Controls.Add(Me.txtSaldoAnterior)
        Me.Panel4.Controls.Add(Me.dtpFechaAbono)
        Me.Panel4.Controls.Add(Me.Label18)
        Me.Panel4.Controls.Add(Me.txtCreditoAnono)
        Me.Panel4.Controls.Add(Me.Label21)
        Me.Panel4.Controls.Add(Me.txtCuotaMensual)
        Me.Panel4.Controls.Add(Me.Label15)
        Me.Panel4.Controls.Add(Me.txtNumeroAbono)
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Controls.Add(Me.txtIdAbono)
        Me.Panel4.Controls.Add(Me.Label19)
        Me.Panel4.Location = New System.Drawing.Point(-4, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(903, 536)
        Me.Panel4.TabIndex = 0
        '
        'dtmBusquedaAbono
        '
        Me.dtmBusquedaAbono.CustomFormat = "mm/dd/aa"
        Me.dtmBusquedaAbono.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtmBusquedaAbono.Location = New System.Drawing.Point(723, 32)
        Me.dtmBusquedaAbono.Name = "dtmBusquedaAbono"
        Me.dtmBusquedaAbono.Size = New System.Drawing.Size(122, 27)
        Me.dtmBusquedaAbono.TabIndex = 89
        '
        'btnGenerarAbono
        '
        Me.btnGenerarAbono.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnGenerarAbono.FlatAppearance.BorderSize = 0
        Me.btnGenerarAbono.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerarAbono.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerarAbono.ForeColor = System.Drawing.Color.White
        Me.btnGenerarAbono.Location = New System.Drawing.Point(28, 445)
        Me.btnGenerarAbono.Name = "btnGenerarAbono"
        Me.btnGenerarAbono.Size = New System.Drawing.Size(152, 35)
        Me.btnGenerarAbono.TabIndex = 87
        Me.btnGenerarAbono.Text = "Generar Abono"
        Me.btnGenerarAbono.UseVisualStyleBackColor = False
        '
        'btnActualizarAbono
        '
        Me.btnActualizarAbono.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnActualizarAbono.FlatAppearance.BorderSize = 0
        Me.btnActualizarAbono.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActualizarAbono.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActualizarAbono.ForeColor = System.Drawing.Color.White
        Me.btnActualizarAbono.Location = New System.Drawing.Point(217, 445)
        Me.btnActualizarAbono.Name = "btnActualizarAbono"
        Me.btnActualizarAbono.Size = New System.Drawing.Size(152, 35)
        Me.btnActualizarAbono.TabIndex = 88
        Me.btnActualizarAbono.Text = "Actualizar"
        Me.btnActualizarAbono.UseVisualStyleBackColor = False
        '
        'dgvAbonos
        '
        Me.dgvAbonos.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.dgvAbonos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvAbonos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAbonos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumeroAbonos, Me.SaldoAnterior, Me.SaldoActual, Me.Fecha_pago})
        Me.dgvAbonos.Location = New System.Drawing.Point(409, 67)
        Me.dgvAbonos.Name = "dgvAbonos"
        Me.dgvAbonos.Size = New System.Drawing.Size(436, 350)
        Me.dgvAbonos.TabIndex = 63
        '
        'NumeroAbonos
        '
        Me.NumeroAbonos.HeaderText = "Numero Abonos"
        Me.NumeroAbonos.Name = "NumeroAbonos"
        Me.NumeroAbonos.Width = 80
        '
        'SaldoAnterior
        '
        Me.SaldoAnterior.HeaderText = "SaldoAnterior"
        Me.SaldoAnterior.Name = "SaldoAnterior"
        Me.SaldoAnterior.Width = 120
        '
        'SaldoActual
        '
        Me.SaldoActual.HeaderText = "Saldo Actual"
        Me.SaldoActual.Name = "SaldoActual"
        Me.SaldoActual.Width = 110
        '
        'Fecha_pago
        '
        Me.Fecha_pago.HeaderText = "Fecha"
        Me.Fecha_pago.Name = "Fecha_pago"
        '
        'txtBusquedaAbonos
        '
        Me.txtBusquedaAbonos.Location = New System.Drawing.Point(503, 32)
        Me.txtBusquedaAbonos.Name = "txtBusquedaAbonos"
        Me.txtBusquedaAbonos.Size = New System.Drawing.Size(195, 27)
        Me.txtBusquedaAbonos.TabIndex = 62
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(405, 35)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(92, 21)
        Me.Label8.TabIndex = 61
        Me.Label8.Text = "Busqueda:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(24, 277)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(115, 21)
        Me.Label17.TabIndex = 60
        Me.Label17.Text = "Saldo Actual:"
        '
        'txtSaldoActual
        '
        Me.txtSaldoActual.Location = New System.Drawing.Point(164, 274)
        Me.txtSaldoActual.Name = "txtSaldoActual"
        Me.txtSaldoActual.Size = New System.Drawing.Size(205, 27)
        Me.txtSaldoActual.TabIndex = 5
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(24, 216)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(124, 21)
        Me.Label14.TabIndex = 58
        Me.Label14.Text = "Saldo Anterior:"
        '
        'txtSaldoAnterior
        '
        Me.txtSaldoAnterior.Location = New System.Drawing.Point(164, 213)
        Me.txtSaldoAnterior.Name = "txtSaldoAnterior"
        Me.txtSaldoAnterior.Size = New System.Drawing.Size(205, 27)
        Me.txtSaldoAnterior.TabIndex = 4
        '
        'dtpFechaAbono
        '
        Me.dtpFechaAbono.CustomFormat = "mm/dd/aa"
        Me.dtpFechaAbono.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaAbono.Location = New System.Drawing.Point(164, 157)
        Me.dtpFechaAbono.Name = "dtpFechaAbono"
        Me.dtpFechaAbono.Size = New System.Drawing.Size(205, 27)
        Me.dtpFechaAbono.TabIndex = 3
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(24, 162)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(63, 21)
        Me.Label18.TabIndex = 55
        Me.Label18.Text = "Fecha:"
        '
        'txtCreditoAnono
        '
        Me.txtCreditoAnono.Location = New System.Drawing.Point(164, 101)
        Me.txtCreditoAnono.Name = "txtCreditoAnono"
        Me.txtCreditoAnono.Size = New System.Drawing.Size(205, 27)
        Me.txtCreditoAnono.TabIndex = 2
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(24, 104)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(73, 21)
        Me.Label21.TabIndex = 53
        Me.Label21.Text = "Crédito:"
        '
        'txtCuotaMensual
        '
        Me.txtCuotaMensual.Location = New System.Drawing.Point(164, 390)
        Me.txtCuotaMensual.Name = "txtCuotaMensual"
        Me.txtCuotaMensual.Size = New System.Drawing.Size(205, 27)
        Me.txtCuotaMensual.TabIndex = 7
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(24, 393)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(134, 21)
        Me.Label15.TabIndex = 51
        Me.Label15.Text = "Cuota Mensual:"
        '
        'txtNumeroAbono
        '
        Me.txtNumeroAbono.Location = New System.Drawing.Point(164, 331)
        Me.txtNumeroAbono.Name = "txtNumeroAbono"
        Me.txtNumeroAbono.Size = New System.Drawing.Size(205, 27)
        Me.txtNumeroAbono.TabIndex = 6
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(24, 331)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(113, 21)
        Me.Label16.TabIndex = 49
        Me.Label16.Text = "Num. Abono:"
        '
        'txtIdAbono
        '
        Me.txtIdAbono.Location = New System.Drawing.Point(164, 49)
        Me.txtIdAbono.Name = "txtIdAbono"
        Me.txtIdAbono.Size = New System.Drawing.Size(205, 27)
        Me.txtIdAbono.TabIndex = 1
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(24, 52)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(31, 21)
        Me.Label19.TabIndex = 47
        Me.Label19.Text = "ID:"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(901, 3)
        Me.PictureBox2.TabIndex = 37
        Me.PictureBox2.TabStop = False
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.Label22.Location = New System.Drawing.Point(326, 17)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(171, 33)
        Me.Label22.TabIndex = 38
        Me.Label22.Text = "Formulario /"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(493, 17)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(126, 33)
        Me.Label20.TabIndex = 39
        Me.Label20.Text = "Créditos"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel7.Controls.Add(Me.Label20)
        Me.Panel7.Controls.Add(Me.Label22)
        Me.Panel7.Controls.Add(Me.PictureBox2)
        Me.Panel7.Location = New System.Drawing.Point(0, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(901, 66)
        Me.Panel7.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(668, 448)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(183, 35)
        Me.Button1.TabIndex = 125
        Me.Button1.Text = "Exportar Búsqueda"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(662, 445)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(183, 35)
        Me.Button2.TabIndex = 125
        Me.Button2.Text = "Exportar Búsqueda"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'FrmCreditos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(903, 622)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "FrmCreditos"
        Me.Text = "Créditos"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvCreditos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvAbonos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents Panel1 As Panel
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents Panel4 As Panel
    Friend WithEvents dgvCreditos As DataGridView
    Friend WithEvents txtBusquedaCreditos As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtTotal As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtCuotaMensualCredito As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtNumMeses As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents cbTratamientos As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtCedulaPaciente As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtIdCredito As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents TxtPaciente As TextBox
    Friend WithEvents Label4 As Label
    Private WithEvents btnGuardarACredito As Button
    Private WithEvents btnActualizar As Button
    Friend WithEvents Paciente As DataGridViewTextBoxColumn
    Friend WithEvents Tratamiento As DataGridViewTextBoxColumn
    Friend WithEvents TotalActual As DataGridViewTextBoxColumn
    Friend WithEvents FechaAbono As DataGridViewTextBoxColumn
    Friend WithEvents FechaFinal As DataGridViewTextBoxColumn
    Friend WithEvents dgvAbonos As DataGridView
    Friend WithEvents txtBusquedaAbonos As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents txtSaldoActual As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txtSaldoAnterior As TextBox
    Friend WithEvents dtpFechaAbono As DateTimePicker
    Friend WithEvents Label18 As Label
    Friend WithEvents txtCreditoAnono As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents txtCuotaMensual As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtNumeroAbono As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txtIdAbono As TextBox
    Friend WithEvents Label19 As Label
    Private WithEvents btnGenerarAbono As Button
    Private WithEvents btnActualizarAbono As Button
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Label22 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Panel7 As Panel
    Friend WithEvents NumeroAbonos As DataGridViewTextBoxColumn
    Friend WithEvents SaldoAnterior As DataGridViewTextBoxColumn
    Friend WithEvents SaldoActual As DataGridViewTextBoxColumn
    Friend WithEvents Fecha_pago As DataGridViewTextBoxColumn
    Friend WithEvents dtmBusquedaAbono As DateTimePicker
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Private WithEvents Button1 As Button
    Private WithEvents Button2 As Button
End Class

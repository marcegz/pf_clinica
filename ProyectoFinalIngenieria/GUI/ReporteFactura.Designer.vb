﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ReporteFactura
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource5 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource6 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.ConsultarHistorialDientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataReportes = New ProyectoFinalIngenieria.DataReportes()
        Me.getFacturaReporteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultarHistorialDientesTableAdapter = New ProyectoFinalIngenieria.DataReportesTableAdapters.ConsultarHistorialDientesTableAdapter()
        Me.getFacturaReporteTableAdapter = New ProyectoFinalIngenieria.DataReportesTableAdapters.getFacturaReporteTableAdapter()
        CType(Me.ConsultarHistorialDientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.getFacturaReporteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        ReportDataSource5.Name = "DataFactura"
        ReportDataSource5.Value = Me.ConsultarHistorialDientesBindingSource
        ReportDataSource6.Name = "DataSet1"
        ReportDataSource6.Value = Me.getFacturaReporteBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource5)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource6)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ProyectoFinalIngenieria.Factura.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(809, 581)
        Me.ReportViewer1.TabIndex = 0
        '
        'ConsultarHistorialDientesBindingSource
        '
        Me.ConsultarHistorialDientesBindingSource.DataMember = "ConsultarHistorialDientes"
        Me.ConsultarHistorialDientesBindingSource.DataSource = Me.DataReportes
        '
        'DataReportes
        '
        Me.DataReportes.DataSetName = "DataReportes"
        Me.DataReportes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'getFacturaReporteBindingSource
        '
        Me.getFacturaReporteBindingSource.DataMember = "getFacturaReporte"
        Me.getFacturaReporteBindingSource.DataSource = Me.DataReportes
        '
        'ConsultarHistorialDientesTableAdapter
        '
        Me.ConsultarHistorialDientesTableAdapter.ClearBeforeFill = True
        '
        'getFacturaReporteTableAdapter
        '
        Me.getFacturaReporteTableAdapter.ClearBeforeFill = True
        '
        'ReporteFactura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(765, 581)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "ReporteFactura"
        Me.Text = "Reporte Factura"
        CType(Me.ConsultarHistorialDientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.getFacturaReporteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents ConsultarHistorialDientesBindingSource As BindingSource
    Friend WithEvents DataReportes As DataReportes
    Friend WithEvents getFacturaReporteBindingSource As BindingSource
    Friend WithEvents ConsultarHistorialDientesTableAdapter As DataReportesTableAdapters.ConsultarHistorialDientesTableAdapter
    Friend WithEvents getFacturaReporteTableAdapter As DataReportesTableAdapters.getFacturaReporteTableAdapter
End Class

﻿Public Class FrmCitas
    Dim id_ As Integer
    Dim ln_paci As New Ln_Paciente
    Dim ln_Hora As New Ln_Horas
    Dim ln_Citas As New Ln_Citas
    Dim ln_funcionario As New Ln_Funcionario
    Dim fr_p As FormPrincipal
    Dim cita As Citas

    Public Sub New(id As Integer, fr_p As FormPrincipal)

        'Esta llamada es exigida por el diseñador.
        InitializeComponent()

        'Agregue cualquier inicialización después de la llamada a InitializeComponent().
        id_ = id
        Me.fr_p = fr_p
        If id_ = 1 Then
            txtEspecialidad.Text = "Odontología"
            cbFuncionario.DisplayMember = "NombreCompleto"
            cbFuncionario.ValueMember = "ID_Funcionario"
            cbFuncionario.DataSource = ln_funcionario.funcionariosActivosporEspecialidad(txtEspecialidad.Text)

            txtEspecialidadPacienteDia.Text = "Odontología"
            txtEspecialidadPacienteDia.Enabled = False
            ' txtFuncionarioPacienteDia.Text = "NombreCompleto"
            '  cbFuncionario.ValueMember = "ID_Funcionario"
            cbFuncionario.DataSource = ln_funcionario.funcionariosActivosporEspecialidad(txtEspecialidad.Text)

        Else
            txtEspecialidad.Text = "Medicina General"
            cbFuncionario.DisplayMember = "NombreCompleto"
            cbFuncionario.ValueMember = "ID_Funcionario"
            cbFuncionario.DataSource = ln_funcionario.funcionariosActivosporEspecialidad(txtEspecialidad.Text)
        End If
    End Sub

    Private Sub FrmCitas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Me.PacientesActivosTodosTableAdapter.Fill(Me.DB_A494E9_PFIngenieriaDataSet3.pacientesActivosTodos)
        llenarCombo(Date.Now)
        llenarComboFuncionarios()
        llenarComboPacientes()
        limpiar()
        dgvCitas.DataSource = ln_Citas.ConsultaHorasUtilizadas(Date.Now, txtEspecialidad.Text)
        dgvCitas.Columns("ID_Hora").Visible = False
        dgvCitas.Columns("ID_Citas").Visible = False
        dgvCitas.Columns("ID_Paciente").Visible = False
        dgvCitas.Columns("ID_Funcionario").Visible = False

        Dim btnConsultar As DataGridViewButtonColumn = New DataGridViewButtonColumn()
        btnConsultar.Name = "Consultar"
        btnConsultar.FlatStyle = FlatStyle.Flat
        dgvCitasDia.Columns.Add(btnConsultar)



        dgvCitasDia.DataSource = ln_Citas.ConsultaHorasUtilizadas(Date.Now, txtEspecialidad.Text)
        dgvCitasDia.Columns("ID_Hora").Visible = False
        dgvCitasDia.Columns("ID_Citas").Visible = False
        dgvCitasDia.Columns("ID_Paciente").Visible = False
        dgvCitasDia.Columns("ID_Funcionario").Visible = False
        dgvCitasDia.Columns("Descripción").Visible = False
        dgvCitasDia.Columns("Funcionario").Visible = False

        Dim btnFacturacion As DataGridViewButtonColumn = New DataGridViewButtonColumn()
        btnFacturacion.Name = "Facturación"
        btnFacturacion.FlatStyle = FlatStyle.Flat
        dgvCitasDia.Columns.Add(btnFacturacion)

        dgvCitasDia.Columns(0).HeaderText = "?"
        dgvCitasDia.Columns(9).HeaderText = "$"
        dgvCitasDia.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
    End Sub

    'DETECTA EL CAMBIO DEL COMBO PACIENTES Y AGREGA LOS DATOS NECESARIOS
    Private Sub cbPaciente_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbPaciente.SelectedValueChanged
        Dim pacientex As Pacientes

        Try
            pacientex = ln_paci.Consultar1Paciente(cbPaciente.SelectedValue)
            txtCedula.Text = pacientex.Cedula_
            txtEdad.Text = getEdad(pacientex.FechaNacimiento_.Year())
        Catch

        End Try
    End Sub

    'LLENA TODOS LOS COMBOBOX
    Sub llenarCombo(fecha As Date)
        cbHora.DisplayMember = "Hora"
        cbHora.ValueMember = "ID_Hora"
        cbHora.DataSource = ln_Hora.ConsultarHorasDisponibles(fecha, txtEspecialidad.Text)
    End Sub

    Sub llenarCombo2(id As Integer)
        cbHora.DisplayMember = "Hora"
        cbHora.ValueMember = "ID_Hora"
        Dim data As DataTable = ln_Hora.llenarCombo(dtpFecha.Value, txtEspecialidad.Text)
        Dim dRow As DataRow = data.NewRow()
        Dim ho As Horas = ln_Hora.ConsultarHorasDisponiblesTodas(id)
        dRow("ID_Hora") = ho.ID_Hora_
        dRow("Hora") = ho.Hora_
        dRow("Estado") = ho.Estado_
        data.Rows.Add(dRow)
        cbHora.DataSource = data
    End Sub

    Sub llenarComboFuncionarios()
        cbFuncionario.DisplayMember = "NombreCompleto"
        cbFuncionario.ValueMember = "ID_Funcionario"
        cbFuncionario.DataSource = ln_funcionario.funcionariosActivosporEspecialidad(txtEspecialidad.Text)
    End Sub

    Sub llenarComboPacientes()
        cbPaciente.DisplayMember = "NombreCompleto"
        cbPaciente.ValueMember = "Cedula"
        cbPaciente.DataSource = ln_paci.cargartablaActivos()
    End Sub

    'SACA LA EDAD RECIBIENDO EL AÑO
    Function getEdad(año As Integer) As Integer
        Dim añoactual As Integer = Year(Now)
        getEdad = añoactual - año
    End Function

    'detectar el valor del data time picker
    Private Sub dtpHora_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged
        dgvCitas.DataSource = ln_Citas.ConsultaHorasUtilizadas(dtpFecha.Value.Date, txtEspecialidad.Text)
        dgvCitas.Columns("ID_Hora").Visible = False
        llenarCombo(dtpFecha.Value)
    End Sub

    'DETECTAR CLICK BOTON GUARDAR
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If cbPaciente.SelectedIndex <> -1 And cbFuncionario.SelectedIndex <> -1 And cbHora.SelectedIndex <> -1 And
            tbxDescripcion.Text <> "" Then
            Dim id_funcio As Integer = Convert.ToInt32(cbFuncionario.SelectedValue)
            ln_Citas.insertarCita(New Citas(cbHora.SelectedValue, ln_paci.ExtraerIDPaciente(cbPaciente.SelectedValue),
                                            cbFuncionario.SelectedValue,
                                        dtpFechaCita.Value.Date, txtEspecialidad.Text, tbxDescripcion.Text, "Pendiente"))
            dgvCitas.DataSource = ln_Citas.ConsultaHorasUtilizadas(dtpFechaCita.Value.Date, txtEspecialidad.Text)
            llenarCombo(dtpFechaCita.Value.Date)
            limpiar()

            dgvCitasDia.DataSource = ln_Citas.ConsultaHorasUtilizadas(Date.Now, txtEspecialidad.Text)
            dgvCitasDia.Columns("ID_Hora").Visible = False
            dgvCitasDia.Columns("ID_Citas").Visible = False
            dgvCitasDia.Columns("ID_Paciente").Visible = False
            dgvCitasDia.Columns("ID_Funcionario").Visible = False

        Else
            MsgBox("Debe ingresar al menos los datos obligatorios (*)!!!", MsgBoxStyle.Critical, "Clinica Medica")

        End If
    End Sub

    'DETECTAR CLICK EN EL BOTON ELIMINAR
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click

        cita = ln_Citas.ExtraerCita(CInt(dgvCitas.CurrentRow.Cells(2).Value.ToString))

        If cita.Estado_ = "Pendiente" Or cita.Estado_ = "Ausente" Then

            ln_Citas.EliminarConsulta(cita.ID_Cita_)

        End If

        ln_Citas.EliminarCita(CInt(dgvCitas.CurrentRow.Cells(2).Value.ToString))
        dgvCitas.DataSource = ln_Citas.ConsultaHorasUtilizadas(dtpFecha.Value.Date, txtEspecialidad.Text)
        llenarCombo(dtpFecha.Value.Date)
        btnActualizar.Enabled = False
        btnEliminar.Enabled = False
        limpiar()

        refrescarTablaCitasDia()

    End Sub

    Sub refrescarTablaCitasDia()
        dgvCitasDia.DataSource = ln_Citas.ConsultaHorasUtilizadas(Date.Now, txtEspecialidad.Text)
        dgvCitasDia.Columns("ID_Hora").Visible = False
        dgvCitasDia.Columns("ID_Citas").Visible = False
        dgvCitasDia.Columns("ID_Paciente").Visible = False
        dgvCitasDia.Columns("ID_Funcionario").Visible = False
    End Sub



    'DETECTAR DOBLE CLICK EN LA TABLA
    Private Sub dgvCitas_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles dgvCitas.MouseDoubleClick
        If dgvCitas.SelectedRows.Count > 0 Then
            If dgvCitas.CurrentRow.Cells(2).Value.ToString = "" Then
                'limpiar()
                'cbHora.SelectedText = dgvCitas.CurrentRow.Cells(1).Value.ToString
                'cbHora.SelectedValue = CInt(dgvCitas.CurrentRow.Cells(0).Value.ToString)

                If tbxDescripcion.Text <> "" Then
                    If MsgBox("Se cambiará la fecha de la cita. ¿Desea continuar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
                         MsgBoxResult.Yes Then
                        cbHora.SelectedText = dgvCitas.CurrentRow.Cells(1).Value.ToString
                        cbHora.SelectedValue = CInt(dgvCitas.CurrentRow.Cells(0).Value.ToString)
                    End If

                Else
                    limpiar()
                    cbHora.SelectedText = dgvCitas.CurrentRow.Cells(1).Value.ToString
                    cbHora.SelectedValue = CInt(dgvCitas.CurrentRow.Cells(0).Value.ToString)
                End If
            Else

                cita = ln_Citas.ExtraerCita(CInt(dgvCitas.CurrentRow.Cells(2).Value.ToString))
                btnEliminar.Enabled = True
                If dtpFecha.Value.Date.Day >= Date.Now.Day And dtpFecha.Value.Date.Month >= Date.Now.Month And
                    dtpFecha.Value.Date.Year >= Date.Now.Year Then
                    btnActualizar.Enabled = True
                End If


                limpiar()
                Dim pas As Pacientes = ln_paci.ConsultarPacienteCOMPLETO(CInt(dgvCitas.CurrentRow.Cells(6).Value.ToString))

                cbPaciente.SelectedText = pas.NombreCompleto_
                cbPaciente.SelectedValue = pas.Cedula_

                MsgBox("" + dgvCitas.CurrentRow.Cells(0).Value.ToString)
                llenarCombo2(CInt(dgvCitas.CurrentRow.Cells(0).Value.ToString))
                cbHora.SelectedText = dgvCitas.CurrentRow.Cells(1).Value.ToString
                cbHora.SelectedValue = CInt(dgvCitas.CurrentRow.Cells(0).Value.ToString)

                dtpFechaCita.Value = cita.Fecha_

                cbFuncionario.SelectedText = dgvCitas.CurrentRow.Cells(5).Value.ToString
                cbFuncionario.SelectedValue = CInt(dgvCitas.CurrentRow.Cells(7).Value.ToString)

                tbxDescripcion.Text = dgvCitas.CurrentRow.Cells(4).Value.ToString

            End If

            btnGuardar.Visible = False
            btnLimpiar.Visible = True

        End If
    End Sub

    'METODO LIMPIAR
    Sub limpiar()
        tbxDescripcion.Text = ""
        cbFuncionario.ResetText()
        cbHora.ResetText()
        dtpFechaCita.Value = Date.Now
        cbPaciente.ResetText()
        txtCedula.Text = ""
        txtEdad.Text = ""
    End Sub

    'DETECTAR CLICK EN EL BOTON ELIMINAR
    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click

        ln_Citas.actualizarCita(New Citas(dgvCitas.CurrentRow.Cells(2).Value.ToString, cbHora.SelectedValue,
                                          ln_paci.ExtraerIDPaciente(cbPaciente.SelectedValue), cbFuncionario.SelectedValue,
                                          dtpFechaCita.Value.Date, txtEspecialidad.Text, tbxDescripcion.Text, "Pendiente"))

        dgvCitas.DataSource = ln_Citas.ConsultaHorasUtilizadas(dtpFecha.Value, txtEspecialidad.Text)
        llenarCombo(dtpFechaCita.Value)
        limpiar()

        refrescarTablaCitasDia()

    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        btnGuardar.Visible = False
        btnLimpiar.Visible = True
        btnActualizar.Enabled = False
        btnEliminar.Enabled = False
    End Sub

    Private Sub dgvCitasDia_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgvCitasDia.CellPainting
        If e.ColumnIndex >= 0 AndAlso e.RowIndex >= 0 And (Me.dgvCitasDia.Columns(e.ColumnIndex).Name = "Facturación" Or Me.dgvCitasDia.Columns(e.ColumnIndex).Name = "Consultar") Then


            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = TryCast(Me.dgvCitasDia.Rows(e.RowIndex).Cells("Facturación"), DataGridViewButtonCell)
            Dim icoAtomico As Icon = New Icon("D:\Dropbox\PROYECTO FINAL INGENIERIA\VB\ProyectoFinalIngenieria\ProyectoFinalIngenieria\Resources\33119.ico")
            Dim celBoton1 As DataGridViewButtonCell = TryCast(Me.dgvCitasDia.Rows(e.RowIndex).Cells("Consultar"), DataGridViewButtonCell)
            Dim icoAtomico1 As Icon = New Icon("D:\Dropbox\PROYECTO FINAL INGENIERIA\VB\ProyectoFinalIngenieria\ProyectoFinalIngenieria\Resources\consultar.ico")
            e.Graphics.DrawIcon(icoAtomico, e.CellBounds.Left + 17, e.CellBounds.Top + 4)
            Me.dgvCitasDia.Rows(e.RowIndex).Height = icoAtomico.Height + 7
            Me.dgvCitasDia.Columns(e.ColumnIndex).Width = icoAtomico.Width + 35
            e.Handled = True

        End If


    End Sub




    Private Sub dgvCitasDia_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles dgvCitasDia.MouseDoubleClick

        If dgvCitas.SelectedRows.Count > 0 Then
            If dgvCitasDia.CurrentRow.Cells(3).Value.ToString = "" Then

                MsgBox("Campo sin asignar", MsgBoxStyle.Information, "Citas")

            Else

                cita = ln_Citas.ExtraerCita(CInt(dgvCitasDia.CurrentRow.Cells(4).Value.ToString))
                btnEliminar.Enabled = True
                If dtpFecha.Value.Date.Day >= Date.Now.Day And dtpFecha.Value.Date.Month >= Date.Now.Month And
                    dtpFecha.Value.Date.Year >= Date.Now.Year Then
                    btnActualizar.Enabled = True
                End If


                limpiar()
                Dim pas As Pacientes = ln_paci.ConsultarPacienteCOMPLETO(CInt(dgvCitasDia.CurrentRow.Cells(8).Value.ToString))

                txtNombrePacienteDia.Text = pas.NombreCompleto_
                txtCedulaPacienteDia.Text = pas.Cedula_
                txtEdadPacienteDia.Text = getEdad(pas.FechaNacimiento_.Year())

                Dim ho As Horas = ln_Hora.ConsultarHorasDisponiblesTodas(CInt(dgvCitasDia.CurrentRow.Cells(2).Value.ToString))

                txtHoraPacienteDia.Text = ho.Hora_.ToString

                'cbFuncionario.SelectedText = dgvCitas.CurrentRow.Cells(5).Value.ToString
                'cbFuncionario.SelectedValue = CInt(dgvCitas.CurrentRow.Cells(7).Value.ToString)

                rtbDescripcionPacienteDia.Text = dgvCitasDia.CurrentRow.Cells(5).Value.ToString
                txtFuncionarioPacienteDia.Text = dgvCitasDia.CurrentRow.Cells(6).Value.ToString

            End If

            btnGuardar.Visible = False
            btnLimpiar.Visible = True

        End If
    End Sub

    'Private Sub dgvCitasDia_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCitasDia.CellMouseClick
    '    'Me.Hide()
    '    'Dim f As New FrmAtencionOdontologica()
    '    'f.Show()

    '    Dim hijo As New FrmAtencionOdontologica(CInt(dgvCitasDia.CurrentRow.Cells(4).Value.ToString), fr_p)
    '    hijo.Show()
    '    'fr_p.Visible = False
    'End Sub




    Sub limpiarDvgCitasDia()
        tbxDescripcion.Text = ""
        cbFuncionario.ResetText()
        cbHora.ResetText()
        cbPaciente.ResetText()
        txtCedula.Text = ""
        txtEdad.Text = ""
    End Sub

    Private Sub dgvCitasDia_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCitasDia.CellContentClick

        If e.ColumnIndex = 0 Then
            If dgvCitasDia.CurrentRow.Cells(4).Value.ToString <> "" Then

                If id_ = 1 Then

                    Dim hijo As New FrmAtencionOdontologica(CInt(dgvCitasDia.CurrentRow.Cells(4).Value.ToString), CInt(dgvCitasDia.CurrentRow.Cells(8).Value.ToString), fr_p)
                    hijo.Show()
                    fr_p.Visible = False

                Else
                    Dim hijo As New FrmAtencionMedicinaGeneral(CInt(dgvCitasDia.CurrentRow.Cells(4).Value.ToString), CInt(dgvCitasDia.CurrentRow.Cells(8).Value.ToString), fr_p)
                    hijo.Show()
                    fr_p.Visible = False

                End If
            End If
        End If

        If e.ColumnIndex = 1 Then
            If dgvCitasDia.CurrentRow.Cells(4).Value.ToString <> "" Then
                Dim factura As New FrmFacturas(CInt(dgvCitasDia.CurrentRow.Cells(4).Value.ToString), txtEspecialidad.Text)
                factura.Show()
            End If
        End If

    End Sub


End Class
﻿Imports ProyectoFinalIngenieria.Ln_Funcionario

Public Class FrmInsertarFuncionarios

    Dim funcionario As New Ln_Funcionario
    Dim funci As Funcionario

    'Inserta en tablas FUNCIONArRIOS
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

        If (txtCedula.Text <> "") And (txtNombre.Text <> "") And (txtTelefono1.Text <> "") And
            (txtNombre.Text <> "") And (cbProfesion.SelectedItem <> "") And cbGenero.SelectedItem <> "" Then

            funcionario.insertarfuncionario(New Funcionario(txtNombre.Text, CInt(txtCedula.Text), tbxDireccion.Text, txtCorreo.Text,
                                                            cbProfesion.SelectedItem, getGenero(cbGenero.SelectedItem),
                                                            dtpFechaNacimiento.Value.Date, txtContraseña.Text, txtTelefono1.Text,
                                                            txtTelefono2.Text, 0))

            dgvFuncionarios.DataSource = funcionario.cargartablaActivos()
            limpiarCamposTexto()

        Else
            MsgBox("Debe ingresar al menos los datos obligatorios (*)!!!", MsgBoxStyle.Critical, "Clinica Medica")

        End If

    End Sub

    'OBTIENE EL GENERO DEL COMBO BOX
    Function getGenero(txt As String) As Boolean
        If txt = "Femenino" Then
            getGenero = 0
        Else
            getGenero = 1
        End If
        Return getGenero
    End Function

    'Limpiar campos
    Sub limpiarCamposTexto()

        txtNombre.Text = ""
        txtCedula.Text = ""
        txtCorreo.Text = ""
        txtContraseña.Text = ""
        txtTelefono1.Text = ""
        txtTelefono2.Text = ""
        tbxDireccion.Text = ""
        dtpFechaNacimiento.ResetText()
        cbGenero.ResetText()
        cbProfesion.ResetText()

    End Sub

    'Detecta si se escribe en el textbox busqueda paciente
    Private Sub txtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        dgvFuncionarios.DataSource = funcionario.cargarTabla1Funcionario(txtBusqueda.Text)
    End Sub

    'Detectar cambios en la seleccion del checkbox activos
    Private Sub cbxActivos_CheckedChanged(sender As Object, e As EventArgs) Handles cbxActivos.CheckedChanged
        cbxActivos.Visible = False
        cbxPapelera.Visible = True
        dgvFuncionarios.DataSource = funcionario.cargartablaActivos()
        btnRestablecer.Visible = False
        btnEliminar.Visible = True
        btnActualizar.Visible = True
        btnNuevoFuncionario.Visible = True
        btnEliminar.Enabled = False
        btnActualizar.Enabled = False
    End Sub

    'Detectar cambios en la seleccion del checkbox papelera
    Private Sub cbxPapelera_CheckedChanged(sender As Object, e As EventArgs) Handles cbxPapelera.CheckedChanged
        cbxActivos.Visible = True
        cbxPapelera.Visible = False
        dgvFuncionarios.DataSource = funcionario.cargartablaInactivos()
        btnRestablecer.Visible = True
        btnEliminar.Visible = False
        btnNuevoFuncionario.Visible = False
        btnActualizar.Visible = False
        btnRestablecer.Enabled = False
    End Sub

    'Seleccion fila de tabla Funcionario
    Private Sub dgvfuncionarios_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles dgvFuncionarios.MouseDoubleClick
        btnEliminar.Enabled = True
        btnActualizar.Enabled = True
        btnRestablecer.Enabled = True
        btnNuevoFuncionario.Enabled = True
        If dgvFuncionarios.SelectedRows.Count > 0 Then
            Dim genero, estado As Boolean
            If dgvFuncionarios.CurrentRow.Cells(7).Value = 0 Then
                genero = 0
                cbGenero.Text = "Femenino"
            Else
                genero = 1
                cbGenero.Text = "Masculino"
            End If

            If dgvFuncionarios.CurrentRow.Cells(11).Value = 0 Then
                estado = 0
            Else
                estado = 1
            End If

            funci = New Funcionario(CInt(dgvFuncionarios.CurrentRow.Cells(0).Value.ToString), dgvFuncionarios.CurrentRow.Cells(1).Value.ToString,
                                 CInt(dgvFuncionarios.CurrentRow.Cells(2).Value.ToString), dgvFuncionarios.CurrentRow.Cells(3).Value.ToString,
                                 dgvFuncionarios.CurrentRow.Cells(4).Value.ToString, dgvFuncionarios.CurrentRow.Cells(5).Value.ToString,
                                 genero, Convert.ToDateTime(dgvFuncionarios.CurrentRow.Cells(8).Value.ToString), dgvFuncionarios.CurrentRow.Cells(6).Value.ToString,
                                 dgvFuncionarios.CurrentRow.Cells(9).Value.ToString,
                                 dgvFuncionarios.CurrentRow.Cells(10).Value.ToString, estado)

            txtNombre.Text = funci.NombreCompleto_
            txtCedula.Text = funci.Cedula_
            txtCorreo.Text = funci.Correo_
            txtContraseña.Text = funci.Contraseña_
            txtTelefono1.Text = funci.Telefono1_
            txtTelefono2.Text = funci.Telefono2_
            tbxDireccion.Text = funci.Direccion_
            dtpFechaNacimiento.Value = funci.FechaNacimiento_
            cbProfesion.SelectedItem = funci.Profesion_

        End If
    End Sub

    'Eliminar un Funcionario
    'Private Sub btnEliminar_Click(sender As Object, e As EventArgs)
    '    funcionario.bloquearFuncionario(CInt(dgvFuncionarios.CurrentRow.Cells(0).Value.ToString))
    '    dgvFuncionarios.DataSource = funcionario.cargartablaActivos()
    '    btnEliminar.Enabled = False
    '    btnActualizar.Enabled = False
    '    limpiarCamposTexto()
    'End Sub

    'Restablecer un Funcionario
    'Private Sub btnRestablecer_Click(sender As Object, e As EventArgs)
    '    funcionario.RestablecerFuncionario(CInt(dgvFuncionarios.CurrentRow.Cells(0).Value.ToString))
    '    cbxActivos.Visible = False
    '    cbxPapelera.Visible = True

    '    btnRestablecer.Visible = False
    '    btnEliminar.Visible = True
    '    btnActualizar.Visible = True
    '    btnNuevoFuncionario.Visible = True
    '    btnEliminar.Enabled = False
    '    btnActualizar.Enabled = False
    '    btnNuevoFuncionario.Enabled = False
    '    dgvFuncionarios.DataSource = funcionario.cargartablaActivos()
    '    limpiarCamposTexto()
    'End Sub

    'Actualizar un Funcionario
    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click

        funcionario.actualizarFuncionario(New Funcionario(CInt(dgvFuncionarios.CurrentRow.Cells(0).Value.ToString), txtNombre.Text,
                                                  CInt(txtCedula.Text), tbxDireccion.Text, txtCorreo.Text,
                                                  cbProfesion.SelectedItem, getGenero(cbGenero.SelectedValue),
                                           dtpFechaNacimiento.Value.Date, txtContraseña.Text, txtTelefono1.Text, txtTelefono2.Text, 0))

        dgvFuncionarios.DataSource = funcionario.cargartablaActivos()
        btnEliminar.Enabled = False
        btnActualizar.Enabled = False
        btnNuevoFuncionario.Enabled = False

    End Sub


    'Vaciar para ingresar nuevo paciente
    Private Sub btnNuevoFuncionario_Click(sender As Object, e As EventArgs)
        ''Pacientes
        If MsgBox("¿Está seguro que desea ingresar un nuevo funcionario, se eliminará la busqueda realizada?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            txtNombre.Text = ""
            txtCedula.Text = ""
            txtCorreo.Text = ""
            txtContraseña.Text = ""
            txtTelefono1.Text = ""
            txtTelefono2.Text = ""
            tbxDireccion.Text = ""
            dtpFechaNacimiento.ResetText()
            cbGenero.ResetText()
            cbProfesion.ResetText()

            btnActualizar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    Private Sub FrmInsertarFuncionarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DB_A494E9_PFIngenieriaDataSet2.funcionariosActivosTodos' Puede moverla o quitarla según sea necesario.
        Me.FuncionariosActivosTodosTableAdapter.Fill(Me.DB_A494E9_PFIngenieriaDataSet2.funcionariosActivosTodos)

    End Sub

    'Vaciar para ingresar nuevo paciente
    Private Sub btnNuevoFuncionario_Click_1(sender As Object, e As EventArgs) Handles btnNuevoFuncionario.Click
        ''Pacientes
        If MsgBox("¿Está seguro que desea ingresar un nuevo funcionario, se eliminará la busqueda realizada?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            txtNombre.Text = ""
            txtCedula.Text = ""
            txtCorreo.Text = ""
            txtContraseña.Text = ""
            txtTelefono1.Text = ""
            txtTelefono2.Text = ""
            tbxDireccion.Text = ""
            dtpFechaNacimiento.ResetText()
            cbGenero.ResetText()
            cbProfesion.ResetText()

            btnActualizar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    Private Sub btnEliminar_Click_1(sender As Object, e As EventArgs) Handles btnEliminar.Click
        funcionario.bloquearFuncionario(CInt(dgvFuncionarios.CurrentRow.Cells(0).Value.ToString))
        dgvFuncionarios.DataSource = funcionario.cargartablaActivos()
        btnEliminar.Enabled = False
        btnActualizar.Enabled = False
        btnGuardar.Visible = False
        limpiarCamposTexto()
    End Sub

    Private Sub btnRestablecer_Click_1(sender As Object, e As EventArgs) Handles btnRestablecer.Click
        funcionario.RestablecerFuncionario(CInt(dgvFuncionarios.CurrentRow.Cells(0).Value.ToString))
        cbxActivos.Visible = False
        cbxPapelera.Visible = True

        btnRestablecer.Visible = False
        btnEliminar.Visible = True
        btnActualizar.Visible = True
        btnNuevoFuncionario.Visible = True
        btnEliminar.Enabled = False
        btnActualizar.Enabled = False
        btnNuevoFuncionario.Enabled = False
        btnGuardar.Visible = True
        dgvFuncionarios.DataSource = funcionario.cargartablaActivos()
        limpiarCamposTexto()
    End Sub
End Class
﻿Public Class ReporteCitasAño

    Dim Usuario As String
    Dim ID As Integer
    Dim Fecha As DateTime
    Dim Especialidad As String

    Public Sub New(UsuarioU As String, IDU As Integer, FechaU As DateTime, EspecialidadU As String)
        InitializeComponent()
        Usuario = UsuarioU
        ID = IDU
        Fecha = FechaU
        Especialidad = EspecialidadU
    End Sub


    Private Sub ReporteCitasAño_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DataReportes.ConsultarCitasAño' Puede moverla o quitarla según sea necesario.
        Me.ConsultarCitasAñoTableAdapter.Fill(Me.DataReportes.ConsultarCitasAño, Usuario, ID, Fecha, Especialidad)

    End Sub
End Class
﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms

Public Class FormPrincipal

    Dim lx, ly As Integer
    Dim sw, sh As Integer

    Dim x, y As Integer
    Dim newpoint As Point

    Dim db_citas As New DB_Citas

    Private Sub FormPrincipal_Load(sender As Object, e As EventArgs) Handles Me.Load
        mostrarlogo()
        Abrir()

        If Profesion_Funcionario = "Secretaria (o)" Then
            btnTratamientos.Enabled = False
            btnFuncionarios.Enabled = False
            btnConsultas.Enabled = False

        ElseIf Profesion_Funcionario = "Odontología" Then
            btnMSCMedicinaGeneral.Enabled = False 'CITAS MG 
            btnCMSConsultasMedicina.Enabled = False 'CONSULTA MG
            btnFuncionarios.Enabled = False
            btnContabilidad.Enabled = False
            btnTratamientos.Enabled = False

        ElseIf Profesion_Funcionario = "Asistente Odontología" Then
            btnMSCMedicinaGeneral.Enabled = False 'CITAS MG 
            btnCMSConsultasMedicina.Enabled = False 'CONSULTA MG
            btnFuncionarios.Enabled = False
            btnContabilidad.Enabled = False
            btnTratamientos.Enabled = False

        ElseIf Profesion_Funcionario = "Medicina General" Then
            btnMSCOdontologia.Enabled = False 'CITAS Odon
            btnCMSConsultasOdontologia.Enabled = False 'CONSULTA Odon
            btnFuncionarios.Enabled = False
            btnContabilidad.Enabled = False
            btnTratamientos.Enabled = False

        ElseIf Profesion_Funcionario = "Enfermería" Then
            btnMSCOdontologia.Enabled = False 'CITAS Odon 
            btnCMSConsultasOdontologia.Enabled = False 'CONSULTA Odon
            btnFuncionarios.Enabled = False
            btnContabilidad.Enabled = False
            btnTratamientos.Enabled = False

        ElseIf Profesion_Funcionario = "Administrador" Then

        End If

        db_citas.actualizarCitasAusentes()

    End Sub

    Private Sub mostrarlogo()
        AbrirFormInPanel(New FrmInicio())
    End Sub

    'Mover Formulario-------------------------------------------------------------------------------------

    Private Sub BarraTitulo_MouseDown(sender As Object, e As MouseEventArgs)
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub BarraTitulo_MouseMove(sender As Object, e As MouseEventArgs)
        If e.Button = MouseButtons.Left Then
            newpoint = Control.MousePosition
            newpoint.X -= x
            newpoint.Y -= y
            Me.Location = newpoint
            Application.DoEvents()
        End If
    End Sub

    'Fecha y hora -----------------------------------------------------------------------------------------------

    Private Sub tmFechaHora_Tick(sender As Object, e As EventArgs) Handles tmFechaHora.Tick
        lbFecha.Text = DateTime.Now.ToShortDateString()
        lblHora.Text = DateTime.Now.ToString("HH:mm:ssss")

    End Sub
    'METODOS PARA CERRAR,MAXIMIZAR, MINIMIZAR FORMULARIO------------------------------------------------------

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        If MsgBox("¿Está seguro de cerrar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then
            End
        End If
    End Sub

    Private Sub btnMinimizar_Click(sender As Object, e As EventArgs) Handles btnMinimizar.Click
        WindowState = FormWindowState.Minimized
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles btnFuncionarios.Click
        Dim hijo As New FrmInsertarFuncionarios
        AbrirFormInPanel(hijo)
    End Sub

    Private Sub btnPRODUCTOS_Click(sender As Object, e As EventArgs) Handles btnInicio.Click
        Dim hijo As New FrmInicio
        AbrirFormInPanel(hijo)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnCitas.Click
        cmsCitas.Show(btnCitas, 0, btnCitas.Height)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnConsultas.Click
        cmsConsultas.Show(btnConsultas, 0, btnConsultas.Height)
    End Sub

    Private Sub btnTratamientos_Click(sender As Object, e As EventArgs) Handles btnTratamientos.Click
        Dim hijo As New FrmTratamientos
        AbrirFormInPanel(hijo)
    End Sub

    Private Sub btnMSCOdontologia_Click(sender As Object, e As EventArgs) Handles btnMSCOdontologia.Click
        Dim hijo As New FrmCitas(1, Me)
        AbrirFormInPanel(hijo)
    End Sub

    Private Sub btnMSCMedicinaGeneral_Click(sender As Object, e As EventArgs) Handles btnMSCMedicinaGeneral.Click
        Dim hijo As New FrmCitas(0, Me)
        AbrirFormInPanel(hijo)
    End Sub

    Private Sub btnCMSConsultasOdontologia_Click(sender As Object, e As EventArgs) Handles btnCMSConsultasOdontologia.Click
        Dim hijo As New FrmAtencionOdontologica(Me)
        hijo.Show()
        Me.Visible = False
    End Sub

    Private Sub btnCMSConsultasMedicina_Click(sender As Object, e As EventArgs) Handles btnCMSConsultasMedicina.Click
        Dim hijo As New FrmAtencionMedicinaGeneral(Me)
        'AbrirFormInPanel(hijo)
        hijo.Show()
        Me.Visible = False
    End Sub

    Private Sub btnContabilidad_Click(sender As Object, e As EventArgs) Handles btnContabilidad.Click
        cmsContabilidad.Show(btnContabilidad, 0, btnContabilidad.Height)
    End Sub

    Private Sub ToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem2.Click
        Dim hijo As New FrmCreditos
        AbrirFormInPanel(hijo)
    End Sub

    Private Sub btnFacturasConsulta_Click(sender As Object, e As EventArgs) Handles btnFacturasConsulta.Click
        Dim hijo As New FrmFacturasContabilidad
        AbrirFormInPanel(hijo)
    End Sub

    Private Sub btnReportes_Click(sender As Object, e As EventArgs) Handles btnReportes.Click
        cmsReportes.Show(btnReportes, 0, btnReportes.Height)
    End Sub

    Private Sub btnCMSOdontoReportes_Click(sender As Object, e As EventArgs) Handles btnCMSOdontoReportes.Click
        Dim hijo As New FrmReporteOdontologia
        AbrirFormInPanel(hijo)
    End Sub

    Private Sub ToolStripMenuItem3_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem3.Click
        Dim hijo As New FrmReporteMedicina
        AbrirFormInPanel(hijo)
    End Sub

    Private Sub GeneralToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GeneralToolStripMenuItem.Click
        Dim hijo As New FrmReportesGeneral
        AbrirFormInPanel(hijo)
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Hide()
        FrmLogin.Show()
        FrmLogin.txtCedula.Text = "CÉDULA"
        FrmLogin.txtClave.Text = "CONTRASEÑA"
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnPacientes.Click
        Dim hijo As New FrmInsertarPaciente
        AbrirFormInPanel(hijo)
    End Sub

    Public Sub AbrirFormInPanel(formHijo As Object)

        If panelContenedor.Controls.Count > 0 Then
            panelContenedor.Controls.RemoveAt(0)
        End If
        formHijo.TopLevel = False
        formHijo.FormBorderStyle = FormBorderStyle.None
        formHijo.Dock = DockStyle.Fill
        panelContenedor.Controls.Add(formHijo)
        panelContenedor.Tag = formHijo
        formHijo.Show()

    End Sub

    Private Sub btnListaClientes_Click(sender As Object, e As EventArgs)
        Dim hijo As New FrmInsertarPaciente
        hijo.TopLevel = False
        hijo.FormBorderStyle = FormBorderStyle.None
        hijo.Dock = DockStyle.Fill
        'hijo.AutoScroll = True
        panelContenedor.Controls.Add(hijo)
        panelContenedor.Tag = hijo
        hijo.Show()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmInsertarPaciente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmInsertarPaciente))
        Me.TabPacientes = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnNUevoPaciente = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnRestablecer = New System.Windows.Forms.Button()
        Me.dgvPacientes = New System.Windows.Forms.DataGridView()
        Me.IDPacienteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CedulaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreCompletoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DireccionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CorreoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OcupacionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GeneroDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.FechaNacimientoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Telefono1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Telefono2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDResponsableDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EstadoDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.PacientesActivosTodosBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DB_A494E9_PFIngenieriaDataSet1 = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSet1()
        Me.dtpFechaNacimientoPaciente = New System.Windows.Forms.DateTimePicker()
        Me.cbxActivos = New System.Windows.Forms.CheckBox()
        Me.cbxPapelera = New System.Windows.Forms.CheckBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.txtBusqueda = New System.Windows.Forms.TextBox()
        Me.btnActualizar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.cbGeneroPaciente = New System.Windows.Forms.ComboBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtOcupacionPaciente = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtTelefono2Paciente = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtEdadPaciente = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtTelefono1Paciente = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.tbxDireccionPaciente = New System.Windows.Forms.RichTextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtCedulaPaciente = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCorreoPaciente = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtNombrePaciente = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.btnActualizarHistorial = New System.Windows.Forms.Button()
        Me.btnLimpiarHistorial = New System.Windows.Forms.Button()
        Me.tbxAntecedentesAlergicos = New System.Windows.Forms.RichTextBox()
        Me.tbxAntecedentesQuirurgicos = New System.Windows.Forms.RichTextBox()
        Me.tbxAntecedentesNoPersonales = New System.Windows.Forms.RichTextBox()
        Me.tbxAntecedentesPersonales = New System.Windows.Forms.RichTextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.tbxAntecedentesFamiliares = New System.Windows.Forms.RichTextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.tbxQuejaPrincipal = New System.Windows.Forms.RichTextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.dtpFechaHistorial = New System.Windows.Forms.DateTimePicker()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.lcondicion = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btnGuardarNuevoR = New System.Windows.Forms.Button()
        Me.btnOlvidar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.cbGeneroResponsable = New System.Windows.Forms.ComboBox()
        Me.btnActualizarResponsable = New System.Windows.Forms.Button()
        Me.btnRemplazar = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.tbParentescoResponsable = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.tbTelefono2Responsable = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.tbEdadResponsable = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.tbTelefono1Responsable = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.tbBusquedaResponsable = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.dgvResponsables = New System.Windows.Forms.DataGridView()
        Me.IDResponsableDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CedulaDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreCompletoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DireccionDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CorreoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Telefono1DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Telefono2DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ParentescoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GeneroDataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.FechaNacimientoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EstadoDataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ResponsablesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DBA494E9PFIngenieriaDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DB_A494E9_PFIngenieriaDataSet = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSet()
        Me.dtpFechaNaciResponsable = New System.Windows.Forms.DateTimePicker()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.tbxDireccionResponsable = New System.Windows.Forms.RichTextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.tbCedulaResponsable = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.tbCorreoResponsable = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.tbNombreResponsable = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.PacientesActivosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PacientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PacientesTableAdapter = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSetTableAdapters.PacientesTableAdapter()
        Me.ResponsablesTableAdapter = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSetTableAdapters.ResponsablesTableAdapter()
        Me.PacientesActivosTableAdapter = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSetTableAdapters.pacientesActivosTableAdapter()
        Me.PacientesInactivosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PacientesInactivosTableAdapter = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSetTableAdapters.pacientesInactivosTableAdapter()
        Me.PacientesActivosBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.PacientesInactivosBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.PacientesActivosTodosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PacientesActivosTodosBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.PacientesActivosTodosTableAdapter = New ProyectoFinalIngenieria.DB_A494E9_PFIngenieriaDataSet1TableAdapters.pacientesActivosTodosTableAdapter()
        Me.TabPacientes.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvPacientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PacientesActivosTodosBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DB_A494E9_PFIngenieriaDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvResponsables, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResponsablesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DBA494E9PFIngenieriaDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DB_A494E9_PFIngenieriaDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PacientesActivosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PacientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PacientesInactivosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PacientesActivosBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PacientesInactivosBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PacientesActivosTodosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PacientesActivosTodosBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabPacientes
        '
        Me.TabPacientes.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar
        Me.TabPacientes.Controls.Add(Me.TabPage1)
        Me.TabPacientes.Controls.Add(Me.TabPage2)
        Me.TabPacientes.Controls.Add(Me.TabPage3)
        Me.TabPacientes.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPacientes.Location = New System.Drawing.Point(9, 71)
        Me.TabPacientes.Name = "TabPacientes"
        Me.TabPacientes.SelectedIndex = 0
        Me.TabPacientes.Size = New System.Drawing.Size(882, 539)
        Me.TabPacientes.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage1.Location = New System.Drawing.Point(4, 30)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(874, 505)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Registro de Pacientes"
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel1.Controls.Add(Me.btnNUevoPaciente)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.btnRestablecer)
        Me.Panel1.Controls.Add(Me.dgvPacientes)
        Me.Panel1.Controls.Add(Me.dtpFechaNacimientoPaciente)
        Me.Panel1.Controls.Add(Me.cbxActivos)
        Me.Panel1.Controls.Add(Me.cbxPapelera)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.txtBusqueda)
        Me.Panel1.Controls.Add(Me.btnActualizar)
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Controls.Add(Me.cbGeneroPaciente)
        Me.Panel1.Controls.Add(Me.Label30)
        Me.Panel1.Controls.Add(Me.txtOcupacionPaciente)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txtTelefono2Paciente)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txtEdadPaciente)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtTelefono1Paciente)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.tbxDireccionPaciente)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.txtCedulaPaciente)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txtCorreoPaciente)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txtNombrePaciente)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(881, 514)
        Me.Panel1.TabIndex = 24
        '
        'btnNUevoPaciente
        '
        Me.btnNUevoPaciente.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnNUevoPaciente.Enabled = False
        Me.btnNUevoPaciente.FlatAppearance.BorderSize = 0
        Me.btnNUevoPaciente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNUevoPaciente.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNUevoPaciente.ForeColor = System.Drawing.Color.White
        Me.btnNUevoPaciente.Location = New System.Drawing.Point(20, 449)
        Me.btnNUevoPaciente.Name = "btnNUevoPaciente"
        Me.btnNUevoPaciente.Size = New System.Drawing.Size(102, 35)
        Me.btnNUevoPaciente.TabIndex = 76
        Me.btnNUevoPaciente.Text = "Nuevo"
        Me.btnNUevoPaciente.UseVisualStyleBackColor = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Red
        Me.Label16.Location = New System.Drawing.Point(309, 180)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(19, 22)
        Me.Label16.TabIndex = 75
        Me.Label16.Text = "*"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Red
        Me.Label15.Location = New System.Drawing.Point(110, 180)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(19, 22)
        Me.Label15.TabIndex = 74
        Me.Label15.Text = "*"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Red
        Me.Label14.Location = New System.Drawing.Point(88, 102)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(19, 22)
        Me.Label14.TabIndex = 73
        Me.Label14.Text = "*"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Red
        Me.Label13.Location = New System.Drawing.Point(295, 27)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(19, 22)
        Me.Label13.TabIndex = 72
        Me.Label13.Text = "*"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(88, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(19, 22)
        Me.Label5.TabIndex = 71
        Me.Label5.Text = "*"
        '
        'btnRestablecer
        '
        Me.btnRestablecer.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnRestablecer.FlatAppearance.BorderSize = 0
        Me.btnRestablecer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRestablecer.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRestablecer.ForeColor = System.Drawing.Color.White
        Me.btnRestablecer.Location = New System.Drawing.Point(151, 449)
        Me.btnRestablecer.Name = "btnRestablecer"
        Me.btnRestablecer.Size = New System.Drawing.Size(120, 35)
        Me.btnRestablecer.TabIndex = 70
        Me.btnRestablecer.Text = "Restablecer"
        Me.btnRestablecer.UseVisualStyleBackColor = False
        Me.btnRestablecer.Visible = False
        '
        'dgvPacientes
        '
        Me.dgvPacientes.AllowUserToAddRows = False
        Me.dgvPacientes.AllowUserToDeleteRows = False
        Me.dgvPacientes.AutoGenerateColumns = False
        Me.dgvPacientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvPacientes.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.dgvPacientes.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPacientes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDPacienteDataGridViewTextBoxColumn, Me.CedulaDataGridViewTextBoxColumn, Me.NombreCompletoDataGridViewTextBoxColumn, Me.DireccionDataGridViewTextBoxColumn, Me.CorreoDataGridViewTextBoxColumn, Me.OcupacionDataGridViewTextBoxColumn, Me.GeneroDataGridViewCheckBoxColumn, Me.FechaNacimientoDataGridViewTextBoxColumn, Me.Telefono1DataGridViewTextBoxColumn, Me.Telefono2DataGridViewTextBoxColumn, Me.IDResponsableDataGridViewTextBoxColumn, Me.EstadoDataGridViewCheckBoxColumn})
        Me.dgvPacientes.DataSource = Me.PacientesActivosTodosBindingSource2
        Me.dgvPacientes.Location = New System.Drawing.Point(433, 61)
        Me.dgvPacientes.Name = "dgvPacientes"
        Me.dgvPacientes.ReadOnly = True
        Me.dgvPacientes.RowHeadersVisible = False
        Me.dgvPacientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPacientes.Size = New System.Drawing.Size(426, 423)
        Me.dgvPacientes.TabIndex = 69
        '
        'IDPacienteDataGridViewTextBoxColumn
        '
        Me.IDPacienteDataGridViewTextBoxColumn.DataPropertyName = "ID_Paciente"
        Me.IDPacienteDataGridViewTextBoxColumn.HeaderText = "ID_Paciente"
        Me.IDPacienteDataGridViewTextBoxColumn.Name = "IDPacienteDataGridViewTextBoxColumn"
        Me.IDPacienteDataGridViewTextBoxColumn.ReadOnly = True
        Me.IDPacienteDataGridViewTextBoxColumn.Visible = False
        '
        'CedulaDataGridViewTextBoxColumn
        '
        Me.CedulaDataGridViewTextBoxColumn.DataPropertyName = "Cedula"
        Me.CedulaDataGridViewTextBoxColumn.HeaderText = "Cedula"
        Me.CedulaDataGridViewTextBoxColumn.Name = "CedulaDataGridViewTextBoxColumn"
        Me.CedulaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NombreCompletoDataGridViewTextBoxColumn
        '
        Me.NombreCompletoDataGridViewTextBoxColumn.DataPropertyName = "NombreCompleto"
        Me.NombreCompletoDataGridViewTextBoxColumn.HeaderText = "Nombre"
        Me.NombreCompletoDataGridViewTextBoxColumn.Name = "NombreCompletoDataGridViewTextBoxColumn"
        Me.NombreCompletoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DireccionDataGridViewTextBoxColumn
        '
        Me.DireccionDataGridViewTextBoxColumn.DataPropertyName = "Direccion"
        Me.DireccionDataGridViewTextBoxColumn.HeaderText = "Direccion"
        Me.DireccionDataGridViewTextBoxColumn.Name = "DireccionDataGridViewTextBoxColumn"
        Me.DireccionDataGridViewTextBoxColumn.ReadOnly = True
        Me.DireccionDataGridViewTextBoxColumn.Visible = False
        '
        'CorreoDataGridViewTextBoxColumn
        '
        Me.CorreoDataGridViewTextBoxColumn.DataPropertyName = "Correo"
        Me.CorreoDataGridViewTextBoxColumn.HeaderText = "Correo"
        Me.CorreoDataGridViewTextBoxColumn.Name = "CorreoDataGridViewTextBoxColumn"
        Me.CorreoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'OcupacionDataGridViewTextBoxColumn
        '
        Me.OcupacionDataGridViewTextBoxColumn.DataPropertyName = "Ocupacion"
        Me.OcupacionDataGridViewTextBoxColumn.HeaderText = "Ocupacion"
        Me.OcupacionDataGridViewTextBoxColumn.Name = "OcupacionDataGridViewTextBoxColumn"
        Me.OcupacionDataGridViewTextBoxColumn.ReadOnly = True
        Me.OcupacionDataGridViewTextBoxColumn.Visible = False
        '
        'GeneroDataGridViewCheckBoxColumn
        '
        Me.GeneroDataGridViewCheckBoxColumn.DataPropertyName = "Genero"
        Me.GeneroDataGridViewCheckBoxColumn.HeaderText = "Genero"
        Me.GeneroDataGridViewCheckBoxColumn.Name = "GeneroDataGridViewCheckBoxColumn"
        Me.GeneroDataGridViewCheckBoxColumn.ReadOnly = True
        Me.GeneroDataGridViewCheckBoxColumn.Visible = False
        '
        'FechaNacimientoDataGridViewTextBoxColumn
        '
        Me.FechaNacimientoDataGridViewTextBoxColumn.DataPropertyName = "FechaNacimiento"
        Me.FechaNacimientoDataGridViewTextBoxColumn.HeaderText = "FechaNacimiento"
        Me.FechaNacimientoDataGridViewTextBoxColumn.Name = "FechaNacimientoDataGridViewTextBoxColumn"
        Me.FechaNacimientoDataGridViewTextBoxColumn.ReadOnly = True
        Me.FechaNacimientoDataGridViewTextBoxColumn.Visible = False
        '
        'Telefono1DataGridViewTextBoxColumn
        '
        Me.Telefono1DataGridViewTextBoxColumn.DataPropertyName = "Telefono1"
        Me.Telefono1DataGridViewTextBoxColumn.HeaderText = "Telefono"
        Me.Telefono1DataGridViewTextBoxColumn.Name = "Telefono1DataGridViewTextBoxColumn"
        Me.Telefono1DataGridViewTextBoxColumn.ReadOnly = True
        '
        'Telefono2DataGridViewTextBoxColumn
        '
        Me.Telefono2DataGridViewTextBoxColumn.DataPropertyName = "Telefono2"
        Me.Telefono2DataGridViewTextBoxColumn.HeaderText = "Telefono2"
        Me.Telefono2DataGridViewTextBoxColumn.Name = "Telefono2DataGridViewTextBoxColumn"
        Me.Telefono2DataGridViewTextBoxColumn.ReadOnly = True
        Me.Telefono2DataGridViewTextBoxColumn.Visible = False
        '
        'IDResponsableDataGridViewTextBoxColumn
        '
        Me.IDResponsableDataGridViewTextBoxColumn.DataPropertyName = "ID_Responsable"
        Me.IDResponsableDataGridViewTextBoxColumn.HeaderText = "ID_Responsable"
        Me.IDResponsableDataGridViewTextBoxColumn.Name = "IDResponsableDataGridViewTextBoxColumn"
        Me.IDResponsableDataGridViewTextBoxColumn.ReadOnly = True
        Me.IDResponsableDataGridViewTextBoxColumn.Visible = False
        '
        'EstadoDataGridViewCheckBoxColumn
        '
        Me.EstadoDataGridViewCheckBoxColumn.DataPropertyName = "Estado"
        Me.EstadoDataGridViewCheckBoxColumn.HeaderText = "Estado"
        Me.EstadoDataGridViewCheckBoxColumn.Name = "EstadoDataGridViewCheckBoxColumn"
        Me.EstadoDataGridViewCheckBoxColumn.ReadOnly = True
        Me.EstadoDataGridViewCheckBoxColumn.Visible = False
        '
        'PacientesActivosTodosBindingSource2
        '
        Me.PacientesActivosTodosBindingSource2.DataMember = "pacientesActivosTodos"
        Me.PacientesActivosTodosBindingSource2.DataSource = Me.DB_A494E9_PFIngenieriaDataSet1
        '
        'DB_A494E9_PFIngenieriaDataSet1
        '
        Me.DB_A494E9_PFIngenieriaDataSet1.DataSetName = "DB_A494E9_PFIngenieriaDataSet1"
        Me.DB_A494E9_PFIngenieriaDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'dtpFechaNacimientoPaciente
        '
        Me.dtpFechaNacimientoPaciente.CustomFormat = "yyyy-MM-dd"
        Me.dtpFechaNacimientoPaciente.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaNacimientoPaciente.Location = New System.Drawing.Point(16, 210)
        Me.dtpFechaNacimientoPaciente.Name = "dtpFechaNacimientoPaciente"
        Me.dtpFechaNacimientoPaciente.Size = New System.Drawing.Size(173, 27)
        Me.dtpFechaNacimientoPaciente.TabIndex = 0
        '
        'cbxActivos
        '
        Me.cbxActivos.AutoSize = True
        Me.cbxActivos.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxActivos.ForeColor = System.Drawing.Color.White
        Me.cbxActivos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cbxActivos.Location = New System.Drawing.Point(433, 27)
        Me.cbxActivos.Name = "cbxActivos"
        Me.cbxActivos.Size = New System.Drawing.Size(88, 25)
        Me.cbxActivos.TabIndex = 63
        Me.cbxActivos.Text = "Activos"
        Me.cbxActivos.UseVisualStyleBackColor = True
        Me.cbxActivos.Visible = False
        '
        'cbxPapelera
        '
        Me.cbxPapelera.AutoSize = True
        Me.cbxPapelera.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPapelera.ForeColor = System.Drawing.Color.White
        Me.cbxPapelera.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cbxPapelera.Location = New System.Drawing.Point(433, 27)
        Me.cbxPapelera.Name = "cbxPapelera"
        Me.cbxPapelera.Size = New System.Drawing.Size(99, 25)
        Me.cbxPapelera.TabIndex = 62
        Me.cbxPapelera.Text = "Papelera"
        Me.cbxPapelera.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(839, 27)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox1.TabIndex = 61
        Me.PictureBox1.TabStop = False
        '
        'txtBusqueda
        '
        Me.txtBusqueda.Location = New System.Drawing.Point(548, 27)
        Me.txtBusqueda.Name = "txtBusqueda"
        Me.txtBusqueda.Size = New System.Drawing.Size(285, 27)
        Me.txtBusqueda.TabIndex = 60
        '
        'btnActualizar
        '
        Me.btnActualizar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnActualizar.Enabled = False
        Me.btnActualizar.FlatAppearance.BorderSize = 0
        Me.btnActualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActualizar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActualizar.ForeColor = System.Drawing.Color.White
        Me.btnActualizar.Location = New System.Drawing.Point(151, 449)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(120, 35)
        Me.btnActualizar.TabIndex = 57
        Me.btnActualizar.Text = "Actualizar"
        Me.btnActualizar.UseVisualStyleBackColor = False
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnEliminar.Enabled = False
        Me.btnEliminar.FlatAppearance.BorderSize = 0
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.ForeColor = System.Drawing.Color.White
        Me.btnEliminar.Location = New System.Drawing.Point(295, 449)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(102, 35)
        Me.btnEliminar.TabIndex = 56
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'cbGeneroPaciente
        '
        Me.cbGeneroPaciente.AutoCompleteCustomSource.AddRange(New String() {"Femenino", "Masculino"})
        Me.cbGeneroPaciente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbGeneroPaciente.FormattingEnabled = True
        Me.cbGeneroPaciente.Items.AddRange(New Object() {"Femenino", "Masculino"})
        Me.cbGeneroPaciente.Location = New System.Drawing.Point(220, 57)
        Me.cbGeneroPaciente.Name = "cbGeneroPaciente"
        Me.cbGeneroPaciente.Size = New System.Drawing.Size(174, 29)
        Me.cbGeneroPaciente.TabIndex = 54
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label30.Location = New System.Drawing.Point(220, 24)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(73, 21)
        Me.Label30.TabIndex = 53
        Me.Label30.Text = "Género:"
        '
        'txtOcupacionPaciente
        '
        Me.txtOcupacionPaciente.Location = New System.Drawing.Point(224, 132)
        Me.txtOcupacionPaciente.Name = "txtOcupacionPaciente"
        Me.txtOcupacionPaciente.Size = New System.Drawing.Size(173, 27)
        Me.txtOcupacionPaciente.TabIndex = 52
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label9.Location = New System.Drawing.Point(220, 99)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(103, 21)
        Me.Label9.TabIndex = 51
        Me.Label9.Text = "Ocupación:"
        '
        'txtTelefono2Paciente
        '
        Me.txtTelefono2Paciente.Location = New System.Drawing.Point(225, 289)
        Me.txtTelefono2Paciente.Name = "txtTelefono2Paciente"
        Me.txtTelefono2Paciente.Size = New System.Drawing.Size(173, 27)
        Me.txtTelefono2Paciente.TabIndex = 50
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label6.Location = New System.Drawing.Point(221, 256)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 21)
        Me.Label6.TabIndex = 49
        Me.Label6.Text = "Teléfono 2:"
        '
        'txtEdadPaciente
        '
        Me.txtEdadPaciente.Location = New System.Drawing.Point(19, 286)
        Me.txtEdadPaciente.Name = "txtEdadPaciente"
        Me.txtEdadPaciente.Size = New System.Drawing.Size(174, 27)
        Me.txtEdadPaciente.TabIndex = 48
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.Location = New System.Drawing.Point(16, 253)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 21)
        Me.Label3.TabIndex = 47
        Me.Label3.Text = "Edad:"
        '
        'txtTelefono1Paciente
        '
        Me.txtTelefono1Paciente.Location = New System.Drawing.Point(224, 210)
        Me.txtTelefono1Paciente.Name = "txtTelefono1Paciente"
        Me.txtTelefono1Paciente.Size = New System.Drawing.Size(173, 27)
        Me.txtTelefono1Paciente.TabIndex = 46
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label11.Location = New System.Drawing.Point(220, 177)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(93, 21)
        Me.Label11.TabIndex = 45
        Me.Label11.Text = "Teléfono 1:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label12.Location = New System.Drawing.Point(16, 177)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 21)
        Me.Label12.TabIndex = 36
        Me.Label12.Text = "Fecha Nac."
        '
        'tbxDireccionPaciente
        '
        Me.tbxDireccionPaciente.Location = New System.Drawing.Point(21, 363)
        Me.tbxDireccionPaciente.Name = "tbxDireccionPaciente"
        Me.tbxDireccionPaciente.Size = New System.Drawing.Size(174, 60)
        Me.tbxDireccionPaciente.TabIndex = 31
        Me.tbxDireccionPaciente.Text = ""
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label10.Location = New System.Drawing.Point(18, 330)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 21)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Dirección:"
        '
        'txtCedulaPaciente
        '
        Me.txtCedulaPaciente.Location = New System.Drawing.Point(16, 132)
        Me.txtCedulaPaciente.Name = "txtCedulaPaciente"
        Me.txtCedulaPaciente.Size = New System.Drawing.Size(174, 27)
        Me.txtCedulaPaciente.TabIndex = 29
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label7.Location = New System.Drawing.Point(16, 99)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 21)
        Me.Label7.TabIndex = 28
        Me.Label7.Text = "Cédula:"
        '
        'txtCorreoPaciente
        '
        Me.txtCorreoPaciente.Location = New System.Drawing.Point(225, 363)
        Me.txtCorreoPaciente.Name = "txtCorreoPaciente"
        Me.txtCorreoPaciente.Size = New System.Drawing.Size(174, 27)
        Me.txtCorreoPaciente.TabIndex = 27
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label8.Location = New System.Drawing.Point(222, 330)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 21)
        Me.Label8.TabIndex = 26
        Me.Label8.Text = "Correo:"
        '
        'txtNombrePaciente
        '
        Me.txtNombrePaciente.Location = New System.Drawing.Point(13, 57)
        Me.txtNombrePaciente.Name = "txtNombrePaciente"
        Me.txtNombrePaciente.Size = New System.Drawing.Size(174, 27)
        Me.txtNombrePaciente.TabIndex = 23
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label4.Location = New System.Drawing.Point(13, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 21)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Nombre:"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.TabPage2.Controls.Add(Me.Panel3)
        Me.TabPage2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage2.Location = New System.Drawing.Point(4, 30)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(874, 505)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Historial Médico"
        '
        'Panel3
        '
        Me.Panel3.AutoScroll = True
        Me.Panel3.Controls.Add(Me.Panel8)
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(898, 623)
        Me.Panel3.TabIndex = 0
        '
        'Panel8
        '
        Me.Panel8.AutoScroll = True
        Me.Panel8.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel8.Controls.Add(Me.btnActualizarHistorial)
        Me.Panel8.Controls.Add(Me.btnLimpiarHistorial)
        Me.Panel8.Controls.Add(Me.tbxAntecedentesAlergicos)
        Me.Panel8.Controls.Add(Me.tbxAntecedentesQuirurgicos)
        Me.Panel8.Controls.Add(Me.tbxAntecedentesNoPersonales)
        Me.Panel8.Controls.Add(Me.tbxAntecedentesPersonales)
        Me.Panel8.Controls.Add(Me.Label28)
        Me.Panel8.Controls.Add(Me.Label29)
        Me.Panel8.Controls.Add(Me.Label27)
        Me.Panel8.Controls.Add(Me.Label26)
        Me.Panel8.Controls.Add(Me.tbxAntecedentesFamiliares)
        Me.Panel8.Controls.Add(Me.Label25)
        Me.Panel8.Controls.Add(Me.tbxQuejaPrincipal)
        Me.Panel8.Controls.Add(Me.Label24)
        Me.Panel8.Controls.Add(Me.dtpFechaHistorial)
        Me.Panel8.Controls.Add(Me.Label22)
        Me.Panel8.Location = New System.Drawing.Point(0, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(1021, 597)
        Me.Panel8.TabIndex = 14
        '
        'btnActualizarHistorial
        '
        Me.btnActualizarHistorial.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnActualizarHistorial.FlatAppearance.BorderSize = 0
        Me.btnActualizarHistorial.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActualizarHistorial.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActualizarHistorial.ForeColor = System.Drawing.Color.White
        Me.btnActualizarHistorial.Location = New System.Drawing.Point(577, 20)
        Me.btnActualizarHistorial.Name = "btnActualizarHistorial"
        Me.btnActualizarHistorial.Size = New System.Drawing.Size(120, 35)
        Me.btnActualizarHistorial.TabIndex = 85
        Me.btnActualizarHistorial.Text = "Actualizar"
        Me.btnActualizarHistorial.UseVisualStyleBackColor = False
        '
        'btnLimpiarHistorial
        '
        Me.btnLimpiarHistorial.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnLimpiarHistorial.FlatAppearance.BorderSize = 0
        Me.btnLimpiarHistorial.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLimpiarHistorial.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLimpiarHistorial.ForeColor = System.Drawing.Color.White
        Me.btnLimpiarHistorial.Location = New System.Drawing.Point(726, 20)
        Me.btnLimpiarHistorial.Name = "btnLimpiarHistorial"
        Me.btnLimpiarHistorial.Size = New System.Drawing.Size(120, 35)
        Me.btnLimpiarHistorial.TabIndex = 84
        Me.btnLimpiarHistorial.Text = "Limpiar"
        Me.btnLimpiarHistorial.UseVisualStyleBackColor = False
        '
        'tbxAntecedentesAlergicos
        '
        Me.tbxAntecedentesAlergicos.Location = New System.Drawing.Point(479, 415)
        Me.tbxAntecedentesAlergicos.Name = "tbxAntecedentesAlergicos"
        Me.tbxAntecedentesAlergicos.Size = New System.Drawing.Size(368, 71)
        Me.tbxAntecedentesAlergicos.TabIndex = 61
        Me.tbxAntecedentesAlergicos.Text = ""
        '
        'tbxAntecedentesQuirurgicos
        '
        Me.tbxAntecedentesQuirurgicos.Location = New System.Drawing.Point(28, 415)
        Me.tbxAntecedentesQuirurgicos.Name = "tbxAntecedentesQuirurgicos"
        Me.tbxAntecedentesQuirurgicos.Size = New System.Drawing.Size(379, 71)
        Me.tbxAntecedentesQuirurgicos.TabIndex = 60
        Me.tbxAntecedentesQuirurgicos.Text = ""
        '
        'tbxAntecedentesNoPersonales
        '
        Me.tbxAntecedentesNoPersonales.Location = New System.Drawing.Point(479, 271)
        Me.tbxAntecedentesNoPersonales.Name = "tbxAntecedentesNoPersonales"
        Me.tbxAntecedentesNoPersonales.Size = New System.Drawing.Size(368, 71)
        Me.tbxAntecedentesNoPersonales.TabIndex = 59
        Me.tbxAntecedentesNoPersonales.Text = ""
        '
        'tbxAntecedentesPersonales
        '
        Me.tbxAntecedentesPersonales.Location = New System.Drawing.Point(28, 271)
        Me.tbxAntecedentesPersonales.Name = "tbxAntecedentesPersonales"
        Me.tbxAntecedentesPersonales.Size = New System.Drawing.Size(379, 71)
        Me.tbxAntecedentesPersonales.TabIndex = 58
        Me.tbxAntecedentesPersonales.Text = ""
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label28.Location = New System.Drawing.Point(485, 378)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(199, 21)
        Me.Label28.TabIndex = 57
        Me.Label28.Text = "Antecedentes Alérgicos"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label29.Location = New System.Drawing.Point(35, 378)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(215, 21)
        Me.Label29.TabIndex = 55
        Me.Label29.Text = "Antecedentes Quirúrgicos"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label27.Location = New System.Drawing.Point(485, 235)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(328, 21)
        Me.Label27.TabIndex = 53
        Me.Label27.Text = "Antecedentes Patologicos No Personales"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label26.Location = New System.Drawing.Point(35, 235)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(302, 21)
        Me.Label26.TabIndex = 51
        Me.Label26.Text = "Antecedentes Patologicos Personales"
        '
        'tbxAntecedentesFamiliares
        '
        Me.tbxAntecedentesFamiliares.Location = New System.Drawing.Point(479, 127)
        Me.tbxAntecedentesFamiliares.Name = "tbxAntecedentesFamiliares"
        Me.tbxAntecedentesFamiliares.Size = New System.Drawing.Size(368, 71)
        Me.tbxAntecedentesFamiliares.TabIndex = 50
        Me.tbxAntecedentesFamiliares.Text = ""
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label25.Location = New System.Drawing.Point(485, 96)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(297, 21)
        Me.Label25.TabIndex = 49
        Me.Label25.Text = "Antecedentes Patologicos Familiares"
        '
        'tbxQuejaPrincipal
        '
        Me.tbxQuejaPrincipal.Location = New System.Drawing.Point(28, 127)
        Me.tbxQuejaPrincipal.Name = "tbxQuejaPrincipal"
        Me.tbxQuejaPrincipal.Size = New System.Drawing.Size(379, 71)
        Me.tbxQuejaPrincipal.TabIndex = 48
        Me.tbxQuejaPrincipal.Text = ""
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label24.Location = New System.Drawing.Point(35, 96)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(131, 21)
        Me.Label24.TabIndex = 47
        Me.Label24.Text = "Queja Principal:"
        '
        'dtpFechaHistorial
        '
        Me.dtpFechaHistorial.CustomFormat = "mm/dd/aa"
        Me.dtpFechaHistorial.Enabled = False
        Me.dtpFechaHistorial.Location = New System.Drawing.Point(104, 30)
        Me.dtpFechaHistorial.Name = "dtpFechaHistorial"
        Me.dtpFechaHistorial.Size = New System.Drawing.Size(218, 27)
        Me.dtpFechaHistorial.TabIndex = 46
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label22.Location = New System.Drawing.Point(35, 35)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(63, 21)
        Me.Label22.TabIndex = 45
        Me.Label22.Text = "Fecha "
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.TabPage3.CausesValidation = False
        Me.TabPage3.Controls.Add(Me.lcondicion)
        Me.TabPage3.Controls.Add(Me.Panel4)
        Me.TabPage3.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage3.Location = New System.Drawing.Point(4, 30)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(874, 505)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Encargados"
        '
        'lcondicion
        '
        Me.lcondicion.AutoSize = True
        Me.lcondicion.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lcondicion.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lcondicion.Location = New System.Drawing.Point(214, 250)
        Me.lcondicion.Name = "lcondicion"
        Me.lcondicion.Size = New System.Drawing.Size(415, 33)
        Me.lcondicion.TabIndex = 70
        Me.lcondicion.Text = "El paciente es mayor de edad"
        Me.lcondicion.Visible = False
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label21)
        Me.Panel4.Controls.Add(Me.Label20)
        Me.Panel4.Controls.Add(Me.Label19)
        Me.Panel4.Controls.Add(Me.Label18)
        Me.Panel4.Controls.Add(Me.Label17)
        Me.Panel4.Controls.Add(Me.btnGuardarNuevoR)
        Me.Panel4.Controls.Add(Me.btnOlvidar)
        Me.Panel4.Controls.Add(Me.btnNuevo)
        Me.Panel4.Controls.Add(Me.cbGeneroResponsable)
        Me.Panel4.Controls.Add(Me.btnActualizarResponsable)
        Me.Panel4.Controls.Add(Me.btnRemplazar)
        Me.Panel4.Controls.Add(Me.PictureBox2)
        Me.Panel4.Controls.Add(Me.Label31)
        Me.Panel4.Controls.Add(Me.tbParentescoResponsable)
        Me.Panel4.Controls.Add(Me.Label32)
        Me.Panel4.Controls.Add(Me.tbTelefono2Responsable)
        Me.Panel4.Controls.Add(Me.Label33)
        Me.Panel4.Controls.Add(Me.tbEdadResponsable)
        Me.Panel4.Controls.Add(Me.Label34)
        Me.Panel4.Controls.Add(Me.tbTelefono1Responsable)
        Me.Panel4.Controls.Add(Me.Label35)
        Me.Panel4.Controls.Add(Me.tbBusquedaResponsable)
        Me.Panel4.Controls.Add(Me.Label36)
        Me.Panel4.Controls.Add(Me.dgvResponsables)
        Me.Panel4.Controls.Add(Me.dtpFechaNaciResponsable)
        Me.Panel4.Controls.Add(Me.Label37)
        Me.Panel4.Controls.Add(Me.tbxDireccionResponsable)
        Me.Panel4.Controls.Add(Me.Label38)
        Me.Panel4.Controls.Add(Me.tbCedulaResponsable)
        Me.Panel4.Controls.Add(Me.Label39)
        Me.Panel4.Controls.Add(Me.tbCorreoResponsable)
        Me.Panel4.Controls.Add(Me.Label40)
        Me.Panel4.Controls.Add(Me.tbNombreResponsable)
        Me.Panel4.Controls.Add(Me.Label41)
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(878, 513)
        Me.Panel4.TabIndex = 0
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Red
        Me.Label21.Location = New System.Drawing.Point(89, 96)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(19, 22)
        Me.Label21.TabIndex = 73
        Me.Label21.Text = "*"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Red
        Me.Label20.Location = New System.Drawing.Point(112, 174)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(19, 22)
        Me.Label20.TabIndex = 73
        Me.Label20.Text = "*"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Red
        Me.Label19.Location = New System.Drawing.Point(322, 174)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(19, 22)
        Me.Label19.TabIndex = 73
        Me.Label19.Text = "*"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Red
        Me.Label18.Location = New System.Drawing.Point(297, 24)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(19, 22)
        Me.Label18.TabIndex = 73
        Me.Label18.Text = "*"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Red
        Me.Label17.Location = New System.Drawing.Point(97, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(19, 22)
        Me.Label17.TabIndex = 73
        Me.Label17.Text = "*"
        '
        'btnGuardarNuevoR
        '
        Me.btnGuardarNuevoR.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnGuardarNuevoR.FlatAppearance.BorderSize = 0
        Me.btnGuardarNuevoR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardarNuevoR.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardarNuevoR.ForeColor = System.Drawing.Color.White
        Me.btnGuardarNuevoR.Location = New System.Drawing.Point(42, 437)
        Me.btnGuardarNuevoR.Name = "btnGuardarNuevoR"
        Me.btnGuardarNuevoR.Size = New System.Drawing.Size(108, 44)
        Me.btnGuardarNuevoR.TabIndex = 87
        Me.btnGuardarNuevoR.Text = "Guardar"
        Me.btnGuardarNuevoR.UseVisualStyleBackColor = False
        Me.btnGuardarNuevoR.Visible = False
        '
        'btnOlvidar
        '
        Me.btnOlvidar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnOlvidar.Enabled = False
        Me.btnOlvidar.FlatAppearance.BorderSize = 0
        Me.btnOlvidar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOlvidar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOlvidar.ForeColor = System.Drawing.Color.White
        Me.btnOlvidar.Location = New System.Drawing.Point(645, 437)
        Me.btnOlvidar.Name = "btnOlvidar"
        Me.btnOlvidar.Size = New System.Drawing.Size(189, 44)
        Me.btnOlvidar.TabIndex = 86
        Me.btnOlvidar.Text = "Responsable Actual"
        Me.btnOlvidar.UseVisualStyleBackColor = False
        '
        'btnNuevo
        '
        Me.btnNuevo.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnNuevo.FlatAppearance.BorderSize = 0
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.ForeColor = System.Drawing.Color.White
        Me.btnNuevo.Location = New System.Drawing.Point(42, 437)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(108, 44)
        Me.btnNuevo.TabIndex = 85
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = False
        '
        'cbGeneroResponsable
        '
        Me.cbGeneroResponsable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbGeneroResponsable.FormattingEnabled = True
        Me.cbGeneroResponsable.Items.AddRange(New Object() {"Femenino", "Masculino"})
        Me.cbGeneroResponsable.Location = New System.Drawing.Point(228, 54)
        Me.cbGeneroResponsable.Name = "cbGeneroResponsable"
        Me.cbGeneroResponsable.Size = New System.Drawing.Size(174, 29)
        Me.cbGeneroResponsable.TabIndex = 84
        '
        'btnActualizarResponsable
        '
        Me.btnActualizarResponsable.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnActualizarResponsable.FlatAppearance.BorderSize = 0
        Me.btnActualizarResponsable.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActualizarResponsable.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActualizarResponsable.ForeColor = System.Drawing.Color.White
        Me.btnActualizarResponsable.Location = New System.Drawing.Point(167, 437)
        Me.btnActualizarResponsable.Name = "btnActualizarResponsable"
        Me.btnActualizarResponsable.Size = New System.Drawing.Size(108, 44)
        Me.btnActualizarResponsable.TabIndex = 83
        Me.btnActualizarResponsable.Text = "Actualizar"
        Me.btnActualizarResponsable.UseVisualStyleBackColor = False
        '
        'btnRemplazar
        '
        Me.btnRemplazar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnRemplazar.Enabled = False
        Me.btnRemplazar.FlatAppearance.BorderSize = 0
        Me.btnRemplazar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRemplazar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemplazar.ForeColor = System.Drawing.Color.White
        Me.btnRemplazar.Location = New System.Drawing.Point(292, 437)
        Me.btnRemplazar.Name = "btnRemplazar"
        Me.btnRemplazar.Size = New System.Drawing.Size(108, 44)
        Me.btnRemplazar.TabIndex = 82
        Me.btnRemplazar.Text = "Remplazar"
        Me.btnRemplazar.UseVisualStyleBackColor = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(842, 22)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox2.TabIndex = 81
        Me.PictureBox2.TabStop = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label31.Location = New System.Drawing.Point(223, 21)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(73, 21)
        Me.Label31.TabIndex = 79
        Me.Label31.Text = "Género:"
        '
        'tbParentescoResponsable
        '
        Me.tbParentescoResponsable.Location = New System.Drawing.Point(227, 129)
        Me.tbParentescoResponsable.Name = "tbParentescoResponsable"
        Me.tbParentescoResponsable.Size = New System.Drawing.Size(173, 27)
        Me.tbParentescoResponsable.TabIndex = 78
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label32.Location = New System.Drawing.Point(223, 96)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(102, 21)
        Me.Label32.TabIndex = 77
        Me.Label32.Text = "Parentesco:"
        '
        'tbTelefono2Responsable
        '
        Me.tbTelefono2Responsable.Location = New System.Drawing.Point(228, 286)
        Me.tbTelefono2Responsable.Name = "tbTelefono2Responsable"
        Me.tbTelefono2Responsable.Size = New System.Drawing.Size(173, 27)
        Me.tbTelefono2Responsable.TabIndex = 76
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label33.Location = New System.Drawing.Point(224, 253)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(93, 21)
        Me.Label33.TabIndex = 75
        Me.Label33.Text = "Teléfono 2:"
        '
        'tbEdadResponsable
        '
        Me.tbEdadResponsable.Enabled = False
        Me.tbEdadResponsable.Location = New System.Drawing.Point(16, 286)
        Me.tbEdadResponsable.Name = "tbEdadResponsable"
        Me.tbEdadResponsable.Size = New System.Drawing.Size(174, 27)
        Me.tbEdadResponsable.TabIndex = 74
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label34.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label34.Location = New System.Drawing.Point(19, 250)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(56, 21)
        Me.Label34.TabIndex = 73
        Me.Label34.Text = "Edad:"
        '
        'tbTelefono1Responsable
        '
        Me.tbTelefono1Responsable.Location = New System.Drawing.Point(227, 207)
        Me.tbTelefono1Responsable.Name = "tbTelefono1Responsable"
        Me.tbTelefono1Responsable.Size = New System.Drawing.Size(173, 27)
        Me.tbTelefono1Responsable.TabIndex = 72
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label35.Location = New System.Drawing.Point(223, 174)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(93, 21)
        Me.Label35.TabIndex = 71
        Me.Label35.Text = "Teléfono 1:"
        '
        'tbBusquedaResponsable
        '
        Me.tbBusquedaResponsable.Location = New System.Drawing.Point(531, 22)
        Me.tbBusquedaResponsable.Name = "tbBusquedaResponsable"
        Me.tbBusquedaResponsable.Size = New System.Drawing.Size(305, 27)
        Me.tbBusquedaResponsable.TabIndex = 70
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label36.Location = New System.Drawing.Point(433, 21)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(92, 21)
        Me.Label36.TabIndex = 69
        Me.Label36.Text = "Busqueda:"
        '
        'dgvResponsables
        '
        Me.dgvResponsables.AllowUserToAddRows = False
        Me.dgvResponsables.AllowUserToDeleteRows = False
        Me.dgvResponsables.AutoGenerateColumns = False
        Me.dgvResponsables.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.dgvResponsables.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvResponsables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvResponsables.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDResponsableDataGridViewTextBoxColumn1, Me.CedulaDataGridViewTextBoxColumn1, Me.NombreCompletoDataGridViewTextBoxColumn1, Me.DireccionDataGridViewTextBoxColumn1, Me.CorreoDataGridViewTextBoxColumn1, Me.Telefono1DataGridViewTextBoxColumn1, Me.Telefono2DataGridViewTextBoxColumn1, Me.ParentescoDataGridViewTextBoxColumn, Me.GeneroDataGridViewCheckBoxColumn1, Me.FechaNacimientoDataGridViewTextBoxColumn1, Me.EstadoDataGridViewCheckBoxColumn1})
        Me.dgvResponsables.DataSource = Me.ResponsablesBindingSource
        Me.dgvResponsables.Location = New System.Drawing.Point(436, 58)
        Me.dgvResponsables.Name = "dgvResponsables"
        Me.dgvResponsables.ReadOnly = True
        Me.dgvResponsables.RowHeadersVisible = False
        Me.dgvResponsables.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResponsables.Size = New System.Drawing.Size(426, 362)
        Me.dgvResponsables.TabIndex = 68
        '
        'IDResponsableDataGridViewTextBoxColumn1
        '
        Me.IDResponsableDataGridViewTextBoxColumn1.DataPropertyName = "ID_Responsable"
        Me.IDResponsableDataGridViewTextBoxColumn1.HeaderText = "ID_Responsable"
        Me.IDResponsableDataGridViewTextBoxColumn1.Name = "IDResponsableDataGridViewTextBoxColumn1"
        Me.IDResponsableDataGridViewTextBoxColumn1.ReadOnly = True
        Me.IDResponsableDataGridViewTextBoxColumn1.Visible = False
        '
        'CedulaDataGridViewTextBoxColumn1
        '
        Me.CedulaDataGridViewTextBoxColumn1.DataPropertyName = "Cedula"
        Me.CedulaDataGridViewTextBoxColumn1.HeaderText = "Cedula"
        Me.CedulaDataGridViewTextBoxColumn1.Name = "CedulaDataGridViewTextBoxColumn1"
        Me.CedulaDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'NombreCompletoDataGridViewTextBoxColumn1
        '
        Me.NombreCompletoDataGridViewTextBoxColumn1.DataPropertyName = "NombreCompleto"
        Me.NombreCompletoDataGridViewTextBoxColumn1.HeaderText = "Nombre"
        Me.NombreCompletoDataGridViewTextBoxColumn1.Name = "NombreCompletoDataGridViewTextBoxColumn1"
        Me.NombreCompletoDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DireccionDataGridViewTextBoxColumn1
        '
        Me.DireccionDataGridViewTextBoxColumn1.DataPropertyName = "Direccion"
        Me.DireccionDataGridViewTextBoxColumn1.HeaderText = "Direccion"
        Me.DireccionDataGridViewTextBoxColumn1.Name = "DireccionDataGridViewTextBoxColumn1"
        Me.DireccionDataGridViewTextBoxColumn1.ReadOnly = True
        Me.DireccionDataGridViewTextBoxColumn1.Visible = False
        '
        'CorreoDataGridViewTextBoxColumn1
        '
        Me.CorreoDataGridViewTextBoxColumn1.DataPropertyName = "Correo"
        Me.CorreoDataGridViewTextBoxColumn1.HeaderText = "Correo"
        Me.CorreoDataGridViewTextBoxColumn1.Name = "CorreoDataGridViewTextBoxColumn1"
        Me.CorreoDataGridViewTextBoxColumn1.ReadOnly = True
        Me.CorreoDataGridViewTextBoxColumn1.Visible = False
        '
        'Telefono1DataGridViewTextBoxColumn1
        '
        Me.Telefono1DataGridViewTextBoxColumn1.DataPropertyName = "Telefono1"
        Me.Telefono1DataGridViewTextBoxColumn1.HeaderText = "Telefono"
        Me.Telefono1DataGridViewTextBoxColumn1.Name = "Telefono1DataGridViewTextBoxColumn1"
        Me.Telefono1DataGridViewTextBoxColumn1.ReadOnly = True
        Me.Telefono1DataGridViewTextBoxColumn1.Width = 80
        '
        'Telefono2DataGridViewTextBoxColumn1
        '
        Me.Telefono2DataGridViewTextBoxColumn1.DataPropertyName = "Telefono2"
        Me.Telefono2DataGridViewTextBoxColumn1.HeaderText = "Telefono2"
        Me.Telefono2DataGridViewTextBoxColumn1.Name = "Telefono2DataGridViewTextBoxColumn1"
        Me.Telefono2DataGridViewTextBoxColumn1.ReadOnly = True
        Me.Telefono2DataGridViewTextBoxColumn1.Visible = False
        '
        'ParentescoDataGridViewTextBoxColumn
        '
        Me.ParentescoDataGridViewTextBoxColumn.DataPropertyName = "Parentesco"
        Me.ParentescoDataGridViewTextBoxColumn.HeaderText = "Parentesco"
        Me.ParentescoDataGridViewTextBoxColumn.Name = "ParentescoDataGridViewTextBoxColumn"
        Me.ParentescoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'GeneroDataGridViewCheckBoxColumn1
        '
        Me.GeneroDataGridViewCheckBoxColumn1.DataPropertyName = "Genero"
        Me.GeneroDataGridViewCheckBoxColumn1.HeaderText = "Genero"
        Me.GeneroDataGridViewCheckBoxColumn1.Name = "GeneroDataGridViewCheckBoxColumn1"
        Me.GeneroDataGridViewCheckBoxColumn1.ReadOnly = True
        Me.GeneroDataGridViewCheckBoxColumn1.Visible = False
        '
        'FechaNacimientoDataGridViewTextBoxColumn1
        '
        Me.FechaNacimientoDataGridViewTextBoxColumn1.DataPropertyName = "FechaNacimiento"
        Me.FechaNacimientoDataGridViewTextBoxColumn1.HeaderText = "FechaNacimiento"
        Me.FechaNacimientoDataGridViewTextBoxColumn1.Name = "FechaNacimientoDataGridViewTextBoxColumn1"
        Me.FechaNacimientoDataGridViewTextBoxColumn1.ReadOnly = True
        Me.FechaNacimientoDataGridViewTextBoxColumn1.Visible = False
        '
        'EstadoDataGridViewCheckBoxColumn1
        '
        Me.EstadoDataGridViewCheckBoxColumn1.DataPropertyName = "Estado"
        Me.EstadoDataGridViewCheckBoxColumn1.HeaderText = "Estado"
        Me.EstadoDataGridViewCheckBoxColumn1.Name = "EstadoDataGridViewCheckBoxColumn1"
        Me.EstadoDataGridViewCheckBoxColumn1.ReadOnly = True
        Me.EstadoDataGridViewCheckBoxColumn1.Visible = False
        '
        'ResponsablesBindingSource
        '
        Me.ResponsablesBindingSource.DataMember = "Responsables"
        Me.ResponsablesBindingSource.DataSource = Me.DBA494E9PFIngenieriaDataSetBindingSource
        '
        'DBA494E9PFIngenieriaDataSetBindingSource
        '
        Me.DBA494E9PFIngenieriaDataSetBindingSource.DataSource = Me.DB_A494E9_PFIngenieriaDataSet
        Me.DBA494E9PFIngenieriaDataSetBindingSource.Position = 0
        '
        'DB_A494E9_PFIngenieriaDataSet
        '
        Me.DB_A494E9_PFIngenieriaDataSet.DataSetName = "DB_A494E9_PFIngenieriaDataSet"
        Me.DB_A494E9_PFIngenieriaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'dtpFechaNaciResponsable
        '
        Me.dtpFechaNaciResponsable.CustomFormat = "mm/dd/aa"
        Me.dtpFechaNaciResponsable.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaNaciResponsable.Location = New System.Drawing.Point(16, 207)
        Me.dtpFechaNaciResponsable.Name = "dtpFechaNaciResponsable"
        Me.dtpFechaNaciResponsable.Size = New System.Drawing.Size(173, 27)
        Me.dtpFechaNaciResponsable.TabIndex = 67
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label37.Location = New System.Drawing.Point(16, 174)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(100, 21)
        Me.Label37.TabIndex = 66
        Me.Label37.Text = "Fecha Nac."
        '
        'tbxDireccionResponsable
        '
        Me.tbxDireccionResponsable.Location = New System.Drawing.Point(16, 360)
        Me.tbxDireccionResponsable.Name = "tbxDireccionResponsable"
        Me.tbxDireccionResponsable.Size = New System.Drawing.Size(174, 60)
        Me.tbxDireccionResponsable.TabIndex = 65
        Me.tbxDireccionResponsable.Text = ""
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label38.Location = New System.Drawing.Point(21, 327)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(87, 21)
        Me.Label38.TabIndex = 64
        Me.Label38.Text = "Dirección:"
        '
        'tbCedulaResponsable
        '
        Me.tbCedulaResponsable.Location = New System.Drawing.Point(16, 129)
        Me.tbCedulaResponsable.Name = "tbCedulaResponsable"
        Me.tbCedulaResponsable.Size = New System.Drawing.Size(174, 27)
        Me.tbCedulaResponsable.TabIndex = 63
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label39.Location = New System.Drawing.Point(16, 96)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(72, 21)
        Me.Label39.TabIndex = 62
        Me.Label39.Text = "Cédula:"
        '
        'tbCorreoResponsable
        '
        Me.tbCorreoResponsable.Location = New System.Drawing.Point(228, 360)
        Me.tbCorreoResponsable.Name = "tbCorreoResponsable"
        Me.tbCorreoResponsable.Size = New System.Drawing.Size(174, 27)
        Me.tbCorreoResponsable.TabIndex = 61
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label40.Location = New System.Drawing.Point(225, 327)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(67, 21)
        Me.Label40.TabIndex = 60
        Me.Label40.Text = "Correo:"
        '
        'tbNombreResponsable
        '
        Me.tbNombreResponsable.Location = New System.Drawing.Point(16, 54)
        Me.tbNombreResponsable.Name = "tbNombreResponsable"
        Me.tbNombreResponsable.Size = New System.Drawing.Size(174, 27)
        Me.tbNombreResponsable.TabIndex = 59
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label41.Location = New System.Drawing.Point(16, 21)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(77, 21)
        Me.Label41.TabIndex = 58
        Me.Label41.Text = "Nombre:"
        '
        'PacientesActivosBindingSource
        '
        Me.PacientesActivosBindingSource.DataMember = "pacientesActivos"
        Me.PacientesActivosBindingSource.DataSource = Me.DB_A494E9_PFIngenieriaDataSet
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel2.Controls.Add(Me.PictureBox3)
        Me.Panel2.Controls.Add(Me.btnGuardar)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(901, 71)
        Me.Panel2.TabIndex = 5
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(901, 3)
        Me.PictureBox3.TabIndex = 37
        Me.PictureBox3.TabStop = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.btnGuardar.FlatAppearance.BorderSize = 0
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.White
        Me.btnGuardar.Location = New System.Drawing.Point(739, 22)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(120, 35)
        Me.btnGuardar.TabIndex = 34
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(453, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(144, 33)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Pacientes"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(182, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(286, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(171, 33)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Formulario /"
        '
        'PacientesBindingSource
        '
        Me.PacientesBindingSource.DataMember = "Pacientes"
        Me.PacientesBindingSource.DataSource = Me.DB_A494E9_PFIngenieriaDataSet
        '
        'PacientesTableAdapter
        '
        Me.PacientesTableAdapter.ClearBeforeFill = True
        '
        'ResponsablesTableAdapter
        '
        Me.ResponsablesTableAdapter.ClearBeforeFill = True
        '
        'PacientesActivosTableAdapter
        '
        Me.PacientesActivosTableAdapter.ClearBeforeFill = True
        '
        'PacientesInactivosBindingSource
        '
        Me.PacientesInactivosBindingSource.DataMember = "pacientesInactivos"
        Me.PacientesInactivosBindingSource.DataSource = Me.DB_A494E9_PFIngenieriaDataSet
        '
        'PacientesInactivosTableAdapter
        '
        Me.PacientesInactivosTableAdapter.ClearBeforeFill = True
        '
        'PacientesActivosBindingSource1
        '
        Me.PacientesActivosBindingSource1.DataMember = "pacientesActivos"
        Me.PacientesActivosBindingSource1.DataSource = Me.DB_A494E9_PFIngenieriaDataSet
        '
        'PacientesInactivosBindingSource1
        '
        Me.PacientesInactivosBindingSource1.DataMember = "pacientesInactivos"
        Me.PacientesInactivosBindingSource1.DataSource = Me.DB_A494E9_PFIngenieriaDataSet
        '
        'PacientesActivosTodosBindingSource
        '
        Me.PacientesActivosTodosBindingSource.DataMember = "pacientesActivosTodos"
        Me.PacientesActivosTodosBindingSource.DataSource = Me.DBA494E9PFIngenieriaDataSetBindingSource
        '
        'PacientesActivosTodosBindingSource1
        '
        Me.PacientesActivosTodosBindingSource1.DataMember = "pacientesActivosTodos"
        Me.PacientesActivosTodosBindingSource1.DataSource = Me.DBA494E9PFIngenieriaDataSetBindingSource
        '
        'PacientesActivosTodosTableAdapter
        '
        Me.PacientesActivosTodosTableAdapter.ClearBeforeFill = True
        '
        'FrmInsertarPaciente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(903, 622)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.TabPacientes)
        Me.Name = "FrmInsertarPaciente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Registro de Pacientes"
        Me.TabPacientes.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvPacientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PacientesActivosTodosBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DB_A494E9_PFIngenieriaDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvResponsables, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResponsablesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DBA494E9PFIngenieriaDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DB_A494E9_PFIngenieriaDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PacientesActivosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PacientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PacientesInactivosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PacientesActivosBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PacientesInactivosBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PacientesActivosTodosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PacientesActivosTodosBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabPacientes As TabControl
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label12 As Label
    Friend WithEvents tbxDireccionPaciente As RichTextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtCedulaPaciente As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtCorreoPaciente As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtNombrePaciente As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel8 As Panel
    Friend WithEvents dtpFechaHistorial As DateTimePicker
    Friend WithEvents Label22 As Label
    Friend WithEvents tbxQuejaPrincipal As RichTextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents tbxAntecedentesFamiliares As RichTextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents txtTelefono1Paciente As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents cbGeneroPaciente As ComboBox
    Friend WithEvents Label30 As Label
    Friend WithEvents txtOcupacionPaciente As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtTelefono2Paciente As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtEdadPaciente As TextBox
    Friend WithEvents Label3 As Label
    Private WithEvents btnActualizar As Button
    Private WithEvents btnEliminar As Button
    Private WithEvents btnActualizarHistorial As Button
    Private WithEvents btnLimpiarHistorial As Button
    Friend WithEvents tbxAntecedentesAlergicos As RichTextBox
    Friend WithEvents tbxAntecedentesQuirurgicos As RichTextBox
    Friend WithEvents tbxAntecedentesNoPersonales As RichTextBox
    Friend WithEvents tbxAntecedentesPersonales As RichTextBox
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents Panel4 As Panel
    Private WithEvents btnActualizarResponsable As Button
    Private WithEvents btnRemplazar As Button
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Label31 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents tbTelefono2Responsable As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents tbEdadResponsable As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents tbTelefono1Responsable As TextBox
    Friend WithEvents Label35 As Label
    Friend WithEvents tbBusquedaResponsable As TextBox
    Friend WithEvents Label36 As Label
    Friend WithEvents dgvResponsables As DataGridView
    Friend WithEvents dtpFechaNaciResponsable As DateTimePicker
    Friend WithEvents Label37 As Label
    Friend WithEvents tbxDireccionResponsable As RichTextBox
    Friend WithEvents Label38 As Label
    Friend WithEvents tbCedulaResponsable As TextBox
    Friend WithEvents Label39 As Label
    Friend WithEvents tbCorreoResponsable As TextBox
    Friend WithEvents Label40 As Label
    Friend WithEvents tbNombreResponsable As TextBox
    Friend WithEvents Label41 As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents PictureBox3 As PictureBox
    Private WithEvents btnGuardar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cbxPapelera As CheckBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents txtBusqueda As TextBox
    Friend WithEvents cbxActivos As CheckBox
    Friend WithEvents tbParentescoResponsable As TextBox
    Friend WithEvents lcondicion As Label
    Friend WithEvents dtpFechaNacimientoPaciente As DateTimePicker
    Friend WithEvents DB_A494E9_PFIngenieriaDataSet As DB_A494E9_PFIngenieriaDataSet
    Friend WithEvents PacientesBindingSource As BindingSource
    Friend WithEvents PacientesTableAdapter As DB_A494E9_PFIngenieriaDataSetTableAdapters.PacientesTableAdapter
    Friend WithEvents DBA494E9PFIngenieriaDataSetBindingSource As BindingSource
    Friend WithEvents ResponsablesBindingSource As BindingSource
    Friend WithEvents ResponsablesTableAdapter As DB_A494E9_PFIngenieriaDataSetTableAdapters.ResponsablesTableAdapter
    Friend WithEvents PacientesActivosBindingSource As BindingSource
    Friend WithEvents PacientesActivosTableAdapter As DB_A494E9_PFIngenieriaDataSetTableAdapters.pacientesActivosTableAdapter
    Friend WithEvents PacientesActivosBindingSource1 As BindingSource
    Friend WithEvents PacientesInactivosBindingSource As BindingSource
    Friend WithEvents PacientesInactivosTableAdapter As DB_A494E9_PFIngenieriaDataSetTableAdapters.pacientesInactivosTableAdapter
    Friend WithEvents PacientesInactivosBindingSource1 As BindingSource
    Friend WithEvents PacientesActivosTodosBindingSource As BindingSource
    Friend WithEvents PacientesActivosTodosBindingSource1 As BindingSource
    Friend WithEvents dgvPacientes As DataGridView
    Friend WithEvents DB_A494E9_PFIngenieriaDataSet1 As DB_A494E9_PFIngenieriaDataSet1
    Friend WithEvents PacientesActivosTodosBindingSource2 As BindingSource
    Friend WithEvents PacientesActivosTodosTableAdapter As DB_A494E9_PFIngenieriaDataSet1TableAdapters.pacientesActivosTodosTableAdapter
    Private WithEvents btnRestablecer As Button
    Friend WithEvents cbGeneroResponsable As ComboBox
    Private WithEvents btnNuevo As Button
    Private WithEvents btnOlvidar As Button
    Private WithEvents btnGuardarNuevoR As Button
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents IDPacienteDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CedulaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NombreCompletoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DireccionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CorreoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents OcupacionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GeneroDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents FechaNacimientoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Telefono1DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Telefono2DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents IDResponsableDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents EstadoDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Private WithEvents btnNUevoPaciente As Button
    Friend WithEvents IDResponsableDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CedulaDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents NombreCompletoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DireccionDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CorreoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents Telefono1DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents Telefono2DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ParentescoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GeneroDataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
    Friend WithEvents FechaNacimientoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents EstadoDataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReporteCitasDia
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.DataReportes = New ProyectoFinalIngenieria.DataReportes()
        Me.ConsultarCitasDiaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultarCitasDiaTableAdapter = New ProyectoFinalIngenieria.DataReportesTableAdapters.ConsultarCitasDiaTableAdapter()
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultarCitasDiaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        ReportDataSource1.Name = "DataReporte"
        ReportDataSource1.Value = Me.ConsultarCitasDiaBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ProyectoFinalIngenieria.ReporteCitasDia.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(855, 449)
        Me.ReportViewer1.TabIndex = 0
        '
        'DataReportes
        '
        Me.DataReportes.DataSetName = "DataReportes"
        Me.DataReportes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConsultarCitasDiaBindingSource
        '
        Me.ConsultarCitasDiaBindingSource.DataMember = "ConsultarCitasDia"
        Me.ConsultarCitasDiaBindingSource.DataSource = Me.DataReportes
        '
        'ConsultarCitasDiaTableAdapter
        '
        Me.ConsultarCitasDiaTableAdapter.ClearBeforeFill = True
        '
        'ReporteCitasDia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(856, 450)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "ReporteCitasDia"
        Me.Text = "Reporte Citas Día"
        CType(Me.DataReportes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultarCitasDiaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents ConsultarCitasDiaBindingSource As BindingSource
    Friend WithEvents DataReportes As DataReportes
    Friend WithEvents ConsultarCitasDiaTableAdapter As DataReportesTableAdapters.ConsultarCitasDiaTableAdapter
End Class

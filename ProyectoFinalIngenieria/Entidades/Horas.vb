﻿Public Class Horas
    Dim ID_Hora As Integer
    Dim Estado As Boolean
    Dim Hora As TimeSpan

    Public Sub New()
    End Sub

    Public Sub New(iD_Hora As Integer, estado As Boolean, hora As TimeSpan)
        Me.ID_Hora = iD_Hora
        Me.Estado = estado
        Me.Hora = hora
    End Sub

    Public Sub New(iD_Hora As Integer, hora As TimeSpan)
        Me.ID_Hora = iD_Hora
        Me.Hora = hora
    End Sub

    Public Sub New(estado As Boolean, hora As TimeSpan)
        Me.Estado = estado
        Me.Hora = hora
    End Sub


    ReadOnly Property ID_Hora_() As Integer
        Get
            Return ID_Hora
        End Get
    End Property

    Public Property Estado_() As Boolean
        Get
            Return Estado
        End Get
        Set(ByVal est As Boolean)
            Estado = est
        End Set
    End Property

    Public Property Hora_() As TimeSpan
        Get
            Return Hora
        End Get
        Set(ByVal fn As TimeSpan)
            Hora = fn
        End Set
    End Property


End Class

﻿Public Class HistorialFamiliar_Alergico

    Private ID_Historial As Integer
    Private ID_Paciente As Integer
    Private Queja_principal As String
    Private Antecedentes_patologicos_familiares As String
    Private Antecedentes_patologicos_personales As String
    Private Antecedentes_patologicos_no_personales As String
    Private Antecedentes_quirurgicos As String
    Private Antecedentes_alergicos As String
    Private Fecha As Date
    Private Estado As Boolean

    Public Sub New()
    End Sub

    'CONSULTAR HISTORIAL
    Public Sub New(iD_Historial As Integer, iD_Paciente As Integer,
                   queja_principal As String, antecedentes_patologicos_familiares As String,
                   antecedentes_patologicos_personales As String, antecedentes_patologicos_no_personales As String,
                   antecedentes_quirurgicos As String, antecedentes_alergicos As String, fecha As Date, estado As Boolean)
        Me.ID_Historial = iD_Historial
        Me.ID_Paciente = iD_Paciente
        Me.Queja_principal = queja_principal
        Me.Antecedentes_patologicos_familiares = antecedentes_patologicos_familiares
        Me.Antecedentes_patologicos_personales = antecedentes_patologicos_personales
        Me.Antecedentes_patologicos_no_personales = antecedentes_patologicos_no_personales
        Me.Antecedentes_quirurgicos = antecedentes_quirurgicos
        Me.Antecedentes_alergicos = antecedentes_alergicos
        Me.Fecha = fecha
        Me.Estado = estado
    End Sub

    Public Sub New(iD_Paciente As Integer, queja_principal As String,
                   antecedentes_patologicos_familiares As String,
                   antecedentes_patologicos_personales As String,
                   antecedentes_patologicos_no_personales As String,
                   antecedentes_quirurgicos As String, antecedentes_alergicos As String,
                   fecha As Date, estado As Boolean)
        Me.ID_Paciente = iD_Paciente
        Me.Queja_principal = queja_principal
        Me.Antecedentes_patologicos_familiares = antecedentes_patologicos_familiares
        Me.Antecedentes_patologicos_personales = antecedentes_patologicos_personales
        Me.Antecedentes_patologicos_no_personales = antecedentes_patologicos_no_personales
        Me.Antecedentes_quirurgicos = antecedentes_quirurgicos
        Me.Antecedentes_alergicos = antecedentes_alergicos
        Me.Fecha = fecha
        Me.Estado = estado

        Me.ID_Historial = 0
    End Sub

    ReadOnly Property ID_Historial_() As Integer
        Get
            ID_Historial_ = ID_Historial
        End Get
    End Property

    Public Property ID_Paciente_() As Integer
        Get
            ID_Paciente_ = ID_Paciente
        End Get
        Set(ByVal idp As Integer)
            ID_Paciente = idp
        End Set
    End Property

    Public Property Queja_principal_() As String
        Get
            Queja_principal_ = Queja_principal
        End Get
        Set(ByVal qp As String)
            Queja_principal = qp
        End Set
    End Property

    Public Property Antecedentes_patologicos_familiares_() As String
        Get
            Antecedentes_patologicos_familiares_ = Antecedentes_patologicos_familiares
        End Get
        Set(ByVal apf As String)
            Antecedentes_patologicos_familiares = apf
        End Set
    End Property

    Public Property Antecedentes_patologicos_personales_() As String
        Get
            Antecedentes_patologicos_personales_ = Antecedentes_patologicos_personales
        End Get
        Set(ByVal app As String)
            Antecedentes_patologicos_personales = app
        End Set
    End Property

    Public Property Antecedentes_patologicos_no_personales_() As String
        Get
            Antecedentes_patologicos_no_personales_ = Antecedentes_patologicos_no_personales
        End Get
        Set(ByVal apnp As String)
            Antecedentes_patologicos_no_personales = apnp
        End Set
    End Property

    Public Property Antecedentes_quirurgicos_() As String
        Get
            Antecedentes_quirurgicos_ = Antecedentes_quirurgicos
        End Get
        Set(ByVal aq As String)
            Antecedentes_quirurgicos = aq
        End Set
    End Property

    Public Property Antecedentes_alergicos_() As String
        Get
            Antecedentes_alergicos_ = Antecedentes_alergicos
        End Get
        Set(ByVal aa As String)
            Antecedentes_alergicos = aa
        End Set
    End Property

    Public Property Fecha_() As Date
        Get
            Fecha_ = Fecha
        End Get
        Set(ByVal f As Date)
            Fecha = f
        End Set
    End Property

    Public Property Estado_() As Boolean
        Get
            Estado_ = Estado
        End Get
        Set(ByVal est As Boolean)
            Estado = est
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return MyBase.ToString()
    End Function
End Class

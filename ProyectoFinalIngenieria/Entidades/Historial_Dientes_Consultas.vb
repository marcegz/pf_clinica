﻿Public Class Historial_Dientes_Consultas

    Private ID_Consulta As Integer
    Private ID_Diente As Integer
    Private ID_Tratamiento As Integer
    Private Decripcion As String
    Private LadoDiente_Tratamiento As String

    Public Sub New()
    End Sub

    Public Sub New(iD_Consulta As Integer, iD_Diente As Integer, iD_Tratamiento As Integer, decripcion As String, ladoDiente_Tratamiento As String)
        Me.ID_Consulta1 = iD_Consulta
        Me.ID_Diente1 = iD_Diente
        Me.ID_Tratamiento1 = iD_Tratamiento
        Me.Decripcion1 = decripcion
        Me.LadoDiente_Tratamiento1 = ladoDiente_Tratamiento
    End Sub



    ' ------------ SET & GET

    Public Property ID_Consulta1 As Integer
        Get
            Return ID_Consulta
        End Get
        Set(value As Integer)
            ID_Consulta = value
        End Set
    End Property

    Public Property ID_Diente1 As Integer
        Get
            Return ID_Diente
        End Get
        Set(value As Integer)
            ID_Diente = value
        End Set
    End Property

    Public Property ID_Tratamiento1 As Integer
        Get
            Return ID_Tratamiento
        End Get
        Set(value As Integer)
            ID_Tratamiento = value
        End Set
    End Property

    Public Property Decripcion1 As String
        Get
            Return Decripcion
        End Get
        Set(value As String)
            Decripcion = value
        End Set
    End Property

    Public Property LadoDiente_Tratamiento1 As String
        Get
            Return LadoDiente_Tratamiento
        End Get
        Set(value As String)
            LadoDiente_Tratamiento = value
        End Set
    End Property


End Class

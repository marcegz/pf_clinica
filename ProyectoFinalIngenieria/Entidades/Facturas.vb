﻿Public Class Facturas

    Dim ID_Cita As Integer
    Dim Fecha As Date
    Dim Hora As TimeSpan
    Dim Descuento As Decimal

    Public Sub New(iD_Cita As Integer, fecha As Date, hora As TimeSpan, descuento As Decimal)
        Me.ID_Cita1 = iD_Cita
        Me.Fecha1 = fecha
        Me.Hora1 = hora
        Me.Descuento1 = descuento
    End Sub

    Public Property ID_Cita1 As Integer
        Get
            Return ID_Cita
        End Get
        Set(value As Integer)
            ID_Cita = value
        End Set
    End Property

    Public Property Fecha1 As Date
        Get
            Return Fecha
        End Get
        Set(value As Date)
            Fecha = value
        End Set
    End Property

    Public Property Hora1 As TimeSpan
        Get
            Return Hora
        End Get
        Set(value As TimeSpan)
            Hora = value
        End Set
    End Property

    Public Property Descuento1 As Decimal
        Get
            Return Descuento
        End Get
        Set(value As Decimal)
            Descuento = value
        End Set
    End Property
End Class

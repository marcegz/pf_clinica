﻿Imports ProyectoFinalIngenieria

Public Class Historial_Tratamientos_Consultas

    Private ID_TratamientosConsultas As Integer
    Private ID_Tratamiento As Integer
    Private ID_Consulta As Integer
    Private Sintomas As String
    Private Precio As Decimal
    Private lista As New List(Of Tratamientos)

    Public Sub New(iD_TratamientosConsultas As Integer, iD_Tratamiento As Integer, iD_Consulta As Integer, sintomas As String, precio As Decimal)
        Me.ID_TratamientosConsultas1 = iD_TratamientosConsultas
        Me.ID_Tratamiento1 = iD_Tratamiento
        Me.ID_Consulta1 = iD_Consulta
        Me.Sintomas1 = sintomas
        Me.Precio1 = precio
    End Sub

    Public Sub New(sintomas As String, lista As List(Of Tratamientos))
        Me.ID_Tratamiento = ID_Tratamiento
        Me.ID_Consulta = ID_Consulta
        Me.Sintomas = sintomas
        Me.Precio = Precio
        Me.lista = lista
    End Sub

    Public Sub New(iD_Tratamiento As Integer, iD_Consulta As Integer, sintomas As String, precio As Decimal)
        Me.ID_Tratamiento = iD_Tratamiento
        Me.ID_Consulta = iD_Consulta
        Me.Sintomas = sintomas
        Me.Precio = precio
    End Sub



    ' ------------ SET & GET

    Public Property ID_TratamientosConsultas1 As Integer
        Get
            Return ID_TratamientosConsultas
        End Get
        Set(value As Integer)
            ID_TratamientosConsultas = value
        End Set
    End Property

    Public Property ID_Tratamiento1 As Integer
        Get
            Return ID_Tratamiento
        End Get
        Set(value As Integer)
            ID_Tratamiento = value
        End Set
    End Property

    Public Property Lista_Tratamientos As List(Of Tratamientos)
        Get
            Return lista
        End Get
        Set(value As List(Of Tratamientos))
            lista = value
        End Set
    End Property

    Public Property ID_Consulta1 As Integer
        Get
            Return ID_Consulta
        End Get
        Set(value As Integer)
            ID_Consulta = value
        End Set
    End Property

    Public Property Sintomas1 As String
        Get
            Return Sintomas
        End Get
        Set(value As String)
            Sintomas = value
        End Set
    End Property

    Public Property Precio1 As Decimal
        Get
            Return Precio
        End Get
        Set(value As Decimal)
            Precio = value
        End Set
    End Property



End Class

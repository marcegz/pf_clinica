﻿Public Class Pacientes

    Private ID_Paciente As Integer
    Private NombreCompleto As String
    Private Cedula As Integer
    Private Direccion As String
    Private Correo As String
    Private Ocupacion As String
    Private Genero As Boolean
    Private FechaNacimiento As Date
    Private Telefono1 As String
    Private Telefono2 As String
    Private ID_Responsable As Integer
    Private Estado As Boolean

    Public Sub New()
    End Sub

    'Constructor para Consultas
    Public Sub New(iD_Paciente As Integer, nombreCompleto As String,
                   cedula As Integer, direccion As String, correo As String,
                   ocupacion As String, genero As Boolean, fechaNacimiento As Date,
                   telefono1 As String, telefono2 As String, iD_Responsable As Integer, estado As Boolean)
        Me.ID_Paciente = iD_Paciente
        Me.NombreCompleto = nombreCompleto
        Me.Cedula = cedula
        Me.Direccion = direccion
        Me.Correo = correo
        Me.Ocupacion = ocupacion
        Me.Genero = genero
        Me.FechaNacimiento = fechaNacimiento
        Me.Telefono1 = telefono1
        Me.Telefono2 = telefono2
        Me.ID_Responsable = iD_Responsable
        Me.Estado = estado
    End Sub

    'Constructor para actualizar paciente mayor
    Public Sub New(iD_Paciente As Integer, nombreCompleto As String,
                   cedula As Integer, direccion As String, correo As String,
                   ocupacion As String, genero As Boolean, fechaNacimiento As Date,
                   telefono1 As String, telefono2 As String, estado As Boolean)
        Me.ID_Paciente = iD_Paciente
        Me.NombreCompleto = nombreCompleto
        Me.Cedula = cedula
        Me.Direccion = direccion
        Me.Correo = correo
        Me.Ocupacion = ocupacion
        Me.Genero = genero
        Me.FechaNacimiento = fechaNacimiento
        Me.Telefono1 = telefono1
        Me.Telefono2 = telefono2
        Me.Estado = estado
    End Sub

    'Constructor para insertar paciente menor de edad con encargados
    Public Sub New(nombreCompleto As String, cedula As Integer, direccion As String,
                   correo As String, ocupacion As String, genero As Boolean,
                   fechaNacimiento As Date, telefono1 As String, telefono2 As String,
                   iD_Responsable As Integer, estado As Boolean)
        Me.NombreCompleto = nombreCompleto
        Me.Cedula = cedula
        Me.Direccion = direccion
        Me.Correo = correo
        Me.Ocupacion = ocupacion
        Me.Genero = genero
        Me.FechaNacimiento = fechaNacimiento
        Me.Telefono1 = telefono1
        Me.Telefono2 = telefono2
        Me.ID_Responsable = iD_Responsable
        Me.Estado = estado
        Me.ID_Paciente = 0

    End Sub

    'Insertar paciente mayor de edad
    Public Sub New(nombreCompleto As String, cedula As Integer, direccion As String,
                   correo As String, ocupacion As String, genero As Boolean,
                   fechaNacimiento As Date, telefono1 As String, telefono2 As String, estado As Boolean)

        Me.NombreCompleto = nombreCompleto
        Me.Cedula = cedula
        Me.Direccion = direccion
        Me.Correo = correo
        Me.Ocupacion = ocupacion
        Me.Genero = genero
        Me.FechaNacimiento = fechaNacimiento
        Me.Telefono1 = telefono1
        Me.Telefono2 = telefono2
        Me.Estado = estado
        Me.ID_Paciente = 0
        Me.ID_Responsable = 0
    End Sub

    ReadOnly Property ID_Paciente_() As Integer
        Get
            Return ID_Paciente
        End Get
    End Property

    Public Property NombreCompleto_() As String
        Get
            Return NombreCompleto
        End Get
        Set(ByVal Nombre As String)
            NombreCompleto = Nombre
        End Set
    End Property

    Public Property Cedula_() As Integer
        Get
            Return Cedula
        End Get
        Set(ByVal ced As Integer)
            Cedula = ced
        End Set
    End Property

    Public Property Direccion_() As String
        Get
            Return Direccion
        End Get
        Set(ByVal direcc As String)
            Direccion = direcc
        End Set
    End Property

    Public Property Correo_() As String
        Get
            Return Correo
        End Get
        Set(ByVal corr As String)
            Correo = corr
        End Set
    End Property

    Public Property Ocupacion_() As String
        Get
            Return Ocupacion
        End Get
        Set(ByVal ocu As String)
            Ocupacion = ocu
        End Set
    End Property

    Public Property Genero_() As Boolean
        Get
            Return Genero
        End Get
        Set(ByVal gene As Boolean)
            Genero = gene
        End Set
    End Property

    Public Property FechaNacimiento_() As Date
        Get
            Return FechaNacimiento
        End Get
        Set(ByVal fn As Date)
            FechaNacimiento = fn
        End Set
    End Property

    Public Property Telefono1_() As String
        Get
            Return Telefono1
        End Get
        Set(ByVal tel1 As String)
            Telefono1 = tel1
        End Set
    End Property

    Public Property Telefono2_() As String
        Get
            Return Telefono2
        End Get
        Set(ByVal tel2 As String)
            Telefono2 = tel2
        End Set
    End Property

    Public Property ID_Responsable_() As Integer
        Get
            Return ID_Responsable
        End Get
        Set(ByVal idre As Integer)
            ID_Responsable = idre
        End Set
    End Property

    Public Property Estado_() As Boolean
        Get
            Return Estado
        End Get
        Set(ByVal est As Boolean)
            Estado = est
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return MyBase.ToString()
    End Function
End Class

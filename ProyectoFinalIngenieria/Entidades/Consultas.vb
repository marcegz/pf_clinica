﻿Public Class Consultas

    Private ID_Consulta As Integer
    Private Sintomas As String
    Private ID_Analisis As Integer
    Private ID_Cita As Integer
    Private Estado As String

    Private Cedula_Funcionario As Integer
    Private NombreFuncionario As String

    Private ID_Hora As Integer
    Private Hora As String

    Public Sub New()
    End Sub

    'Consulta Medicina General
    Public Sub New(iD_Consulta As Integer, sintomas As String, iD_Analisis As Integer, iD_Cita As Integer, Estado As String)
        Me.ID_Consulta = iD_Consulta
        Me.Sintomas = sintomas
        Me.ID_Analisis = iD_Analisis
        Me.ID_Cita = iD_Cita
        Me.Estado = Estado
    End Sub

    'Consulta Odontologia
    Public Sub New(iD_Consulta As Integer, sintomas As String, iD_Cita As Integer, Estado As String)
        Me.ID_Consulta = iD_Consulta
        Me.Sintomas = sintomas
        Me.ID_Cita = iD_Cita
        Me.Estado = Estado
    End Sub

    'Insertar Consulta Medicina General
    Public Sub New(iD_Analisis As Integer, iD_Cita As Integer)

        Me.ID_Analisis = iD_Analisis
        Me.ID_Cita = iD_Cita
    End Sub

    'Insertar Consulta Odontologia
    Public Sub New(sintomas As String, iD_Cita As Integer, Estado As String)
        Me.Sintomas = sintomas
        Me.ID_Cita = iD_Cita
        Me.Estado = Estado
    End Sub

    Public Property ID_Analisis_() As Integer
        Get
            Return ID_Analisis
        End Get
        Set(ByVal an As Integer)
            ID_Analisis = an
        End Set
    End Property

    Public Property ID_Consulta_() As Integer
        Get
            Return ID_Consulta
        End Get
        Set(ByVal con As Integer)
            ID_Consulta = con
        End Set
    End Property

    Public Property ID_Cita_() As Integer
        Get
            Return ID_Cita
        End Get
        Set(ByVal con As Integer)
            ID_Cita = con
        End Set
    End Property

    Public Property Sintomas_() As String
        Get
            Return Sintomas
        End Get
        Set(ByVal sin As String)
            Sintomas = sin
        End Set
    End Property

    Public Property Estado_() As String
        Get
            Return Estado
        End Get
        Set(ByVal est As String)
            Estado = est
        End Set
    End Property

    Public Property ID_Funcionario_() As Integer
        Get
            Return ID_Funcionario
        End Get
        Set(ByVal con As Integer)
            ID_Funcionario = con
        End Set
    End Property

    Public Property NombreFuncionario_() As String
        Get
            Return NombreFuncionario
        End Get
        Set(ByVal sin As String)
            NombreFuncionario = sin
        End Set
    End Property

End Class

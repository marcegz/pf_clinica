﻿Public Class Dientes
    Dim ID_Diente As Integer
    Dim Nombre, X, Y As String
    Dim Numero As Integer

    Public Sub New()
    End Sub

    Public Sub New(iD_Diente As Integer, nombre As String, x As String, y As String, numero As Integer)
        Me.ID_Diente = iD_Diente
        Me.Nombre = nombre
        Me.X = x
        Me.Y = y
        Me.Numero = numero
    End Sub

    Public Sub New(nombre As String, x As String, y As String, numero As Integer)
        Me.Nombre = nombre
        Me.X = x
        Me.Y = y
        Me.Numero = numero
    End Sub

    ReadOnly Property ID_Diente_() As Integer
        Get
            Return ID_Diente
        End Get
    End Property

    Public Property Nombre_() As String
        Get
            Return Nombre
        End Get
        Set(ByVal Nom As String)
            Nombre = Nom
        End Set
    End Property

    Public Property X_() As String
        Get
            Return X
        End Get
        Set(ByVal XX As String)
            X = XX
        End Set
    End Property

    Public Property Y_() As String
        Get
            Return Y
        End Get
        Set(ByVal YY As String)
            Y = YY
        End Set
    End Property

    Public Property Numero_() As Integer
        Get
            Return Numero
        End Get
        Set(ByVal Num As Integer)
            Numero = Num
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return MyBase.ToString()
    End Function
End Class

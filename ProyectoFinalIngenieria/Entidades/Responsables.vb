﻿Imports ProyectoFinalIngenieria

Public Class Responsables

    Private ID_Responsable As Integer
    Private NombreCompleto As String
    Private Cedula As Integer
    Private Direccion As String
    Private Correo As String
    Private Telefono1 As String
    Private Telefono2 As String
    Private Parentesco As String
    Private Genero As Boolean
    Private FechaNacimiento As Date
    Private Estado As Boolean

    Public Sub New()
    End Sub

    Public Sub New(nombreCompleto As String,
                   cedula As Integer, direccion As String, correo As String,
                   telefono1 As String, telefono2 As String, parentesco As String,
                   genero As Boolean, fechaNacimiento As Date, estado As Boolean)
        Me.ID_Responsable = 0
        Me.NombreCompleto = nombreCompleto
        Me.Cedula = cedula
        Me.Direccion = direccion
        Me.Correo = correo
        Me.Telefono1 = telefono1
        Me.Telefono2 = telefono2
        Me.Parentesco = parentesco
        Me.Genero = genero
        Me.FechaNacimiento = fechaNacimiento
        Me.Estado = estado
    End Sub

    Public Sub New(iD_Responsable As Integer, nombreCompleto As String,
                   cedula As Integer, direccion As String, correo As String,
                   telefono1 As String, telefono2 As String, parentesco As String,
                   genero As Boolean, fechaNacimiento As Date, estado As Boolean)
        Me.ID_Responsable = iD_Responsable
        Me.NombreCompleto = nombreCompleto
        Me.Cedula = cedula
        Me.Direccion = direccion
        Me.Correo = correo
        Me.Telefono1 = telefono1
        Me.Telefono2 = telefono2
        Me.Parentesco = parentesco
        Me.Genero = genero
        Me.FechaNacimiento = fechaNacimiento
        Me.Estado = estado
    End Sub

    ReadOnly Property ID_Responsable_() As Integer
        Get
            ID_Responsable_ = ID_Responsable
        End Get
    End Property

    Public Property NombreCompleto_() As String
        Get
            NombreCompleto_ = NombreCompleto
        End Get
        Set(ByVal Nombre As String)
            NombreCompleto = Nombre
        End Set
    End Property

    Public Property Cedula_() As Integer
        Get
            Cedula_ = Cedula
        End Get
        Set(ByVal ced As Integer)
            Cedula = ced
        End Set
    End Property

    Public Property Direccion_() As String
        Get
            Direccion_ = Direccion
        End Get
        Set(ByVal direcc As String)
            Direccion = direcc
        End Set
    End Property

    Public Property Correo_() As String
        Get
            Correo_ = Correo
        End Get
        Set(ByVal corr As String)
            Correo = corr
        End Set
    End Property

    Public Property Telefono1_() As String
        Get
            Telefono1_ = Telefono1
        End Get
        Set(ByVal tel1 As String)
            Telefono1 = tel1
        End Set
    End Property

    Public Property Telefono2_() As String
        Get
            Telefono2_ = Telefono2
        End Get
        Set(ByVal tel2 As String)
            Telefono2 = tel2
        End Set
    End Property

    Public Property Parentesco_() As String
        Get
            Parentesco_ = Parentesco
        End Get
        Set(ByVal paren As String)
            Parentesco = paren
        End Set
    End Property

    Public Property Genero_() As Boolean
        Get
            Genero_ = Genero
        End Get
        Set(ByVal gene As Boolean)
            Genero = gene
        End Set
    End Property

    Public Property FechaNacimiento_() As Date
        Get
            FechaNacimiento_ = FechaNacimiento
        End Get
        Set(ByVal fn As Date)
            FechaNacimiento = fn
        End Set
    End Property

    Public Property Estado_() As Boolean
        Get
            Estado_ = Estado
        End Get
        Set(ByVal est As Boolean)
            Estado = est
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return MyBase.ToString()
    End Function
End Class

﻿Public Class Analisis

    Private ID_Analisis, Tamaño, Peso As Integer
    Private Presion_Arterial As Integer

    Public Sub New()
    End Sub

    Public Sub New(iD_Analisis As Integer, tamaño As Integer, peso As Integer, presion_Arterial As Integer)
        Me.ID_Analisis = iD_Analisis
        Me.Tamaño = tamaño
        Me.Peso = peso
        Me.Presion_Arterial = presion_Arterial
    End Sub

    Public Sub New(tamaño As Integer, peso As Integer, presion_Arterial As Integer)
        Me.Tamaño = tamaño
        Me.Peso = peso
        Me.Presion_Arterial = presion_Arterial
    End Sub

    ReadOnly Property ID_Analisis_() As Integer
        Get
            Return ID_Analisis
        End Get
    End Property

    Public Property Presion_Arterial_() As Integer
        Get
            Return Presion_Arterial
        End Get
        Set(ByVal pr_ar As Integer)
            Presion_Arterial = pr_ar
        End Set
    End Property

    Public Property Tamaño_() As Integer
        Get
            Return Tamaño
        End Get
        Set(ByVal tam As Integer)
            Tamaño = tam
        End Set
    End Property

    Public Property Peso_() As Integer
        Get
            Return Peso
        End Get
        Set(ByVal pes As Integer)
            Peso = pes
        End Set
    End Property
End Class

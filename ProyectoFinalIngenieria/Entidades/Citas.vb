﻿Public Class Citas

    Dim ID_Cita, ID_Hora, ID_Paciente, ID_Funcionario, Cedula As Integer
    Dim Fecha As Date
    Dim Especialidad, Nombre, Telefono As String
    Dim Descripcion As String
    Dim Estado As String

    Public Sub New()
    End Sub

    Public Sub New(iD_Cita As Integer, iD_Hora As Integer, iD_Paciente As Integer, iD_Funcionario As Integer,
                   fecha As Date, especialidad As String, descripcion As String, estado As String)
        Me.ID_Cita = iD_Cita
        Me.ID_Hora = iD_Hora
        Me.ID_Paciente = iD_Paciente
        Me.ID_Funcionario = iD_Funcionario
        Me.Fecha = fecha
        Me.Especialidad = especialidad
        Me.Descripcion = descripcion
        Me.Estado = estado
    End Sub

    Public Sub New(iD_Hora As Integer, iD_Paciente As Integer, iD_Funcionario As Integer, fecha As Date,
                   especialidad As String, descripcion As String, estado As String)
        Me.ID_Hora = iD_Hora
        Me.ID_Paciente = iD_Paciente
        Me.ID_Funcionario = iD_Funcionario
        Me.Fecha = fecha
        Me.Especialidad = especialidad
        Me.Descripcion = descripcion
        Me.Estado = estado
    End Sub

    Public Sub New(iD_Cita As Integer, iD_Hora As Integer, iD_Paciente As Integer, iD_Funcionario As Integer,
                   cedula As Integer, fecha As Date, especialidad As String, nombre As String, telefono As String, descripcion As String,
                   estado As String)
        Me.ID_Cita = iD_Cita
        Me.ID_Hora = iD_Hora
        Me.ID_Paciente = iD_Paciente
        Me.ID_Funcionario = iD_Funcionario
        Me.Cedula = cedula
        Me.Fecha = fecha
        Me.Especialidad = especialidad
        Me.Nombre = nombre
        Me.Descripcion = descripcion
        Me.Estado = estado
        Me.Telefono = telefono
    End Sub

    Public Sub New(iD_Hora As Integer, iD_Paciente As Integer, iD_Funcionario As Integer, cedula As Integer,
                   fecha As Date, especialidad As String, nombre As String, telefono As String, descripcion As String, estado As String)
        Me.ID_Hora = iD_Hora
        Me.ID_Paciente = iD_Paciente
        Me.ID_Funcionario = iD_Funcionario
        Me.Cedula = cedula
        Me.Fecha = fecha
        Me.Especialidad = especialidad
        Me.Nombre = nombre
        Me.Descripcion = descripcion
        Me.Estado = estado
        Me.Telefono = telefono
    End Sub

    ReadOnly Property ID_Cita_() As Integer
        Get
            Return ID_Cita
        End Get
    End Property

    ReadOnly Property ID_Hora_() As Integer
        Get
            Return ID_Hora
        End Get
    End Property

    ReadOnly Property ID_Paciente_() As Integer
        Get
            Return ID_Paciente
        End Get
    End Property

    ReadOnly Property ID_Funcionario_() As Integer
        Get
            Return ID_Funcionario
        End Get
    End Property

    Public Property Fecha_() As Date
        Get
            Return Fecha
        End Get
        Set(ByVal fn As Date)
            Fecha = fn
        End Set
    End Property

    Public Property Especialidad_() As String
        Get
            Return Especialidad
        End Get
        Set(ByVal esp As String)
            Especialidad = esp
        End Set
    End Property

    Public Property Descripcion_() As String
        Get
            Return Descripcion
        End Get
        Set(ByVal desc As String)
            Descripcion = desc
        End Set
    End Property

    Public Property Estado_() As String
        Get
            Return Estado
        End Get
        Set(ByVal est As String)
            Estado = est
        End Set
    End Property
End Class

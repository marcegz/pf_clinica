﻿Public Class Tratamientos

    Private ID_Tratamiento As Integer
    Private Nombre, Especialidad, Descripcion, Color As String
    Private Precio As Decimal
    Private Estado As Boolean

    Public Sub New()
    End Sub

    Public Sub New(iD_Tratamiento As Integer, nombre As String, especialidad As String,
                   descripcion As String, color As String, precio As Decimal, estado As Boolean)
        Me.ID_Tratamiento = iD_Tratamiento
        Me.Nombre = nombre
        Me.Especialidad = especialidad
        Me.Descripcion = descripcion
        Me.Color = color
        Me.Precio = precio
        Me.Estado = estado
    End Sub

    Public Sub New(nombre As String, especialidad As String, descripcion As String,
                   color As String, precio As Decimal, estado As Boolean)
        Me.Nombre = nombre
        Me.Especialidad = especialidad
        Me.Descripcion = descripcion
        Me.Color = color
        Me.Precio = precio
        Me.Estado = estado
    End Sub

    ReadOnly Property ID_Tratamiento_() As Integer
        Get
            Return ID_Tratamiento
        End Get
    End Property

    Public Property Nombre_() As String
        Get
            Return Nombre
        End Get
        Set(ByVal Nomb As String)
            Nombre = Nomb
        End Set
    End Property

    Public Property Especialidad_() As String
        Get
            Return Especialidad
        End Get
        Set(ByVal esp As String)
            Especialidad = esp
        End Set
    End Property

    Public Property Descripcion_() As String
        Get
            Return Descripcion
        End Get
        Set(ByVal des As String)
            Descripcion = des
        End Set
    End Property

    Public Property Color_() As String
        Get
            Return Color
        End Get
        Set(ByVal col As String)
            Color = col
        End Set
    End Property

    Public Property Precio_() As Decimal
        Get
            Return Precio
        End Get
        Set(ByVal pre As Decimal)
            Precio = pre
        End Set
    End Property

    Public Property Estado_() As Boolean
        Get
            Return Estado
        End Get
        Set(ByVal est As Boolean)
            Estado = est
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return MyBase.ToString()
    End Function
End Class

﻿Public Class Funcionario

    Private ID_Funcionario As Integer
    Private NombreCompleto As String
    Private Cedula As Integer
    Private Direccion As String
    Private Correo As String
    Private Profesion As String
    Private Genero As Boolean
    Private FechaNacimiento As Date
    Private Contraseña As String
    Private Telefono1 As String
    Private Telefono2 As String
    Private Estado As Boolean

    Public Sub New()
    End Sub

    Public Sub New(iD_Funcionario As Integer, nombreCompleto As String, cedula As Integer,
                   direccion As String, correo As String, profesion As String,
                   genero As Boolean, fechaNacimiento As Date, contraseña As String,
                   telefono1 As String, telefono2 As String, estado As Boolean)

        Me.ID_Funcionario = iD_Funcionario
        Me.NombreCompleto = nombreCompleto
        Me.Cedula = cedula
        Me.Direccion = direccion
        Me.Correo = correo
        Me.Profesion = profesion
        Me.Genero = genero
        Me.FechaNacimiento = fechaNacimiento
        Me.Contraseña = contraseña
        Me.Telefono1 = telefono1
        Me.Telefono2 = telefono2
        Me.Estado = estado
    End Sub

    Public Sub New(nombreCompleto As String, cedula As Integer, direccion As String,
                   correo As String, profesion As String, genero As Boolean,
                   fechaNacimiento As Date, contraseña As String, telefono1 As String,
                   telefono2 As String, estado As Boolean)

        Me.NombreCompleto = nombreCompleto
        Me.Cedula = cedula
        Me.Direccion = direccion
        Me.Correo = correo
        Me.Profesion = profesion
        Me.Genero = genero
        Me.FechaNacimiento = fechaNacimiento
        Me.Contraseña = contraseña
        Me.Telefono1 = telefono1
        Me.Telefono2 = telefono2
        Me.Estado = estado
        Me.ID_Funcionario = 0
    End Sub

    ReadOnly Property ID_Funcionario_() As Integer
        Get
            Return ID_Funcionario
        End Get
    End Property

    Public Property NombreCompleto_() As String
        Get
            Return NombreCompleto
        End Get
        Set(ByVal Nombre As String)
            NombreCompleto = Nombre
        End Set
    End Property

    Public Property Cedula_() As Integer
        Get
            Return Cedula
        End Get
        Set(ByVal ced As Integer)
            Cedula = ced
        End Set
    End Property

    Public Property Direccion_() As String
        Get
            Return Direccion
        End Get
        Set(ByVal direcc As String)
            Direccion = direcc
        End Set
    End Property

    Public Property Correo_() As String
        Get
            Return Correo
        End Get
        Set(ByVal corr As String)
            Correo = corr
        End Set
    End Property

    Public Property Profesion_() As String
        Get
            Return Profesion
        End Get
        Set(ByVal pro As String)
            Profesion = pro
        End Set
    End Property

    Public Property Contraseña_() As String
        Get
            Return Contraseña
        End Get
        Set(ByVal con As String)
            Contraseña = con
        End Set
    End Property

    Public Property Genero_() As Boolean
        Get
            Return Genero
        End Get
        Set(ByVal gene As Boolean)
            Genero = gene
        End Set
    End Property

    Public Property FechaNacimiento_() As Date
        Get
            Return FechaNacimiento
        End Get
        Set(ByVal fn As Date)
            FechaNacimiento = fn
        End Set
    End Property

    Public Property Telefono1_() As String
        Get
            Return Telefono1
        End Get
        Set(ByVal tel1 As String)
            Telefono1 = tel1
        End Set
    End Property

    Public Property Telefono2_() As String
        Get
            Return Telefono2
        End Get
        Set(ByVal tel2 As String)
            Telefono2 = tel2
        End Set
    End Property

    Public Property Estado_() As Boolean
        Get
            Return Estado
        End Get
        Set(ByVal est As Boolean)
            Estado = est
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return MyBase.ToString()
    End Function
End Class

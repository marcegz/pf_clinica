﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_Analisis

Public Class Ln_Analisis
    Dim db_analisis As New DB_Analisis

    Sub insertarAnalisis(analisis_ As Analisis)
        db_analisis.insertarAnalisis(analisis_)
    End Sub

    Function consultarAnalisis(id As Integer) As Analisis
        consultarAnalisis = db_analisis.consultarAnalisis(id)
    End Function
End Class

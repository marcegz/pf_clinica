﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_Historial_Dientes_Consultas

Public Class Ln_Historial_Dientes_Consultas

    Dim db_historial_dientes_consultas As New DB_Historial_Dientes_Consultas


    Sub insertarHistorialDiente(hist_Diente_Consulta As Historial_Dientes_Consultas)
        db_historial_dientes_consultas.insertarHistorialDiente(hist_Diente_Consulta)
    End Sub

    Function cargarTablaTratamientosDiente(ID_diente As Integer, ID_paciente As Integer) As DataTable
        cargarTablaTratamientosDiente = db_historial_dientes_consultas.cargarTablaTratamientosDiente(ID_diente, ID_paciente)
    End Function

    Sub eliminarTratamientoConsultaOdontologia(id_historial As Integer)
        db_historial_dientes_consultas.eliminarTratamientoConsultaOdontologia(id_historial)
    End Sub


End Class

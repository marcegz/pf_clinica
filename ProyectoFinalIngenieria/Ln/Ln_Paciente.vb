﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_Paciente
Imports ProyectoFinalIngenieria.Pacientes

Public Class Ln_Paciente
    Dim db_Paciente As New DB_Paciente

    Sub insertarPaciente(paciente_ As Pacientes)
        db_Paciente.insertarPaciente(paciente_)
    End Sub

    Sub insertarPacienteMayor(paciente_ As Pacientes)
        db_Paciente.insertarPacienteMayor(paciente_)
    End Sub

    Function cargarTabla1paciente(ByVal dato As String)
        cargarTabla1paciente = db_Paciente.cargarTabla1paciente(dato)
    End Function

    Function cargartablaActivos()
        cargartablaActivos = db_Paciente.cargartablaActivos()
    End Function

    Function cargartablaInactivos()
        cargartablaInactivos = db_Paciente.cargartablaInactivos()
    End Function

    Sub actualizarPaciente(paciente_ As Pacientes)
        db_Paciente.actualizarPaciente(paciente_)
    End Sub

    Sub actualizarPacienteMayor(paciente_ As Pacientes)
        db_Paciente.actualizarPacienteMayor(paciente_)
    End Sub

    Sub bloquearPaciente(ByVal id As Integer)
        db_Paciente.bloquearPaciente(id)
    End Sub

    Sub RestablecerPaciente(ByVal id As Integer)
        db_Paciente.RestablecerPaciente(id)
    End Sub

    Function Consultar1Paciente(id As String) As Pacientes
        Consultar1Paciente = db_Paciente.Consultar1Paciente(id)
    End Function

    Function ConsultarPaciente(id As Integer) As String
        ConsultarPaciente = db_Paciente.ConsultarPaciente(id)
    End Function

    Function ConsultarPacienteCOMPLETO(id As Integer) As Pacientes
        ConsultarPacienteCOMPLETO = db_Paciente.ConsultarPacienteCOMPLETO(id)
    End Function

    Function ExtraerIDPaciente(ced As Integer) As Integer
        ExtraerIDPaciente = db_Paciente.ExtraerIDPaciente(ced)
    End Function

    Function PacientesReporte() As DataTable
        Return db_Paciente.PacientesReporte()
    End Function

    Function ConsultarPacientesReporte(Especialidad As String) As DataTable
        Return db_Paciente.ConsultarPacientesReporte(Especialidad)
    End Function

End Class

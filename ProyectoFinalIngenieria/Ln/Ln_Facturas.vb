﻿Imports System.Data.SqlClient

Public Class Ln_Facturas

    Dim db_Facturas As New DB_Facturas


    Sub insertarFactura(factura As Facturas)
        db_Facturas.insertarFactura(factura)
    End Sub

    Function getIdFactura() As Integer
        Return db_Facturas.getIdFactura()
    End Function

    Function verificarFacturaExiste(ID_Cita As Integer) As String
        Return db_Facturas.verificarFacturaExiste(ID_Cita)
    End Function

    Function getIdFacturaReporte(id_cita As Integer) As Integer
        Return db_Facturas.getIdFacturaReporte(id_cita)
    End Function

    Function getDescuentoFactura(id_cita As Integer) As Decimal
        Return db_Facturas.getDescuentoFactura(id_cita)
    End Function

End Class

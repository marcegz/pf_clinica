﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_Citas

Public Class Ln_Citas
    Dim db_citas As New DB_Citas

    Sub insertarCita(cita_ As Citas)
        db_citas.insertarCita(cita_)
    End Sub

    Function ConsultaHorasUtilizadas(fecha As Date, Especialidad As String)
        ConsultaHorasUtilizadas = db_citas.ConsultaHorasUtilizadas(fecha, Especialidad)
    End Function

    Sub EliminarCita(id As Integer)
        db_citas.EliminarCita(id)
    End Sub

    Sub actualizarCita(cita_ As Citas)
        db_citas.actualizarCita(cita_)
    End Sub

    Function ExtraerCita(id As String) As Citas
        ExtraerCita = db_citas.ExtraerCita(id)
    End Function

    Function ConsultaCitasDia(Usuario As String, ID As Integer, Fecha As Date, Especialidad As String) As DataTable
        Return db_citas.ConsultaCitasDia(Usuario, ID, Fecha, Especialidad)
    End Function

    Function ConsultaCitasAño(Usuario As String, ID As Integer, Fecha As Date, Especialidad As String) As DataTable
        Return db_citas.ConsultaCitasAño(Usuario, ID, Fecha, Especialidad)
    End Function

    Function ConsultaCitasMes(Usuario As String, ID As Integer, Fecha As Date, Especialidad As String) As DataTable
        Return db_citas.ConsultaCitasMes(Usuario, ID, Fecha, Especialidad)
    End Function

    Function ConsultaCitasRangoFechas(Usuario As String, ID As Integer, FechaUno As Date, FechaDos As Date, Especialidad As String) As DataTable
        Return db_citas.ConsultaCitasRangoFechas(Usuario, ID, FechaUno, FechaDos, Especialidad)
    End Function


    Function extraerPacientesDia(fechaHoy As Date, especialidad As String) As DataTable
        Return db_citas.extraerPacientesDia(fechaHoy, especialidad)
    End Function

    Sub EliminarConsulta(id As Integer)
        db_citas.EliminarConsulta(id)
    End Sub

    Sub actualizarEstadoCita(ID_Cita As Integer, Estado As String)
        db_citas.actualizarEstadoCita(ID_Cita, Estado)
    End Sub


End Class

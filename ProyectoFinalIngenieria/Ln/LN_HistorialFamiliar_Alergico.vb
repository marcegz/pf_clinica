﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_HistorialFamiliar_Alergico

Public Class LN_HistorialFamiliar_Alergico

    Dim db_hstorial As New DB_HistorialFamiliar_Alergico

    Sub insertarHistorialP(historial_ As HistorialFamiliar_Alergico)
        db_hstorial.insertarHistorialP(historial_)
    End Sub

    Sub actualizarHistorialP(historial_ As HistorialFamiliar_Alergico)
        db_hstorial.actualizarHistorialP(historial_)
    End Sub

    Function Consultar1Historial(id As Integer) As HistorialFamiliar_Alergico
        Consultar1Historial = db_hstorial.Consultar1Historial(id)
    End Function

    Function ExtraerIDPaciente(cedula As Integer) As Integer
        ExtraerIDPaciente = db_hstorial.ExtraerIDPaciente(cedula)
    End Function
End Class

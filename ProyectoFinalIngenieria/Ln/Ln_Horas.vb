﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_Horas

Public Class Ln_Horas

    Dim db_horas As New DB_Horas

    Function Consultar1Hora(id As Integer) As Horas
        Consultar1Hora = db_horas.Consultar1Hora(id)
    End Function

    Function ConsultarHoras() As DataTable
        ConsultarHoras = db_horas.ConsultarHoras()
    End Function

    Function ConsultarHorasDisponibles(fecha As Date, especialidad As String) As DataTable
        ConsultarHorasDisponibles = db_horas.ConsultarHorasDisponibles(fecha, especialidad)
    End Function

    Function llenarCombo(fecha As Date, especialidad As String) As DataTable
        llenarCombo = db_horas.llenarCombo(fecha, especialidad)
    End Function

    Function ConsultarHorasDisponiblesTodas(id As Integer) As Horas
        ConsultarHorasDisponiblesTodas = db_horas.ConsultarHorasDisponiblesTodas(id)
    End Function
End Class

﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_Historial_Tratamientos_Consultas

Public Class Ln_Historial_Tratamientos_Consultas

    Dim db_historial_tratamientos_consultas As New DB_Historial_Tratamientos_Consultas

    Sub insertarHistorialTratamientosConsultas(analisis_ As Historial_Tratamientos_Consultas)
        db_historial_tratamientos_consultas.insertarHistorialTratamientosConsultas(analisis_)
    End Sub

    Function consultarHistorialTratamientosConsultas(id As Integer) As Historial_Tratamientos_Consultas
        consultarHistorialTratamientosConsultas = db_historial_tratamientos_consultas.consultarHistorialTratamientosConsultas(id)
    End Function

    Function extraerIdConsulta(id As Integer) As Integer
        extraerIdConsulta = db_historial_tratamientos_consultas.extraerIdConsulta(id)
    End Function

    Function extraerIdAnalisis() As Integer
        extraerIdAnalisis = db_historial_tratamientos_consultas.extraerIdAnalisis()
    End Function

    Function sacarAnalisisConsulta(id As Integer) As Analisis
        sacarAnalisisConsulta = db_historial_tratamientos_consultas.sacarAnalisisConsulta(id)
    End Function

    Function ConsultarHistorialMedicinaGeneral(id As Integer) As DataTable
        Return db_historial_tratamientos_consultas.ConsultarHistorialMedicinaGeneral(id)
    End Function

End Class

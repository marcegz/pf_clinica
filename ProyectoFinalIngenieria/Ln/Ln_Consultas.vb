﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_Consultas

Public Class Ln_Consultas

    Dim db_consultas As New DB_Consultas

    Function getIdConsulta(id_cita As Integer) As Integer
        Return db_consultas.getIdConsulta(id_cita)
    End Function

    Function TratamientosActivosporEspecialidad(especialidad As String) As DataTable
        Return db_consultas.TratamientosActivosporEspecialidad(especialidad)
    End Function

    Sub insertarConsulta(consulta_ As Consultas)
        db_consultas.insertarConsulta(consulta_)
    End Sub

End Class

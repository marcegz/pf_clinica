﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_Funcionario
Imports ProyectoFinalIngenieria.Funcionario

Public Class Ln_Funcionario

    Dim db_Funcionario As New DB_Funcionario

    Sub insertarfuncionario(funcionario_ As Funcionario)
        db_Funcionario.insertarfuncionario(funcionario_)
    End Sub

    Function cargarTabla1Funcionario(ByVal dato As String)
        cargarTabla1Funcionario = db_Funcionario.cargarTabla1Funcionario(dato)
    End Function

    Function cargartablaActivos()
        cargartablaActivos = db_Funcionario.cargartablaActivos()
    End Function

    Function cargartablaInactivos()
        cargartablaInactivos = db_Funcionario.cargartablaInactivos()
    End Function

    Sub actualizarFuncionario(funcionario_ As Funcionario)
        db_Funcionario.actualizarFuncionario(funcionario_)
    End Sub

    Sub bloquearFuncionario(ByVal id As Integer)
        db_Funcionario.bloquearFuncionario(id)
    End Sub

    Sub RestablecerFuncionario(ByVal id As Integer)
        db_Funcionario.RestablecerFuncionario(id)
    End Sub

    Function ConsultarFuncionario(id As Integer) As String
        ConsultarFuncionario = db_Funcionario.ConsultarFuncionario(id)
    End Function

    Function funcionariosActivosporEspecialidad(dato As String) As DataTable
        funcionariosActivosporEspecialidad = db_Funcionario.funcionariosActivosporEspecialidad(dato)
    End Function

    Function FuncionariosReporte(Profesion As String) As DataTable
        Return db_Funcionario.FuncionariosReporte(Profesion)
    End Function

    Function ConsultarFuncionarioFactura(ID_Funcionario As Integer) As String
        Return db_Funcionario.ConsultarFuncionarioFactura(ID_Funcionario)
    End Function

End Class

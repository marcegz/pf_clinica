﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_Responsables

Public Class Ln_Responsable

    Dim db_Responsable As New DB_Responsables

    Sub insertarResponsable(responsable_ As Responsables)
        db_Responsable.insertarResponsable(responsable_)
    End Sub

    Function ExtraerIDResponsable(cedula As Integer) As Integer
        ExtraerIDResponsable = db_Responsable.ExtraerIDResponsable(cedula)
    End Function

    Function Consultar1Responsable(id As Integer) As Responsables
        Consultar1Responsable = db_Responsable.Consultar1Responsable(id)
    End Function

    Function Consultar1ResponsableActivo(id As Integer) As DataTable
        Consultar1ResponsableActivo = db_Responsable.cargarTabla1ResponsableActivo(id)
    End Function

    Function ConsultarResponsables() As DataTable
        ConsultarResponsables = db_Responsable.ConsultarResponsables()
    End Function

    Sub actualizarResponsable(responsables_ As Responsables)
        db_Responsable.actualizarResponsable(responsables_)
    End Sub

    Sub reemplazarResponsable(ByVal id_paci As Integer, ByVal id_resp As Integer)
        db_Responsable.reemplazarResponsable(id_paci, id_resp)
    End Sub
End Class

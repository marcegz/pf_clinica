﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_Dientes

Public Class Ln_Dientes

    Dim db_Diente As New DB_Dientes

    Function Consultar1Diente(id As Integer) As Dientes
        Return db_Diente.Consultar1Diente(id)
    End Function

    Function ConsultarDientes() As DataTable
        ConsultarDientes = db_Diente.ConsultarDientes()
    End Function


    'Function ExtraerTratamientosDientes(ByVal diente As Integer) As DataTable
    '    ExtraerTratamientosDientes = db_Diente.ExtraerTratamientosDientes(diente)
    'End Function


End Class

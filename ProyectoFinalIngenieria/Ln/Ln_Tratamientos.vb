﻿Imports System.Data.SqlClient
Imports ProyectoFinalIngenieria.DB_Tratamientos
Imports ProyectoFinalIngenieria.Tratamientos


Public Class Ln_Tratamientos
    Dim db_Tratamientos As New DB_Tratamientos

    Sub insertarTratamiento(tratamiento As Tratamientos)
        db_Tratamientos.insertarTratamiento(tratamiento)
    End Sub

    Function cargartablaActivos()
        cargartablaActivos = db_Tratamientos.cargartablaActivos()
    End Function

    Function cargartablaInactivos()
        cargartablaInactivos = db_Tratamientos.cargartablaInactivos()
    End Function

    Function cargarTabla1TratamientoActivo(ByVal dato As String)
        cargarTabla1TratamientoActivo = db_Tratamientos.cargarTabla1TratamientoActivo(dato)
    End Function

    Function cargarTabla1TratamientoInactivo(ByVal dato As String)
        cargarTabla1TratamientoInactivo = db_Tratamientos.cargarTabla1TratamientoInactivo(dato)
    End Function

    Sub bloquearTratamiento(ByVal id As Integer)
        db_Tratamientos.bloquearTratamiento(id)
    End Sub

    Sub restablecerTratamiento(ByVal id As Integer)
        db_Tratamientos.restablecerTratamiento(id)
    End Sub

    Sub actualizarTratamientos(tratamiento As Tratamientos)
        db_Tratamientos.actualizarTratamientos(tratamiento)
    End Sub

    Function TratamientosActivosporEspecialidad(Especialidad As String) As DataTable
        Return db_Tratamientos.TratamientosActivosporEspecialidad(Especialidad)
    End Function

    Function consultarTratamientosConsultas(id As Integer) As DataTable
        Return db_Tratamientos.consultarTratamientosConsultas(id)
    End Function

    Function ConsultarTratamientoCombo(id As Integer) As Tratamientos
        Return db_Tratamientos.ConsultarTratamientoCombo(id)
    End Function

    Function consultarTratamiento(id As Integer) As Tratamientos
        Return db_Tratamientos.consultarTratamiento(id)
    End Function

    Function ConsultarTratamientosReporte(Especialidad As String) As DataTable
        Return db_Tratamientos.ConsultarTratamientosReporte(Especialidad)
    End Function

    Function ConsultarTratamientosFactura(ID_Cita As Integer) As DataTable
        Return db_Tratamientos.ConsultarTratamientosFactura(ID_Cita)
    End Function

    Function tratamientosConsultaOdontologia(ID_Cita As Integer) As DataTable
        Return db_Tratamientos.tratamientosConsultaOdontologia(ID_Cita)
    End Function

End Class

﻿Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.Responsables

Public Class DB_Responsables

    'Procedimiento que recibe un objeto usuario y permite la inserción del usuario
    Sub insertarResponsable(responsable_ As Responsables)

        Dim sqlSP As String = "insertarResponsable" 'Procedimiento Almacenado
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@NombreCompleto", responsable_.NombreCompleto_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Cedula", responsable_.Cedula_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Direccion", responsable_.Direccion_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Correo", responsable_.Correo_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Telefono1", responsable_.Telefono1_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Telefono2", responsable_.Telefono2_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Parentesco", responsable_.Parentesco_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Genero", responsable_.Genero_))
        cmdInsertar.Parameters.Add(New SqlParameter("@FechaNacimiento", responsable_.FechaNacimiento_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Estado", responsable_.Estado_))
        Try
            cmdInsertar.ExecuteNonQuery()
            MsgBox("Datos almacenados satisfactoriamente", MsgBoxStyle.Information, "Clinica Medica")
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al insertar los datos del Encargado!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    Function ExtraerIDResponsable(cedula As Integer) As Integer
        Dim codigousuario As Integer 'Variable que almacena el máximo actual
        Dim rsCodigo As New SqlCommand("Select r.ID_Responsable from Responsables r where Cedula=" & cedula & "", conn)

        Dim ds As SqlDataReader
        ds = rsCodigo.ExecuteReader
        If ds.Read Then
            codigousuario = ds.Item("ID_Responsable")
        End If
        ds.Close()
        Return codigousuario 'Retorna el valor esperado
    End Function

    Function Consultar1Responsable(id As Integer) As Responsables
        Dim responsabl As New Responsables()
        Dim cmdConsultar As New SqlCommand("Consultar1Responsable", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_Responsable", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader
        If ds.Read Then
            responsabl = New Responsables(ds("ID_Responsable"), ds("NombreCompleto"), ds("Cedula"), ds("Direccion"),
                                       ds("Correo"), ds("Telefono1"), ds("Telefono2"), ds("Parentesco"),
                                       ds("Genero"), ds("FechaNacimiento"), ds("Estado"))

        End If
        ds.Close()

        Return responsabl 'Retorna el valor esperado
    End Function

    '*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Busqueda de 1 Responsable++++++++++++++++++++*'
    Function cargarTabla1ResponsableActivo(ByVal dato As String) As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("consultar1ResponsableActivo", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@dato", dato)

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    Function ConsultarResponsables() As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("ConsultarResponsables", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    'Procedimiento que permite reemplazar responsable de un paciente
    Sub reemplazarResponsable(ByVal id_paci As Integer, ByVal id_respo As Integer)
        Dim sqlSP As String = "reemplazarResponsable" 'Procedimiento Almacenado
        Dim cmdActualizar As New SqlCommand(sqlSP, conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Responsable", id_respo))
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Paciente", id_paci))

        Try
            cmdActualizar.ExecuteNonQuery()
            MsgBox("Se cambio el responsable del paciente correctamente!!!", MsgBoxStyle.Information, "Clinica Medica")
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al remplazar el responsable!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    'Procedimiento que permite actualizar datos de un responsable
    Sub actualizarResponsable(ByVal responsable_ As Responsables)
        Dim sqlSP As String = "actualizarResponsable" 'Procedimiento Almacenado
        Dim cmdActualizar As New SqlCommand(sqlSP, conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Responsable", responsable_.ID_Responsable_))
        cmdActualizar.Parameters.Add(New SqlParameter("@NombreCompleto", responsable_.NombreCompleto_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Cedula", responsable_.Cedula_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Direccion", responsable_.Direccion_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Correo", responsable_.Correo_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Telefono1", responsable_.Telefono1_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Telefono2", responsable_.Telefono2_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Parentesco", responsable_.Parentesco_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Genero", responsable_.Genero_))
        cmdActualizar.Parameters.Add(New SqlParameter("@FechaNacimiento", responsable_.FechaNacimiento_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Estado", responsable_.Estado_))

        Try
            cmdActualizar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al actualizar el responsable!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub




End Class

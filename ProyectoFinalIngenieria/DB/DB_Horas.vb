﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.Horas

Public Class DB_Horas
    Function Consultar1Hora(id As Integer) As Horas
        Dim horas As New Horas()
        Dim cmdConsultar As New SqlCommand("Consultar1Hora", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_Hora", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader
        If ds.Read Then
            horas = New Horas(ds("ID_Hora"), ds("Estado"), ds("Hora"))
        End If
        ds.Close()

        Return horas 'Retorna el valor esperado
    End Function

    Function ConsultarHoras() As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("ConsultarHoras", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    Function ConsultarHorasDisponibles(fecha As Date, especialidad As String) As DataTable
        Dim tabla As New DataTable
        Dim cmdConsultar As New SqlCommand("extraerHorasDisponibles", conn)

        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@Fecha", fecha))
        cmdConsultar.Parameters.Add(New SqlParameter("@Especialidad", especialidad))

        Try
            Dim rsdatos As New SqlDataAdapter(cmdConsultar)

            Dim ds As New DataTable
            'Llena el data set con los datos de la tabla usuarios
            rsdatos.Fill(ds)
            tabla = ds
        Catch ex As Exception
            MsgBox("xxxxxxxxxxxxxxxxxx!!! " & ex.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try

        Return tabla
    End Function

    Function llenarCombo(fecha As Date, especialidad As String) As DataTable
        Dim tabla As New DataTable
        Dim cmdConsultar As New SqlCommand("extraerHorasDisponibles", conn)

        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@Fecha", fecha))
        cmdConsultar.Parameters.Add(New SqlParameter("@Especialidad", especialidad))

        Try
            Dim rsdatos As New SqlDataAdapter(cmdConsultar)

            Dim ds As New DataTable
            'Llena el data set con los datos de la tabla usuarios
            rsdatos.Fill(ds)
            tabla = ds


        Catch ex As Exception
            MsgBox("xxxxxxxxxxxxxxxxxx!!! " & ex.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try

        Return tabla
    End Function

    Function ConsultarHorasDisponiblesTodas(id As Integer) As Horas
        Dim tabla As New Horas()
        Dim cmdConsultar As New SqlCommand("ExtraerHora", conn)

        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_Hora", id))

        Try
            Dim rsdatos As New SqlDataAdapter(cmdConsultar)
            Dim ds As SqlDataReader
            ds = cmdConsultar.ExecuteReader

            If ds.Read Then
                tabla = New Horas(ds("ID_Hora"), ds("Estado"), ds("Hora"))
            End If
            ds.Close()
        Catch ex As Exception
            MsgBox("xxxxxxxxxxxxxxxxxx!!! " & ex.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try

        Return tabla
    End Function
End Class

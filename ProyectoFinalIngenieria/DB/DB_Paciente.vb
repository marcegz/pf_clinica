﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.Pacientes

Public Class DB_Paciente
    '*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Insertar pacientes menores y mayores++++++++++++++++++++*'

    'Procedimiento que permite insertar un paciente menor de edad
    Sub insertarPaciente(paciente_ As Pacientes)

        Dim sqlSP As String = "insertarPaciente" 'Procedimiento Almacenado
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@NombreCompleto", paciente_.NombreCompleto_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Cedula", paciente_.Cedula_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Direccion", paciente_.Direccion_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Correo", paciente_.Correo_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Ocupacion", paciente_.Ocupacion_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Genero", paciente_.Genero_))
        cmdInsertar.Parameters.Add(New SqlParameter("@FechaNacimiento", paciente_.FechaNacimiento_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Telefono1", paciente_.Telefono1_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Telefono2", paciente_.Telefono2_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Estado", paciente_.Estado_))
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Responsable", paciente_.ID_Responsable_))
        Try
            cmdInsertar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al insertar los datos del ´Paciente!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    'Procedimiento que permite insertar un paciente mayor de edad
    Sub insertarPacienteMayor(ByVal paciente_ As Pacientes)

        Dim sqlSP As String = "insertarPacienteMayor" 'Procedimiento Almacenado
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@NombreCompleto", paciente_.NombreCompleto_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Cedula", paciente_.Cedula_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Direccion", paciente_.Direccion_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Correo", paciente_.Correo_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Ocupacion", paciente_.Ocupacion_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Genero", paciente_.Genero_))
        cmdInsertar.Parameters.Add(New SqlParameter("@FechaNacimiento", paciente_.FechaNacimiento_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Telefono1", paciente_.Telefono1_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Telefono2", paciente_.Telefono2_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Estado", paciente_.Estado_))
        Try
            cmdInsertar.ExecuteNonQuery()
            MsgBox("Datos almacenados satisfactoriamente", MsgBoxStyle.Information, "Clinica Medica")
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al insertar los datos del Paciente!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub
    '*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Busqueda de 1 paciente++++++++++++++++++++*'
    Function cargarTabla1paciente(ByVal dato As String) As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("consultar1PacienteActivo", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@dato", dato)

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    '*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Busqueda de 1 paciente++++++++++++++++++++*'
    Function Consultar1Paciente(id As String) As Pacientes

        Dim paci As New Pacientes()
        Dim cmdConsultar As New SqlCommand("consultar1PacienteActivo", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@Dato", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader

        If ds.Read Then
            If IsDBNull(ds("ID_Responsable")) Then
                paci = New Pacientes(ds("ID_Paciente"), ds("NombreCompleto"), ds("Cedula"), ds("Direccion"),
                                       ds("Correo"), ds("Ocupacion"), ds("Genero"), ds("FechaNacimiento"),
                                       ds("Telefono1"), ds("Telefono2"), Nothing, ds("Estado"))
            Else
                paci = New Pacientes(ds("ID_Paciente"), ds("NombreCompleto"), ds("Cedula"), ds("Direccion"),
                                                           ds("Correo"), ds("Ocupacion"), ds("Genero"), ds("FechaNacimiento"),
                                                           ds("Telefono1"), ds("Telefono2"), ds("ID_Responsable"), ds("Estado"))
            End If
        End If
        ds.Close()
        Return paci 'Retorna el valor esperado
    End Function

    Function ConsultarPaciente(id As Integer) As String
        Dim paci As String = ""
        Dim cmdConsultar As New SqlCommand("consultar1Paciente", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader

        If ds.Read Then
            paci = ds("NombreCompleto")
        End If
        ds.Close()
        Return paci 'Retorna el valor esperado
    End Function

    Function ConsultarPacienteCOMPLETO(id As Integer) As Pacientes
        Dim paci As New Pacientes()
        Dim cmdConsultar As New SqlCommand("consultarpaciente", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader

        If ds.Read Then
            If IsDBNull(ds("ID_Responsable")) Then
                paci = New Pacientes(ds("ID_Paciente"), ds("NombreCompleto"), ds("Cedula"), ds("Direccion"),
                                       ds("Correo"), ds("Ocupacion"), ds("Genero"), ds("FechaNacimiento"),
                                       ds("Telefono1"), ds("Telefono2"), Nothing, ds("Estado"))
            Else
                paci = New Pacientes(ds("ID_Paciente"), ds("NombreCompleto"), ds("Cedula"), ds("Direccion"),
                                                           ds("Correo"), ds("Ocupacion"), ds("Genero"), ds("FechaNacimiento"),
                                                           ds("Telefono1"), ds("Telefono2"), ds("ID_Responsable"), ds("Estado"))
            End If
        End If
        ds.Close()
        Return paci 'Retorna el valor esperado
    End Function

    Function ExtraerIDPaciente(ced As Integer) As Integer
        Dim paci As Integer
        Dim cmdConsultar As New SqlCommand("ExtraerIDPaciente", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@Cedula", ced))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader

        If ds.Read Then
            paci = ds("ID_Paciente")
        End If
        ds.Close()
        Return paci 'Retorna el valor esperado
    End Function
    '*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Cargar tabla activos e inactivos++++++++++++++++++++*'
    Function cargartablaActivos() As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("pacientesActivosTodos", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    Function cargartablaInactivos() As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("pacientesInactivosTodos", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function
    '*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Actualizar paciente mayor y menor++++++++++++++++++++*'

    'Procedimiento que permite actualizar el paciente mayor de edad
    Sub actualizarPacienteMayor(paciente_ As Pacientes)
        Dim sqlSP As String = "actualizarPacienteMayor" 'Procedimiento Almacenado
        Dim cmdActualizar As New SqlCommand(sqlSP, conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Paciente", paciente_.ID_Paciente_))
        cmdActualizar.Parameters.Add(New SqlParameter("@NombreCompleto", paciente_.NombreCompleto_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Cedula", paciente_.Cedula_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Direccion", paciente_.Direccion_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Correo", paciente_.Correo_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Ocupacion", paciente_.Ocupacion_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Genero", paciente_.Genero_))
        cmdActualizar.Parameters.Add(New SqlParameter("@FechaNacimiento", paciente_.FechaNacimiento_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Telefono1", paciente_.Telefono1_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Telefono2", paciente_.Telefono2_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Estado", paciente_.Estado_))

        Try
            cmdActualizar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al actualizar el paciente!!!" & e.ToString, MsgBoxStyle.Critical, "SAC")
        End Try
    End Sub

    'Procedimiento que permite actualizar el paciente menor de edad
    Sub actualizarPaciente(paciente_ As Pacientes)
        Dim sqlSP As String = "actualizarPaciente" 'Procedimiento Almacenado
        Dim cmdActualizar As New SqlCommand(sqlSP, conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Paciente", paciente_.ID_Paciente_))
        cmdActualizar.Parameters.Add(New SqlParameter("@NombreCompleto", paciente_.NombreCompleto_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Cedula", paciente_.Cedula_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Direccion", paciente_.Direccion_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Correo", paciente_.Correo_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Ocupacion", paciente_.Ocupacion_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Genero", paciente_.Genero_))
        cmdActualizar.Parameters.Add(New SqlParameter("@FechaNacimiento", paciente_.FechaNacimiento_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Telefono1", paciente_.Telefono1_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Telefono2", paciente_.Telefono2_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Estado", paciente_.Estado_))
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Responsable", paciente_.ID_Responsable_))

        Try
            cmdActualizar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al actualizar el paciente!!!" & e.ToString, MsgBoxStyle.Critical, "SAC")
        End Try
    End Sub
    '*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Eliminar Restablecer+++++++++++++++++++++++++++++*'
    'Procedimiento que permite desactivar un paciente
    Sub bloquearPaciente(ByVal id As Integer)
        Dim sqlSP As String = "eliminarPaciente" 'Procedimiento Almacenado
        Dim cmdEliminar As New SqlCommand(sqlSP, conn)
        cmdEliminar.CommandType = System.Data.CommandType.StoredProcedure
        cmdEliminar.Parameters.Add(New SqlParameter("@ID_Paciente", id))
        Try
            cmdEliminar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al eliminar al paciente!!!", MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    'Procedimiento que permite ACTIVAR DE LA PAPELERA un paciente
    Sub RestablecerPaciente(ByVal id As Integer)
        Dim sqlSP As String = "restablecerPaciente" 'Procedimiento Almacenado
        Dim cmdEliminar As New SqlCommand(sqlSP, conn)
        cmdEliminar.CommandType = System.Data.CommandType.StoredProcedure
        cmdEliminar.Parameters.Add(New SqlParameter("@ID_Paciente", id))
        Try
            cmdEliminar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al restablecer al paciente!!!", MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    'Procedimiento que consulta los pacientes activos para el reporte
    Function PacientesReporte() As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("PacientesReporte", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable

        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    'Procedimiento que consulta los pacientes por especialidad
    Function ConsultarPacientesReporte(Especialidad As String) As DataTable
        Dim tabla As New DataTable

        Dim command As New SqlCommand("ConsultarPacientesReporte", conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@Especialidad", Especialidad)

        If CBool(command.ExecuteNonQuery) Then
            Dim rsdatos = New SqlDataAdapter(command)
            rsdatos.Fill(tabla)
        End If

        Return tabla

    End Function

End Class

﻿Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.Citas

Public Class DB_Citas

    'Procedimiento que permite insertar una nueva cita
    Sub insertarCita(cita_ As Citas)

        Dim sqlSP As String = "insertarCita" 'Procedimiento Almacenado
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@Fecha", cita_.Fecha_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Especialidad", cita_.Especialidad_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Descripcion", cita_.Descripcion_))
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Hora", cita_.ID_Hora_))
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Paciente", cita_.ID_Paciente_))
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Funcionario", cita_.ID_Funcionario_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Estado", cita_.Estado_))
        Try
            cmdInsertar.ExecuteNonQuery()
            MsgBox("Datos almacenados satisfactoriamente", MsgBoxStyle.Information, "Clinica Medica")
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al insertar los datos de la cita!!!" & e.ToString, MsgBoxStyle.Critical, "Clínica Médica")
        End Try
    End Sub

    'Procedimiento que permite consultar las horas utilizadas
    Function ConsultaHorasUtilizadas(fecha As Date, Especialidad As String) As DataTable
        Dim tabla As DataTable
        Dim ds As New DataTable
        Dim nueva As New DataTable
        nueva.Columns.Add("ID_Hora")
        nueva.Columns.Add("Hora")
        nueva.Columns.Add("ID_Citas")
        nueva.Columns.Add("Paciente")
        nueva.Columns.Add("Descripción")
        nueva.Columns.Add("Funcionario")
        nueva.Columns.Add("ID_Paciente")
        nueva.Columns.Add("ID_Funcionario")

        Dim cmdConsultar As New SqlCommand("ConsultarHorasUtilizadas", conn)

        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@Fecha", fecha)
        cmdConsultar.Parameters.Add(New SqlParameter("@Especialidad", Especialidad))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds

        Dim ln_Paci As New Ln_Paciente
        Dim ln_Funci As New Ln_Funcionario

        For Each row As DataRow In tabla.Rows
            Dim Renglon As DataRow = nueva.NewRow()
            Renglon("ID_Hora") = row("ID_Hora")
            Renglon("Hora") = row("Hora")
            Renglon("ID_Citas") = row("ID_Citas")
            Renglon("Descripción") = row("Descripción")
            Renglon("ID_Paciente") = row("ID_Paciente")
            If IsDBNull(row("ID_Paciente")) Then
            Else
                Renglon("Paciente") = ln_Paci.ConsultarPaciente(Convert.ToInt32(row("ID_Paciente")))
            End If
            Renglon("ID_Funcionario") = row("ID_Funcionario")

            If IsDBNull(row("ID_Funcionario")) Then
            Else
                Renglon("Funcionario") = ln_Funci.ConsultarFuncionario(Convert.ToInt32(row("ID_Funcionario")))
            End If
            nueva.Rows.Add(Renglon)
        Next
        Return nueva
    End Function

    'Procedimiento que permite actualizar la cita
    Sub actualizarCita(cita_ As Citas)
        Dim sqlSP As String = "actualizarCita" 'Procedimiento Almacenado
        Dim cmdActualizar As New SqlCommand(sqlSP, conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Cita", cita_.ID_Cita_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Fecha", cita_.Fecha_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Especialidad", cita_.Especialidad_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Descripcion", cita_.Descripcion_))
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Hora", cita_.ID_Hora_))
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Paciente", cita_.ID_Paciente_))
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Funcionario", cita_.ID_Funcionario_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Estado", cita_.Estado_))

        Try
            cmdActualizar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al actualizar la cita, por favor verifique los datos e intente de nuevo!!!" & e.ToString, MsgBoxStyle.Critical, "SAC")
        End Try
    End Sub

    'Procedimiento que permite eliminar una CONSULTA POR CITA
    Sub EliminarConsulta(id As Integer)
        Dim sqlSP As String = "eliminarConsultaOdontologia" 'Procedimiento Almacenado

        Dim cmdEliminar As New SqlCommand(sqlSP, conn)
        cmdEliminar.CommandType = System.Data.CommandType.StoredProcedure
        cmdEliminar.Parameters.Add(New SqlParameter("@ID_Cita", id))
        Try
            cmdEliminar.ExecuteNonQuery()
        Catch e As SqlException
            ' MsgBox("Ha ocurrido un error al eliminar la cita!!!", MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    'Procedimiento que permite eliminar una cita
    Sub EliminarCita(id As Integer)
        Dim sqlSP As String = "EliminarCita" 'Procedimiento Almacenado

        Dim cmdEliminar As New SqlCommand(sqlSP, conn)
        cmdEliminar.CommandType = System.Data.CommandType.StoredProcedure
        cmdEliminar.Parameters.Add(New SqlParameter("@ID_Cita", id))
        Try
            cmdEliminar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al eliminar la cita!!!", MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    Function ExtraerCita(id As Integer) As Citas
        Dim cita As New Citas()
        Dim cmdConsultar As New SqlCommand("ExtraerCita", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_Cita", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader

        If ds.Read Then
            cita = New Citas(ds("ID_Cita"), ds("ID_Hora"), ds("ID_Paciente"), ds("ID_Funcionario"),
                            ds("Fecha"), ds("Especialidad"), ds("Descripcion"), ds("Estado"))
        End If
        ds.Close()
        Return cita 'Retorna el valor esperado
    End Function

    'Procedimiento que consulta las citas por dia
    Function ConsultaCitasDia(Usuario As String, ID As Integer, Fecha As Date, Especialidad As String) As DataTable
        Dim tabla As New DataTable

        Dim command As New SqlCommand("ConsultarCitasDia", conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@Usuario", Usuario)
        command.Parameters.AddWithValue("@ID", ID)
        command.Parameters.AddWithValue("@Fecha", Fecha)
        command.Parameters.AddWithValue("@Especialidad", Especialidad)

        If CBool(command.ExecuteNonQuery) Then
            Dim rsdatos = New SqlDataAdapter(command)
            rsdatos.Fill(tabla)

        End If

        Return tabla

    End Function


    'Procedimiento que consulta las citas por mes y año
    Function ConsultaCitasMes(Usuario As String, ID As Integer, Fecha As Date, Especialidad As String) As DataTable
        Dim tabla As New DataTable

        Dim command As New SqlCommand("ConsultarCitasMes", conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@Usuario", Usuario)
        command.Parameters.AddWithValue("@ID", ID)
        command.Parameters.AddWithValue("@Fecha", Fecha)
        command.Parameters.AddWithValue("@Especialidad", Especialidad)

        If CBool(command.ExecuteNonQuery) Then
            Dim rsdatos = New SqlDataAdapter(command)
            rsdatos.Fill(tabla)

        End If

        Return tabla

    End Function


    'Procedimiento que consulta las citas por año
    Function ConsultaCitasAño(Usuario As String, ID As Integer, Fecha As Date, Especialidad As String) As DataTable
        Dim tabla As New DataTable

        Dim command As New SqlCommand("ConsultarCitasAño", conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@Usuario", Usuario)
        command.Parameters.AddWithValue("@ID", ID)
        command.Parameters.AddWithValue("@Fecha", Fecha)
        command.Parameters.AddWithValue("@Especialidad", Especialidad)

        If CBool(command.ExecuteNonQuery) Then
            Dim rsdatos = New SqlDataAdapter(command)
            rsdatos.Fill(tabla)

        End If

        Return tabla

    End Function

    'Procedimiento que consulta las citas por año
    Function ConsultaCitasRangoFechas(Usuario As String, ID As Integer, FechaUno As Date, FechaDos As Date, Especialidad As String) As DataTable
        Dim tabla As New DataTable

        Dim command As New SqlCommand("ConsultarCitasRangoFechas", conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@Usuario", Usuario)
        command.Parameters.AddWithValue("@ID", ID)
        command.Parameters.AddWithValue("@FechaUno", FechaUno)
        command.Parameters.AddWithValue("@FechaDos", FechaDos)
        command.Parameters.AddWithValue("@Especialidad", Especialidad)

        If CBool(command.ExecuteNonQuery) Then
            Dim rsdatos = New SqlDataAdapter(command)
            rsdatos.Fill(tabla)

        End If

        Return tabla

    End Function

    '------------- Consulta Pacientes Citas por dia para el combo Consulta Odontología

    Function extraerPacientesDia(fechaHoy As Date, especialidad As String) As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("extraerPacientesDia", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@Fecha", fechaHoy)
        cmdConsultar.Parameters.AddWithValue("@Especialidad", especialidad)
        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla citas
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    'Procedimiento que consulta todas las citas
    Function ConsultarTodasCitas() As DataTable
        Dim tabla As New DataTable

        Dim command As New SqlCommand("Select * from Citas;", conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim rsdatos = New SqlDataAdapter(command)
            rsdatos.Fill(tabla)
        End If

        Return tabla

    End Function

    Sub actualizarEstadoCita(ID_Cita As Integer, Estado As String)

        Dim sqlSP As String = "actualizarEstadoCita"
        Dim cmdActualizar As New SqlCommand(sqlSP, conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Cita", ID_Cita))
        cmdActualizar.Parameters.Add(New SqlParameter("@Estado", Estado))

        Try
            cmdActualizar.ExecuteNonQuery()
        Catch e As SqlException
        End Try

    End Sub

    Sub actualizarCitasAusentes()
        For Each citas As DataRow In ConsultarTodasCitas().Rows
            If Convert.ToString(citas("Estado")) = "Pendiente" And Convert.ToDateTime(citas("Fecha")) < Date.Now.Date Then
                actualizarEstadoCita(Convert.ToInt32(citas("ID_Cita")), "Ausente")
            End If
        Next
    End Sub

End Class

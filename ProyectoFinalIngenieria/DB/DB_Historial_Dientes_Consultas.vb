﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.Historial_Dientes_Consultas

Public Class DB_Historial_Dientes_Consultas

    'Procedimiento que permite insertar un nuevo Tratamiento diente
    Sub insertarHistorialDiente(hist_Diente_Consulta As Historial_Dientes_Consultas)

        Dim sqlSP As String = "insertarHistorialDiente" 'Procedimiento Almacenado
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Consulta", hist_Diente_Consulta.ID_Consulta1))
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Diente", hist_Diente_Consulta.ID_Diente1))
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Tratamiento", hist_Diente_Consulta.ID_Tratamiento1))
        cmdInsertar.Parameters.Add(New SqlParameter("@Descripcion", hist_Diente_Consulta.Decripcion1))
        cmdInsertar.Parameters.Add(New SqlParameter("@Lado_Diente", hist_Diente_Consulta.LadoDiente_Tratamiento1))
        Try
            cmdInsertar.ExecuteNonQuery()
            MsgBox("Datos almacenados satisfactoriamente", MsgBoxStyle.Information, "Clinica Medica")
        Catch e As SqlException
            'MsgBox("Ha ocurrido un error al insertar los datos!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
            MessageBox.Show(String.Concat(e))
        End Try
    End Sub

    '*++++++++++++++++++++++++++++++++       LLENAR TABLA TRATAMIENTOS DE UN DIENTE          +++++++++++++++++++++++++++++++++++*'
    Function cargarTablaTratamientosDiente(ID_diente As Integer, ID_paciente As Integer) As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("tratamientosDiente", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@ID_Diente", ID_diente)
        cmdConsultar.Parameters.AddWithValue("@ID_Paciente", ID_paciente)

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function


    'Procedimiento que permite eliminar un TRATAMIENTO CONSULTA ODONTOLOGIA
    Sub eliminarTratamientoConsultaOdontologia(id_historial As Integer)
        Dim sqlSP As String = "eliminarTratamientoConsultaOdontologia" 'Procedimiento Almacenado

        Dim cmdEliminar As New SqlCommand(sqlSP, conn)
        cmdEliminar.CommandType = System.Data.CommandType.StoredProcedure
        cmdEliminar.Parameters.Add(New SqlParameter("@ID_Historial", id_historial))
        Try
            cmdEliminar.ExecuteNonQuery()
        Catch e As SqlException
            ' MsgBox("Ha ocurrido un error al eliminar la cita!!!", MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub


End Class

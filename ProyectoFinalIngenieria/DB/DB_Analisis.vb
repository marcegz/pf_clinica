﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.Analisis

Public Class DB_Analisis
    'Extrae 1 solo analisis con respecto a cada consulta
    Function consultarAnalisis(id As Integer) As Analisis
        Dim returnAnalisis As New Analisis()
        Dim cmdConsultar As New SqlCommand("consultarAnalisis", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader
        If ds.Read Then
            returnAnalisis = New Analisis(ds("ID_Analisis"), ds("Presion_Arterial"), ds("Peso"), ds("Tamaño"))
        End If
        ds.Close()

        Return returnAnalisis 'Retorna el valor esperado
    End Function

    'Procedimiento que permite insertar una nueva cita
    Sub insertarAnalisis(analisis_ As Analisis)

        Dim sqlSP As String = "insertarAnalisis" 'Procedimiento Almacenado
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@Presion_Arterial", analisis_.Presion_Arterial_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Peso", analisis_.Peso_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Tamaño", analisis_.Tamaño_))
        Try
            cmdInsertar.ExecuteNonQuery()
            MsgBox("Datos almacenados satisfactoriamente", MsgBoxStyle.Information, "Clinica Medica")
        Catch e As SqlException
            MsgBox("No se inserto el analisis correctamete, Por favor verifique que los datos esten correctamente.!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub
End Class

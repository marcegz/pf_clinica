﻿Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.HistorialFamiliar_Alergico

Public Class DB_HistorialFamiliar_Alergico
    'Procedimiento que permite insertar un historial familiar alergico del paciente
    Sub insertarHistorialP(historial_ As HistorialFamiliar_Alergico)

        Dim sqlSP As String = "insertarHistorialFamiliarAlergico" 'Procedimiento Almacenado
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Paciente", historial_.ID_Paciente_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Queja_principal", historial_.Queja_principal_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Antecedentes_patologicos_familiares", historial_.Antecedentes_patologicos_familiares_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Antecedentes_patologicos_personales", historial_.Antecedentes_patologicos_personales_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Antecedentes_patologicos_no_personales", historial_.Antecedentes_patologicos_no_personales_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Antecedentes_quirurgicos", historial_.Antecedentes_quirurgicos_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Antecedentes_alergicos", historial_.Antecedentes_alergicos_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Fecha", historial_.Fecha_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Estado", historial_.Estado_))
        Try
            cmdInsertar.ExecuteNonQuery()
            MsgBox("Datos almacenados satisfactoriamente", MsgBoxStyle.Information, "Clinica Medica")
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al insertar los datos del Historial!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    'Procedimiento que permite actualizar datos del historial familiar de un paciente
    Sub actualizarHistorialP(historial_ As HistorialFamiliar_Alergico)
        Dim sqlSP As String = "actualizarHistorialFamiliar_Alergico" 'Procedimiento Almacenado
        Dim cmdActualizar As New SqlCommand(sqlSP, conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Paciente", historial_.ID_Paciente_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Queja_principal", historial_.Queja_principal_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Antecedentes_patologicos_familiares", historial_.Antecedentes_patologicos_familiares_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Antecedentes_patologicos_personales", historial_.Antecedentes_patologicos_personales_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Antecedentes_patologicos_no_personales", historial_.Antecedentes_patologicos_no_personales_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Antecedentes_quirurgicos", historial_.Antecedentes_quirurgicos_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Antecedentes_alergicos", historial_.Antecedentes_alergicos_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Fecha", historial_.Fecha_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Estado", historial_.Estado_))

        Try
            cmdActualizar.ExecuteNonQuery()
            MsgBox("Se inserto el historial correctamente!!!", MsgBoxStyle.Information, "Clinica Medica")
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al actualizar el responsable!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub


    Function Consultar1Historial(id As Integer) As HistorialFamiliar_Alergico
        Dim histo As New HistorialFamiliar_Alergico()
        Dim cmdConsultar As New SqlCommand("Consultar1HistorialFamiliarAlergico", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_Paciente", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader
        If ds.Read Then
            histo = New HistorialFamiliar_Alergico(ds("ID_Paciente"), ds("Queja_principal"), ds("Antecedentes_patologicos_familiares"), ds("Antecedentes_patologicos_personales"),
                                       ds("Antecedentes_patologicos_no_personales"), ds("Antecedentes_quirurgicos"), ds("Antecedentes_alergicos"), ds("Fecha"),
                                       ds("Estado"))
        Else
            MsgBox("noooooo", MsgBoxStyle.Critical, "Clinica Medica")
        End If
        ds.Close()

        Return histo 'Retorna el valor esperado
    End Function

    Function ExtraerIDPaciente(cedula As Integer) As Integer
        Dim codigousuario As Integer 'Variable que almacena el máximo actual
        Dim rsCodigo As New SqlCommand("select p.ID_Paciente from Pacientes p where Cedula=" & cedula & "", conn)

        Dim ds As SqlDataReader
        ds = rsCodigo.ExecuteReader
        If ds.Read Then
            codigousuario = ds.Item("ID_Paciente")
        End If
        ds.Close()
        Return codigousuario 'Retorna el valor esperado
    End Function
End Class

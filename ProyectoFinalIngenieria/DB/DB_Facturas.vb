﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class DB_Facturas

    'Procedimiento que permite insertar una nueva cita
    Sub insertarFactura(factura As Facturas)

        Dim sqlSP As String = "insertarFactura"
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Cita", factura.ID_Cita1))
        cmdInsertar.Parameters.Add(New SqlParameter("@Fecha", factura.Fecha1))
        cmdInsertar.Parameters.Add(New SqlParameter("@Hora", factura.Hora1))
        cmdInsertar.Parameters.Add(New SqlParameter("@Descuento", factura.Descuento1))

        Try
            cmdInsertar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al insertar los datos de la factura!!!" & e.ToString, MsgBoxStyle.Critical, "Clínica Médica")
        End Try
    End Sub

    Function getIdFactura() As Integer

        Dim idFactura As Integer
        Dim rs As New SqlCommand("SELECT IDENT_CURRENT('Facturas') As idFactura", conn)
        Dim ds As SqlDataReader
        ds = rs.ExecuteReader

        Dim dt As New DataTable
        Dim command As New SqlCommand("select * from Facturas;", conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)
        End If

        If ds.Read Then
            If dt.Rows.Count = 0 Then
                idFactura = ds.Item("idFactura")
            Else
                idFactura = ds.Item("idFactura") + 1
            End If
        End If

        ds.Close()

        Return idFactura

    End Function


    Function verificarFacturaExiste(ID_Cita As Integer) As String

        Dim sqlSP As String = "verificarFacturaExiste"
        Dim command As New SqlCommand(sqlSP, conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@ID_Cita", ID_Cita)

        Dim ds As SqlDataReader
        ds = command.ExecuteReader

        If CBool(ds.Read) = True Then
            verificarFacturaExiste = ds("ID_Factura")
        Else
            verificarFacturaExiste = ""
        End If

        ds.Close()
        Return verificarFacturaExiste

    End Function

    Function getIdFacturaReporte(id_cita As Integer) As Integer
        Dim id As Integer
        Dim cmdConsultar As New SqlCommand("getIdFactura", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_Cita", id_cita))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader
        If ds.Read Then
            id = ds("ID_Factura")
        End If
        ds.Close()

        Return id 'Retorna el valor esperado
    End Function

    Function getDescuentoFactura(id_cita As Integer) As Decimal
        Dim id As Decimal
        Dim cmdConsultar As New SqlCommand("getDescuentoFactura", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_Cita", id_cita))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader
        If ds.Read Then
            id = ds("Descuento")
        End If
        ds.Close()

        Return id 'Retorna el valor esperado
    End Function

End Class

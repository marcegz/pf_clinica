﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.Consultas

Public Class DB_Consultas

    'Procedimiento que permite insertar una nueva consulta a medicina general
    Sub insertarConsulta(consulta_ As Consultas)

        Dim sqlSP As String = "insertarConsulta" 'Procedimiento Almacenado
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Analisis", consulta_.ID_Analisis_))
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Cita", consulta_.ID_Cita_))
        Try
            cmdInsertar.ExecuteNonQuery()
            MsgBox("Datos almacenados satisfactoriamente", MsgBoxStyle.Information, "Clinica Medica")
        Catch e As SqlException
            MsgBox("No se inserto la consulta correctamete, Por favor verifique que los datos esten correctamente.!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    Function extraerPacientesMedicinaGeneralDia(ced As Integer, fecha As Date) As DataTable
        Dim tabla As New DataTable
        Dim cmdConsultar As New SqlCommand("extraerPacientesMedicinaGeneralDia", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@ced", ced)
        cmdConsultar.Parameters.AddWithValue("@Fecha", fecha)

        If CBool(cmdConsultar.ExecuteNonQuery) Then
            Dim rsdatos = New SqlDataAdapter(cmdConsultar)
            rsdatos.Fill(tabla)

        End If

        Return tabla
    End Function

    'TratamientosActivosporEspecialidad
    'Procedimiento que consulta las citas por mes y año
    Function TratamientosActivosporEspecialidad(Especialidad As String) As DataTable
        Dim tabla As New DataTable

        Dim command As New SqlCommand("TratamientosActivosporEspecialidad", conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@Especialidad", Especialidad)

        If CBool(command.ExecuteNonQuery) Then
            Dim rsdatos = New SqlDataAdapter(command)
            rsdatos.Fill(tabla)

        End If

        Return tabla

    End Function

    Function extraerDatosParaConsultaMedicinaG(id As String, fecha As Date) As Consultas
        Dim consulta As New Consultas()
        Dim cmdConsultar As New SqlCommand("consultar1PacienteActivo", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@Dato", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader

        If ds.Read Then
            If IsDBNull(ds("ID_Responsable")) Then
                'consulta = New Pacientes(ds("ID_Paciente"), ds("NombreCompleto"), ds("Cedula"), ds("Direccion"),
                '                       ds("Correo"), ds("Ocupacion"), ds("Genero"), ds("FechaNacimiento"),
                '                       ds("Telefono1"), ds("Telefono2"), Nothing, ds("Estado"))
            Else
                'consulta = New Pacientes(ds("ID_Paciente"), ds("NombreCompleto"), ds("Cedula"), ds("Direccion"),
                '                                           ds("Correo"), ds("Ocupacion"), ds("Genero"), ds("FechaNacimiento"),
                '                                           ds("Telefono1"), ds("Telefono2"), ds("ID_Responsable"), ds("Estado"))
            End If
        End If
        ds.Close()
        Return consulta 'Retorna el valor esperado
    End Function




    Function getIdConsulta(id_cita As Integer) As Integer
        Dim id As Integer
        Dim cmdConsultar As New SqlCommand("getIdConsulta", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_Cita", id_cita))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader
        If ds.Read Then
            id = ds("ID_Consulta")
        End If
        ds.Close()

        Return id 'Retorna el valor esperado
    End Function

End Class

﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.Funcionario

Public Class DB_Funcionario

    'Procedimiento que permite insertar un funcionario 
    Sub insertarfuncionario(funcionario_ As Funcionario)

        Dim sqlSP As String = "insertarFuncionario" 'Procedimiento Almacenado
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@NombreCompleto", funcionario_.NombreCompleto_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Cedula", funcionario_.Cedula_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Direccion", funcionario_.Direccion_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Correo", funcionario_.Correo_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Profesion", funcionario_.Profesion_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Contraseña", funcionario_.Contraseña_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Genero", funcionario_.Genero_))
        cmdInsertar.Parameters.Add(New SqlParameter("@FechaNacimiento", funcionario_.FechaNacimiento_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Telefono1", funcionario_.Telefono1_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Telefono2", funcionario_.Telefono2_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Estado", funcionario_.Estado_))
        Try
            cmdInsertar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al insertar los datos del Funcionario!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    '*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Busqueda de 1 paciente++++++++++++++++++++*'
    Function cargarTabla1Funcionario(ByVal dato As String) As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("consultar1FuncionarioActivo", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@dato", dato)

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function


    '*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Cargar tabla activos e inactivos++++++++++++++++++++*'
    Function cargartablaActivos() As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("funcionariosActivosTodos", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    Function funcionariosActivosporEspecialidad(dato As String) As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("funcionariosActivosporEspecialidad", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@Especialidad", dato)
        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    Function cargartablaInactivos() As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("funcionariosInactivosTodos", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    'Procedimiento que permite actualizar el Funcionario mayor de edad
    Sub actualizarFuncionario(funcionario_ As Funcionario)
        Dim sqlSP As String = "actualizarFuncionario" 'Procedimiento Almacenado
        Dim cmdActualizar As New SqlCommand(sqlSP, conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Funcionario", funcionario_.ID_Funcionario_))
        cmdActualizar.Parameters.Add(New SqlParameter("@NombreCompleto", funcionario_.NombreCompleto_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Cedula", funcionario_.Cedula_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Direccion", funcionario_.Direccion_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Correo", funcionario_.Correo_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Profesion", funcionario_.Profesion_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Contraseña", funcionario_.Contraseña_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Genero", funcionario_.Genero_))
        cmdActualizar.Parameters.Add(New SqlParameter("@FechaNacimiento", funcionario_.FechaNacimiento_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Telefono1", funcionario_.Telefono1_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Telefono2", funcionario_.Telefono2_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Estado", funcionario_.Estado_))

        Try
            cmdActualizar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al actualizar el funcionario!!!" & e.ToString, MsgBoxStyle.Critical, "SAC")
        End Try
    End Sub

    '*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Eliminar Restablecer+++++++++++++++++++++++++++++*'
    'Procedimiento que permite desactivar un funcionario
    Sub bloquearFuncionario(ByVal id As Integer)
        Dim sqlSP As String = "eliminarFuncionario" 'Procedimiento Almacenado
        Dim cmdEliminar As New SqlCommand(sqlSP, conn)
        cmdEliminar.CommandType = System.Data.CommandType.StoredProcedure
        cmdEliminar.Parameters.Add(New SqlParameter("@ID_Funcionario", id))
        Try
            cmdEliminar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al eliminar al Funcionario!!!", MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    'Procedimiento que permite ACTIVAR DE LA PAPELERA un Funcionario
    Sub RestablecerFuncionario(ByVal id As Integer)
        Dim sqlSP As String = "restablecerFuncionario" 'Procedimiento Almacenado
        Dim cmdEliminar As New SqlCommand(sqlSP, conn)
        cmdEliminar.CommandType = System.Data.CommandType.StoredProcedure
        cmdEliminar.Parameters.Add(New SqlParameter("@ID_Funcionario", id))
        Try
            cmdEliminar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al restablecer al Funcionario!!!", MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    Function ConsultarFuncionario(id As Integer) As String
        Dim funci As String = ""
        Dim cmdConsultar As New SqlCommand("consultarFuncionario", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader

        If ds.Read Then
            funci = ds("NombreCompleto")
        End If

        ds.Close()
        Return funci 'Retorna el valor esperado
    End Function

    'Procedimiento que consulta los funcionario activos para el reporte
    Function FuncionariosReporte(Profesion As String) As DataTable
        Dim cmdConsultar As New SqlCommand("FuncionariosReporte", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As New DataTable
        rsdatos.Fill(ds)

        Dim tabla As New DataTable
        Dim dc1 As New DataColumn("ID_Funcionario")
        Dim dc2 As New DataColumn("NombreCompleto")
        tabla.Columns.AddRange(New DataColumn() {dc1, dc2})

        For Each row As DataRow In ds.Rows

            If Convert.ToString(row("Profesion")) = Profesion Then
                Dim drNewRow As DataRow
                drNewRow = tabla.NewRow
                drNewRow.Item("ID_Funcionario") = Convert.ToInt32(row("ID_Funcionario"))
                drNewRow.Item("NombreCompleto") = Convert.ToString(row("NombreCompleto"))
                tabla.Rows.Add(drNewRow)
            End If
        Next

        Return tabla

    End Function

    ' Procedimiento que consulta un funcionario para la factura
    Function ConsultarFuncionarioFactura(ID_Funcionario As Integer) As String

        Dim funcionario As String = ""

        Dim cmdConsultar As New SqlCommand("ConsultFuncionario", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_Funcionario", ID_Funcionario))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader

        If ds.Read Then
            funcionario = ds("Cedula") & "-" & ds("NombreCompleto")
        End If

        ds.Close()
        Return funcionario
    End Function

End Class

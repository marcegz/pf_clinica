﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.Dientes

Public Class DB_Dientes

    Function Consultar1Diente(id As Integer) As Dientes
        Dim diente As New Dientes()
        Dim cmdConsultar As New SqlCommand("Consultar1Diente", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@Numero_Diente", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader
        If ds.Read Then
            diente = New Dientes(ds("ID_Diente"), ds("Nombre"), ds("X"), ds("Y"), ds("Numero"))
        End If
        ds.Close()

        Return diente 'Retorna el valor esperado
    End Function

    Function ConsultarDientes() As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("ConsultarDientes", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function


    ''*++++++++++++++++++++++++  Extraer Tratamientos Dientes   +++++++++++++++++++++++*'
    'Function ExtraerTratamientosDientes(ByVal diente As Integer) As DataTable
    '    Dim tabla As DataTable
    '    Dim cmdConsultar As New SqlCommand("ExtraerTratamientosDientes", conn)
    '    cmdConsultar.CommandType = CommandType.StoredProcedure
    '    cmdConsultar.Parameters.AddWithValue("@Diente", diente)

    '    Dim rsdatos As New SqlDataAdapter(cmdConsultar)

    '    Dim ds As New DataTable
    '    'Llena el data set con los datos de la tabla usuarios
    '    rsdatos.Fill(ds)
    '    tabla = ds
    '    Return tabla
    'End Function


End Class

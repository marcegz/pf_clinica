﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.Historial_Tratamientos_Consultas

Public Class DB_Historial_Tratamientos_Consultas


    Function consultarHistorialTratamientosConsultas(id As Integer) As Historial_Tratamientos_Consultas
        Dim returnAnalisis As Historial_Tratamientos_Consultas
        Dim cmdConsultar As New SqlCommand("consultarHistorialTratamientosConsultas", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_consulta", id))
        Dim list As New List(Of Tratamientos)
        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader
        If ds.Read Then

            'While ds.Read
            '    list.Add(New Tratamientos(ds("id"), ds("nombre"), ds(""), ds("descripcion"), ds(""), ds("precio")))
            'End While

            returnAnalisis = New Historial_Tratamientos_Consultas(ds("sintomas"), list)
        End If
        ds.Close()

        Return returnAnalisis 'Retorna el valor esperado
    End Function

    'Procedimiento que permite insertar una nueva cita
    Sub insertarHistorialTratamientosConsultas(htp As Historial_Tratamientos_Consultas)

        Dim sqlSP As String = "insertarHistorialTratamientosConsultas" 'Procedimiento Almacenado
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Tratamiento", htp.ID_Tratamiento1))
        cmdInsertar.Parameters.Add(New SqlParameter("@ID_Consulta", htp.ID_Consulta1))
        cmdInsertar.Parameters.Add(New SqlParameter("@Sintomas", htp.Sintomas1))
        cmdInsertar.Parameters.Add(New SqlParameter("@Precio", htp.Precio1))
        Try
            cmdInsertar.ExecuteNonQuery()
            MsgBox("Datos almacenados satisfactoriamente", MsgBoxStyle.Information, "Clinica Medica")
        Catch e As SqlException
            MsgBox("No se inserto la consulta correctamete, Por favor verifique que los datos esten correctamente.!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    Function extraerIdConsulta(id As Integer) As Integer
        Dim resultado As Integer = 0
        Dim cmdConsultar As New SqlCommand("extraerIdConsulta", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_cita", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader

        If ds.Read Then
            resultado = ds("ID_Consulta")
        End If
        ds.Close()
        Return resultado 'Retorna el valor esperado
    End Function

    Function extraerIdAnalisis() As Integer
        Dim resultado As Integer = 0
        Dim cmdConsultar As New SqlCommand("extraerIdAnalisis", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader

        If ds.Read Then
            resultado = ds("ID_Analisis")
        End If
        ds.Close()
        Return resultado 'Retorna el valor esperado
    End Function

    Function sacarAnalisisConsulta(id As Integer) As Analisis
        Dim resultado As Analisis
        Dim cmdConsultar As New SqlCommand("sacarAnalisisConsulta", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID_cita", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader

        If ds.Read Then
            resultado = New Analisis(ds("ID_Analisis"), ds("Presion_Arterial"), ds("Peso"), ds("Tamaño"))
        End If
        ds.Close()
        Return resultado 'Retorna el valor esperado
    End Function

    Function ConsultarHistorialMedicinaGeneral(ByVal id As Integer) As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("ConsultarHistorialMedicinaGeneral", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@ID_Cita", id)

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function
End Class

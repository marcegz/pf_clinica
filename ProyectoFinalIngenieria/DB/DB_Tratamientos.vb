﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ProyectoFinalIngenieria.Tratamientos

Public Class DB_Tratamientos

    'Procedimiento que permite insertar un Tratamiento 
    Sub insertarTratamiento(tratamiento As Tratamientos)

        Dim sqlSP As String = "insertarTratamientos" 'Procedimiento Almacenado
        Dim cmdInsertar As New SqlCommand(sqlSP, conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@Nombre", tratamiento.Nombre_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Especialidad", tratamiento.Especialidad_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Precio", tratamiento.Precio_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Descripcion", tratamiento.Descripcion_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Color", tratamiento.Color_))
        cmdInsertar.Parameters.Add(New SqlParameter("@Estado", tratamiento.Estado_))
        Try
            cmdInsertar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al insertar los datos del Tratamiento!!!" & e.ToString, MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    '*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Cargar tabla activos e inactivos++++++++++++++++++++*'
    Function consultarTratamientosConsultas(ByVal id As Integer) As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("consultarTratamientosConsultas", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@ID", id)

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    Function cargartablaActivos() As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("TratamientosActivosTodos", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    Function cargartablaInactivos() As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("TratamientosInactivosTodos", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    '*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Busqueda de 1 paciente++++++++++++++++++++*'
    Function cargarTabla1TratamientoActivo(ByVal dato As String) As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("consultar1TratamientoActivo", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@Dato", dato)

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    Function cargarTabla1TratamientoInactivo(ByVal dato As String) As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("consultar1TratamientoInactivo", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@Dato", dato)

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable
        'Llena el data set con los datos de la tabla usuarios
        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function

    '*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Eliminar Restablecer+++++++++++++++++++++++++++++*'
    'Procedimiento que permite desactivar un funcionario
    Sub bloquearTratamiento(ByVal id As Integer)
        Dim sqlSP As String = "eliminarTratamiento" 'Procedimiento Almacenado
        Dim cmdEliminar As New SqlCommand(sqlSP, conn)
        cmdEliminar.CommandType = System.Data.CommandType.StoredProcedure
        cmdEliminar.Parameters.Add(New SqlParameter("@ID_Tratamiento", id))
        Try
            cmdEliminar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al eliminar el Tratamiento!!!", MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    'Procedimiento que permite ACTIVAR DE LA PAPELERA un Funcionario
    Sub restablecerTratamiento(ByVal id As Integer)
        Dim sqlSP As String = "restablecerTratamiento" 'Procedimiento Almacenado
        Dim cmdEliminar As New SqlCommand(sqlSP, conn)
        cmdEliminar.CommandType = System.Data.CommandType.StoredProcedure
        cmdEliminar.Parameters.Add(New SqlParameter("@ID_Tratamiento", id))
        Try
            cmdEliminar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al restablecer el Tratamiento!!!", MsgBoxStyle.Critical, "Clinica Medica")
        End Try
    End Sub

    'Procedimiento que permite actualizar el tratamiento
    Sub actualizarTratamientos(tratamiento As Tratamientos)
        Dim sqlSP As String = "actualizarTratamientos" 'Procedimiento Almacenado
        Dim cmdActualizar As New SqlCommand(sqlSP, conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@ID_Tratamiento", tratamiento.ID_Tratamiento_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Nombre", tratamiento.Nombre_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Especialidad", tratamiento.Especialidad_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Precio", tratamiento.Precio_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Descripcion", tratamiento.Descripcion_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Color", tratamiento.Color_))
        cmdActualizar.Parameters.Add(New SqlParameter("@Estado", tratamiento.Estado_))

        Try
            cmdActualizar.ExecuteNonQuery()
        Catch e As SqlException
            MsgBox("Ha ocurrido un error al actualizar el Tratamiento!!!" & e.ToString, MsgBoxStyle.Critical, "SAC")
        End Try
    End Sub

    '------------------ TRATAMIENTOS ACTIVOS X ESPECIALIDAD

    Function TratamientosActivosporEspecialidad(Especialidad As String) As DataTable
        Dim tabla As DataTable
        Dim cmdConsultar As New SqlCommand("TratamientosActivosporEspecialidad", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.AddWithValue("@Especialidad", Especialidad)

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)

        Dim ds As New DataTable

        rsdatos.Fill(ds)
        tabla = ds
        Return tabla
    End Function


    Function ConsultarTratamientoCombo(id As Integer) As Tratamientos
        Dim tratamientos As New Tratamientos()
        Dim cmdConsultar As New SqlCommand("consultarTratamientoCombo", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader
        If ds.Read Then
            tratamientos = New Tratamientos(Convert.ToString(ds("Nombre")), Convert.ToString(ds("Especialidad")), Convert.ToString(ds("Descripcion")), Convert.ToString(ds("Color")), Convert.ToDecimal(ds("Precio")), Convert.ToBoolean(ds("Estado")))
        End If
        ds.Close()

        Return tratamientos 'Retorna el valor esperado
    End Function

    Function consultarTratamiento(id As Integer) As Tratamientos
        Dim tratamientos As New Tratamientos()
        Dim cmdConsultar As New SqlCommand("consultarTratamiento", conn)
        cmdConsultar.CommandType = CommandType.StoredProcedure
        cmdConsultar.Parameters.Add(New SqlParameter("@ID", id))

        Dim rsdatos As New SqlDataAdapter(cmdConsultar)
        Dim ds As SqlDataReader
        ds = cmdConsultar.ExecuteReader
        If ds.Read Then
            tratamientos = New Tratamientos(id, Convert.ToString(ds("Nombre")), Convert.ToString(ds("Especialidad")), Convert.ToString(ds("Descripcion")), Convert.ToString(ds("Color")), Convert.ToDecimal(ds("Precio")), Convert.ToBoolean(ds("Estado")))
        End If
        ds.Close()

        Return tratamientos 'Retorna el valor esperado
    End Function


    'Procedimiento que consulta los tratamientos por especialidad
    Function ConsultarTratamientosReporte(Especialidad As String) As DataTable
        Dim tabla As New DataTable

        Dim command As New SqlCommand("ConsultarTratamientosReporte", conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@Especialidad", Especialidad)

        If CBool(command.ExecuteNonQuery) Then
            Dim rsdatos = New SqlDataAdapter(command)
            rsdatos.Fill(tabla)
        End If

        Return tabla

    End Function


    'Procedimiento que consulta los tratamientos para la factura
    Function ConsultarTratamientosFactura(ID_Cita As Integer) As DataTable
        Dim tabla As New DataTable

        Dim command As New SqlCommand("ConsultarHistorialDientes", conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@ID_Cita", ID_Cita)

        If CBool(command.ExecuteNonQuery) Then
            Dim rsdatos = New SqlDataAdapter(command)
            rsdatos.Fill(tabla)
        End If

        Return tabla

    End Function

    'Procedimiento que consulta los tratamientos para la consulta odontologica
    Function tratamientosConsultaOdontologia(ID_Cita As Integer) As DataTable
        Dim tabla As New DataTable

        Dim command As New SqlCommand("tratamientosConsultaOdontologia", conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@ID_Cita", ID_Cita)

        If CBool(command.ExecuteNonQuery) Then
            Dim rsdatos = New SqlDataAdapter(command)
            rsdatos.Fill(tabla)
        End If

        Return tabla

    End Function

End Class
